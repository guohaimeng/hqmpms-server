package com.hqmpms.api.advert;

import com.hqmpms.dao.advert.AdvertDao;
import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.system.DoOperationLogThread;
import com.hqmpms.dispatcher.Startup;
import com.hqmpms.utils.DoCachedThreadPool;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2016/9/7 0007.
 * 广告删除
 * input parameter:
 * advertid(选填,string): ID
 */
@WebServlet("/advert/advertDelete.do")
public class AdvertDelete extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(AdvertDelete.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CheckParameters check = new CheckParameters(request.getParameterMap());
        Long eventId= Startup.getId();
        m_logger.info(String.format("INPUT event=%s params=%s",eventId, check.getParameterJSON()));
        try {
            check.addParameter("advertid", CheckParameters.paraType.STRING,1,30);
            // 获取session
            Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
            String productId = selfInfo.get("loginpid");

            HashMap<String,String> advertInfo = AdvertDao.getAdvertByAdvertId(check.get("advertid").toString());
            AdvertDao.deleteById(check.get("advertid").toString(),productId);

            String ret = ApiErrorCode.echoOk();
            m_logger.info(String.format("OUTPUT  ret_code=%s", ApiErrorCode.SUCCESS));
            response.getWriter().write(ret);
            DoCachedThreadPool.getCachedThreadPool().cachedThreadPool.execute(new DoOperationLogThread(selfInfo.get("mid"),"删除了广告'"+advertInfo.get("title").toString()+"'",check.get("advertid").toString()));
        } catch (CheckParameterException e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT event=%s ret_code=%s",eventId, ApiErrorCode.PARAMETER_ERROR));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED event=%s params=%s %s",eventId,"system exception=",e),e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
        }
    }
}
