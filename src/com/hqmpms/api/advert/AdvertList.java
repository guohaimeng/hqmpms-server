package com.hqmpms.api.advert;

import com.hqmpms.dao.advert.AdvertDao;
import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dispatcher.Config;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2016/9/7 0007.
 * 获取广告列表
 * input parameter:
 * isenablesta(选填,string): 是否启用
 * isreadydel(选填,string): 是否已删除
 * startdate(选填,string): 开始时间
 * enddate(选填,string): 结束时间
 * rows(选填,string): 页数
 * page(选填,string): 每页行数
 */
@WebServlet("/advert/advertList.do")
public class AdvertList extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(AdvertList.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
            String isenablesta="";
            if(!StringUtils.isEmpty(request.getParameter("isenablesta"))){
                check.addParameter("isenablesta", CheckParameters.paraType.EXP,"^[0,1]$");
                isenablesta=check.get("isenablesta").toString();
            }
            String isreadydel="";
            if(!StringUtils.isEmpty(request.getParameter("isreadydel"))){
                check.addParameter("isreadydel", CheckParameters.paraType.EXP,"^[0,1]$");
                isreadydel=check.get("isreadydel").toString();
            }
            String immediateshow="";
            if(!StringUtils.isEmpty(request.getParameter("immediateshow"))){
                check.addParameter("immediateshow", CheckParameters.paraType.EXP,"^[0,1]$");
                immediateshow=check.get("immediateshow").toString();
            }
            String advertstatus="";
            if(!StringUtils.isEmpty(request.getParameter("advertstatus"))){
                check.addParameter("advertstatus", CheckParameters.paraType.EXP,"^[1,2,3]$");
                advertstatus=check.get("advertstatus").toString();
            }
            String startdate="";
            if(!StringUtils.isEmpty(request.getParameter("startdate"))){
                check.addParameter("startdate", CheckParameters.paraType.STRING,19,24);
                startdate=check.get("startdate").toString();
            }
            String enddate="";
            if(!StringUtils.isEmpty(request.getParameter("enddate"))){
                check.addParameter("enddate", CheckParameters.paraType.EXP,19,24);
                enddate=check.get("enddate").toString();
            }
            String rows;
            String page;
            if(!StringUtils.isEmpty(request.getParameter("rows"))){
                check.addParameter("rows", CheckParameters.paraType.EXP, Config.getInstance().getString("params_page"));
                rows = check.get("rows").toString();
            }else{
                rows = "10";
            }
            if(!StringUtils.isEmpty(request.getParameter("page")) && Integer.parseInt(request.getParameter("page"))>0){
                check.addParameter("page", CheckParameters.paraType.EXP, Config.getInstance().getString("params_page"));
                page = check.get("page").toString();
            }else{
                page = "1";
            }
            // 获取session
            Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
            String productId = selfInfo.get("loginpid");

            HashMap<String,Object> productList = AdvertDao.getAdvertList(productId,Integer.parseInt(page),Integer.parseInt(rows),isenablesta,isreadydel,immediateshow,advertstatus,startdate,enddate);
            HashMap<String ,Object> res = new HashMap<>();
            res.put("total",productList.get("count"));
            res.put("rows",new JSONArray((ArrayList<HashMap<String,String>>)productList.get("rows")));
            String ret = ApiErrorCode.echoOkMap(res);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            response.getWriter().write(ret);
            return;
        } catch (CheckParameterException e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s","system exception=",e),e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        doPost(request,response);
    }
}
