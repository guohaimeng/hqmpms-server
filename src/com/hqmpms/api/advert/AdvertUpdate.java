package com.hqmpms.api.advert;

import com.hqmpms.api.utils.*;
import com.hqmpms.dao.advert.AdvertDao;
import com.hqmpms.dao.system.DoOperationLogThread;
import com.hqmpms.dispatcher.Startup;
import com.hqmpms.utils.DoCachedThreadPool;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Servlet implementation class Update
 * 广告修改
 * * input parameter:
 * advertid(必填,string): 广告ID
 * title(必填,string): 标题
 * type(必填,string): 类型
 * immediateshow(必填,string): 是否立即显示
 * prioritylevel(必填,string): 级别
 * isenable(必填,string): 是否启用
 * begintime(必填,string): 开始时间
 * endtime(必填,string): 结束时间
 * jsonimg(必填,string): 图片链接地址及尺寸
 * linkurl(选填,string): 链接
 * location(选填,string): 位置
 * advertdesc(选填,string): 描述
 * delimgid(选填,string): 已删除附件ID
 */
@WebServlet("/advert/advertUpdate.do")
public class AdvertUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger m_logger = Logger.getLogger(AdvertUpdate.class);

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CheckParameters check = new CheckParameters(request.getParameterMap());
		m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
		try {
			check.addParameter("advertid", CheckParameters.paraType.STRING,1,30);
			check.addParameter("title", CheckParameters.paraType.STRING,1,250);
			check.addParameter("type", CheckParameters.paraType.EXP, "^[1-3]$");
			check.addParameter("immediateshow", CheckParameters.paraType.INT,0,1);
			check.addParameter("prioritylevel", CheckParameters.paraType.EXP, "^[1-3]$");
			check.addParameter("isenable", CheckParameters.paraType.INT,0,1);
			check.addParameter("begintime", CheckParameters.paraType.STRING, 0, 200);
			check.addParameter("endtime", CheckParameters.paraType.STRING, 0, 200);
			String  jsonimg="";
			if(!StringUtils.isEmpty(request.getParameter("jsonimg"))) {
				check.addParameter("jsonimg", CheckParameters.paraType.JSONARR);
				jsonimg = check.get("jsonimg").toString();
			}
			String linkurl="";
			if(!StringUtils.isEmpty(request.getParameter("linkurl"))){
				check.addParameter("linkurl", CheckParameters.paraType.STRING, 1,300);
				linkurl=check.get("linkurl").toString();
			}
			String location="";
			if(!StringUtils.isEmpty(request.getParameter("location"))){
				check.addParameter("location", CheckParameters.paraType.EXP, "^([1-9][0-9]{0,1}|100)$");
				location=check.get("location").toString();
			}
			String advert_desc="";
			if(!StringUtils.isEmpty(request.getParameter("advert_desc"))){
				check.addParameter("advert_desc", CheckParameters.paraType.STRING, 0, 600);
				advert_desc=check.get("advert_desc").toString();
			}
			String delimgid="";
			if(!StringUtils.isEmpty(request.getParameter("delimgid"))){
				check.addParameter("delimgid", CheckParameters.paraType.STRING, 0, 500);
				delimgid=check.get("delimgid").toString();
			}

			HashMap<String,String> advertInfo = AdvertDao.getAdvertByAdvertId(check.get("advertid").toString());
			// 获取session
			Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
			String productId = selfInfo.get("loginpid");
			//数据封装
			Map<String,String> condition= new HashMap<>();

			condition.put("advertid",check.get("advertid").toString());
			condition.put("uid",selfInfo.get("mid"));
			condition.put("title",check.opt("title").toString());
			condition.put("location",location);
			condition.put("immediateshow",check.opt("immediateshow").toString());
			condition.put("prioritylevel",check.opt("prioritylevel").toString());
			condition.put("type",check.opt("type").toString());
			condition.put("isenable",check.opt("isenable").toString());
			condition.put("linkurl",linkurl);
			condition.put("productid",productId);
			condition.put("begintime",check.opt("begintime").toString());
			condition.put("endtime",check.opt("endtime").toString());
			condition.put("jsonimg",jsonimg);
			condition.put("description",advert_desc);
			condition.put("delimgid",delimgid);

			if(AdvertDao.AdvertUpdate(condition)){
				String ret = ApiErrorCode.echoOk();
				m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
				response.getWriter().write(ret);
				DoCachedThreadPool.getCachedThreadPool().cachedThreadPool.execute(new DoOperationLogThread(selfInfo.get("mid"),"修改广告'"+advertInfo.get("title").toString()+"'",check.get("advertid").toString()));
			}else {
				m_logger.info(String.format("FAILED params=%s","updateAdvertErr"));
				response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
			}


		} catch (CheckParameterException e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s","system exception=",e),e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
        }
	}

}
