package com.hqmpms.api.advert;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;

import com.hqmpms.dao.advert.AdvertDao;
import org.apache.log4j.Logger;
import org.json.JSONArray;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2018/6/1.
 * 客户端获取广告附件
 * input parameter:
 * adtype(必填,string): 类型
 * adwidth(必填,string): 宽度
 * adheight(必填,string): 高度
 */
@WebServlet("/advert/getClientAdvert.do")
public class GetClientAdvert extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(GetClientAdvert.class);
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
            check.addParameter("adtype", CheckParameters.paraType.EXP,"^[1-3]$");
            check.addParameter("adwidth", CheckParameters.paraType.STRING,1,4);
            check.addParameter("adheight", CheckParameters.paraType.EXP.STRING,1,4);
            Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
            String productId = selfInfo.get("loginpid");


            ArrayList<HashMap<String,String>> clientAdvertList = AdvertDao.getClientAdvertPicture(productId,check.get("adtype").toString(),check.get("adwidth").toString(),check.get("adheight").toString());
            HashMap<String ,Object> res = new HashMap<>();
            res.put("data",new JSONArray(clientAdvertList));
            String ret = ApiErrorCode.echoOkMap(res);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            response.getWriter().write(ret);

        } catch (CheckParameterException e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s","system exception=",e),e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
        }

    }
}
