package com.hqmpms.api.appmodule;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.appmodule.AppModuleDao;
import com.hqmpms.dao.screeningconditions.ScreeningConditionsDao;
import com.hqmpms.dao.system.OperationLogDao;
import com.hqmpms.dispatcher.Startup;
import net.sf.json.JSONObject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * app功能模块新增接口
 * Created by dml on  2018/7/25
 */
@WebServlet("/module/add.do")
public class AppModuleAdd extends HttpServlet{


    private static final Logger LOGGER = Logger.getLogger(AppModuleAdd.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // 参数校验
        CheckParameters check;
        try {
            check = new CheckParameters(request.getParameterMap());
            LOGGER.info(String.format("INPUT params=%s", check.getParameterJSON()));
            check.addParameter("moduleName", CheckParameters.paraType.STRING, 1, 50);
            check.addParameter("iconUrl", CheckParameters.paraType.STRING, 1,255);
            check.addParameter("status", CheckParameters.paraType.INT, 0,1);
            check.addParameter("isIndex", CheckParameters.paraType.INT, 0,1);
            check.addParameter("orderNum", CheckParameters.paraType.INT, 1,10000);
            check.addParameter("moduleType", CheckParameters.paraType.INT, 1, 3);
            check.addParameter("moduleCode", CheckParameters.paraType.STRING, 1, 50);
            check.addParameter("userType", CheckParameters.paraType.STRING, 1, 50);
            check.addParameter("modulePower", CheckParameters.paraType.INT, 0, 1);
            if ("1".equals(check.opt("modulePower").toString())){
                check.addParameter("areaId", CheckParameters.paraType.STRING, 1, 60000);
                check.addParameter("osTypes", CheckParameters.paraType.STRING, 1, 500);
                check.addParameter("versions", CheckParameters.paraType.STRING, 1, 60000);
            }
            String moduleTag = request.getParameter("moduleTag");
            if (!StringUtils.isEmpty(moduleTag)){
                if (moduleTag.length()>1){
                    response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR));
                    return;
                }
            }
            String moduleLink = request.getParameter("moduleLink");
            if (!StringUtils.isEmpty(moduleLink)){
                if (moduleLink.length()>255){
                    response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR));
                    return;
                }
            }
            int isOnlyOne = AppModuleDao.isOnlyOne(null,check.opt("userType").toString(),check.opt("moduleName").toString(),check.opt("orderNum").toString(),check.opt("moduleCode").toString());
            if (isOnlyOne==-1){
                response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.MODULE_NAME_EXIST));
                return;
            }
            if (isOnlyOne==-2){
                response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.MODULE_ORDER_NUM_EXIST));
                return;
            }
            if (isOnlyOne==-3){
                response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.MODULE_CODE_EXIST));
                return;
            }
            long id = Startup.getId();
             int result =  AppModuleDao.appModuleAdd(
                            id,
                            check.opt("moduleName").toString(),
                            check.opt("iconUrl").toString(),
                            check.opt("status").toString(),
                            check.opt("isIndex").toString(),
                            check.opt("orderNum").toString(),
                            check.opt("moduleType").toString(),
                            moduleLink,
                            moduleTag,
                            check.opt("moduleCode").toString(),
                            check.opt("userType").toString());
             if (result>0){
                 //判断是否选择了部分用户进行权限设置
                 if ("1".equals(check.opt("modulePower").toString())&&"1".equals(check.opt("moduleType").toString())){
                     String moduleAreaId = request.getParameter("areaId");
                     String osType = check.opt("osTypes").toString();
                     String version = check.opt("versions").toString();
                     if (!StringUtils.isEmpty(moduleAreaId)){
                         String[] moduleAreaIds = moduleAreaId.split("\\|");
                         String[] osTypes = osType.split(",");
                         String[] versions = version.split(",");
                         if (moduleAreaIds.length>0){
                             for (int i =0;i<moduleAreaIds.length;i++){
                                 JSONObject record = JSONObject.fromObject(moduleAreaIds[i]);
                                 List<HashMap<String, String>> list =
                                         ScreeningConditionsDao.getUserListByAllConditions(
                                                 record.getString("provinceId"),
                                                 record.getString("cityId"),
                                                 record.getString("areaId"),
                                                 record.getString("schoolId"),null,null,null);
                                 if (!CollectionUtils.isEmpty(list)){
                                     List<String> userIds = new ArrayList<>();
                                     for (HashMap<String, String> m:list) {
                                         userIds.add(m.get("UserID"));
                                     }
                                     long modulePowerId = Startup.getId();
                                     AppModuleDao.appModulePowerAdd(modulePowerId,id,moduleAreaIds[i],osTypes[i],versions[i],userIds.toString().substring(1,userIds.toString().length()-1));
                                 }
                             }
                         }
                     }
                 }
                 // session校验
                 @SuppressWarnings("unchecked")
                 Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
                 OperationLogDao.operationLogRecord(selfInfo.get("mid"),"app功能模块新增",String.valueOf(id));
                 String ret = ApiErrorCode.echoOk();
                 LOGGER.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SUCCESS));
                 response.getWriter().write(ret);
             }else {
                 response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
             }
        } catch (IOException e) {
            LOGGER.info(String.format("OUTPUT  ret_code=%s,%s", ApiErrorCode.FILE_TOO_LARGE,e));
            String ret = ApiErrorCode.echoErr(ApiErrorCode.FILE_TOO_LARGE);
            response.getWriter().write(ret);
        } catch (CheckParameterException ce) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            LOGGER.info(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.PARAMETER_ERROR,ce));
            response.getWriter().write(ret);
        }catch (Exception e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            LOGGER.info(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.SYSTEM_ERROR,e));
            response.getWriter().write(ret);
        }
    }
}
