package com.hqmpms.api.appmodule;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.appmodule.AppModuleDao;
import com.hqmpms.dao.system.OperationLogDao;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * app功能模块删除接口
 * Created by dml on  2018/7/25
 */
@WebServlet("/module/delete.do")
public class AppModuleDelete extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(AppModuleDelete.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // 参数校验
        CheckParameters check;
        try {
            check = new CheckParameters(request.getParameterMap());
            LOGGER.info(String.format("INPUT params=%s", check.getParameterJSON()));
            check.addParameter("moduleId", CheckParameters.paraType.STRING, 1, 20);
            int result =  AppModuleDao.appModuleDelete(check.opt("moduleId").toString());
            if (result>0){
                // session校验
                @SuppressWarnings("unchecked")
                Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
                OperationLogDao.operationLogRecord(selfInfo.get("mid"),"app功能模块删除",check.opt("moduleId").toString());
                String ret = ApiErrorCode.echoOk();
                LOGGER.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SUCCESS));
                response.getWriter().write(ret);
            }else {
                response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
            }
        } catch (IOException e) {
            LOGGER.info(String.format("OUTPUT  ret_code=%s %s", ApiErrorCode.FILE_TOO_LARGE,e));
            String ret = ApiErrorCode.echoErr(ApiErrorCode.FILE_TOO_LARGE);
            response.getWriter().write(ret);
        } catch (CheckParameterException ce) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            LOGGER.info(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.PARAMETER_ERROR,ce));
            response.getWriter().write(ret);
        }catch (Exception e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            LOGGER.info(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.SYSTEM_ERROR,e));
            response.getWriter().write(ret);
        }
    }
}
