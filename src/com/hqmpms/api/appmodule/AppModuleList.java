package com.hqmpms.api.appmodule;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.appmodule.AppModuleDao;
import org.apache.log4j.Logger;
import org.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * app功能模块列表
 * Created by dml on  2018/7/25
 */
@WebServlet("/module/list.do")
public class AppModuleList extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(AppModuleList.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //参数校验
        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            LOGGER.info(String.format("INPUT params=%s", check.getParameterJSON()));
            check.addParameter("userType", CheckParameters.paraType.INT, 1, 7);
            String status = request.getParameter("status");
            String isIndex = request.getParameter("isIndex");
            String search = request.getParameter("search");
            String sPage = request.getParameter("page");
            int page = Integer.parseInt(sPage == null ? "1" : sPage);
            String sPageSize = request.getParameter("rows");
            int pageSize = Integer.parseInt(sPageSize == null ? "10": sPageSize);
            long count = AppModuleDao.getAppModuleCount(check.opt("userType").toString(),status,isIndex,search);
            List<HashMap<String, String>> list = new ArrayList<>();
            if (count>0){
               list = AppModuleDao.getAppModuleList(check.opt("userType").toString(),status,isIndex,search,page, pageSize);
            }
            HashMap<String, Object> resultMap = new HashMap<>();
            resultMap.put("total", count);
            resultMap.put("rows",new JSONArray(list));
            String ret = ApiErrorCode.echoOkMap(resultMap);
            response.getWriter().write(ret);
            LOGGER.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SUCCESS));
            response.getWriter().close();
        } catch (Exception e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            LOGGER.info(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.SYSTEM_ERROR, e));
            try {
                response.getWriter().write(ret);
            }catch (Exception ex){
                LOGGER.error(ex);
            }
        }

    }
}
