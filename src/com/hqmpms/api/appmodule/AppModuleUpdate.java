package com.hqmpms.api.appmodule;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.appmodule.AppModuleDao;
import com.hqmpms.dao.system.OperationLogDao;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * app功能模块修改接口
 * Created by dml on  2018/7/25
 */
@WebServlet("/module/update.do")
public class AppModuleUpdate extends HttpServlet{


    private static final Logger LOGGER = Logger.getLogger(AppModuleUpdate.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // 参数校验
        CheckParameters check;
        try {
            check = new CheckParameters(request.getParameterMap());
            LOGGER.info(String.format("INPUT params=%s", check.getParameterJSON()));
            check.addParameter("moduleId", CheckParameters.paraType.STRING, 1, 20);
            check.addParameter("moduleName", CheckParameters.paraType.STRING, 1, 50);
            check.addParameter("iconUrl", CheckParameters.paraType.STRING, 1,255);
            check.addParameter("status", CheckParameters.paraType.INT, 0,1);
            check.addParameter("isIndex", CheckParameters.paraType.INT, 0,1);
            check.addParameter("orderNum", CheckParameters.paraType.INT, 1,10000);
            check.addParameter("moduleType", CheckParameters.paraType.INT, 1, 3);
            check.addParameter("moduleCode", CheckParameters.paraType.STRING, 1, 50);
            check.addParameter("userType", CheckParameters.paraType.STRING, 1, 50);
            String moduleTag = request.getParameter("moduleTag");
            if (!StringUtils.isEmpty(moduleTag)){
                if (moduleTag.length()>1){
                    response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR));
                    return;
                }
            }
            String moduleLink = request.getParameter("moduleLink");
            if (!StringUtils.isEmpty(moduleLink)){
                if (moduleLink.length()>255){
                    response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR));
                    return;
                }
            }
            int isOnlyOne = AppModuleDao.isOnlyOne(check.opt("moduleId").toString(),check.opt("userType").toString(),check.opt("moduleName").toString(),check.opt("orderNum").toString(),check.opt("moduleCode").toString());
            if (isOnlyOne==-1){
                response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.MODULE_NAME_EXIST));
                return;
            }
            if (isOnlyOne==-2){
                response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.MODULE_ORDER_NUM_EXIST));
                return;
            }
            if (isOnlyOne==-3){
                response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.MODULE_CODE_EXIST));
                return;
            }
             int result =  AppModuleDao.appModuleUpdate(
                            check.opt("moduleName").toString(),
                            check.opt("iconUrl").toString(),
                            check.opt("status").toString(),
                            check.opt("isIndex").toString(),
                            check.opt("orderNum").toString(),
                            check.opt("moduleType").toString(),
                            moduleLink,
                            moduleTag,
                            check.opt("moduleCode").toString(),
                            check.opt("userType").toString(),
                            check.opt("moduleId").toString());
             if (result>0){
                 // session校验
                 @SuppressWarnings("unchecked")
                 Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
                 OperationLogDao.operationLogRecord(selfInfo.get("mid"),"app功能模块修改",check.opt("moduleId").toString());
                 String ret = ApiErrorCode.echoOk();
                 LOGGER.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SUCCESS));
                 response.getWriter().write(ret);
             }else {
                 response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
             }
        } catch (IOException e) {
            LOGGER.info(String.format("OUTPUT  ret_code=%s %s", ApiErrorCode.FILE_TOO_LARGE,e));
            String ret = ApiErrorCode.echoErr(ApiErrorCode.FILE_TOO_LARGE);
            response.getWriter().write(ret);
        } catch (CheckParameterException ce) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            LOGGER.info(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.PARAMETER_ERROR,ce));
            response.getWriter().write(ret);
        }catch (Exception e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            LOGGER.info(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.SYSTEM_ERROR,e));
            response.getWriter().write(ret);
        }
    }
}
