package com.hqmpms.api.apprelease;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.apprelease.AppReleaseDao;
import com.hqmpms.dao.system.OperationLogDao;
import com.hqmpms.dispatcher.Config;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * 版本发布删除
 * Created by dml on  2018/5/22
 */
@WebServlet("/appRelease/delete.do")
public class AppReleaseDelete extends HttpServlet{
    private static final Logger LOGGER = Logger.getLogger(AppReleaseDelete.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //参数校验
        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            LOGGER.info(String.format("INPUT params=%s", check.getParameterJSON()));
            check.addParameter("releaseId", CheckParameters.paraType.EXP, Config.getInstance().getString("params_gid"));
            String releaseId = request.getParameter("releaseId");
            int result = AppReleaseDao.appReleaseDelete(releaseId);
            if (result>0){
                // session校验
                @SuppressWarnings("unchecked")
                Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
                OperationLogDao.operationLogRecord(selfInfo.get("mid"),"对发布的版本删除",check.opt("releaseId").toString());
                String ret = ApiErrorCode.echoOk();
                LOGGER.info(String.format("OUTPUT event=%s ret_code=%s",releaseId, ApiErrorCode.SUCCESS));
                response.getWriter().write(ret);
            }
        } catch (CheckParameterException ce) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            LOGGER.info(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.PARAMETER_ERROR, ce));
            try {
                response.getWriter().write(ret);
            }catch (Exception e){
                LOGGER.error(e);
            }
        } catch (Exception e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            LOGGER.info(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.SYSTEM_ERROR, e));
            try {
                response.getWriter().write(ret);
            }catch (Exception ex){
                LOGGER.error(ex);
            }
        }
    }
}
