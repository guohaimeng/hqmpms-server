package com.hqmpms.api.apprelease;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.apprelease.AppReleaseDao;
import com.hqmpms.dao.system.OperationLogDao;
import com.hqmpms.dispatcher.Startup;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * 版本发布
 * Created by dml on  2018/5/22
 */
@WebServlet("/appRelease/publish.do")
public class AppReleasePublish extends HttpServlet{

    private static final Logger LOGGER = Logger.getLogger(AppReleasePublish.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //参数校验
        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            LOGGER.info(String.format("INPUT params=%s", check.getParameterJSON()));
            check.addParameter("isMandatory", CheckParameters.paraType.INT, 0, 1);
            check.addParameter("isOpen", CheckParameters.paraType.INT, 0,1);
            check.addParameter("testId", CheckParameters.paraType.STRING, 1,20);
            //获取session
            @SuppressWarnings("unchecked")
            Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
            int result = AppReleaseDao.appReleasePublish(
                    Startup.getId(),
                    (int)check.get("isMandatory"),
                    request.getParameter("remark"),
                    selfInfo.get("mid"),
                    check.get("testId").toString(),
                    selfInfo.get("loginpid")
            );
            if(result>0){
                OperationLogDao.operationLogRecord(selfInfo.get("mid"),"版本发布",check.opt("testId").toString());
                String ret = ApiErrorCode.echoOk();
                LOGGER.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SUCCESS));
                response.getWriter().write(ret);
            }else {
                String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
                LOGGER.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SYSTEM_ERROR));
                response.getWriter().write(ret);
            }
            response.getWriter().close();
        } catch (CheckParameterException ce) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            LOGGER.info(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.PARAMETER_ERROR, ce));
            try {
                response.getWriter().write(ret);
            }catch (Exception e){
                LOGGER.error(e);
            }
        } catch (Exception e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            LOGGER.info(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.SYSTEM_ERROR, e));
            try {
                response.getWriter().write(ret);
            }catch (Exception ex){
                LOGGER.error(ex);
            }
        }
    }
}
