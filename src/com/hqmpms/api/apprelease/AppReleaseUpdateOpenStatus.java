package com.hqmpms.api.apprelease;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.apprelease.AppReleaseDao;
import com.hqmpms.dao.system.OperationLogDao;
import com.hqmpms.dispatcher.Config;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 更改版本发布公开状态
 * Created by dml on  2018/6/4
 */
@WebServlet("/appRelease/updateOpenStatus.do")
public class AppReleaseUpdateOpenStatus extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(AppReleaseUpdate.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // 参数校验
        CheckParameters check;
        try {
            check = new CheckParameters(request.getParameterMap());
            LOGGER.info(String.format("INPUT params=%s", check.getParameterJSON()));
            check.addParameter("releaseId", CheckParameters.paraType.EXP, Config.getInstance().getString("params_gid"));
            check.addParameter("isOpen", CheckParameters.paraType.INT, 0,1);
            int result =  AppReleaseDao.appReleaseUpdateOpenStatus(
                    check.opt("isOpen").toString(),
                    check.opt("releaseId").toString());
            HashMap<String, Object> resultMap = new HashMap<>();
            if (result>0){
                // session校验
                @SuppressWarnings("unchecked")
                Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
                OperationLogDao.operationLogRecord(selfInfo.get("mid"),"更改版本发布公开状态："+(check.opt("isOpen").toString().equals("1")?"公开":"不公开"),check.opt("releaseId").toString());
                resultMap.put("SUCCESS", "0");
                LOGGER.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
                String ret = ApiErrorCode.echoOkMap(resultMap);
                response.getWriter().write(ret);
            }else {
                String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
                LOGGER.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SYSTEM_ERROR));
                response.getWriter().write(ret);
            }
            response.getWriter().close();
        }  catch (CheckParameterException ce) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            LOGGER.info(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.PARAMETER_ERROR,ce));
            try {
                response.getWriter().write(ret);
            }catch (Exception e){
                LOGGER.error(e);
            }
        } catch (Exception e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            LOGGER.info(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.SYSTEM_ERROR,e));
            try {
                response.getWriter().write(ret);
            }catch (Exception ex){
                LOGGER.error(ex);
            }
        }
    }
}
