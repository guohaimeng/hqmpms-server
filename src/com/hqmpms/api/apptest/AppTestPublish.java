package com.hqmpms.api.apptest;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.apptest.AppTestDao;
import com.hqmpms.dao.system.OperationLogDao;
import com.hqmpms.dispatcher.Startup;
import com.hqmpms.qrcode.EncoderHandler;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * Created by dml on  2018/5/18
 * app版本测试发布接口
 */
@WebServlet("/appTest/publish.do")
public class AppTestPublish extends HttpServlet{


    private static final Logger LOGGER = Logger.getLogger(AppTestPublish.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // 参数校验
        CheckParameters check;
        try {
            check = new CheckParameters(request.getParameterMap());
            LOGGER.info(String.format("INPUT params=%s", check.getParameterJSON()));
            check.addParameter("osType", CheckParameters.paraType.INT, 1, 2);
            check.addParameter("version", CheckParameters.paraType.STRING, 1,200);
            check.addParameter("buildCode", CheckParameters.paraType.STRING, 1,100);
            check.addParameter("fileName", CheckParameters.paraType.STRING, 1,200);
            check.addParameter("testPhase", CheckParameters.paraType.INT, 1,3);
            check.addParameter("remark", CheckParameters.paraType.STRING, 0, 500);
            check.addParameter("fileUrl", CheckParameters.paraType.STRING, 1, 500);
            check.addParameter("fileSize", CheckParameters.paraType.STRING, 1, 500);
            // session校验
            @SuppressWarnings("unchecked")
            Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
            long id = Startup.getId();
                  int result =  AppTestDao.appTestPublish(
                            id,
                            check.opt("osType").toString(),
                            check.opt("version").toString(),
                            check.opt("buildCode").toString(),
                            check.opt("fileName").toString(),
                            check.opt("fileUrl").toString(),
                            check.opt("testPhase").toString(),
                            AppTestDao.getAppTestMaxTestRotation(check.opt("osType").toString(),check.opt("version").toString(),check.opt("testPhase").toString(),selfInfo.get("loginpid")),
                            selfInfo.get("mid"),
                            check.opt("remark").toString(),
                            selfInfo.get("loginpid"),
                            EncoderHandler.encoderQRCoder(check.opt("fileUrl").toString()),
                            check.opt("fileSize").toString());
                  if(result>0){
                      OperationLogDao.operationLogRecord(selfInfo.get("mid"),"版本测试发布",String.valueOf(id));
                      String ret = ApiErrorCode.echoOk();
                      LOGGER.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
                      response.getWriter().write(ret);
                  }else {
                      String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
                      LOGGER.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SYSTEM_ERROR));
                      response.getWriter().write(ret);
                  }
                  response.getWriter().close();
        } catch (IOException e) {
            LOGGER.info(String.format("OUTPUT  ret_code=%s,%s", ApiErrorCode.FILE_TOO_LARGE,e));
            String ret = ApiErrorCode.echoErr(ApiErrorCode.FILE_TOO_LARGE);
            response.getWriter().write(ret);
        } catch (CheckParameterException ce) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            LOGGER.info(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.PARAMETER_ERROR,ce));
            response.getWriter().write(ret);
        }catch (Exception e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            LOGGER.info(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.SYSTEM_ERROR,e));
            response.getWriter().write(ret);
        }
    }
}
