package com.hqmpms.api.apptest;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.apptest.AppTestDao;
import com.hqmpms.dao.system.OperationLogDao;
import com.hqmpms.dispatcher.Config;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * 更改版本测试状态
 * Created by dml on  2018/5/22
 */
@WebServlet("/appTest/statusChange.do")
public class AppTestStatusChange  extends HttpServlet{
    private static final Logger LOGGER = Logger.getLogger(AppTestStatusChange.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 参数校验
        CheckParameters check;
        try {
            check = new CheckParameters(request.getParameterMap());
            LOGGER.info(String.format("INPUT params=%s", check.getParameterJSON()));
            check.addParameter("testId", CheckParameters.paraType.EXP, Config.getInstance().getString("params_gid"));
            check.addParameter("status", CheckParameters.paraType.STRING, -1,3);
            //获取session
            @SuppressWarnings("unchecked")
            Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
            boolean result =  AppTestDao.appTestStatusChange(
                    check.opt("status").toString(),
                    check.opt("testId").toString(),
                    request.getParameter("remark"),
                    selfInfo.get("mid"));
            if (result){
                OperationLogDao.operationLogRecord(selfInfo.get("mid"),"更改版本测试状态到"+check.opt("status").toString(),check.opt("testId").toString());
                LOGGER.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SUCCESS));
                String ret = ApiErrorCode.echoOk();
                response.getWriter().write(ret);
            }else {
                String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
                LOGGER.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SYSTEM_ERROR));
                response.getWriter().write(ret);
            }
            response.getWriter().close();
        }  catch (CheckParameterException ce) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            LOGGER.info(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.PARAMETER_ERROR,ce));
            try {
                response.getWriter().write(ret);
            }catch (Exception e){
                LOGGER.error(e);
            }
        } catch (Exception e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            LOGGER.info(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.SYSTEM_ERROR,e));
            try {
                response.getWriter().write(ret);
            }catch (Exception ex){
                LOGGER.error(ex);
            }
        }
    }
}
