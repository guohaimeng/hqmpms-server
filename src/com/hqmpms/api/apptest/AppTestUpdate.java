package com.hqmpms.api.apptest;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.apptest.AppTestDao;
import com.hqmpms.dao.system.OperationLogDao;
import com.hqmpms.dispatcher.Config;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 版本测试修改
 * Created by dml on  2018/5/21
 */
@WebServlet("/appTest/update.do")
public class AppTestUpdate extends HttpServlet{
    private static final Logger LOGGER = Logger.getLogger(AppTestUpdate.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // 参数校验
        CheckParameters check;
        try {
            // session校验
            @SuppressWarnings("unchecked")
            Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
            check = new CheckParameters(request.getParameterMap());
            LOGGER.info(String.format("INPUT params=%s", check.getParameterJSON()));
            check.addParameter("testId", CheckParameters.paraType.EXP, Config.getInstance().getString("params_gid"));
            check.addParameter("testPhase", CheckParameters.paraType.STRING, 1,2);
            check.addParameter("osTypeUpd", CheckParameters.paraType.STRING, 1,2);
            check.addParameter("versionUpd", CheckParameters.paraType.STRING, 1, 500);
            check.addParameter("remark", CheckParameters.paraType.STRING, 1, 500);
            check.addParameter("isTypeChange", CheckParameters.paraType.STRING, 1, 2);
            String testRotation = "";
            if ("1".equals(check.opt("isTypeChange").toString())){
                testRotation = String.valueOf(AppTestDao.getAppTestMaxTestRotation(check.opt("osTypeUpd").toString(),check.opt("versionUpd").toString(),check.opt("testPhase").toString(),selfInfo.get("loginpid")));
            }
            HashMap<String, Object> fileMap = new HashMap<>();
            if("3".equals(check.opt("testPhase").toString())&&"2".equals(testRotation)){
                fileMap.put("SUCCESS","-2");
                String ret = ApiErrorCode.echoOkMap(fileMap);
                response.getWriter().write(ret);
                return;
            }
            int result =  AppTestDao.appTestUpdate(
                    check.opt("testPhase").toString(),
                    check.opt("remark").toString(),
                    testRotation,
                    check.opt("testId").toString());
            if (result>0){
                OperationLogDao.operationLogRecord(selfInfo.get("mid"),"版本测试修改",check.opt("testId").toString());
                LOGGER.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
                String ret = ApiErrorCode.echoOk();
                response.getWriter().write(ret);
            }else {
                String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
                LOGGER.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SYSTEM_ERROR));
                response.getWriter().write(ret);
            }
            response.getWriter().close();
        }  catch (CheckParameterException ce) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            LOGGER.info(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.PARAMETER_ERROR,ce));
            try {
                response.getWriter().write(ret);
            }catch (Exception e){
                LOGGER.error(e);
            }
        } catch (Exception e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            LOGGER.info(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.SYSTEM_ERROR,e));
            try {
                response.getWriter().write(ret);
            }catch (Exception ex){
                LOGGER.error(ex);
            }
        }

    }
}
