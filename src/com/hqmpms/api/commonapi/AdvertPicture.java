package com.hqmpms.api.commonapi;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.advert.AdvertDao;
import com.hqmpms.dispatcher.Config;
import org.apache.log4j.Logger;
import org.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by Administrator on 2018/6/1.
 * 获取广告附件
 * input parameter:
 * advertid(选填,string): ID
 */
@WebServlet("/commonapi/advert/advertPicture.do")
public class AdvertPicture extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(AdvertPicture.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
            check.addParameter("advertid", CheckParameters.paraType.EXP, Config.getInstance().getString("params_gid"));


            ArrayList<HashMap<String,String>> list = AdvertDao.getAdvertPicture(check.get("advertid").toString());

            HashMap<String ,Object> res = new HashMap<>();

            res.put("data",new JSONArray(list));

            String ret = ApiErrorCode.echoOkMap(res);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            response.getWriter().write(ret);
        } catch (CheckParameterException e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s","system exception=",e),e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
        }

    }


}
