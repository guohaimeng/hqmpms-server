package com.hqmpms.api.commonapi;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.message.MessageDao;
import com.hqmpms.dao.system.OperationLogDao;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * 根据编号删除发送人群记录
 * Created by dml on  2018/5/25
 */
@WebServlet("/commonapi/message/delRecordById.do")
public class DeleteMessageRecordById extends HttpServlet{
    private static final Logger LOGGER = Logger.getLogger(DeleteMessageRecordById.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //参数校验
        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            LOGGER.info(String.format("INPUT params=%s", check.getParameterJSON()));
            check.addParameter("recordId", CheckParameters.paraType.STRING, 1, 20);
            int result = MessageDao.delMessageRecordById(check.opt("recordId").toString());
            if (result!=-1){
                // session校验
                @SuppressWarnings("unchecked")
                Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
                OperationLogDao.operationLogRecord(selfInfo.get("mid"),"根据编号删除消息发送记录",check.opt("recordId").toString());
                String ret = ApiErrorCode.echoOk();
                LOGGER.info(String.format("OUTPUT event=%s ret_code=%s",check.opt("recordId").toString(), ApiErrorCode.SUCCESS));
                response.getWriter().write(ret);
            }
        }catch (Exception e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            LOGGER.error(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.SYSTEM_ERROR, e));
            try {
                response.getWriter().write(ret);
            }catch (Exception ex){
                LOGGER.error(ex);
            }
        }
    }
}
