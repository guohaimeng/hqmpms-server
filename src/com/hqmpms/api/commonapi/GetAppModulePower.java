package com.hqmpms.api.commonapi;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.appmodule.AppModuleDao;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * 获取功能模块权限
 * Created by dml on  2018/10/8 10:47
 */
@WebServlet("/commonapi/getAppModulePower.do")
public class GetAppModulePower extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(GetAppModulePower.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            LOGGER.info(String.format("INPUT params=%s", check.getParameterJSON()));
            check.addParameter("moduleId", CheckParameters.paraType.STRING, 1, 20);
            List<HashMap<String, String>> modulePowerList = AppModuleDao.getAppModulePower(check.opt("moduleId").toString());
            HashMap<String, Object> resultMap = new HashMap<>();
            resultMap.put("rows",new JSONArray(modulePowerList));
            String ret = ApiErrorCode.echoOkMap(resultMap);
            response.getWriter().write(ret);
            LOGGER.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SUCCESS));
        }catch (Exception e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            LOGGER.error(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.SYSTEM_ERROR, e));
            response.getWriter().write(ret);
        }finally {
            response.getWriter().close();
        }
    }
}
