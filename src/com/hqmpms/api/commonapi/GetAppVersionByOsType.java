package com.hqmpms.api.commonapi;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.appmodule.AppModuleDao;
import org.apache.log4j.Logger;
import org.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * 根据平台获取app版本号
 * Created by dml on  2018/9/18 10:42
 */
@WebServlet("/commonapi/getAppVersionByOsType.do")
public class GetAppVersionByOsType extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(GetAppTestRecordRemark.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            LOGGER.info(String.format("INPUT params=%s", check.getParameterJSON()));
            check.addParameter("osType", CheckParameters.paraType.INT, 0, 2);
            List<HashMap<String, String>> allList = AppModuleDao.getAppVersionByOsType(check.opt("osType").toString());
            HashMap<String, Object> resultMap = new HashMap<>();
            resultMap.put("rows",new JSONArray(allList));
            String ret = ApiErrorCode.echoOkMap(resultMap);
            response.getWriter().write(ret);
            LOGGER.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SUCCESS));
            response.getWriter().close();
        }catch (Exception e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            LOGGER.error(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.SYSTEM_ERROR, e));
            try {
                response.getWriter().write(ret);
            }catch (Exception ex){
                LOGGER.error(ex);
            }
        }
    }
}
