package com.hqmpms.api.commonapi;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.screeningconditions.ScreeningConditionsDao;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

/**
 * 根据班级ID获取班级
 * Created by dml on  2018/7/13
 */
@WebServlet("/commonapi/getClassById.do")
public class GetClassById extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(GetClassById.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            LOGGER.info(String.format("INPUT params=%s", check.getParameterJSON()));
            check.addParameter("classId", CheckParameters.paraType.STRING, 1, 36);
            HashMap<String, String> classMap = ScreeningConditionsDao.getClassInfoById(check.opt("classId").toString());
            HashMap<String, Object> resultMap = new HashMap<>();
            resultMap.put("rows", classMap);
            JSONObject obj = JSONObject.fromObject(resultMap);
            response.getWriter().write(obj.toString());
            LOGGER.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SUCCESS));
            response.getWriter().close();
        }catch (Exception e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            LOGGER.error(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.SYSTEM_ERROR, e));
            try {
                response.getWriter().write(ret);
            }catch (Exception ex){
                LOGGER.error(ex);
            }
        }
    }
}
