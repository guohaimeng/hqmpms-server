package com.hqmpms.api.commonapi;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.screeningconditions.ScreeningConditionsDao;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 获取筛选后的用户列表(根据产品code获取不同产品对应的用户列表)
 * Created by dml on  2018/5/23
 */
@WebServlet("/commonapi/getConditionsUserList.do")
public class GetConditionsUserList extends HttpServlet{

    private static final Logger LOGGER = Logger.getLogger(GetConditionsUserList.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //参数校验
        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            LOGGER.info(String.format("INPUT params=%s", check.getParameterJSON()));
            // session校验
            @SuppressWarnings("unchecked")
            Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
            String productCode = selfInfo.get("loginpcode");
            List<HashMap<String, String>> list = new ArrayList<>();
            if ("yext".equals(productCode)){
                list =  ScreeningConditionsDao.getUserListByAllConditions(check.opt("provinceId").toString(),
                        check.opt("cityId").toString(),check.opt("countyId").toString(),check.opt("schoolId").toString(),check.opt("classId").toString(),check.opt("memberType").toString(),null);
            }
            HashMap<String, Object> resultMap = new HashMap<>();
            resultMap.put("rows", list);
            JSONObject obj = JSONObject.fromObject(resultMap);
            response.getWriter().write(obj.toString());
            LOGGER.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SUCCESS));
            response.getWriter().close();
        }catch (Exception e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            LOGGER.error(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.SYSTEM_ERROR, e));
            try {
                response.getWriter().write(ret);
            }catch (Exception ex){
                LOGGER.error(ex);
            }
        }
    }
}
