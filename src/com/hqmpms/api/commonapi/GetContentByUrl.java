package com.hqmpms.api.commonapi;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameters;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

/**
 * 根据url获取页面内容
 * Created by dml on  2018/6/29
 */
@WebServlet("/commonapi/getContentByUrl.do")
public class GetContentByUrl extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(GetClassList.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            LOGGER.info(String.format("INPUT params=%s", check.getParameterJSON()));
            check.addParameter("url", CheckParameters.paraType.STRING, 1, 500);
            URL url = new URL(check.opt("url").toString());
            InputStream inputStream = null;
            InputStreamReader inputStreamReader = null;
            BufferedReader reader = null;
            StringBuilder resultBuilder = new StringBuilder();
            String tempLine;
            try {
                inputStream = url.openStream();
                inputStreamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
                reader = new BufferedReader(inputStreamReader);

                while ((tempLine = reader.readLine()) != null) {
                    resultBuilder.append(tempLine);
                }

            }finally {

                if (reader != null) {
                    reader.close();
                }

                if (inputStreamReader != null) {
                    inputStreamReader.close();
                }

                if (inputStream != null) {
                    inputStream.close();
                }

            }
            HashMap<String, Object> resultMap = new HashMap<>();
            resultMap.put("rows", resultBuilder.toString());
            JSONObject obj = JSONObject.fromObject(resultMap);
            response.getWriter().write(obj.toString());
            LOGGER.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SUCCESS));
            response.getWriter().close();
        }catch (Exception e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            LOGGER.error(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.SYSTEM_ERROR, e));
            try {
                response.getWriter().write(ret);
            }catch (Exception ex){
                LOGGER.error(ex);
            }
        }
    }
}
