package com.hqmpms.api.commonapi;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.screeningconditions.ScreeningConditionsDao;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * 查询行政区划列表
 * Created by dml on  2018/5/23
 */
@WebServlet("/commonapi/getDistrictList.do")
public class GetDistrictList extends HttpServlet{

    private static final Logger LOGGER = Logger.getLogger(GetDistrictList.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //参数校验
        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            LOGGER.info(String.format("INPUT params=%s", check.getParameterJSON()));
            check.addParameter("parentId", CheckParameters.paraType.STRING, 0, 6);
            check.addParameter("nStar", CheckParameters.paraType.INT, 0, 2);
            List<HashMap<String, String>> list = ScreeningConditionsDao.getDistrictList(check.opt("parentId").toString(),check.opt("nStar").toString());
            HashMap<String, Object> resultMap = new HashMap<>();
            resultMap.put("rows", list);
            JSONObject obj = JSONObject.fromObject(resultMap);
            response.getWriter().write(obj.toString());
            LOGGER.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SUCCESS));
            response.getWriter().close();
        }catch (Exception e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            LOGGER.error(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.SYSTEM_ERROR, e));
            try {
                response.getWriter().write(ret);
            }catch (Exception ex){
                LOGGER.error(ex);
            }
        }
    }
}
