package com.hqmpms.api.commonapi;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.api.utils.DatabaseManager;
import com.hqmpms.dao.system.PermissionDao;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * home页面数据初始化
 */
@WebServlet("/commonapi/homeinit.do")
public class HomeInit extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(HomeInit.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
            // 获取session
            Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
            String pid = request.getParameter("pid");
            if(!StringUtils.isEmpty(pid)){
                check.addParameter("pid",CheckParameters.paraType.STRING, 0, 20);
                selfInfo.put("loginpid", check.get("pid").toString());
                String[]  productmenus = PermissionDao.getHomeMenus(selfInfo.get("mid"),check.get("pid").toString());
                selfInfo.put("menus", productmenus[1]);
                selfInfo.put("loginpcode",productmenus[0]);
                DatabaseManager.updateProfile(selfInfo.get("mid"),selfInfo);
            }

            HashMap<String, Object> res = new HashMap<String, Object>();
            res.put("loginpid",selfInfo.get("loginpid"));
            res.put("menus",new JSONArray(selfInfo.get("menus")));
            res.put("permiproducts",new JSONArray(selfInfo.get("permiproducts")));
            res.put("rname",selfInfo.get("realNmae"));
            String ret = ApiErrorCode.echoOkMap(res);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            response.getWriter().write(ret);
            return;
        }catch (CheckParameterException ce) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR, ce));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s", "system exception=", e), e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
            return;
        }

    }

}
