/**
 * 系统登录
 */
package com.hqmpms.api.commonapi;

import com.hqmpms.api.utils.*;
import com.hqmpms.dao.system.ManagerDao;
import com.hqmpms.dao.system.OperationLogDao;
import com.hqmpms.dao.system.PermissionDao;
import com.hqmpms.dispatcher.Config;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

/**
 * 登录接口
 *
 */
@WebServlet("/commonapi/login.do")
public class Login extends HttpServlet {

	private static final long serialVersionUID = -8030229540888167965L;
	private static final Logger m_logger = Logger.getLogger(Login.class);

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			
			//参数校验
			CheckParameters check = new CheckParameters(request.getParameterMap());
			m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
			check.addParameter("acc", CheckParameters.paraType.EXP,"^[a-zA-Z0-9_\u4e00-\u9fa5]{2,16}");
			check.addParameter("pwd", CheckParameters.paraType.EXP, Config.getInstance().getString("params_pwd"));

			//获取用户表基本信息
			HashMap<String, String> selfInfo;
			selfInfo = ManagerDao.getUserByUserName(check.opt("acc").toString());

			if (selfInfo == null || selfInfo.size()==0) {
				String ret = ApiErrorCode.echoErr(ApiErrorCode.ACC_NOT_EXIST);
				m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.ACC_NOT_EXIST));
				response.getWriter().write(ret);
				return;
			}else{
				//
			}
			
			//校验用户密码是否匹配
			if (!Tools.md5(check.opt("pwd").toString()).substring(8, 24).equals(selfInfo.get("pwd"))) {
				String ret = ApiErrorCode.echoErr(ApiErrorCode.ACC_PWD_ERR);
				m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.ACC_PWD_ERR));
				response.getWriter().write(ret);
				return;
			}


			HashMap<String,Object> permissions = PermissionDao.getManagerPermission(selfInfo.get("mid"));

			selfInfo.put("permissions",permissions.get("permissions").toString());
			selfInfo.put("permiproducts",permissions.get("permiproducts").toString());
			selfInfo.put("menus",permissions.get("menus").toString());
			selfInfo.put("loginpid",permissions.get("loginpid").toString());
			selfInfo.put("loginpcode",permissions.get("loginpcode").toString());

			//将用户基本信息放                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   入缓存redis
			String ss = Tools.getRandomStr(30);
			selfInfo.put("ss", ss);
			DatabaseManager.addAdminProfile(selfInfo);

			//返回结果
			selfInfo.put("ss", ss);
			HashMap<String, Object> map = new HashMap<String, Object>();
			JSONObject obj = new JSONObject(selfInfo);
			//map.put("user", obj);
			map.put("ss",ss);
			String ret = ApiErrorCode.echoOkMap(map);
			response.getWriter().write(ret);
			OperationLogDao.operationLogRecord(selfInfo.get("mid"),"登录系统",selfInfo.get("mid"));
			return;
		} catch (CheckParameterException e) {
			String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
			m_logger.info(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.PARAMETER_ERROR,e));
			response.getWriter().write(ret);
			return;
		}catch(Exception e){
			String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
			m_logger.info(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.SYSTEM_ERROR,e));
			response.getWriter().write(ret);
			return;
		}
	}



//	public static void main(String [] args){
//
//		new Thread() {
//			public void run() {
//				try {
//					File f = new File("F:\\优学项目 2x\\优学项目 2x");
//					if (null != f) {
//						File[] fileList = f.listFiles();
//						if (null != fileList && fileList.length > 0) {
//							int count = fileList.length;
//							System.out.println(count);
//							for (int i = 0; i < count; i++) {
//								fileList[i].renameTo(new File("F:\\优学项目 2x\\优学项目 2x_num\\" + "test" + i + ".png"));
//							}
//						}
//					} else {
//						Log.w("info", "the file is null!!!!");
//						System.out.println("the file is null!!!!");
//					}
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//
//			}
//		}.start();
//	}
}
