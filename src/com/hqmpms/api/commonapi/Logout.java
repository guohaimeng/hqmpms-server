/**
 * 系统登录
 */
package com.hqmpms.api.commonapi;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.api.utils.DatabaseManager;
import com.hqmpms.dao.system.OperationLogDao;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * @author 退出登录
 *
 */
@WebServlet("/commonapi/logout.do")
public class Logout extends HttpServlet {

	private static final long serialVersionUID = -8030229540888167965L;
	private static final Logger m_logger = Logger.getLogger(Logout.class);

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			
			//参数校验
			CheckParameters check = new CheckParameters(request.getParameterMap());
			m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
			Map<String, String> selfInfo =( Map<String, String> )request.getSession().getAttribute("selfInfo");
			DatabaseManager.redisdelKey(new String[]{selfInfo.get("ss"),selfInfo.get("mid")+".ainf"});
			String ret = ApiErrorCode.echoOk();
			response.getWriter().write(ret);
			m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
			OperationLogDao.operationLogRecord(selfInfo.get("mid"),"退出登录",selfInfo.get("mid"));
			return;
		}catch(Exception e){
			String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
			m_logger.info(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.SYSTEM_ERROR,e));
			response.getWriter().write(ret);
			return;
		}
	}

}
