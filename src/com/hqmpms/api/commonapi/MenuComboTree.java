package com.hqmpms.api.commonapi;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.api.utils.EasyUiTree;
import com.hqmpms.dao.system.MenuDao;
import com.hqmpms.dispatcher.Config;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * 获取菜单combotree
 */
@WebServlet("/commonapi/menu/combotree.do")
public class MenuComboTree extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(MenuComboTree.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CheckParameters check = new CheckParameters(request.getParameterMap());
        m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
        try {
            check.addParameter("ss", CheckParameters.paraType.EXP, Config.getInstance().getString("params_ss"));
            Integer   is_sys_menu = null;
            if(!StringUtils.isEmpty(request.getParameter("is_sys_menu"))){
                check.addParameter("is_sys_menu", CheckParameters.paraType.INT, 0,1);
                is_sys_menu=Integer.parseInt(check.get("is_sys_menu").toString());
            }
            String product_id=null;
            if(is_sys_menu!=null){
                if(is_sys_menu==0){
                    check.addParameter("product_id", CheckParameters.paraType.STRING, 8, 20);
                    product_id = check.get("product_id").toString();
                }else{
                    product_id="0";
                }
            }
            ArrayList<HashMap<String,String>> list = MenuDao.menuComboTree(is_sys_menu,product_id);

            ArrayList<HashMap<String,String>> noToplist = new ArrayList<>();

            ArrayList<EasyUiTree> trees = new ArrayList<>();

            for(HashMap<String,String> map : list){
                if(map.get("parent_id").equals(product_id)|| StringUtils.isEmpty(map.get("parent_id"))){
                    EasyUiTree node = new EasyUiTree();
                    node.setId(map.get("menu_id"));
                    node.setText(map.get("menu_name"));
                    node.setAttribute1(map.get("menu_type"));
                    node.setAttribute2(map.get("menu_order"));
                    node.setChecked(false);
                    trees.add(node);
                }else{
                    noToplist.add(map);
                }
            }

            for(EasyUiTree tree : trees){
                buildTreeNode(tree,noToplist);
            }


            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            response.getWriter().write(new JSONArray(trees).toString());

        } catch (CheckParameterException e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s","system exception=",e),e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
        }
    }

    private  void  buildTreeNode(EasyUiTree node,ArrayList<HashMap<String,String>> noToplist){
        for(HashMap<String,String> map :noToplist){
            if(map.get("parent_id").equals(node.getId())){
                EasyUiTree tnode = new EasyUiTree();
                tnode.setId(map.get("menu_id"));
                tnode.setText(map.get("menu_name"));
                tnode.setAttribute1(map.get("menu_type"));
                tnode.setAttribute2(map.get("menu_order"));
                tnode.setChecked(false);
                node.addChildren(tnode);
                buildTreeNode(tnode,noToplist);
            }
        }
    }
}
