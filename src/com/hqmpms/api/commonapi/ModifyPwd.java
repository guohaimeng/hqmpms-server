package com.hqmpms.api.commonapi;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.api.utils.Tools;
import com.hqmpms.dao.system.ManagerDao;
import com.hqmpms.dao.system.OperationLogDao;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 密码修改
 */
@WebServlet("/commonapi/modifypwd.do")
public class ModifyPwd extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(ModifyPwd.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CheckParameters check = new CheckParameters(request.getParameterMap());
        m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
        try {
            Map<String, String> selfInfo =( Map<String, String> )request.getSession().getAttribute("selfInfo");
            check.addParameter("pwd", CheckParameters.paraType.STRING, 6, 18);
            check.addParameter("repwd", CheckParameters.paraType.STRING, 6, 18);
            if(check.get("pwd").equals(check.get("repwd"))){
                String pwd = Tools.md5(check.opt("pwd").toString()).substring(8, 24);
                Map<String,String> manager = new HashMap<>();
                manager.put("pwd",pwd);
                manager.put("manager_id",selfInfo.get("mid"));
                ManagerDao.managerUpdate(manager);
            }else {
                String ret = ApiErrorCode.echoErr(ApiErrorCode.PWD_UN_COINCIDENT);
                m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PWD_UN_COINCIDENT));
                response.getWriter().write(ret);
                return;
            }

            String ret = ApiErrorCode.echoOk();
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            response.getWriter().write(ret);
            OperationLogDao.operationLogRecord(selfInfo.get("mid"),"修改了登录密码",selfInfo.get("mid"));
        } catch (CheckParameterException e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s","system exception=",e),e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
        }
    }
}
