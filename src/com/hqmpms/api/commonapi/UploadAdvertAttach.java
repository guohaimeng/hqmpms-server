/**
 * 
 */
package com.hqmpms.api.commonapi;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.api.utils.Tools;
import com.hqmpms.utils.FastDfsUtil;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

/**
 * Created by Administrator on 2018/6/1.
 * 上传广告附件
 */
@WebServlet("/commonapi/advert/uploadAdvertAttach.do")
public class UploadAdvertAttach extends HttpServlet {

	private static final long serialVersionUID = -1935476189897248137L;
	private static final Logger m_logger = Logger.getLogger(UploadAdvertAttach.class);

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 参数校验
		CheckParameters check = null;
		ByteArrayOutputStream mmsFile = null;// 全局保存数据流
		HashMap<String, String> postParamsMap = new HashMap<String, String>();
		try {
			// 和普通post方式最大不同，在有stream的情况下，需要通过ServletFileUpload来获取参数
			ServletFileUpload upload = new ServletFileUpload(null);
			// 允许上传最大值
//			upload.setFileSizeMax(Config.getInstance().getInt(Config.KEY_MAX_FILE_LIMIT, Config.V_MAX_HEADIMG_FILE_LIMIT) * 1024 * 1024);
			FileItemIterator items = null;
			String headEncoding = request.getHeader("Content-Encoding");
			if (headEncoding != null && (headEncoding.indexOf("gzip") != -1)) {
				items = upload.getItemIteratorGzip(request);
			}else{
				items = upload.getItemIterator(request);
			}

			String contentEncrypt = request.getHeader("Content-encrypt");
			String key = null;
			String value = null;
			InputStream is = null;
			while (items.hasNext()) {
				FileItemStream itemStream = items.next();
				if (itemStream.isFormField()) {
					is = itemStream.openStream();
					key = itemStream.getFieldName();
					value = IOUtils.toString(is, "UTF-8");
					postParamsMap.put(key, value);
				} else {
					is = itemStream.openStream();
					String fileName = itemStream.getName();
					postParamsMap.put("file", fileName);
					mmsFile = Tools.InputStreamCopy(is);
					if(mmsFile.size() > 2 * 1024 * 1024){
						m_logger.info(String.format("OUTPUT  ret_code=%s %s", ApiErrorCode.FILE_TOO_LARGE,mmsFile.size()));
						String ret = ApiErrorCode.echoErr(ApiErrorCode.FILE_TOO_LARGE);
						response.getWriter().write(ret);
						return;
					}
					postParamsMap.put("size", String.valueOf(mmsFile.size()));
				}
				is.close();
			}
			
			if(mmsFile==null || mmsFile.size() == 0){
				String ret = ApiErrorCode.echoErr(ApiErrorCode.FILE_NOT_EXSIT);
				m_logger.info(String.format("OUTPUT mmsFile is null =%s ", ApiErrorCode.FILE_NOT_EXSIT));
				response.getWriter().write(ret);
				return;
			}
			check = new CheckParameters(postParamsMap);
			m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));



			// 上传文件到fastdfs
			String[] fileInfo = null;

			InputStream inStream =new ByteArrayInputStream(mmsFile.toByteArray());
			BufferedImage bis = ImageIO.read(inStream);//读取原始图片
			int imgWidth = bis.getWidth();
			int imgHeight = bis.getHeight();

//			if(check.get("type").toString().equals("1")){
//				if(!(imgWidth==640 && imgHeight==960)){
//					String ret = ApiErrorCode.echoErr(ApiErrorCode.IMG_SIZE_ERROR);
//					m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.IMG_SIZE_ERROR));
//					response.getWriter().write(ret);
//					return;
//				}
//			}
//
//			if(check.get("type").toString().equals("2")){
//				if(!(imgWidth==750 && imgHeight==1334)){
//					String ret = ApiErrorCode.echoErr(ApiErrorCode.IMG_SIZE_ERROR);
//					m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.IMG_SIZE_ERROR));
//					response.getWriter().write(ret);
//					return;
//				}
//			}


			fileInfo = FastDfsUtil.uploadFileByStream(mmsFile, check.opt("file").toString(), Long.parseLong(check.opt("size").toString()), "advert");
			//fileInfo = LocalUploadUtil.uploadFileByStream(mmsFile, check.opt("file").toString(), Long.parseLong(check.opt("size").toString()), "avatar","user");
			if (fileInfo == null) {
				String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
				m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SYSTEM_ERROR));
				response.getWriter().write(ret);
				return;
			} else {
				HashMap<String, Object> fileMap = new HashMap<String, Object>();
				fileMap.put("furl", fileInfo[0]);
				fileMap.put("imgwidth",imgWidth);
				fileMap.put("imgheight", imgHeight);
				String ret = ApiErrorCode.echoOkMap(fileMap);
				m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SUCCESS));
				response.getWriter().write(ret);
				return;	
			}
		
		}catch (IOException e) {
			m_logger.error("uploadAdvertImgError",e);
			String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
			response.getWriter().write(ret);
			return;
		}
		catch (FileUploadException e) {
			m_logger.error("uploadAdvertImgError",e);
			String ret = ApiErrorCode.echoErr(ApiErrorCode.FILE_NOT_EXSIT);
			response.getWriter().write(ret);
			return;
		}catch (Exception e) {
			String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
			m_logger.error("uploadAdvertImgError",e);
			response.getWriter().write(ret);
			return;
		}


	}


}
