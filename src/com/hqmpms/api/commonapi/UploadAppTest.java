package com.hqmpms.api.commonapi;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.api.utils.IpaUtil;
import com.hqmpms.api.utils.Tools;
import com.hqmpms.dao.apptest.AppTestDao;
import com.hqmpms.utils.FastDfsUtil;
import net.dongliu.apk.parser.ApkFile;
import net.dongliu.apk.parser.bean.ApkMeta;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * 文件上传
 * Created by dml on  2018/5/28
 */
@WebServlet("/commonapi/appTest/uploadAppTest.do")
public class UploadAppTest extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(UploadAppTest.class);

    private static final long serialVersionUID = 1L;

    // 上传文件存储目录
    private static final String UPLOAD_DIRECTORY = "upload";

    /**
     * 上传数据及保存文件
     */
    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws IOException {
        // 参数校验
        CheckParameters check;
        ByteArrayOutputStream mmsFile = null;// 全局保存数据流
        ApkMeta apkMeta = null;
        Map<String,String> map =null;
        HashMap<String, String> postParamsMap = new HashMap<>();
        String fileName = "";
        String osType = "";
        try {
            // 和普通post方式最大不同，在有stream的情况下，需要通过ServletFileUpload来获取参数
            ServletFileUpload upload = new ServletFileUpload(null);
            FileItemIterator items;
            items = upload.getItemIterator(request);
            String key;
            String value;
            InputStream is = null;
            try {
                while (items.hasNext()) {
                    FileItemStream itemStream = items.next();
                    if (itemStream.isFormField()) {
                        is = itemStream.openStream();
                        key = itemStream.getFieldName();
                        value = IOUtils.toString(is, "UTF-8");
                        postParamsMap.put(key, value);
                    } else {
                        is = itemStream.openStream();
                        fileName = itemStream.getName();
                        postParamsMap.put("file", fileName);
                        mmsFile = Tools.InputStreamCopy(is);
                        postParamsMap.put("size", String.valueOf(mmsFile.size()));
                    }
                }
            }catch (Exception e){
                LOGGER.info(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.SYSTEM_ERROR,e));
            }finally {
                if(null!=is){
                    is.close();
                }
            }
            if(mmsFile==null || mmsFile.size() == 0){
                String ret = ApiErrorCode.echoErr(ApiErrorCode.FILE_NOT_EXSIT);
                LOGGER.info(String.format("OUTPUT mmsFile is null =%s ", ApiErrorCode.FILE_NOT_EXSIT));
                response.getWriter().write(ret);
                return;
            }
            check = new CheckParameters(postParamsMap);
            LOGGER.info(String.format("INPUT params=%s", check.getParameterJSON()));
            // 上传文件到fastdfs
            String[] fileInfo = FastDfsUtil.uploadFileByStream(mmsFile, fileName, (long) mmsFile.size(), "app");
            if (fileInfo == null) {
                String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
                LOGGER.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SYSTEM_ERROR));
                response.getWriter().write(ret);
            } else {
                // 这个路径相对当前应用的目录
                String uploadPath = request.getServletContext().getRealPath("/") + File.separator + UPLOAD_DIRECTORY;

                // 如果目录不存在则创建
                File uploadDir = new File(uploadPath);
                if (!uploadDir.exists()) {
                    uploadDir.mkdir();
                }
                String filePath = uploadPath + File.separator + fileName;
                OutputStream outputStream = new FileOutputStream(filePath);
                mmsFile.writeTo(outputStream);
                outputStream.close();
                if(fileName.indexOf("apk")==-1&&fileName.indexOf("ipa")==-1){
                    String ret = ApiErrorCode.echoErr(ApiErrorCode.NOT_ALLOW_FILE);
                    response.getWriter().write(ret);
                    return;
                }
                if(fileName.indexOf("apk")>0){
                                LOGGER.info("apk开始解析,filePath:"+filePath);
                                osType = "1";
                                ApkFile apkParser = new ApkFile(new File(filePath));
                                try {
                                    apkMeta = apkParser.getApkMeta();
                                    LOGGER.info("apk解析完成");
                                }catch (Exception e){
                                    LOGGER.error("apk解析发生异常！"+e);
                                }finally {
                                    apkParser.close();
                                }
                }

                if (fileName.indexOf("ipa")>=0){
                                LOGGER.info("ipa开始解析,filePath:"+filePath);
                                osType = "2";
                                try {
                                    map = IpaUtil.getVersionInfo(new File(filePath));
                                    LOGGER.info("ipa解析完成");
                                }catch (Exception e){
                                    LOGGER.error("ipa解析发生异常！"+e);
                                }finally {
                                    //强制解除占用
                                    System.gc();
                                }
                }

                // session校验
                @SuppressWarnings("unchecked")
                Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
                String productId = selfInfo.get("loginpid");

                if (apkMeta!=null){
                LOGGER.info("解析apk成功！正在返回...");
                HashMap<String, Object> fileMap = new HashMap<>();
                HashMap<String, String> phaseMap = AppTestDao.getAppTestPhase(osType,apkMeta.getVersionName(),productId);
                if (phaseMap!=null){
                    fileMap.put("status",phaseMap.get("status")==null?1:phaseMap.get("status"));
                    fileMap.put("testPhase",phaseMap.get("test_phase")==null?0:phaseMap.get("test_phase"));
                }
                fileMap.put("versionName",apkMeta.getVersionName());
                fileMap.put("buildCode",apkMeta.getVersionCode());
                fileMap.put("filePath",fileInfo[0]);
                fileMap.put("fileLength",mmsFile.size());
                fileMap.put("fileName",fileName);
                String ret = ApiErrorCode.echoOkMap(fileMap);
                File file = new File(filePath);
                file.delete();
                LOGGER.info("解析apk成功！已返回并删除临时包！");
                LOGGER.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SUCCESS));
                response.getWriter().write(ret);
            }else if (map!=null){
                LOGGER.info("解析ipa成功！正在返回...");
                HashMap<String, Object> fileMap = new HashMap<>();
                HashMap<String, String> phaseMap = AppTestDao.getAppTestPhase(osType,map.get("CFBundleShortVersionString"),productId);
                if (phaseMap!=null){
                    fileMap.put("status",phaseMap.get("status")==null?1:phaseMap.get("status"));
                    fileMap.put("testPhase",phaseMap.get("test_phase")==null?0:phaseMap.get("test_phase"));
                }
                fileMap.put("versionName",map.get("CFBundleShortVersionString"));
                fileMap.put("buildCode",map.get("CFBundleVersion"));
                fileMap.put("filePath",fileInfo[0]);
                fileMap.put("fileLength",mmsFile.size());
                fileMap.put("fileName",fileName);
                String ret = ApiErrorCode.echoOkMap(fileMap);
                    String filePathDor = uploadPath + File.separator ;
                    File file = new File(filePathDor);
                        //参数说明---------path:要删除的文件的文件夹的路径---------str:要匹配的字符串的头
                        File[] tempFile = file.listFiles();
                        for(int i = 0; i < tempFile.length; i++){
                            if(tempFile[i].getName().startsWith(fileName .substring(0,fileName .lastIndexOf(".")))||tempFile[i].getName().endsWith(fileName .substring(0,fileName .lastIndexOf(".")))){
                                System.out.println("将被删除的文件名:"+tempFile[i].getName());
                                File delFile = new File(filePathDor+tempFile[i].getName());
                                boolean del=delFile.delete();
                                if(del){
                                    System.out.println("文件"+tempFile[i].getName()+"删除成功");
                                }else{
                                    delFile = new File(tempFile[i]+"/Info.plist");
                                    delFile.delete();
                                    delFile = new File(filePathDor+tempFile[i].getName());
                                    del = delFile.delete();
                                    if(del){
                                        System.out.println("文件"+tempFile[i].getName()+"删除成功");
                                    }else {
                                        System.out.println("文件"+tempFile[i].getName()+"删除失败");
                                    }
                                }
                            }
                        }
                LOGGER.info("解析ipa成功！已返回并删除临时包！");
                LOGGER.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SUCCESS));
                response.getWriter().write(ret);
            }else {
                    String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
                LOGGER.info("上传后没有解析成功app安装包！");
                response.getWriter().write(ret);
            }
            }
        }catch (IOException e) {
            LOGGER.error("uploadAdvertImgError",e);
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            response.getWriter().write(ret);
        }
        catch (FileUploadException e) {
            LOGGER.error("uploadAdvertImgError",e);
            String ret = ApiErrorCode.echoErr(ApiErrorCode.FILE_NOT_EXSIT);
            response.getWriter().write(ret);
        }catch (Exception e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            LOGGER.error("uploadAdvertImgError",e);
            response.getWriter().write(ret);
        }

    }
}
