package com.hqmpms.api.commonapi;


import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.api.utils.Tools;
import com.hqmpms.dao.system.ClientLogDao;
import com.hqmpms.dispatcher.Config;
import com.hqmpms.utils.FastDfsUtil;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;
import java.util.Map;
/**
 * Created by Administrator on 2018/6/1.
 * 客户端上传日志
 * input parameter:
 */
@WebServlet("/commonapi/clientLog/upload.do")
public class UploadClientLog extends HttpServlet {
	private static final Logger m_logger = Logger.getLogger(UploadClientLog.class);

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 参数校验
		CheckParameters check = null;
		ByteArrayOutputStream mmsFile = null;// 全局保存数据流
		HashMap<String, String> postParamsMap = new HashMap<String, String>();
		InputStream is = null;
		try {
			// 和普通post方式最大不同，在有stream的情况下，需要通过ServletFileUpload来获取参数
			ServletFileUpload upload = new ServletFileUpload(null);
			// 允许上传最大值
			upload.setFileSizeMax(Config.getInstance().getInt(Config.KEY_MAX_FILE_LIMIT, Config.getInstance().getInt("V_MAX_HEADIMG_FILE_LIMIT")) * 1024 * 1024);

			FileItemIterator items = upload.getItemIterator(request);
			String contentEncrypt = request.getHeader("Content-encrypt");
			String key = null;
			String value = null;
			String impcontent =null;

			String fileName = null;
			while (items.hasNext()) {
				FileItemStream itemStream = items.next();
				if (itemStream.isFormField()) {
					is = itemStream.openStream();
					key = itemStream.getFieldName();
					value = IOUtils.toString(is, "UTF-8");
					postParamsMap.put(key, value);
				} else {
					is = itemStream.openStream();
					fileName = itemStream.getName();
					postParamsMap.put("file", fileName);
					mmsFile = Tools.InputStreamCopy(is);
					postParamsMap.put("size", String.valueOf(mmsFile.size()));

					InputStream inStream =new ByteArrayInputStream(mmsFile.toByteArray());
					LineNumberReader bre = new LineNumberReader(new InputStreamReader(inStream));
					String str = null;
					while((str = bre.readLine())!=null){
						if(str.contains("Exception")){
							impcontent = str;
							break;
						}

					}
					bre.close();
				}
				is.close();
			}

			String fileExtName = "";
			if (fileName.contains(".")) {
				fileExtName = fileName.substring(fileName.lastIndexOf(".") + 1);
			}
			if(!fileExtName .equals("log") && !fileExtName .equals("crash")){
				String ret = ApiErrorCode.echoErr(ApiErrorCode.NOT_ALLOW_FILE);
				m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.NOT_ALLOW_FILE));
				response.getWriter().write(ret);
				return;
			}

			postParamsMap.put("appversion",request.getParameter("appversion"));
			postParamsMap.put("exceptionname",request.getParameter("exceptionname"));
			postParamsMap.put("osversion",request.getParameter("osversion"));
			postParamsMap.put("devicename",request.getParameter("devicename"));
			check = new CheckParameters(postParamsMap);
			m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
			String fNamefNameTxt=check.opt("file").toString();
			String fName=fNamefNameTxt.replace("log", "txt");
//			fName=fName.replace("crash", "txt");

			String  ostype="";//客户端类型
			check.addParameter("ostype", CheckParameters.paraType.INT, 1, 2);
			ostype = check.get("ostype").toString();
			String  appversion="";//App版本
			if(!StringUtils.isEmpty(check.get("appversion").toString())){
				check.addParameter("appversion", CheckParameters.paraType.STRING, 1,6);
				appversion = check.get("appversion").toString();
			}
			String  exceptionname="";//异常名称
			if(!StringUtils.isEmpty(check.get("exceptionname").toString())){
				check.addParameter("exceptionname", CheckParameters.paraType.STRING, 1,50);
				exceptionname = check.get("exceptionname").toString();
			}
			String  osversion="";//系统版本
			if(!StringUtils.isEmpty(check.get("osversion").toString())){
				check.addParameter("osversion", CheckParameters.paraType.STRING, 1,6);
				osversion = check.get("osversion").toString();
			}
			String  devicename="";//设备名称
			if(!StringUtils.isEmpty(check.get("devicename").toString())){
				check.addParameter("devicename", CheckParameters.paraType.STRING, 1,20);
				devicename = check.get("devicename").toString();
			}
			Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
			String productId = selfInfo.get("loginpid");


			// 上传文件到fastdfs
			String[] fileInfo = FastDfsUtil.uploadFileByStream(mmsFile, fName, Long.parseLong(check.opt("size").toString()), "log");
			if (fileInfo == null) {
				String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
				m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SYSTEM_ERROR));
				response.getWriter().write(ret);
				return;
			} else {				
				//fileUrl是URL地址
				//date就是上传文件时间
				//path是上传设备类型	
				String fileUrl = fileInfo[0];
				HashMap<String, String> condition = new HashMap<>();
				condition.put("abstract", impcontent);
				condition.put("filename", fNamefNameTxt);
				condition.put("fileurl", fileUrl);
				condition.put("ostype",ostype);
				condition.put("appversion",appversion);
				condition.put("exceptionname",exceptionname);
				condition.put("osversion",osversion);
				condition.put("devicename",devicename);
				condition.put("productid",productId);

				if(ClientLogDao.addClientLog(condition)){
					String ret = ApiErrorCode.echoOk();
					m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
					response.getWriter().write(ret);
				}else {
					m_logger.info(String.format("FAILED params=%s","addAppErr"));
					response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
				}
			}
		
		}catch (IOException e) {
			if(is != null){
				is.close();
			}
			if(mmsFile != null){
				mmsFile.close();
			}
			m_logger.error(" UploadClientLog error",e);
			m_logger.info(String.format("OUTPUT  ret_code=%s ", ApiErrorCode.FILE_TOO_LARGE));
			String ret = ApiErrorCode.echoErr(ApiErrorCode.FILE_TOO_LARGE);
			response.getWriter().write(ret);
			return;
		} catch (FileUploadException e) {
			if(is != null){
				is.close();
			}
			if(mmsFile != null){
				mmsFile.close();
			}
			m_logger.error(" UploadClientLog error",e);
			m_logger.info(String.format("OUTPUT  ret_code=%s ", ApiErrorCode.FILE_NOT_EXSIT));
			String ret = ApiErrorCode.echoErr(ApiErrorCode.FILE_NOT_EXSIT);
			response.getWriter().write(ret);
			return;
		}catch (CheckParameterException e) {
			if(is != null){
				is.close();
			}
			if(mmsFile != null){
				mmsFile.close();
			}
			String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
			m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
			response.getWriter().write(ret);
			return;
		} catch (Exception e) {
			if(is != null){
				is.close();
			}
			if(mmsFile != null){
				mmsFile.close();
			}
			String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
			m_logger.error(" UploadClientLog error",e);
			m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SYSTEM_ERROR));
			response.getWriter().write(ret);
			return;
		}


	}


}
