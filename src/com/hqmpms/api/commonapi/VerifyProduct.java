package com.hqmpms.api.commonapi;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.system.ProductDao;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * 校验产品
 * input parameter:
 * productname(选填,string):产品名称
 * product_id(选填,string):产品ID
 * procode(选填,string):产品编码
 */

@WebServlet("/commonapi/product/verify.do")
public class VerifyProduct extends HttpServlet {
	private static final Logger m_logger = Logger.getLogger(VerifyProduct.class);
	protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		try {
			CheckParameters check = new CheckParameters(request.getParameterMap());
			m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
			if(!StringUtils.isEmpty(check.get("product_id").toString())){
				check.addParameter("product_id", CheckParameters.paraType.STRING,1,20);
			}
			if(!StringUtils.isEmpty(check.get("productname").toString())){
				check.addParameter("productname", CheckParameters.paraType.STRING,1,50);
			}
			if(!StringUtils.isEmpty(check.get("procode").toString())){
				check.addParameter("procode", CheckParameters.paraType.STRING,1,50);
			}

			//判断产品是否存在
			if(!StringUtils.isEmpty(check.get("product_id").toString())){
				Map<String, String> isproduct = ProductDao.getProductById(check.get("product_id").toString());
				String  isproduct_name = isproduct.get("product_name");
				String  isproduct_code = isproduct.get("product_code");
				boolean isexitproduct_name = StringUtils.isEmpty(isproduct_name);
				boolean isexitproduct_code = StringUtils.isEmpty(isproduct_code);

				String  productname = check.get("productname").toString();
				String  procode = check.get("procode").toString();
				String  product_id = check.get("product_id").toString();
				boolean ifexitproduct_name = StringUtils.isEmpty(productname);
				boolean ifexitproduct_code = StringUtils.isEmpty(procode);
				//此处判断遗留问题，修改时产品编码为null,
				if(StringUtils.isEmpty(isproduct_code)){
					if(!ifexitproduct_name && ifexitproduct_code){
						if(!isproduct_name.equals(productname)){
							if(ProductDao.isProductExistByid(productname,"",product_id)){
								String ret = ApiErrorCode.echoErr(ApiErrorCode.PRODUCT_IS_EXIST);
								m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.PRODUCT_IS_EXIST));
								response.getWriter().write(ret);
								return;
							}
						}
					}else if(ifexitproduct_name && !ifexitproduct_code){
						if(ProductDao.isProductExistByid("",procode,product_id)){
							String ret = ApiErrorCode.echoErr(ApiErrorCode.PRODUCT_IS_EXIST);
							m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.PRODUCT_IS_EXIST));
							response.getWriter().write(ret);
							return;
						}
					}else if(!ifexitproduct_name && !ifexitproduct_code){
						if(!isproduct_name.equals(productname)){
							if(ProductDao.isProductExistByid(productname,procode,product_id)){
								String ret = ApiErrorCode.echoErr(ApiErrorCode.PRODUCT_IS_EXIST);
								m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.PRODUCT_IS_EXIST));
								response.getWriter().write(ret);
								return;
							}
						}else{
							if(ProductDao.isProductExistByid(productname,procode,product_id)){
								String ret = ApiErrorCode.echoErr(ApiErrorCode.PRODUCT_IS_EXIST);
								m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.PRODUCT_IS_EXIST));
								response.getWriter().write(ret);
								return;
							}

						}
					}

				}else {
                    //此处验证修改时产品与编码不为null
					if (!ifexitproduct_name && ifexitproduct_code) {
						if (!isproduct_name.equals(productname)) {
							if (ProductDao.isProductExistByid(productname, procode, product_id)) {
								String ret = ApiErrorCode.echoErr(ApiErrorCode.PRODUCT_IS_EXIST);
								m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.PRODUCT_IS_EXIST));
								response.getWriter().write(ret);
								return;
							}
						}
					} else if (ifexitproduct_name && !ifexitproduct_code) {
						if (!isproduct_code.equals(procode)) {
							if (ProductDao.isProductExistByid(productname, procode, product_id)) {
								String ret = ApiErrorCode.echoErr(ApiErrorCode.PRODUCT_IS_EXIST);
								m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.PRODUCT_IS_EXIST));
								response.getWriter().write(ret);
								return;
							}
						}
					} else if (!ifexitproduct_name && !ifexitproduct_code) {
						if (!isproduct_name.equals(productname) && isproduct_code.equals(procode)) {
							if (ProductDao.isProductExistByid(productname, "", product_id)) {
								String ret = ApiErrorCode.echoErr(ApiErrorCode.PRODUCT_IS_EXIST);
								m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.PRODUCT_IS_EXIST));
								response.getWriter().write(ret);
								return;
							}
						} else if (isproduct_name.equals(productname) && !isproduct_code.equals(procode)) {
							if (ProductDao.isProductExistByid("", procode, product_id)) {
								String ret = ApiErrorCode.echoErr(ApiErrorCode.PRODUCT_IS_EXIST);
								m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.PRODUCT_IS_EXIST));
								response.getWriter().write(ret);
								return;
							}
						} else if (!isproduct_name.equals(productname) && !isproduct_code.equals(procode)) {
							if (ProductDao.isProductExistByid(productname, procode, product_id)) {
								String ret = ApiErrorCode.echoErr(ApiErrorCode.PRODUCT_IS_EXIST);
								m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.PRODUCT_IS_EXIST));
								response.getWriter().write(ret);
								return;
							}
						}
					}
				}
			}else{
				if(ProductDao.isProductExist(check.get("productname").toString(),check.get("procode").toString(),check.get("product_id").toString())){
					String ret = ApiErrorCode.echoErr(ApiErrorCode.PRODUCT_IS_EXIST);
					m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.PRODUCT_IS_EXIST));
					response.getWriter().write(ret);
					return;
				}
			}
			String ret = ApiErrorCode.echoOk();
			m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
			response.getWriter().write(ret);
			return;
		} catch (CheckParameterException ce) {
			String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
			m_logger.info(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.PARAMETER_ERROR, ce));
			response.getWriter().write(ret);
			return;
		} catch (Exception e) {
			String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
			m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SYSTEM_ERROR, e));
			response.getWriter().write(ret);
			return;
		}

	}

}
