package com.hqmpms.api.commonapi;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.subapp.SubappDao;
import com.hqmpms.dispatcher.Config;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

/**
 * 校验子应用版本号
 * input parameter:
 * openappid(必填,string):应用ID
 * ostype(必填,string):应用类型
 */
@WebServlet("/commonapi/subapp/verifySubappVersion.do")
public class VerifySubappVersion extends HttpServlet {
	private static final Logger m_logger = Logger.getLogger(VerifySubappVersion.class);
	protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		try {
			CheckParameters check = new CheckParameters(request.getParameterMap());
			m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
			check.addParameter("openappid", CheckParameters.paraType.EXP, Config.getInstance().getString("params_uuid"));
			check.addParameter("ostype", CheckParameters.paraType.INT, 1, 2);
			response.setContentType("text/html;charset=UTF-8");  
			String openappid = check.get("openappid").toString();
			String ostype = check.get("ostype").toString();
			//判断数据是否存在
			HashMap<String, String> list = 	SubappDao.inquireSubAppVersion(openappid,ostype);
			if(list.size()>0) {
			    String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
	            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
	            response.getWriter().write(ret);
	            return;
			} else {
			    String ret = ApiErrorCode.echoOk();
                m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
                response.getWriter().write(ret);
                return;
			}
		} catch (CheckParameterException ce) {
			String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
			m_logger.info(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.PARAMETER_ERROR, ce));
			response.getWriter().write(ret);
			return;
		} catch (Exception e) {
			String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
			m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SYSTEM_ERROR, e));
			response.getWriter().write(ret);
			return;
		}

	}

}
