package com.hqmpms.api.commonapi;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.api.utils.Tools;
import com.hqmpms.utils.FastDfsUtil;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

/**
 * 上传app功能模块图标
 * Created by dml on  2018/7/25
 */
@WebServlet("/commonapi/uploadAppModuleIcon.do")
public class uploadAppModuleIcon extends HttpServlet {

	private static final long serialVersionUID = -1935476189897248154L;
	private static final Logger LOGGER = Logger.getLogger(uploadAppModuleIcon.class);

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 参数校验
		CheckParameters check;
		ByteArrayOutputStream mmsFile = null;// 全局保存数据流
		HashMap<String, String> postParamsMap = new HashMap<>();
		try {
			ServletFileUpload upload = new ServletFileUpload(null);
			FileItemIterator items;
			items = upload.getItemIterator(request);
			String key;
			String value;
			InputStream is;
			while (items.hasNext()) {
				FileItemStream itemStream = items.next();
				if (itemStream.isFormField()) {
					is = itemStream.openStream();
					key = itemStream.getFieldName();
					value = IOUtils.toString(is, "UTF-8");
					postParamsMap.put(key, value);
				} else {
					is = itemStream.openStream();
					String fileName = itemStream.getName();
					String imgType = "image,png,tif,jpg,jpeg,bmp";
					String extension = "";
					int i = fileName.lastIndexOf('.');
					if (i > 0) {
						extension = fileName.substring(i+1);
					}
					if(imgType.indexOf(extension)==-1){
						LOGGER.info(String.format("OUTPUT  ret_code=%s", ApiErrorCode.NOT_ALLOW_FILE));
						String ret = ApiErrorCode.echoErr(ApiErrorCode.NOT_ALLOW_FILE);
						response.getWriter().write(ret);
						return;
					}
					postParamsMap.put("file", fileName);
					mmsFile = Tools.InputStreamCopy(is);
					if(mmsFile.size() > 2 * 1024 * 1024){
						LOGGER.info(String.format("OUTPUT  ret_code=%s %s", ApiErrorCode.FILE_TOO_LARGE,mmsFile.size()));
						String ret = ApiErrorCode.echoErr(ApiErrorCode.FILE_TOO_LARGE);
						response.getWriter().write(ret);
						return;
					}
					postParamsMap.put("size", String.valueOf(mmsFile.size()));
				}
				is.close();
			}
			
			if(mmsFile==null || mmsFile.size() == 0){
				String ret = ApiErrorCode.echoErr(ApiErrorCode.FILE_NOT_EXSIT);
				LOGGER.info(String.format("OUTPUT mmsFile is null =%s ", ApiErrorCode.FILE_NOT_EXSIT));
				response.getWriter().write(ret);
				return;
			}
			check = new CheckParameters(postParamsMap);
			LOGGER.info(String.format("INPUT params=%s", check.getParameterJSON()));
			// 上传文件到fastdfs
			String[] fileInfo = FastDfsUtil.uploadFileByStream(mmsFile, check.opt("file").toString(), Long.parseLong(check.opt("size").toString()), "img");
			if (fileInfo == null) {
				String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
				LOGGER.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SYSTEM_ERROR));
				response.getWriter().write(ret);
			} else {
				JSONObject obj = new JSONObject();
				obj.put("filePath", fileInfo[0]);
				response.getWriter().write(ApiErrorCode.echoClassOk(obj));
				LOGGER.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SUCCESS));
			}
		}catch (IOException e) {
			LOGGER.error("uploadAdvertImgError",e);
			String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
			response.getWriter().write(ret);
		}
		catch (FileUploadException e) {
			LOGGER.error("uploadAdvertImgError",e);
			String ret = ApiErrorCode.echoErr(ApiErrorCode.FILE_NOT_EXSIT);
			response.getWriter().write(ret);
		}catch (Exception e) {
			String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
			LOGGER.error("uploadAdvertImgError",e);
			response.getWriter().write(ret);
		}


	}


}
