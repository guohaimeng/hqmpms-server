package com.hqmpms.api.internaltestuser;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.internaltestuser.InternalTestUserDao;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 根据用户名精确查询用户(需要根据产品编号配置不同的查询Dao)
 * Created by dml on  2018/5/23
 */
@WebServlet("/internalTestUser/getByUserName.do")
public class GetUserByUserName extends HttpServlet{
    private static final Logger LOGGER = Logger.getLogger(GetUserByUserName.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            LOGGER.info(String.format("INPUT params=%s", check.getParameterJSON()));
            // session校验
            @SuppressWarnings("unchecked")
            Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
            String productCode = selfInfo.get("loginpcode");
            check.addParameter("userName", CheckParameters.paraType.STRING, 1, 50);
            List<HashMap<String, String>> list = new ArrayList<>();
            //目前只查询优E学堂
            if ("yext".equals(productCode)){
                list = InternalTestUserDao.getUserListsByName(check.opt("userName").toString());
            }
            HashMap<String, Object> resultMap = new HashMap<>();
            resultMap.put("rows", list);
            resultMap.put("total", list.size());
            JSONObject obj = JSONObject.fromObject(resultMap);
            response.getWriter().write(obj.toString());
            LOGGER.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SUCCESS));
            response.getWriter().close();
        }catch (Exception e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            LOGGER.error(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.SYSTEM_ERROR, e));
            try {
                response.getWriter().write(ret);
            }catch (Exception ex){
                LOGGER.error(ex);
            }
        }
    }
}
