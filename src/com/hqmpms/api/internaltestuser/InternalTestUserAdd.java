package com.hqmpms.api.internaltestuser;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.internaltestuser.InternalTestUserDao;
import com.hqmpms.dao.system.OperationLogDao;
import com.hqmpms.dispatcher.Startup;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * 内测用户新增
 * Created by dml on  2018/5/23
 */
@WebServlet("/internalTestUser/add.do")
public class InternalTestUserAdd extends HttpServlet{
    private static final Logger LOGGER = Logger.getLogger(InternalTestUserAdd.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //参数校验
        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            LOGGER.info(String.format("INPUT params=%s", check.getParameterJSON()));
            check.addParameter("userId", CheckParameters.paraType.STRING, 1,36);
            // session校验
            @SuppressWarnings("unchecked")
            Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
            String productId = selfInfo.get("loginpid");
            long id = Startup.getId();
            int result = InternalTestUserDao.internalTestUserAdd(
                                                        id,
                                                        check.get("userId").toString(),
                                                        productId);
            String ret;
            if(result>0){
                OperationLogDao.operationLogRecord(selfInfo.get("mid"),"内测用户新增",String.valueOf(id));
                LOGGER.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
                ret = ApiErrorCode.echoOk();
                response.getWriter().write(ret);
            }else if(result==-2){
                ret = ApiErrorCode.echoErr(ApiErrorCode.ACC_EXIST);
                LOGGER.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.ACC_EXIST));
                response.getWriter().write(ret);
            }else {
                ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
                LOGGER.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SYSTEM_ERROR));
                response.getWriter().write(ret);
            }
            response.getWriter().close();
        } catch (CheckParameterException ce) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            LOGGER.info(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.PARAMETER_ERROR, ce));
            try {
                response.getWriter().write(ret);
            }catch (Exception e){
                LOGGER.error(e);
            }
        } catch (Exception e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            LOGGER.info(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.SYSTEM_ERROR, e));
            try {
                response.getWriter().write(ret);
            }catch (Exception ex){
                LOGGER.error(ex);
            }
        }
    }
}
