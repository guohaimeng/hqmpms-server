package com.hqmpms.api.mail;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.mail.MailDao;
import com.hqmpms.dispatcher.Config;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 */
@WebServlet( "/mail/sendResList.do")
public class SendResList extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(SendResList.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    CheckParameters check = new CheckParameters(request.getParameterMap());
    m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
    try {
        String sendmobile="";
        if(!StringUtils.isEmpty(request.getParameter("sendmobile"))){
            check.addParameter("sendmobile", CheckParameters.paraType.EXP, Config.getInstance().getString("params_mobile"));
            sendmobile=check.get("sendmobile").toString();
        }
        String sendmail="";
        if(!StringUtils.isEmpty(request.getParameter("sendmail"))){
            check.addParameter("sendmail", CheckParameters.paraType.EXP,".+@.+\\.[a-z]{1,3}");
            sendmail=check.get("sendmail").toString();
        }
        String receivemail="";
        if(!StringUtils.isEmpty(request.getParameter("receivemail"))){
            check.addParameter("receivemail", CheckParameters.paraType.EXP,".+@.+\\.[a-z]{1,3}");
            receivemail=check.get("receivemail").toString();
        }
        String startTime="";
        String endTime="";
        if(!StringUtils.isEmpty(request.getParameter("startTime"))){
            check.addParameter("startTime", CheckParameters.paraType.STRING, 1, 100);
            startTime= request.getParameter("startTime");
        }

        if(!StringUtils.isEmpty(request.getParameter("endTime"))){
            check.addParameter("endTime", CheckParameters.paraType.STRING, 1, 100);
            endTime= request.getParameter("endTime");
        }
        String rows;
        String page;
        if(!StringUtils.isEmpty(request.getParameter("rows"))){
            check.addParameter("rows", CheckParameters.paraType.EXP, Config.getInstance().getString("params_page"));
            rows = check.get("rows").toString();
        }else{
            rows = "10";
        }
        if(!StringUtils.isEmpty(request.getParameter("page")) && Integer.parseInt(request.getParameter("page"))>0){
            check.addParameter("page", CheckParameters.paraType.EXP, Config.getInstance().getString("params_page"));
            page = check.get("page").toString();
        }else{
            page = "1";
        }

        HashMap<String,Object> productList = MailDao.getMailList(sendmobile,Integer.parseInt(page),Integer.parseInt(rows),sendmail,receivemail,startTime,endTime);
        HashMap<String ,Object> res = new HashMap<>();
        res.put("total",productList.get("count"));
        res.put("rows",new JSONArray((ArrayList<HashMap<String,String>>)productList.get("rows")));
        String ret = ApiErrorCode.echoOkMap(res);
        m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
        response.getWriter().write(ret);
        return;
    } catch (CheckParameterException e) {
        String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
        m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
        response.getWriter().write(ret);
        return;
    } catch (Exception e) {
        m_logger.error(String.format("FAILED params=%s %s","system exception=",e),e);
        response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
    }
}
}
