package com.hqmpms.api.message;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.message.MessageDao;
import org.apache.log4j.Logger;
import org.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 获取消息列表
 * Created by dml on  2018/5/25
 */
@WebServlet("/message/getListByAllConditions.do")
public class GetMessageListByAllConditions extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(GetMessageListByAllConditions.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            LOGGER.info(String.format("INPUT params=%s", check.getParameterJSON()));
            // session校验
            @SuppressWarnings("unchecked")
            Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
            String productId = selfInfo.get("loginpid");
            String sPage = request.getParameter("page");
            Integer page = Integer.parseInt(sPage == null ? "1" : sPage);
            String sPageSize = request.getParameter("rows");
            Integer pageSize = Integer.parseInt(sPageSize == null ? "10": sPageSize);
            List<HashMap<String, String>> list = MessageDao.getMessageListByAllConditions(
                    productId,
                    check.opt("messageType")==null?"":check.opt("messageType").toString(),
                    check.opt("startDate")==null?"":check.opt("startDate").toString(),
                    check.opt("endDate")==null?"":check.opt("endDate").toString(),
                    page,pageSize
                    );
            long count =  MessageDao.getMessageListByAllConditions(
                    productId,
                    check.opt("messageType")==null?"":check.opt("messageType").toString(),
                    check.opt("startDate")==null?"":check.opt("startDate").toString(),
                    check.opt("endDate")==null?"":check.opt("endDate").toString(),
                    null,null
            ).size();
            HashMap<String, Object> resultMap = new HashMap<>();
            resultMap.put("total", count);
            resultMap.put("rows", new JSONArray(list));
            String ret = ApiErrorCode.echoOkMap(resultMap);
            response.getWriter().write(ret);
            LOGGER.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SUCCESS));
            response.getWriter().close();
        }catch (Exception e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            LOGGER.error(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.SYSTEM_ERROR, e));
            try {
                response.getWriter().write(ret);
            }catch (Exception ex){
                LOGGER.error(ex);
            }
        }
    }
}
