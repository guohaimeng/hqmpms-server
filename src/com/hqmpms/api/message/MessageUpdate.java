package com.hqmpms.api.message;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.api.utils.Tools;
import com.hqmpms.dao.message.MessageDao;
import com.hqmpms.dao.screeningconditions.ScreeningConditionsDao;
import com.hqmpms.dao.system.OperationLogDao;
import com.hqmpms.dispatcher.Startup;
import com.hqmpms.utils.FastDfsUtil;
import com.hqmpms.utils.YsServerSDK;
import net.sf.json.JSONObject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * 消息编辑
 * Created by dml on  2018/6/1
 */
@WebServlet("/message/update.do")
public class MessageUpdate extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(MessageUpdate.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // 参数校验
        CheckParameters check;
        try {
            check = new CheckParameters(request.getParameterMap());
            LOGGER.info(String.format("INPUT params=%s", check.getParameterJSON()));
            check.addParameter("messageType", CheckParameters.paraType.INT, 0, 1);
            check.addParameter("title", CheckParameters.paraType.STRING, 1,200);
            check.addParameter("status", CheckParameters.paraType.INT, -1,1);
            check.addParameter("conditionRecord", CheckParameters.paraType.STRING, 1, 5000);
            // session校验
            @SuppressWarnings("unchecked")
            Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
            HashMap<String,Object> params = new HashMap<>();
            if("0".equals(check.opt("messageType").toString())){
                params.put("msgtype", ApiErrorCode.SYS_MSG);
                params.put("content",request.getParameter("content"));
            }
            String messageUrl = "";
            if("1".equals(check.opt("messageType").toString())){
                params.put("msgtype", ApiErrorCode.SYS_MEDIA_MSG);
              String  content = "<html> <head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /></head> <body>" +request.getParameter("messageUrl") + "</body> </html>";
                String [] addr =  FastDfsUtil.uploadFileByString(content,"abcd.html");
                if(null!=addr){
                    messageUrl = addr[0];
                    LOGGER.info(addr[0]);
                    params.put("link",addr[0]);
                }
            }
            params.put("title",check.opt("title").toString());
            String msg = ApiErrorCode.echoClassMsg(params);
            List<HashMap<String, String>> list =new ArrayList<>();
            String conditionRecord = check.opt("conditionRecord").toString();
            JSONObject record = JSONObject.fromObject(conditionRecord);
            List<String> resultList = new ArrayList<>();
            if("内测用户".equals(record.getString("sendType"))){
                if(Integer.valueOf(record.getString("userCounts"))>0){
                    String[] rArray = record.getString("userIds").split(",");
                    resultList = Arrays.asList(rArray);
                }
            }else {
                String[] records = check.opt("conditionRecord").toString().split("\\|");
                @SuppressWarnings("unchecked")
                List<String> recordList = new ArrayList<>(new HashSet(Arrays.asList(records)));
                if(!CollectionUtils.isEmpty(recordList)){
                    for (String r:recordList) {
                        JSONObject re = JSONObject.fromObject(r);
                        list.addAll(ScreeningConditionsDao.getUserListByAllConditions(
                                re.getString("provinceId"),
                                re.getString("cityId"),
                                re.getString("areaId"),
                                re.getString("schoolId"),
                                re.getString("classId"),
                                re.getString("identityId"),null));
                    }
                }
                if(!StringUtils.isEmpty(request.getParameter("conditionSendRecord"))){
                    String[] sendRecords =request.getParameter("conditionSendRecord").split("\\|");
                    @SuppressWarnings("unchecked")
                    List<String> sendList = new ArrayList<>(new HashSet(Arrays.asList(sendRecords)));
                    if(!CollectionUtils.isEmpty(sendList)){
                        for (String r:sendList) {
                            MessageDao.addMessageRecord(String.valueOf(Startup.getId()),selfInfo.get("mid"),r);
                        }
                    }
                }
                if(!CollectionUtils.isEmpty(list)){
                    for (HashMap h:list) {
                        resultList.add(h.get("UserID").toString());
                    }
                }
            }
            //去除重复用户编号
            resultList = new ArrayList<>(new HashSet<>(resultList));
            int status = (int)check.opt("status");
            if(CollectionUtils.isEmpty(resultList)&&1==status){
                String ret = ApiErrorCode.echoErr(ApiErrorCode.NOT_FOUND_SEND_PEOPLES);
                LOGGER.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.NOT_FOUND_SEND_PEOPLES));
                response.getWriter().write(ret);
                return;
            }
            //草稿状态不发送
            if (!CollectionUtils.isEmpty(resultList)&&"1".equals(check.opt("status").toString())) {
                YsServerSDK client = Tools.geYsServerAPI();
                List<String> tempList;
                if (!CollectionUtils.isEmpty(resultList)) {
                    int yu = resultList.size() / 50;
                    for (int i = 0; i < yu; i++) {
                        tempList =  resultList.subList(i * 50, (i+1) * 50);
                        String uids = StringUtils.join(tempList, ",");
                        client.SendCustomMsgAsynchronousWithMsgsum("100000000000000000", uids, msg,check.opt("title").toString());
                    }
                    tempList =  resultList.subList(yu * 50, resultList.size());
                    String uids = StringUtils.join(tempList, ",");
                    client.SendCustomMsgAsynchronousWithMsgsum("100000000000000000", uids, msg,check.opt("title").toString());
                    status =1;
                }
            }
            long id = Startup.getId();
            conditionRecord = "["+conditionRecord.replace("|",",")+"]";
            int result = MessageDao.messageUpdate(
                    request.getParameter("messageId"),
                    check.opt("messageType").toString(),
                    check.opt("title").toString(),
                    request.getParameter("content"),
                    String.valueOf(status),
                    messageUrl,
                    conditionRecord,
                    selfInfo.get("mid"),
                    selfInfo.get("loginpid"));
            if (result>0){
                OperationLogDao.operationLogRecord(selfInfo.get("mid"),"消息编辑",String.valueOf(id));
                HashMap<String, Object> fileMap = new HashMap<>();
                fileMap.put("sendCount",resultList.size());
                String ret = ApiErrorCode.echoOkMap(fileMap);
                LOGGER.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
                response.getWriter().write(ret);
            }else {
                String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
                LOGGER.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SYSTEM_ERROR));
                response.getWriter().write(ret);
            }
            response.getWriter().close();
        } catch (CheckParameterException ce) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            LOGGER.info(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.PARAMETER_ERROR,ce));
            response.getWriter().write(ret);
        }catch (Exception e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            LOGGER.info(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.SYSTEM_ERROR,e));
            response.getWriter().write(ret);
        }
    }
}
