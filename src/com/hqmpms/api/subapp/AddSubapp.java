package com.hqmpms.api.subapp;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.subapp.SubappDao;
import com.hqmpms.dao.system.DoOperationLogThread;
import com.hqmpms.dispatcher.Config;
import com.hqmpms.dispatcher.Startup;
import com.hqmpms.utils.DoCachedThreadPool;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 添加子应用
 * input parameter:
 * openappid(必填,string):应用ID
 * ostype(必填,string):应用类型
 */
@WebServlet("/subapp/addSubapp.do")
public class AddSubapp extends HttpServlet {
	private static final Logger m_logger = Logger.getLogger(AddSubapp.class);
	protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		try {
			CheckParameters check = new CheckParameters(request.getParameterMap());
			m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
			check.addParameter("openappid", CheckParameters.paraType.EXP, Config.getInstance().getString("params_uuid"));
			check.addParameter("ostype", CheckParameters.paraType.INT, 1, 2);
			//判断添加数据是否存在
			String openappid = check.get("openappid").toString();
			String ostype = check.get("ostype").toString();
			// 获取session
			Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
			HashMap<String, String> list = 	SubappDao.inquireSubAppVersion(openappid,ostype);
			if(list.size()>0) {
			    String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
	            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
	            response.getWriter().write(ret);
	            return;
			}

			HashMap<String, String> subappInfo = SubappDao.getMarketWebAppById(openappid);


			String subappid = String.valueOf(Startup.getId());
			HashMap<String, String> condition = new HashMap<String, String>();
			condition.put("subappid",subappid);
			condition.put("openappid",openappid);
			condition.put("ostype",ostype);
			if (SubappDao.addSubapp(condition)) {
			    response.getWriter().write(ApiErrorCode.echoOk());
			    m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SUCCESS));
				DoCachedThreadPool.getCachedThreadPool().cachedThreadPool.execute(new DoOperationLogThread(selfInfo.get("mid"),"发布应用'"+subappInfo.get("appName")+"'",subappid));
				return;
			} else {
				m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SYSTEM_ERROR));
				response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
				return;
			}
		} catch (CheckParameterException ce) {
			String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
			m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR, ce));
			response.getWriter().write(ret);
			return;
		} catch (Exception e) {
			m_logger.error(String.format("FAILED params=%s %s", "system exception=", e), e);
			response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
			return;
		}

	}

}
