package com.hqmpms.api.subapp;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.subapp.SubappDao;
import com.hqmpms.dao.system.DoOperationLogThread;
import com.hqmpms.dispatcher.Config;
import com.hqmpms.utils.DoCachedThreadPool;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 删除子应用
 * input parameter:
 * openappid(必填,string):应用ID
 */
@WebServlet("/subapp/deleteSubapp.do")
public class DeleteSubapp extends HttpServlet {
	private static final Logger m_logger = Logger.getLogger(DeleteSubapp.class);
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			CheckParameters check = new CheckParameters(request.getParameterMap());
			m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
			check.addParameter("openappid", CheckParameters.paraType.EXP, Config.getInstance().getString("params_uuid"));
			if (check.get("subappid").toString().length() <= 0) {
				throw new CheckParameterException("subappid", check.get("subappid").toString(),
						CheckParameterException.errFormat);
			}
			Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
			HashMap<String, String> subappInfo = SubappDao.getMarketWebAppById(check.get("openappid").toString());

			if(SubappDao.deleteSubapp(check.get("subappid").toString())){
				 response.getWriter().write(ApiErrorCode.echoOk());
				 m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SUCCESS));
				 DoCachedThreadPool.getCachedThreadPool().cachedThreadPool.execute(new DoOperationLogThread(selfInfo.get("mid"),"删除应用'"+subappInfo.get("appName")+"'",check.get("subappid").toString()));
			     return;
			} else {
			     m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SYSTEM_ERROR));
				 response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
				 return;
			}
		} catch (CheckParameterException ce) {
			String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
			m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR, ce));
			response.getWriter().write(ret);
			return;
		} catch (Exception e) {
			m_logger.error(String.format("FAILED params=%s %s", "system exception=", e), e);
			response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
			return;
		}
	
	
	}

}
