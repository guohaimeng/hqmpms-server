package com.hqmpms.api.subapp;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.subapp.SubappDao;
import com.hqmpms.dispatcher.Config;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * 获取子应用列表
 * input parameter:
 * ostype(必填,string):类型
 * rows(选填,string): 页数
 * page(选填,string): 每页行数
 */
@WebServlet("/subapp/getSubappList.do")
public class GetSubappList extends HttpServlet {
	private static final Logger m_logger = Logger.getLogger(GetSubappList.class);
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			CheckParameters check = new CheckParameters(request.getParameterMap());
			m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
			String rows = "";
			String page = "";
			String ostype = "";
            if(!StringUtils.isEmpty(request.getParameter("rows"))){
            	check.addParameter("rows", CheckParameters.paraType.EXP, Config.getInstance().getString("params_page"));
            	rows = check.get("rows").toString();
	        }else{
	        	rows = "10";
	        }
            if(!StringUtils.isEmpty(request.getParameter("page")) && Integer.parseInt(request.getParameter("page"))>0){
            	check.addParameter("page", CheckParameters.paraType.EXP, Config.getInstance().getString("params_page"));
            	page = check.get("page").toString();
	        }else{
	        	page = "1";
	        }
            if(!StringUtils.isEmpty(request.getParameter("ostype"))){
            	check.addParameter("ostype", CheckParameters.paraType.STRING,0,2);
            	ostype = check.get("ostype").toString();
	        }

			ArrayList<HashMap<String,String>> subapplist = SubappDao.getSubappList(page,rows,ostype);
			int count  = SubappDao.getSubappCount(ostype);
			HashMap<String ,Object> res = new HashMap<>();
            res.put("total",count);
            res.put("rows",new JSONArray(subapplist));
            String ret = ApiErrorCode.echoOkMap(res);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            response.getWriter().write(ret);
		}catch (CheckParameterException ce) {
			String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
			m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR, ce));
			response.getWriter().write(ret);
			return;
		} catch (Exception e) {
			m_logger.error(String.format("FAILED params=%s %s", "system exception=", e), e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
            return;
		}
	
	}

}
