package com.hqmpms.api.subapp;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.subapp.SubappDao;
import com.hqmpms.dao.system.DoOperationLogThread;
import com.hqmpms.dispatcher.Config;
import com.hqmpms.utils.DoCachedThreadPool;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 修改子应用
 * openappid(必填,string):应用ID
 * ostype(必填,string):应用类型
 * appstatus(必填,string):状态是否启用
 * islocal(必填,string):是否本地化
 */
@WebServlet("/subapp/updateSubapp.do")
public class UpdateSubapp extends HttpServlet {
	private static final Logger m_logger = Logger.getLogger(UpdateSubapp.class);
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			CheckParameters check = new CheckParameters(request.getParameterMap());
			m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
			check.addParameter("subappid", CheckParameters.paraType.EXP, Config.getInstance().getString("params_uuid"));
			check.addParameter("openappid", CheckParameters.paraType.EXP, Config.getInstance().getString("params_uuid"));
			String appstatus="";
			String islocal="";
			if(!StringUtils.isEmpty(check.get("appstatus").toString())){
				check.addParameter("appstatus", CheckParameters.paraType.INT, 0, 1);
				appstatus = check.get("appstatus").toString();
			}
			if(!StringUtils.isEmpty(check.get("islocal").toString())){
				check.addParameter("islocal", CheckParameters.paraType.STRING, 0,1);
				islocal = check.get("islocal").toString();
			}
			Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
			HashMap<String, String> subappInfo = SubappDao.getMarketWebAppById(check.get("openappid").toString());



			//判断修改数据是否存在
			HashMap<String, String> condition = new HashMap<String, String>();
			condition.put("subappid", check.get("subappid").toString());
			condition.put("appstatus",appstatus);
			condition.put("islocal",islocal);
			if(SubappDao.updateSubapp(condition)){
				 response.getWriter().write(ApiErrorCode.echoOk());
				 m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SUCCESS));
				DoCachedThreadPool.getCachedThreadPool().cachedThreadPool.execute(new DoOperationLogThread(selfInfo.get("mid"),"修改应用'"+subappInfo.get("appName")+"'",check.get("subappid").toString()));
			     return;
			} else {
			     m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SYSTEM_ERROR));
				 response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
				 return;
			}
		}catch (CheckParameterException ce) {
			String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
			m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR, ce));
			response.getWriter().write(ret);
			return;
		} catch (Exception e) {
			m_logger.error(String.format("FAILED params=%s %s", "system exception=", e), e);
			response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
			return;
		}
	
	}

}
