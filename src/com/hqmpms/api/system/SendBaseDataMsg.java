/**
 * 
 */
package com.hqmpms.api.system;

import com.hqmpms.api.utils.*;
import com.hqmpms.dao.system.DoOperationLogThread;
import com.hqmpms.dispatcher.Config;
import com.hqmpms.utils.DoCachedThreadPool;
import com.hqmpms.utils.YsServerSDK;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 *基础数据更新通知
 */
@WebServlet("/system/baseDataMsg/send.do")
public class SendBaseDataMsg extends HttpServlet {

	private static final long serialVersionUID = -8030229540888167965L;
	private static final Logger m_logger = Logger.getLogger(SendBaseDataMsg.class);

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {

			//参数校验
			CheckParameters check = new CheckParameters(request.getParameterMap());
			m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
			check.addParameter("ss", CheckParameters.paraType.EXP, Config.getInstance().getString("params_ss"));
			//type 数据类型  1地区数据 2年级班级学段数据  3科目数据, 4考勤基础数据
			check.addParameter("datatype", CheckParameters.paraType.INT, 1, 4);
            String datatype = check.get("datatype").toString();
			// session校验
			Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
			String datatypestr="";
			if(datatype.equals("1")){
				datatypestr="地区数据";
			}else if(datatype.equals("2")){
				datatypestr="年级班级学段数据";
			}else if(datatype.equals("3")){
				datatypestr="科目数据";
			}else if(datatype.equals("4")){
				datatypestr="考勤基础数据";
			}else{
			}
			ArrayList<String> result = DatabaseManager.getSdkUser();
			if (result.size() != 0) {
				HashMap obj = new HashMap<String,Object>();
				obj.put("type", ApiErrorCode.SEND_MSG_PPM_TYPE);
				obj.put("msgtype", ApiErrorCode.UPDATE_BASE_DATA_MSG);
				obj.put("src", "100000000000000000");
				obj.put("srcname","系统消息");
				obj.put("datatype",datatypestr);
				obj.put("time",System.currentTimeMillis()/1000);
				String msgStr = ApiErrorCode.echoClassMsg(obj);
				List<String> tempList;
				YsServerSDK client = Tools.geYsServerAPI();
				if (result.size() > 50) {
					int yu = result.size() / 50;
					for (int i = 0; i < yu; i++) {
						tempList =  result.subList(i * 50, (i+1) * 50);
						String uids = StringUtils.join(tempList, ",");
						client.SendMessageAsynchronousSilence("100000000000000000", uids, msgStr);
					}

					tempList =  result.subList(yu * 50, result.size());
					String uids = StringUtils.join(tempList, ",");
					client.SendMessageAsynchronousSilence("100000000000000000", uids, msgStr);
				} else {
					String uids = StringUtils.join(result, ",");
					client.SendMessageAsynchronousSilence("100000000000000000", uids, msgStr);
				}
				m_logger.info(String.format("OUTPUT ret_code=%s","SEND BASEDATA SUCCESS"));
			}
			String ret = ApiErrorCode.echoOk();
			m_logger.info(String.format("OUTPUT ret_code=%s",ret));
			response.getWriter().write(ret);
			DoCachedThreadPool.getCachedThreadPool().cachedThreadPool.execute(new DoOperationLogThread(selfInfo.get("mid"),"发送'"+datatypestr+"'"+"更新通知",""));
			return;
		} catch (CheckParameterException ce) {
			String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
			m_logger.info(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.PARAMETER_ERROR, ce));
			response.getWriter().write(ret);
			return;
		} catch (Exception e) {
			String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
			m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SYSTEM_ERROR, e));
			response.getWriter().write(ret);
			return;
		}
	}
}
