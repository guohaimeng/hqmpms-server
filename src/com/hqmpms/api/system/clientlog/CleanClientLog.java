package com.hqmpms.api.system.clientlog;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.system.ClientLogDao;
import com.hqmpms.dao.system.DoOperationLogThread;
import com.hqmpms.utils.DoCachedThreadPool;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * Created by Administrator on 2018/6/6.
 * input parameter:
 * timenode(必填,string): 清除时间节点
 */
@WebServlet("/system/clientlog/clean.do")
public class CleanClientLog extends HttpServlet {
    private static final Logger m_Logger = Logger.getLogger(CleanClientLog.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            m_Logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
            String timenode="";
            check.addParameter("timenode", CheckParameters.paraType.STRING,19,24);
            timenode = check.get("timenode").toString();

            Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
            String productId = selfInfo.get("loginpid");

            int result = ClientLogDao.delClientLogLists(productId,timenode);

            if(result >= 0){
                String res = ApiErrorCode.echoOk();
                response.getWriter().write(res);
                DoCachedThreadPool.getCachedThreadPool().cachedThreadPool.execute(new DoOperationLogThread(selfInfo.get("mid"),"清除'"+timenode+"'"+"之前的日志",""));
            }else{
                String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
                m_Logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SYSTEM_ERROR));
                response.getWriter().write(ret);
                return;
            }
        } catch (CheckParameterException ce) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_Logger.info(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.PARAMETER_ERROR, ce));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            m_Logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SYSTEM_ERROR, e));
            response.getWriter().write(ret);
            return;
        }
    }



}


