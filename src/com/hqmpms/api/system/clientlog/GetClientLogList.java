package com.hqmpms.api.system.clientlog;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.system.ClientLogDao;
import com.hqmpms.dispatcher.Config;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2018/6/6.
 * input parameter:
 * ostype(选填,string): 类型
 * fileabstract(选填,string): 摘要
 * startdate(选填,string): 开始时间
 * enddate(选填,string): 结束时间
 * rows(选填,string): 页数
 * page(选填,string): 每页行数
 */
@WebServlet("/system/clientLog/list.do")
public class GetClientLogList extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(GetClientLogList.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
            String ostype="";
            if(!StringUtils.isEmpty(request.getParameter("ostype"))){
                check.addParameter("ostype", CheckParameters.paraType.EXP,"^[1,2]$");
                ostype=check.get("ostype").toString();
            }
            String fileabstract="";
            if(!StringUtils.isEmpty(request.getParameter("fileabstract"))){
                check.addParameter("fileabstract", CheckParameters.paraType.STRING,1,100);
                fileabstract=check.get("fileabstract").toString();
            }
            String startdate="";
            if(!StringUtils.isEmpty(request.getParameter("startdate"))){
                check.addParameter("startdate", CheckParameters.paraType.STRING,19,24);
                startdate=check.get("startdate").toString();
            }
            String enddate="";
            if(!StringUtils.isEmpty(request.getParameter("enddate"))){
                check.addParameter("enddate", CheckParameters.paraType.EXP,19,24);
                enddate=check.get("enddate").toString();
            }
            Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
            String productId = selfInfo.get("loginpid");
            String rows;
            String page;
            if(!StringUtils.isEmpty(request.getParameter("rows"))){
                check.addParameter("rows", CheckParameters.paraType.EXP, Config.getInstance().getString("params_page"));
                rows = check.get("rows").toString();
            }else{
                rows = "10";
            }
            if(!StringUtils.isEmpty(request.getParameter("page")) && Integer.parseInt(request.getParameter("page"))>0){
                check.addParameter("page", CheckParameters.paraType.EXP, Config.getInstance().getString("params_page"));
                page = check.get("page").toString();
            }else{
                page = "1";
            }
            HashMap<String,Object>  clientLogList = ClientLogDao.getClientLogList(productId,Integer.parseInt(page),Integer.parseInt(rows),ostype,startdate,enddate,fileabstract);

            HashMap<String ,Object> res = new HashMap<>();
            res.put("total",clientLogList.get("count"));
            res.put("rows",new JSONArray((ArrayList<HashMap<String,String>>)clientLogList.get("rows")));
            String ret = ApiErrorCode.echoOkMap(res);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            response.getWriter().write(ret);
            return;
        } catch (CheckParameterException e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s","system exception=",e),e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
        }
    }

}
