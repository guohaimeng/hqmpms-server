package com.hqmpms.api.system.feedback;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.system.FeedBackDao;
import com.hqmpms.dispatcher.Config;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Servlet implementation class Suggestion
 * 获取意见反馈
 * input parameter:
 * type(必填,string):类型
 * rows(选填,string): 页数
 * page(选填,string): 每页行数
 */
@WebServlet("/system/feedback/list.do")
public class Suggestion extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private static final Logger m_logger = Logger.getLogger(Suggestion.class);
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 
		//参数校验
		try{
			CheckParameters check = new CheckParameters(request.getParameterMap());
			m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
			// 获取session
			Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");

			ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String,String>>();
			String rows = "";
			String page = "";
			String type = "";
			if(!StringUtils.isEmpty(request.getParameter("rows"))){
				check.addParameter("rows", CheckParameters.paraType.EXP, Config.getInstance().getString("params_page"));
				rows = check.get("rows").toString();
			}else{
				rows = "10";
			}
			if(!StringUtils.isEmpty(request.getParameter("page")) && Integer.parseInt(request.getParameter("page"))>0){
				check.addParameter("page", CheckParameters.paraType.EXP, Config.getInstance().getString("params_page"));
				page = check.get("page").toString();
			}else{
				page = "1";
			}
			if(!StringUtils.isEmpty(request.getParameter("type"))){
				check.addParameter("type", CheckParameters.paraType.EXP,"^[1,2,3]$");
				type = check.get("type").toString();
			}

			int count = FeedBackDao.getSuggCount(type);
			arrayList = FeedBackDao.getSuggestion(type,Integer.parseInt(page),Integer.parseInt(rows));

			HashMap<String, Object> hmap = new HashMap<String, Object>();
			hmap.put("total", count);
			hmap.put("rows", new JSONArray(arrayList));
			String ret = ApiErrorCode.echoOkMap(hmap);
			m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
			response.getWriter().write(ret);
			return;
		}catch (CheckParameterException ce) {
			String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
			m_logger.info(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.PARAMETER_ERROR, ce));
			response.getWriter().write(ret);
			return;
		} catch (Exception e) {
			String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
			m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SYSTEM_ERROR, e));
			response.getWriter().write(ret);
			return;
		}
	}

}
