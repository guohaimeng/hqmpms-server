package com.hqmpms.api.system.manager;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.api.utils.Tools;
import com.hqmpms.dao.system.DoOperationLogThread;
import com.hqmpms.dao.system.ManagerDao;
import com.hqmpms.dao.system.OperationLogDao;
import com.hqmpms.dispatcher.Startup;
import com.hqmpms.utils.DoCachedThreadPool;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 系统管理员添加
 */
@WebServlet("/system/manager/add.do")
public class ManagerAdd extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(ManagerAdd.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CheckParameters check = new CheckParameters(request.getParameterMap());
        m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
        try {
            Map<String, String> selfInfo =( Map<String, String> )request.getSession().getAttribute("selfInfo");
            check.addParameter("real_name", CheckParameters.paraType.STRING, 1, 50);
            check.addParameter("user_name", CheckParameters.paraType.STRING, 1, 50);
            check.addParameter("pwd", CheckParameters.paraType.STRING, 1, 50);
            check.addParameter("role_id", CheckParameters.paraType.STRING, 10, 20);
            check.addParameter("is_enable", CheckParameters.paraType.INT, 0, 1);
            HashMap<String ,String> checked = ManagerDao.CheckUserName(check.opt("user_name").toString());
            if(checked.size()>0){
                String ret = ApiErrorCode.echoErr(ApiErrorCode.ACC_EXIST);
                m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.ACC_EXIST));
                response.getWriter().write(ret);
                return;
            }

            String newid= String.valueOf(Startup.getId());
            Map<String,String> manager= new HashMap<>();
            manager.put("manager_id",newid);
            manager.put("real_name",check.opt("real_name").toString());
            manager.put("is_enable",check.opt("is_enable").toString());
            manager.put("user_name",check.opt("user_name").toString());
            String pwd = Tools.md5(check.opt("pwd").toString()).substring(8, 24);
            manager.put("pwd",pwd);
            manager.put("creator_id",selfInfo.get("mid"));
            manager.put("role_id",check.opt("role_id").toString());
            ManagerDao.managerAdd(manager);
            String ret = ApiErrorCode.echoOk();
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            response.getWriter().write(ret);
            OperationLogDao.operationLogRecord(selfInfo.get("mid"), "新增了管理员'" + check.opt("real_name").toString() + "'", newid);
        } catch (CheckParameterException e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s","system exception=",e),e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
        }
    }
}
