package com.hqmpms.api.system.manager;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.system.ManagerDao;
import com.hqmpms.dao.system.OperationLogDao;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * 系统管理员删除
 */
@WebServlet("/system/manager/delete.do")
public class ManagerDelete extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(ManagerDelete.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CheckParameters check = new CheckParameters(request.getParameterMap());
        m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
        try {
            Map<String, String> selfInfo =( Map<String, String> )request.getSession().getAttribute("selfInfo");
            check.addParameter("manager_id", CheckParameters.paraType.STRING, 0, 200);
            ManagerDao.managerDelete(check.get("manager_id").toString());
                String ret = ApiErrorCode.echoOk();
                m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
                response.getWriter().write(ret);
            OperationLogDao.operationLogRecord(selfInfo.get("mid"),"删除了管理员",check.get("manager_id").toString());
        } catch (CheckParameterException e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s","system exception=",e),e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
        }
    }
}
