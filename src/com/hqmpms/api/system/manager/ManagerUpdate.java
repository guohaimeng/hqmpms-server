package com.hqmpms.api.system.manager;

import com.hqmpms.api.utils.*;
import com.hqmpms.dao.system.ManagerDao;
import com.hqmpms.dao.system.OperationLogDao;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 系统管理员修改
 */
@WebServlet("/system/manager/update.do")
public class ManagerUpdate extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(ManagerUpdate.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CheckParameters check = new CheckParameters(request.getParameterMap());
        m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
        try {
            Map<String, String> selfInfo =( Map<String, String> )request.getSession().getAttribute("selfInfo");
            check.addParameter("manager_id", CheckParameters.paraType.STRING, 1, 36);
            Map<String,String> manager= new HashMap<>();
            manager.put("manager_id",check.get("manager_id").toString());

            String real_name=request.getParameter("real_name");
            if(!StringUtils.isEmpty(real_name)){
                check.addParameter("real_name", CheckParameters.paraType.STRING, 1, 50);
                manager.put("real_name",real_name);
            }

            String user_name=request.getParameter("user_name");
            if(!StringUtils.isEmpty(user_name)){
                check.addParameter("user_name", CheckParameters.paraType.STRING, 1, 50);
                manager.put("user_name",user_name);
            }

            String pwd=request.getParameter("pwd");
            if(!StringUtils.isEmpty(pwd)){
                check.addParameter("pwd", CheckParameters.paraType.STRING, 1, 50);
                manager.put("pwd",Tools.md5(check.opt("pwd").toString()).substring(8, 24));
            }


            String is_enable=request.getParameter("is_enable");
            if(!StringUtils.isEmpty(is_enable)){
                check.addParameter("is_enable", CheckParameters.paraType.STRING, 0, 200);
                manager.put("is_enable",is_enable);
            }

            String role_id=request.getParameter("role_id");
            String original_role_id=request.getParameter("original_role_id");
            if(!StringUtils.isEmpty(role_id)){
                check.addParameter("role_id", CheckParameters.paraType.STRING, 0, 200);
                manager.put("role_id",role_id);
                check.addParameter("original_role_id", CheckParameters.paraType.STRING, 0, 200);
                manager.put("original_role_id",role_id);
            }

            HashMap<String ,String> checked = ManagerDao.CheckUserName(check.opt("user_name").toString());
            if(checked.size()>0 && !checked.get("manager_id").equals(check.get("manager_id").toString())){
                String ret = ApiErrorCode.echoErr(ApiErrorCode.ACC_EXIST);
                m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.ACC_EXIST));
                response.getWriter().write(ret);
                return;
            }
            ManagerDao.managerUpdate(manager);

            if(!StringUtils.isEmpty(is_enable) && is_enable.equals("0")){
                DatabaseManager.redisdelKey(manager.get("manager_id")+".ainf");
            }

            String ret = ApiErrorCode.echoOk();
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            response.getWriter().write(ret);
            OperationLogDao.operationLogRecord(selfInfo.get("mid"),"修改了管理员",check.get("manager_id").toString());
        } catch (CheckParameterException e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s","system exception=",e),e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
        }
    }
}
