package com.hqmpms.api.system.menu;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.system.MenuDao;
import com.hqmpms.dao.system.OperationLogDao;
import com.hqmpms.dao.system.RoleDao;
import com.hqmpms.dispatcher.Startup;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 菜单添加
 */
@WebServlet("/system/menu/add.do")
public class MenuAdd extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(MenuAdd.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CheckParameters check = new CheckParameters(request.getParameterMap());
        m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
        try {
            Map<String, String> selfInfo =( Map<String, String> )request.getSession().getAttribute("selfInfo");
            check.addParameter("menu_name", CheckParameters.paraType.STRING, 1, 50);
            check.addParameter("menu_code", CheckParameters.paraType.STRING, 1, 50);
            check.addParameter("menu_link", CheckParameters.paraType.STRING, 0, 100);
            check.addParameter("menu_order", CheckParameters.paraType.INT, 0, 100);
            check.addParameter("menu_type", CheckParameters.paraType.INT, 1, 10);
            check.addParameter("is_sys_menu", CheckParameters.paraType.INT, 0, 1);
            check.addParameter("is_top", CheckParameters.paraType.INT, 0, 1);
            int   is_sys_menu = Integer.parseInt(check.get("is_sys_menu").toString());
            String product_id="0";
            if(is_sys_menu==0){
                check.addParameter("product_id", CheckParameters.paraType.STRING, 1, 36);
                product_id = check.get("product_id").toString();
            }

            String  parent_id = "0";
            if(!StringUtils.isEmpty(request.getParameter("parent_id"))){
                check.addParameter("parent_id", CheckParameters.paraType.STRING, 1, 36);
                parent_id = request.getParameter("parent_id");
            }

//            HashMap<String ,String> checked = MenuDao.CheckMenuCode(check.opt("menu_code").toString());
//            if(checked.size()>0){
//                String ret = ApiErrorCode.echoErr(ApiErrorCode.MENU_CODE_EXIST);
//                m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.MENU_CODE_EXIST));
//                response.getWriter().write(ret);
//                return;
//            }

            String newid= String.valueOf(Startup.getId());
            Map<String,String> menu= new HashMap<>();
            menu.put("menu_id",newid);
            menu.put("menu_name",check.opt("menu_name").toString());
            menu.put("menu_code",check.opt("menu_code").toString());
            menu.put("menu_link",check.opt("menu_link").toString());
            menu.put("menu_order",check.opt("menu_order").toString());
            menu.put("menu_type",check.opt("menu_type").toString());
            menu.put("is_sys_menu",check.opt("is_sys_menu").toString());
            menu.put("is_top",check.opt("is_top").toString());
            menu.put("product_id",product_id);
            menu.put("parent_id",parent_id);
            menu.put("creator_id",selfInfo.get("mid"));
            MenuDao.menuAdd(menu);
                String ret = ApiErrorCode.echoOk();
                m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
                response.getWriter().write(ret);
            OperationLogDao.operationLogRecord(selfInfo.get("mid"),"新增了菜单'"+check.opt("menu_name").toString()+"'",newid);
        } catch (CheckParameterException e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s","system exception=",e),e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
        }
    }
}
