package com.hqmpms.api.system.menu;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.system.MenuDao;
import com.hqmpms.dispatcher.Config;
import org.apache.log4j.Logger;
import org.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * 菜单列表
 */
@WebServlet("/system/menu/list.do")
public class MenuList extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(MenuList.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CheckParameters check = new CheckParameters(request.getParameterMap());
        m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
        try {
            check.addParameter("ss", CheckParameters.paraType.EXP, Config.getInstance().getString("params_ss"));
            ArrayList<HashMap<String,String>> list = MenuDao.menuList();

            HashMap<String ,Object> res = new HashMap<>();

            res.put("rows",new JSONArray(list));

            String ret = ApiErrorCode.echoOkMap(res);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            response.getWriter().write(ret);

        } catch (CheckParameterException e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s","system exception=",e),e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
        }
    }
}
