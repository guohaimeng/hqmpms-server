package com.hqmpms.api.system.menu;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.system.MenuDao;
import com.hqmpms.dao.system.OperationLogDao;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 菜单修改
 */
@WebServlet("/system/menu/update.do")
public class MenuUpdate extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(MenuUpdate.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CheckParameters check = new CheckParameters(request.getParameterMap());
        m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
        try {
            Map<String, String> selfInfo =( Map<String, String> )request.getSession().getAttribute("selfInfo");
            check.addParameter("menu_id", CheckParameters.paraType.STRING, 1, 36);
            Map<String,String> menu= new HashMap<>();
            menu.put("menu_id",check.get("menu_id").toString());

            String menu_name=request.getParameter("menu_name");
            if(!StringUtils.isEmpty(menu_name)){
                check.addParameter("menu_name", CheckParameters.paraType.STRING, 1, 50);
                menu.put("menu_name",menu_name);
            }

            String menu_code=request.getParameter("menu_code");
            if(!StringUtils.isEmpty(menu_code)){
                check.addParameter("menu_code", CheckParameters.paraType.STRING, 1, 50);
                menu.put("menu_code",menu_code);
            }

            String  parent_id = null;
            if(!StringUtils.isEmpty(request.getParameter("parent_id"))){
                check.addParameter("parent_id", CheckParameters.paraType.STRING, 1, 36);
                parent_id = request.getParameter("parent_id");
                menu.put("parent_id",parent_id);
            }

            String product_id=request.getParameter("product_id");
            if(!StringUtils.isEmpty(product_id)){
                check.addParameter("product_id", CheckParameters.paraType.STRING, 1, 36);
                menu.put("product_id",product_id);
            }

            String menu_link=request.getParameter("menu_link");
            if(!StringUtils.isEmpty(menu_link)){
                check.addParameter("menu_link", CheckParameters.paraType.STRING, 0, 100);
                menu.put("menu_link",menu_link);
            }


            String menu_order=request.getParameter("menu_order");
            if(!StringUtils.isEmpty(menu_order)){
                check.addParameter("menu_order", CheckParameters.paraType.INT, 1, 100);
                menu.put("menu_order",menu_order);
            }

            String menu_type=request.getParameter("menu_type");
            if(!StringUtils.isEmpty(menu_type)){
                check.addParameter("menu_type", CheckParameters.paraType.INT, 1, 10);
                menu.put("menu_type",menu_type);
            }

            String is_sys_menu=request.getParameter("is_sys_menu");
            if(!StringUtils.isEmpty(is_sys_menu)){
                check.addParameter("is_sys_menu", CheckParameters.paraType.INT, 0, 1);
                menu.put("is_sys_menu",is_sys_menu);
            }

            String is_top=request.getParameter("is_top");
            if(!StringUtils.isEmpty(is_top)){
                check.addParameter("is_top", CheckParameters.paraType.INT, 0, 1);
                menu.put("is_top",is_top);
            }

            String is_enable=request.getParameter("is_enable");
            if(!StringUtils.isEmpty(is_enable)){
                check.addParameter("is_enable", CheckParameters.paraType.INT, 0, 1);
                menu.put("is_enable",is_enable);
            }
//            HashMap<String ,String> checked = MenuDao.CheckMenuCode(check.opt("menu_code").toString());
//            if(checked.size()>0 && !checked.get("menu_id").equals(check.get("menu_id").toString())){
//                String ret = ApiErrorCode.echoErr(ApiErrorCode.MENU_CODE_EXIST);
//                m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.MENU_CODE_EXIST));
//                response.getWriter().write(ret);
//                return;
//            }
            MenuDao.menuUpdate(menu);
            String ret = ApiErrorCode.echoOk();
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            response.getWriter().write(ret);
            OperationLogDao.operationLogRecord(selfInfo.get("mid"),"修改了菜单'"+check.opt("menu_name").toString()+"'",check.get("menu_id").toString());
        } catch (CheckParameterException e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s","system exception=",e),e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
        }
    }
}
