package com.hqmpms.api.system.operationlog;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.system.ManagerDao;
import com.hqmpms.dao.system.OperationLogDao;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * 操作记录列表
 */
@WebServlet("/system/operationlog/list.do")
public class OperationLogList extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(OperationLogList.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CheckParameters check = new CheckParameters(request.getParameterMap());
        m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
        try {
            check.addParameter("rows", CheckParameters.paraType.INT, 0, 200);
            check.addParameter("page", CheckParameters.paraType.INT, 0, 20000000);
            String operator="";
            String ObjectId="";
            String startTime="";
            String endTime="";

            if(!StringUtils.isEmpty(request.getParameter("operator"))){
                check.addParameter("operator", CheckParameters.paraType.STRING, 1, 100);
                operator= request.getParameter("operator");
            }
            if(!StringUtils.isEmpty(request.getParameter("ObjectId"))){
                check.addParameter("ObjectId", CheckParameters.paraType.STRING, 1, 100);
                ObjectId= request.getParameter("ObjectId");
            }

            if(!StringUtils.isEmpty(request.getParameter("startTime"))){
                check.addParameter("startTime", CheckParameters.paraType.STRING, 1, 100);
                startTime= request.getParameter("startTime");
            }

            if(!StringUtils.isEmpty(request.getParameter("endTime"))){
                check.addParameter("endTime", CheckParameters.paraType.STRING, 1, 100);
                endTime= request.getParameter("endTime");
            }
            HashMap<String,Object> DaoResult = OperationLogDao.operationLogList(check.get("page").toString(),check.get("rows").toString(),operator,ObjectId,startTime,endTime);
            ArrayList<HashMap<String,String>> list = (ArrayList<HashMap<String,String>>)DaoResult.get("list");

            String total = DaoResult.get("count").toString();

            HashMap<String ,Object> res = new HashMap<>();
            res.put("total",total);

            res.put("rows",new JSONArray(list));

            String ret = ApiErrorCode.echoOkMap(res);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            response.getWriter().write(ret);

        } catch (CheckParameterException e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s","system exception=",e),e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
        }
    }
}
