package com.hqmpms.api.system.parameter;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.system.OperationLogDao;
import com.hqmpms.dao.system.SysParameterDao;
import com.hqmpms.dispatcher.Startup;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 系统参数添加
 */
@WebServlet("/system/parameter/add.do")
public class SysParameterAdd extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(SysParameterAdd.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CheckParameters check = new CheckParameters(request.getParameterMap());
        m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
        try {
            Map<String, String> selfInfo =( Map<String, String> )request.getSession().getAttribute("selfInfo");
            check.addParameter("parameter_name", CheckParameters.paraType.STRING, 1, 80);
            check.addParameter("parameter_code", CheckParameters.paraType.STRING, 1, 80);
            check.addParameter("parameter_value", CheckParameters.paraType.STRING, 1, 80);
            check.addParameter("parameter_text", CheckParameters.paraType.STRING, 1, 80);
            String newid= String.valueOf(Startup.getId());
            Map<String,String> sysParameter= new HashMap<>();
            sysParameter.put("parameter_id",newid);
            sysParameter.put("parameter_name",check.opt("parameter_name").toString());
            sysParameter.put("parameter_code",check.opt("parameter_code").toString());
            sysParameter.put("parameter_value",check.opt("parameter_value").toString());
            sysParameter.put("parameter_text",check.opt("parameter_text").toString());
            sysParameter.put("creator_id",selfInfo.get("mid"));
            SysParameterDao.sysParameterAdd(sysParameter);
                String ret = ApiErrorCode.echoOk();
                m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
                response.getWriter().write(ret);
            OperationLogDao.operationLogRecord(selfInfo.get("mid"),"添加了系统参数",newid);
        } catch (CheckParameterException e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s","system exception=",e),e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
        }
    }
}
