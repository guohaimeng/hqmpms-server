package com.hqmpms.api.system.parameter;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.system.OperationLogDao;
import com.hqmpms.dao.system.SysParameterDao;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 系统参数修改
 */
@WebServlet("/system/parameter/update.do")
public class SysParameterUpdate extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(SysParameterUpdate.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CheckParameters check = new CheckParameters(request.getParameterMap());
        m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
        try {
            Map<String, String> selfInfo =( Map<String, String> )request.getSession().getAttribute("selfInfo");
            check.addParameter("parameter_id", CheckParameters.paraType.STRING, 10, 36);
            Map<String,String> sysParameter= new HashMap<>();
            sysParameter.put("parameter_id",check.get("parameter_id").toString());

            String parameter_name=request.getParameter("parameter_name");
            if(!StringUtils.isEmpty(parameter_name)){
                check.addParameter("parameter_name", CheckParameters.paraType.STRING, 1, 80);
                sysParameter.put("parameter_name",parameter_name);
            }

            String parameter_code=request.getParameter("parameter_code");
            if(!StringUtils.isEmpty(parameter_code)){
                check.addParameter("parameter_code", CheckParameters.paraType.STRING, 1, 80);
                sysParameter.put("parameter_code",parameter_code);
            }

            String parameter_value=request.getParameter("parameter_value");
            if(!StringUtils.isEmpty(parameter_value)){
                check.addParameter("parameter_value", CheckParameters.paraType.STRING, 1, 80);
                sysParameter.put("parameter_value",parameter_value);
            }


            String parameter_text=request.getParameter("parameter_text");
            if(!StringUtils.isEmpty(parameter_text)){
                check.addParameter("parameter_text", CheckParameters.paraType.STRING, 1, 80);
                sysParameter.put("parameter_text",parameter_text);
            }

            SysParameterDao.sysParameterUpdate(sysParameter);
            String ret = ApiErrorCode.echoOk();
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            response.getWriter().write(ret);
            OperationLogDao.operationLogRecord(selfInfo.get("mid"),"修改了系统参数",check.get("parameter_id").toString());
        } catch (CheckParameterException e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s","system exception=",e),e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
        }
    }
}
