package com.hqmpms.api.system.product;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.system.ProductDao;
import com.hqmpms.dispatcher.Config;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2018/5/21.
 * 产品列表
 * input parameter:
 * rows(选填,string): 页数
 * page(选填,string): 每页行数
 */
@WebServlet("/system/product/list.do")
public class GetProductList extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(GetProductList.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
            // 获取session
            Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
            check.addParameter("ss", CheckParameters.paraType.EXP, Config.getInstance().getString("params_ss"));
            String rows;
            String page;
            if(!StringUtils.isEmpty(request.getParameter("rows"))){
                check.addParameter("rows", CheckParameters.paraType.EXP, Config.getInstance().getString("params_page"));
                rows = check.get("rows").toString();
            }else{
                rows = "10";
            }
            if(!StringUtils.isEmpty(request.getParameter("page")) && Integer.parseInt(request.getParameter("page"))>0){
                check.addParameter("page", CheckParameters.paraType.EXP, Config.getInstance().getString("params_page"));
                page = check.get("page").toString();
            }else{
                page = "1";
            }

            HashMap<String,Object> productList = ProductDao.getProductList(Integer.parseInt(page),Integer.parseInt(rows));
            HashMap<String ,Object> res = new HashMap<>();
            res.put("total",productList.get("count"));
            res.put("rows",new JSONArray((ArrayList<HashMap<String,String>>)productList.get("rows")));
            String ret = ApiErrorCode.echoOkMap(res);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            response.getWriter().write(ret);
            return;
        }catch (CheckParameterException ce) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR, ce));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s", "system exception=", e), e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
            return;
        }

    }

}
