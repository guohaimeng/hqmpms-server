package com.hqmpms.api.system.product;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.system.DoOperationLogThread;
import com.hqmpms.dao.system.ProductDao;
import com.hqmpms.utils.DoCachedThreadPool;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by guohaimeng on 2018/5/21.
 * 产品修改
 * input parameter:
 * product_id(必填,string):产品ID
 * productname(必填,string):产品名称
 * dbmanagesys(必填,String):数据库管理系统
 * jdbcurl(必填，String)：jdbcurl
 * db_name(必填，String)：用户名
 * db_password(必填，String)：用户密码
 * is_enable(必填，String)：是否启用
 * procode(必填，String)：产品编码
 * remark(必填，String)：备注
 * description(必填，String)：描述
 */
@WebServlet("/system/product/update.do")
public class UpdateProduct extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(UpdateProduct.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
            check.addParameter("product_name", CheckParameters.paraType.STRING,1,50);
            check.addParameter("product_id", CheckParameters.paraType.STRING,1,30);
            check.addParameter("db_manage_sys", CheckParameters.paraType.STRING, 1, 10);
            check.addParameter("jdbc_url", CheckParameters.paraType.STRING, 1, 50);
            check.addParameter("db_name", CheckParameters.paraType.STRING, 1, 50);
            check.addParameter("db_password", CheckParameters.paraType.STRING, 1, 50);
            check.addParameter("is_enable", CheckParameters.paraType.INT, 0, 1);
            String procode="";
            if(!StringUtils.isEmpty(request.getParameter("procode"))){
                check.addParameter("procode", CheckParameters.paraType.STRING ,1,50);
                procode=check.get("procode").toString();
            }
            String remark="";
            if(!StringUtils.isEmpty(request.getParameter("remark"))){
                check.addParameter("remark", CheckParameters.paraType.STRING ,1,250);
                remark=check.get("remark").toString();
            }
            String description="";
            if(!StringUtils.isEmpty(request.getParameter("description"))){
                check.addParameter("description", CheckParameters.paraType.STRING,1,500);
                description=check.get("description").toString();
            }
            // 获取session
            Map<String, String> selfInfo = ( Map<String, String> )request.getSession().getAttribute("selfInfo");
            HashMap<String,String> condition = new HashMap<String,String>();
            condition.put("productid",check.get("product_id").toString());
            condition.put("creatorid",selfInfo.get("mid"));
            condition.put("productname",check.get("product_name").toString());
            condition.put("dbmanagesys",check.get("db_manage_sys").toString());
            condition.put("jdbcurl",check.get("jdbc_url").toString());
            condition.put("dbname",check.get("db_name").toString());
            condition.put("dbpwd",check.get("db_password").toString());
            condition.put("procode",procode);
            condition.put("remark",remark);
            condition.put("description",description);
            condition.put("isenable",check.get("is_enable").toString());
            if(ProductDao.updateProduct(condition)){
                String ret = ApiErrorCode.echoOk();
                m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SUCCESS));
                response.getWriter().write(ret);
                DoCachedThreadPool.getCachedThreadPool().cachedThreadPool.execute(new DoOperationLogThread(selfInfo.get("mid"),"修改产品'"+check.get("product_name").toString()+"'",check.get("product_id").toString()));
                return;
            }else{
                String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
                m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SYSTEM_ERROR));
                response.getWriter().write(ret);
                return;
            }

        } catch (CheckParameterException ce) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR, ce));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s", "system exception=", e), e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
            return;
        }

    }

}
