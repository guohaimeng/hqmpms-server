package com.hqmpms.api.system.role;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.system.ManagerDao;
import com.hqmpms.dao.system.OperationLogDao;
import com.hqmpms.dao.system.RoleDao;
import com.hqmpms.dispatcher.Startup;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 角色添加
 */
@WebServlet("/system/role/add.do")
public class RoleAdd extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(RoleAdd.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CheckParameters check = new CheckParameters(request.getParameterMap());
        m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
        try {
            Map<String, String> selfInfo =( Map<String, String> )request.getSession().getAttribute("selfInfo");
            check.addParameter("role_name", CheckParameters.paraType.STRING, 1, 50);
            check.addParameter("mids", CheckParameters.paraType.STRING, 1, 2048);

            HashMap<String ,String> checked = RoleDao.CheckRoleName(check.opt("role_name").toString());
            if(checked.size()>0){
                String ret = ApiErrorCode.echoErr(ApiErrorCode.ROLE_EXIST);
                m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.ROLE_EXIST));
                response.getWriter().write(ret);
                return;
            }

            String newid= String.valueOf(Startup.getId());
            Map<String,String> role= new HashMap<>();
            role.put("role_id",newid);
            role.put("role_name",check.opt("role_name").toString());
            role.put("creator_id",selfInfo.get("mid"));
            role.put("mids",check.opt("mids").toString());
            RoleDao.roleAdd(role);
                String ret = ApiErrorCode.echoOk();
                m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
                response.getWriter().write(ret);
            OperationLogDao.operationLogRecord(selfInfo.get("mid"),"添加了角色'"+role.get("role_name")+"'",newid);
        } catch (CheckParameterException e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s","system exception=",e),e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
        }
    }
}
