package com.hqmpms.api.system.role;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.system.RoleDao;
import org.apache.log4j.Logger;
import org.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * 获取角色combobox
 */
@WebServlet("/commonapi/role/combobox.do")
public class RoleCombobox extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(RoleCombobox.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CheckParameters check = new CheckParameters(request.getParameterMap());
        m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
        try {
            ArrayList<HashMap<String,String>> list = RoleDao.queryRoleIdAndName();
            response.getWriter().write(new JSONArray(list).toString());
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
        }  catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s","system exception=",e),e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
        }
    }
}
