package com.hqmpms.api.system.role;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.system.OperationLogDao;
import com.hqmpms.dao.system.RoleDao;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 角色删除
 */
@WebServlet("/system/role/delete.do")
public class RoleDelete extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(RoleDelete.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CheckParameters check = new CheckParameters(request.getParameterMap());
        m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
        try {
            Map<String, String> selfInfo =( Map<String, String> )request.getSession().getAttribute("selfInfo");
            check.addParameter("role_id", CheckParameters.paraType.STRING, 10, 20);
            ArrayList<HashMap<String, String>>   roleManagers = RoleDao.selectRoleUseInManager(check.get("role_id").toString());
            if(roleManagers.size()>0){
                String ret = ApiErrorCode.echoErr(ApiErrorCode.ROLE_IN_USE);
                m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.ROLE_IN_USE));
                response.getWriter().write(ret);
                return;
            }

                RoleDao.roleDelete(check.get("role_id").toString());
                String ret = ApiErrorCode.echoOk();
                m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
                response.getWriter().write(ret);
            OperationLogDao.operationLogRecord(selfInfo.get("mid"),"删除了角色",check.get("role_id").toString());
        } catch (CheckParameterException e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s","system exception=",e),e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
        }
    }
}
