package com.hqmpms.api.system.role;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.api.utils.EasyUiTree;
import com.hqmpms.dao.system.MenuDao;
import com.hqmpms.dao.system.RoleDao;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * 角色详情
 */
@WebServlet("/commonapi/role/info.do")
public class RoleInfo extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(RoleInfo.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CheckParameters check = new CheckParameters(request.getParameterMap());
        m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
        try {
            check.addParameter("role_id", CheckParameters.paraType.STRING, 10, 20);
            HashMap<String,String> role = RoleDao.roleInfo(check.get("role_id").toString());
            ArrayList<HashMap<String,String>> menulist = MenuDao.menuComboTree(null,null);
            List<String> midsList  = new ArrayList<>();
            if(!StringUtils.isEmpty(role.get("mids"))){
                midsList = Arrays.asList(StringUtils.split(role.get("mids"),","));
                m_logger.info("roleinfo:m_size="+midsList.size());
            }

            ArrayList<HashMap<String,String>> noToplist = new ArrayList<>();

            ArrayList<EasyUiTree> trees = new ArrayList<>();

            for(HashMap<String,String> map : menulist){
                if( StringUtils.isEmpty(map.get("parent_id"))){
                    EasyUiTree node = new EasyUiTree();
                    node.setId(map.get("menu_id"));
                    node.setText(map.get("menu_name"));
                    node.setAttribute1(map.get("menu_type"));
                    node.setAttribute2(map.get("menu_order"));
//                    if(midsList.contains(map.get("menu_id"))){
//                        node.setChecked(true);
//                    }else {
//                        node.setChecked(false);
//                    }
                    trees.add(node);
                }else{
                    noToplist.add(map);
                }
            }

            for(EasyUiTree tree : trees){
                buildTreeNode(tree,noToplist,midsList);
            }
            JSONObject roleObj = new JSONObject(role);
            roleObj.remove("mids");
            roleObj.put("menutree",new JSONArray(trees));
            //role.put("menutree",new JSONArray(trees).toString());
            //role.put("mids",new JSONArray("["+role.get("mids")+"]").toString());
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            response.getWriter().write(roleObj.toString());

        } catch (CheckParameterException e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s","system exception=",e),e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
        }
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
        doPost(request,response);
    }

    private  void  buildTreeNode(EasyUiTree node,ArrayList<HashMap<String,String>> noToplist,List<String> midsList){
        for(HashMap<String,String> map :noToplist){
            if(map.get("parent_id").equals(node.getId())){
                EasyUiTree tnode = new EasyUiTree();
                tnode.setId(map.get("menu_id"));
                tnode.setText(map.get("menu_name"));
                tnode.setAttribute1(map.get("menu_type"));
                tnode.setAttribute2(map.get("menu_order"));
                if(midsList.contains(map.get("menu_id"))){
                    tnode.setChecked(true);
                }else {
                    tnode.setChecked(false);
                }
                node.addChildren(tnode);
                buildTreeNode(tnode,noToplist,midsList);
            }
        }
        if(node.getChildren()!= null && node.getChildren().size()>0){
            node.setChecked(false);
        }
    }
}
