package com.hqmpms.api.system.role;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.system.OperationLogDao;
import com.hqmpms.dao.system.RoleDao;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 角色修改
 */
@WebServlet("/system/role/update.do")
public class RoleUpdate extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(RoleUpdate.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CheckParameters check = new CheckParameters(request.getParameterMap());
        m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
        try {
            Map<String, String> selfInfo =( Map<String, String> )request.getSession().getAttribute("selfInfo");
            check.addParameter("role_id", CheckParameters.paraType.STRING, 10, 20);
            Map<String,String> role= new HashMap<>();
            role.put("role_id",check.get("role_id").toString());

            String role_name=request.getParameter("role_name");
            if(!StringUtils.isEmpty(role_name)){
                check.addParameter("role_name", CheckParameters.paraType.STRING, 1, 50);
                role.put("role_name",role_name);
            }

            String role_code=request.getParameter("mids");
            if(!StringUtils.isEmpty(role_code)){
                check.addParameter("mids", CheckParameters.paraType.STRING, 1, 2048);
                role.put("mids",role_code);
            }


            String is_enable=request.getParameter("is_enable");
            if(!StringUtils.isEmpty(is_enable)){
                check.addParameter("is_enable", CheckParameters.paraType.INT, 0, 1);
                role.put("is_enable",is_enable);
            }

            HashMap<String ,String> checked = RoleDao.CheckRoleName(check.opt("role_name").toString());
            if(checked.size()>0 && !checked.get("role_id").equals(check.get("role_id").toString())){
                String ret = ApiErrorCode.echoErr(ApiErrorCode.ROLE_EXIST);
                m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.ROLE_EXIST));
                response.getWriter().write(ret);
                return;
            }

            RoleDao.roleUpdate(role);
            String ret = ApiErrorCode.echoOk();
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            response.getWriter().write(ret);
            OperationLogDao.operationLogRecord(selfInfo.get("mid"),"删除了角色",check.get("role_id").toString());
        } catch (CheckParameterException e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s","system exception=",e),e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
        }
    }
}
