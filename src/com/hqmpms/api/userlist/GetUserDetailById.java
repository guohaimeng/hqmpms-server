package com.hqmpms.api.userlist;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.userlist.UserListDao;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

/**
 * 根据用户编号查询详情
 * Created by dml on  2018/5/24
 */
@WebServlet("/userList/getDetailById.do")
public class GetUserDetailById extends HttpServlet{
    private static final Logger LOGGER = Logger.getLogger(GetUserListByAllConditions.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //参数校验
        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            LOGGER.info(String.format("INPUT params=%s", check.getParameterJSON()));
            check.addParameter("userId", CheckParameters.paraType.STRING, 1, 36);
            HashMap<String, String> resultMap = UserListDao.getUserDetailById(check.opt("userId").toString());
            JSONObject obj = JSONObject.fromObject(resultMap);
            response.getWriter().write(obj.toString());
            LOGGER.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SUCCESS));
            response.getWriter().close();
        }catch (Exception e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            LOGGER.error(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.SYSTEM_ERROR, e));
            try {
                response.getWriter().write(ret);
            }catch (Exception ex){
                LOGGER.error(ex);
            }
        }
    }
}
