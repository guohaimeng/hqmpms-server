package com.hqmpms.api.userlist;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.dao.userlist.UserListDao;
import org.apache.log4j.Logger;
import org.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * 获取筛选后的用户列表
 * Created by dml on  2018/5/24
 */
@WebServlet("/userList/getByAllConditions.do")
public class GetUserListByAllConditions extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(GetUserListByAllConditions.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //参数校验
        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            LOGGER.info(String.format("INPUT params=%s", check.getParameterJSON()));
            String sPage = request.getParameter("page");
            Integer page = Integer.parseInt(sPage == null ? "1" : sPage);
            String sPageSize = request.getParameter("rows");
            Integer pageSize = Integer.parseInt(sPageSize == null ? "10": sPageSize);
            List<HashMap<String, String>> list = UserListDao.getUserListByAllConditions(
                    check.opt("userIds").toString(),
                    check.opt("temp").toString(),
                    check.opt("search").toString(),
                    page,pageSize
                    );
            long count = UserListDao.getUserListByAllConditions(
                    check.opt("userIds").toString(),
                    check.opt("temp").toString(),
                    check.opt("search").toString(),
                    null,null
            ).size();
            HashMap<String, Object> resultMap = new HashMap<>();
            resultMap.put("total", count);
            resultMap.put("rows", new JSONArray(list));
            String ret = ApiErrorCode.echoOkMap(resultMap);
            response.getWriter().write(ret);
            LOGGER.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SUCCESS));
            response.getWriter().close();
        }catch (Exception e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            LOGGER.error(String.format("OUTPUT ret_code=%s,%s", ApiErrorCode.SYSTEM_ERROR, e));
            try {
                response.getWriter().write(ret);
            }catch (Exception ex){
                LOGGER.error(ex);
            }
        }
    }
}
