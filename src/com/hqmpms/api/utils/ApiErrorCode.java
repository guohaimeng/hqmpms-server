package com.hqmpms.api.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Set;

/**
 * API接口返回给client端的错误代码
 *
 */
public class ApiErrorCode {
	
	/**
	 * 当前返回码
	 */
	private int m_code = 0;

	/**
	 * 描述字符
	 */
	private String m_description = null;

	/**
	 * 返回码+描述字符
	 */
	private String m_string = null;

	public ApiErrorCode(){}
	public ApiErrorCode(int code, String description) {
		m_code = code;
		m_description = description;
		m_string = m_code + " " + m_description;
	}

	public String toString() {
		return m_string;
	}
	
	public int errCode(){
		return m_code;
	}
	
	public String errMsg(){
		return m_description;
	}	
	/**
	 * 默认错误码
	 */
	public static final int DEFAULT_SYSTEM_ERROR = 6001;
	public static final int DEFAULT_SDK_SUCCESS = 0;
	public static final int DEFAULT_SDK_PARAMS_ERROR = 9999;
	public static final int DEFAULT_SDK_SYSTEM_ERROR = 9998;
	public static final int DEFAULT_SDK_ILLEGAL_USER = 1006; //无效的用户
	public static final ApiErrorCode PUBLIC_KEY_INVALID      = new ApiErrorCode(6888, "无效");
	
	public static final String DEFAULT_TYPE = "1";
	public static final String DEFAULT_USER_TYPE = "1";  // 老师
	public static final String ADMIN_USER_TYPE = "5";  // 系统管理员
	public static final String ADMIN_TYPE = "6";  // 系统管理员
	public static final String DEFAULT_USER_STUDENT_TYPE = "7";  // 学生
	public static final String TASK_FILE_TYPE_VIDEO = "1";
	public static final String TASK_FILE_TYPE_ACCESSORY = "2";
	public static final String TASK_TYPE_JOB = "1";//班级作业
	/**************************班级模块使用*************************************/
	public static final String DEFAULT_CLASS_POWER_TYPE = "1";  //班主任
	public static final String CLASS_TEACHER_USER_TYPE = "2"; //任课老师
	public static final String Class_STUDENT_USER_TYPE = "3"; //学生
	/********************班级操作************/
	public static final String DEFAULT_STATUS = "0"; //班级默认状态
	public static final String DEFAULT_IS_AUDIT = "1"; //班级默认为审核通过
	public static final String CLASS_APPLY_STATUS = "1"; //操作申请中
	public static final String CLASS_HANDLE_AGREE_STATUS = "2"; //批准
	public static final String CLASS_HANDLE_REFUSE_STATUS = "3"; //拒绝
	public static final String CLASS_GRADE_INFO           = "'106','109','112'";
	public static final String GRADE_MSEVEN = "116"; //中职年级
	/********************群开关************/
	public static final String DEFAULT_GROUP_INVITE_REFUSE = "0";  //禁止邀请,禁止直接加入
	public static final String LEAVE_GROUP_OWNER_TYPE = "ownerLeave";
	public static final String LEAVE_GROUP_MEMBER_TYPE = "memberLeave";
	/*********************用户校验开关***************/
	public static final String DEFAULT_JOIN_GROUP_INVITE_STATUS = "4,5,6,7";//邀请入群需要验证
	public static final String DEFAULT_RECOMMEND_GROUP_INVITE_STATUS = "0,2,4,6";//允许推荐我
	public static final String DEFAULT_ESSAY_POWER_STATUS = "2,3,6,7";//不允许查看我的随笔
	public static final String DEFAULT_SEE_MYINFO_POWER_STATUS = "4,5,6,7";//不允许查看我的随笔
	/***********************************发送验证码模块Start********************************/
	public static final String BIND_MOBILE                  = "1";
	public static final String FORGET_PASSWORD              = "2";
	/***********************************发送验证码模块End********************************/

	/***********************************广告尺寸start********************************/
	public static final String ADVERT_750                  = "1";
	public static final String ADVERT_1242              = "2";
	/***********************************广告尺寸end********************************/

	/***********************************获取班级或群文件模块Start********************************/
	public static final String ACHIEVE_CLASS_FILE           = "1";
	public static final String ACHIEVE_GROUP_FILE           = "2";
	/***********************************获取班级或群文件模块End********************************/
	/********************消息提示类型说明Start****************************************/
	public static final String UPDATE_BASE_DATA_MSG     = "58";  //基础信息更新
	public static final String ADVERT_PUSH     					= "60";  //广告推送
	public static final String SYS_MSG = "100";

	public static final String SYS_MEDIA_MSG = "101";

	public static final String SEND_MSG_JMG_TYPE            = "JMG";
	public static final String SEND_MSG_PPM_TYPE            = "PPM";
	public static final String GRADE_SCHOOL                 = "1";//小学
	public static final String MIDDLE_SCHOOL                = "2";//初中
	public static final String HIGH_SCHOOL                  = "3";//高中
	public static final String CHILD_EDUCATION              = "4";//幼教
	public static final String SECONDARY_SCHOOL             = "5";//中职
	
	/********************消息提示类型说明Start****************************************/



	
	
	/**
	 * 系统级错误码
	 */
	
	public static final ApiErrorCode SUCCESS           = new ApiErrorCode(0, "SUCCESS");
	public static final ApiErrorCode VOID_REQUEST      = new ApiErrorCode(6000, "无效请求");
	public static final ApiErrorCode SYSTEM_ERROR      = new ApiErrorCode(6001, "系统错误");
	public static final ApiErrorCode PARAMETER_ERROR   = new ApiErrorCode(6002, "参数错误");
	public static final ApiErrorCode SESSION_INVALID   = new ApiErrorCode(6003, "session非法");
	public static final ApiErrorCode UPLOAD_FILE_FAIL  = new ApiErrorCode(6004, "上传文件到fastdfs失败");
	public static final ApiErrorCode FILE_TOO_LARGE    = new ApiErrorCode(6005, "文件太大");
	public static final ApiErrorCode FILE_NOT_EXSIT    = new ApiErrorCode(6006, "文件不存在");
	public static final ApiErrorCode NOT_ALLOW_FILE   = new ApiErrorCode(6008, "文件类型错误");
	public static final ApiErrorCode IMG_SIZE_ERROR   = new ApiErrorCode(6009, "图片尺寸戳无");
	public static final ApiErrorCode ROLE_IN_USE= 		new ApiErrorCode(6010, "角色在使用中，不可删除");
	public static final ApiErrorCode PWD_UN_COINCIDENT=	new ApiErrorCode(6011, "密码不一致");
	public static final ApiErrorCode HANDLE_NO_POWER= new ApiErrorCode(6226, "操作权限不足");
	public static final ApiErrorCode ACC_EXIST             = new ApiErrorCode(6105, "用户名已经存在");
	public static final ApiErrorCode ROLE_EXIST             = new ApiErrorCode(6106, "角色名已经存在");
	public static final ApiErrorCode MENU_CODE_EXIST             = new ApiErrorCode(6107, "菜单编码已经存在");
	public static final ApiErrorCode ACC_NOT_EXIST    	   = new ApiErrorCode(6108, "用户（名）不存在");
	public static final ApiErrorCode ACC_PWD_ERR           = new ApiErrorCode(6109, "用户名或密码错误");
	public static final ApiErrorCode NOT_FOUND_SEND_PEOPLES  = new ApiErrorCode(6110, "没有查询到发送人群");
	public static final ApiErrorCode MODULE_NAME_EXIST  = new ApiErrorCode(6111, "模块名称已存在");
	public static final ApiErrorCode MODULE_ORDER_NUM_EXIST  = new ApiErrorCode(6112, "模块顺序号已存在");
	public static final ApiErrorCode MODULE_CODE_EXIST  = new ApiErrorCode(6113, "模块唯一编码已存在");






	/**
	 * 管理系统
	 */
	public static final ApiErrorCode PRODUCT_IS_EXIST  = new ApiErrorCode(8111, "产品已存在");
	
	private static String retJson(ApiErrorCode errInfo){
		JSONObject retJson = new JSONObject();
		try {
			retJson.put("errcode", errInfo.errCode());
			retJson.put("errmsg", errInfo.errMsg());
			return retJson.toString();
		} catch (JSONException e) {
			return "{\"errcode\":\"6001\",\"errmsg\":\"JSON解析错误\"}";			
		}
	}
	
	public static String echoErr(ApiErrorCode errCode){
		return retJson(errCode);			
	}
	public static String echoOk(){
		return retJson(SUCCESS);	
	}
	
	public static String echoOkArr(HashMap<String,String> map){
		JSONObject retJson = new JSONObject();
		try {
			retJson.put("errcode", SUCCESS.errCode());
			retJson.put("errmsg", SUCCESS.errMsg());
			Set<String> keys = map.keySet();				
			for(String key:keys){
				retJson.put(key,map.get(key));
			}			
			return retJson.toString();
		} catch (JSONException e) {
			return "{\"errcode\":\"6001\",\"errmsg\":\"JSON解析错误\"}";			
		}	
	}
	
	public static String echoErrArr(ApiErrorCode errCode, HashMap<String,Object> map){
		JSONObject retJson = new JSONObject();
		try {
			retJson.put("errcode", errCode.errCode());
			retJson.put("errmsg", errCode.errMsg());
			Set<String> keys = map.keySet();				
			for(String key:keys){
				retJson.put(key,map.get(key));
			}			
			return retJson.toString();
		} catch (JSONException e) {
			return "{\"errcode\":\"6001\",\"errmsg\":\"JSON解析错误\"}";			
		}	
	}
	
	public static String echoErrMap(ApiErrorCode errCode, HashMap<String,Object> map){
		JSONObject retJson = new JSONObject();
		try {
			retJson.put("errcode", errCode.errCode());
			retJson.put("errmsg", errCode.errMsg());
			Set<String> keys = map.keySet();				
			for(String key:keys){
				if(map.get(key) instanceof JSONArray){
					retJson.put(key,map.get(key));
				}else if(map.get(key) instanceof JSONObject){
					retJson.put(key,map.get(key));
				}else{
					retJson.put(key,map.get(key).toString());
				}
					
			}			
			return retJson.toString();
		} catch (JSONException e) {
			return "{\"errcode\":\"6001\",\"errmsg\":\"JSON解析错误\"}";			
		}
		
	}
	
	public static String echoOkMap(HashMap<String,Object> map){
		JSONObject retJson = new JSONObject();
		try {
			retJson.put("errcode", SUCCESS.errCode());
			retJson.put("errmsg", SUCCESS.errMsg());
			Set<String> keys = map.keySet();				
			for(String key:keys){
				if(map.get(key) instanceof JSONArray){
					retJson.put(key,map.get(key));
				}else if(map.get(key) instanceof JSONObject){
					retJson.put(key,map.get(key));
				}else{
					retJson.put(key,map.get(key).toString());
				}
					
			}			
			return retJson.toString();
		} catch (JSONException e) {
			return "{\"errcode\":\"6001\",\"errmsg\":\"JSON解析错误\"}";			
		}
	}
	/**
	 * 班级模块输出
	 * @return
	 */
	public static String echoClassOk(HashMap<String,Object> map,String outType){
		JSONObject retJson = new JSONObject();
		JSONObject ret = new JSONObject();
		try {
			retJson.put("errcode", SUCCESS.errCode());
			retJson.put("errmsg", SUCCESS.errMsg());
			Set<String> keys = map.keySet();				
			for(String key:keys){
				if(map.get(key) instanceof JSONArray){
					ret.put(key,map.get(key));
				}else if(map.get(key) instanceof JSONObject){
					ret.put(key,map.get(key));
				}else{
					ret.put(key,map.get(key).toString());
				}
			}
			retJson.put(outType,ret);
			return retJson.toString();
		} catch (JSONException e) {
			return "{\"errcode\":\"6001\",\"errmsg\":\"JSON解析错误\"}";			
		}
	}
	public static String echoClassOk(JSONObject ret,String outType){
		return retClassJson(SUCCESS,ret,outType);	
	}
	private static String retClassJson(ApiErrorCode errInfo, JSONObject ret, String outType){
		JSONObject retJson = new JSONObject();
		try {
			retJson.put("errcode", errInfo.errCode());
			retJson.put("errmsg", errInfo.errMsg());
			/*if(ret != null){
				retJson.put("result", ret);
			}else{
				retJson.put("result", "{}");
			}*/
			if(ret != null){
				retJson.put(outType,ret);
			}else{
				retJson.put(outType, "{}");
			}
			
			return retJson.toString();
		} catch (JSONException e) {
			return "{\"errcode\":\"6001\",\"errmsg\":\"JSON解析错误\"}";			
		}
	}
	public static String echoClassOk(JSONObject ret){
		return retClassJson(SUCCESS,ret);	
	}
	private static String retClassJson(ApiErrorCode errInfo, JSONObject ret){
		try {
			ret.put("errcode", errInfo.errCode());
			ret.put("errmsg", errInfo.errMsg());
			return ret.toString();
		} catch (JSONException e) {
			return "{\"errcode\":\"6001\",\"errmsg\":\"JSON解析错误\"}";			
		}
	}
	public static String echoClassMsg(HashMap<String,Object> map){
		JSONObject retJson = new JSONObject();
		try {
			if(map != null){
				Set<String> keys = map.keySet();				
				for(String key:keys){
					retJson.put(key,map.get(key));
				}
			}
			return retJson.toString();
		} catch (JSONException e) {
			return "{\"errcode\":\"6001\",\"errmsg\":\"JSON解析错误\"}";			
		}	
	}
	
}
