package com.hqmpms.api.utils;

public class CheckParameterException extends Exception{
	
	/**
	 */
	private static final long serialVersionUID = 1L;
	public static final String errDataType 	= "无效的数据类型";
	public static final String errFormat 	= "无效的数据格式";
	public static final String errDateTime 	= "无效的日期格式";
	public static final String errScope 		= "无效的数据范围";
	public static final String errParameter = "无效的参数名称";
	public static final String errLostPara  = "参数缺失";
	public String name=null;
	public Object value=null;
	public String message=null;
	
	public CheckParameterException(String name,Object value,String errMsg){
		super(errMsg);
		this.name=name;
		this.value = value;
		this.message = errMsg;
	}
	
	public CheckParameterException(String name,String errMsg){
		super(errMsg);
		this.name=name;
		this.message = errMsg;
	}
	
	public String toString(){
		if(value==null){
			return name+":"+message;
		}else{
			return name+"="+value+":"+message;
		}
	}
		
}
