package com.hqmpms.api.utils;



import com.hqmpms.dispatcher.Config;
import com.hqmpms.dispatcher.Startup;
import com.hqmpms.utils.MysqlBaseUtil;
import com.hqmpms.utils.RedisBaseUtil;
import com.hqmpms.utils.SqlServerBaseUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;

import java.sql.*;
import java.util.*;
import java.util.regex.Pattern;

/**
 * 消息端口处理
 * 
 */

public class DatabaseManager {
	public final static String REDIS_CITY_KEY="city.codes";

	private static final Logger m_logger = Logger.getLogger(DatabaseManager.class);



	/**
	 * admin session校验
	 *
	 * @param session
	 *            需要返回对应的fields值
	 * @return
	 */
	public static Map<String, String> checkAdminSession(String session) throws Exception {
		Jedis redis = null;
		Map<String, String> suidInfo = null;
		String suid = null;
		try {
			redis = RedisBaseUtil.getBaseRedis().getResource();
			suid = redis.get(session);
			if (suid != null) {
				suidInfo = redis.hgetAll(suid + ".ainf");
				if (suidInfo == null || suidInfo.size() == 0) {
					redisdelKey(new String[]{session});
				}
			}

		} catch (Exception e) {
			if (redis != null) {
				RedisBaseUtil.getBaseRedis().returnBrokenResource(redis);
				redis=null;
			}
			m_logger.info(" connect exception:" + e);
		} finally {
			RedisBaseUtil.getBaseRedis().returnResource(redis);
		}
		return suidInfo;
	}




	/**
	 * 删除redis数据
	 * @param keys
	 * @return
	 * @throws Exception
     */
	public static Long redisdelKey(String... keys)  {
		Jedis redis = null;
		Long res = 0l;
		try {
			redis = RedisBaseUtil.getBaseRedis().getResource();
			res = redis.del(keys);

		} catch (Exception e) {
			if (redis != null) {
				RedisBaseUtil.getBaseRedis().returnBrokenResource(redis);
				redis=null;
			}
			m_logger.info(" redisdelKey exception:" + e);
		} finally {
			RedisBaseUtil.getBaseRedis().returnResource(redis);
		}
		return res;
	}


	/**
	 * 删除redis数据
	 * @param keys
	 * @return
	 * @throws Exception
	 */
	public static Long redisdelById(String... keys)  {
		Jedis redis = null;
		Long res = 0l;
		try {
			redis = RedisBaseUtil.getBaseRedis().getResource();
			res = redis.del(keys);

		} catch (Exception e) {
			if (redis != null) {
				RedisBaseUtil.getBaseRedis().returnBrokenResource(redis);
				redis=null;
			}
			m_logger.info(" connect exception:" + e);
		} finally {
			RedisBaseUtil.getBaseRedis().returnResource(redis);
		}
		return res;
	}




	/**
	 * 通过用户uid获取用户缓存信息
	 *
	 * @param suid
	 * @return
	 */
	public static Map<String, String> getUserInfoByUidfromRedis(String suid) throws Exception {
		Jedis redis = null;
		Map<String, String> suidInfo = null;
		try {
			redis = RedisBaseUtil.getBaseRedis().getResource();
			suidInfo = redis.hgetAll(suid + ".inf");
		} catch (Exception e) {
			if (redis != null) {
				RedisBaseUtil.getBaseRedis().returnBrokenResource(redis);
			}
			m_logger.info(" connect exception:" + e);
			// throw e;
		} finally {
			RedisBaseUtil.getBaseRedis().returnResource(redis);
		}
		return suidInfo;
	}







	/**
	 * 更新用户信息到redis
	 *
	 * @param suid
	 * @param userInfo
	 * @throws Exception
	 */
	public static void updateProfile(String suid, Map<String, String> userInfo) throws Exception {
		Jedis redis = null;
		Map<String, String> filterUserInfo = new HashMap<String, String>();
		try {
			for (String key : userInfo.keySet()) {
				if (userInfo.get(key) != null) {
					filterUserInfo.put(key, userInfo.get(key));
				}
			}
			redis = RedisBaseUtil.getBaseRedis().getResource();
			redis.hmset(suid + ".ainf", filterUserInfo);

		} catch (Exception e) {
			if (redis != null) {
				RedisBaseUtil.getBaseRedis().returnBrokenResource(redis);
			}
			m_logger.info(" connect exception:" + e);
			// throw e;
		} finally {
			RedisBaseUtil.getBaseRedis().returnResource(redis);
		}
	}

	/**
	 * 更新用户信息到redis
	 *
	 * @param suid
	 * @throws Exception
	 */
	public static void updateProfile(String suid) throws Exception {
		Jedis redis = null;
		try {
			redis = RedisBaseUtil.getBaseRedis().getResource();
			redis.del(suid + ".ainf");
		} catch (Exception e) {
			if (redis != null) {
				RedisBaseUtil.getBaseRedis().returnBrokenResource(redis);
			}
			m_logger.info(" connect exception:" + e);
			// throw e;
		} finally {
			RedisBaseUtil.getBaseRedis().returnResource(redis);
		}
	}









	/**
	 * session校验
	 *
	 *            需要返回对应的fields值
	 * @return
	 */
	public static boolean addAdminProfile(Map<String, String> userInfo) throws Exception {
		Jedis redis = null;
		boolean isSuccess = false;
		try {
			// 过滤value为null的项
			Map<String, String> filterUserInfo = new HashMap<String, String>();
			for (String key : userInfo.keySet()) {
				if (userInfo.get(key) != null) {
					filterUserInfo.put(key, userInfo.get(key));
				}
			}

			String session = userInfo.get("ss");
			String suid = userInfo.get("mid");
			redis = RedisBaseUtil.getBaseRedis().getResource();
			Pipeline pipeline = redis.pipelined();
			pipeline.hget(suid + ".ainf", "ss");
			// 写入newsession与suid的映射关系
			pipeline.set(session, suid);
			pipeline.del(suid + ".ainf");
			pipeline.hmset(suid + ".ainf", filterUserInfo);

			List<Object> results = pipeline.syncAndReturnAll();
			// 删除旧的session和uid映射关系
			if (results != null && results.size() > 0) {
				if (results.get(0) != null) {
					String oldSession = results.get(0).toString();
					if (!"".equals(oldSession)) {
						redis.del(oldSession);
					}
				}
			}
			isSuccess = true;
		} catch (Exception e) {
			if (redis != null) {
				RedisBaseUtil.getBaseRedis().returnBrokenResource(redis);
			}
			m_logger.info("redis connect exception:" + e.getMessage());
		} finally {
			RedisBaseUtil.getBaseRedis().returnResource(redis);
		}
		return isSuccess;
	}








	/**
	 * 获取用户id
	 *
	 * @return long
	 */
	public static Long getUid() {
		String procedure = "{? = call getUid()}";
		Long suid = 0L;
		try {
			CallableStatement statement = SqlServerBaseUtil.getBaseMysqlConn().prepareCall(procedure);
			statement.registerOutParameter(1, Types.BIGINT);
			statement.execute();
			suid = Long.parseLong(statement.getString(1));
		} catch (Exception e) {
			m_logger.info("getUid exception:" + e);
		}

		return suid;

	}









	/**
	 * 获取已注册SDK的用户
	 *
	 * @param
	 * @return
	 */
	public static ArrayList<String> getSdkUser() throws Exception {
		String sql = "select u.id as uid from [user] u where  u.id  in (select uid from yx_user_extend) and userType in (1,7) ";
		ArrayList<Object> params = new ArrayList<Object>();
		ArrayList<String> result = SqlServerBaseUtil.getOneColumns(sql,params);
		return result;
	}


}