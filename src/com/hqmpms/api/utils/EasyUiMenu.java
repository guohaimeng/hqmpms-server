package com.hqmpms.api.utils;

import java.util.ArrayList;
import java.util.List;

public class EasyUiMenu {
    private String menuid;
    private String menuname;
    private List<EasyUiMenu> menus;
    private String url;
    private String type;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    private String code;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMenuid() {
        return menuid;
    }

    public void setMenuid(String menuid) {
        this.menuid = menuid;
    }

    public String getMenuname() {
        return menuname;
    }

    public void setMenuname(String menuname) {
        this.menuname = menuname;
    }

    public List<EasyUiMenu> getMenus() {
        return menus;
    }

    public void setMenus(List<EasyUiMenu> menus) {
        this.menus = menus;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void addChildren(EasyUiMenu menu){
        if(this.menus==null){
            this.setMenus(new ArrayList<EasyUiMenu>());
        }
        this.menus.add(menu);
    }
}
