package com.hqmpms.api.utils;

import java.util.ArrayList;
import java.util.List;

public class EasyUiTree {
    private String id;
    private String text;
    private List<EasyUiTree> children;
    private String attribute1;
    private String attribute2;
    private boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<EasyUiTree> getChildren() {
        return children;
    }

    public void setChildren(List<EasyUiTree> children) {
        this.children = children;
    }

    public String getAttribute1() {
        return attribute1;
    }

    public void setAttribute1(String attribute1) {
        this.attribute1 = attribute1;
    }

    public String getAttribute2() {
        return attribute2;
    }

    public void setAttribute2(String attribute2) {
        this.attribute2 = attribute2;
    }

    public void addChildren(EasyUiTree node){
        if(this.children==null){
            this.setChildren(new ArrayList<EasyUiTree>());
        }
        this.children.add(node);
    }
}
