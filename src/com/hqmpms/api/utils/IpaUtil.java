package com.hqmpms.api.utils;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.dd.plist.PropertyListParser;

import java.io.*;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Created by dml on  2018/5/18
 * 获取app文件详细信息
 */
public class IpaUtil {

   /**
    * 解压IPA文件，只获取IPA文件的Info.plist文件存储指定位置
    * @param file   zip文件
    * @param unzipDirectory     解压到的目录
    */
   private static File getZipInfo(File file, String unzipDirectory) throws Exception{
       // 定义输入输出流对象
       InputStream input = null;
       OutputStream output = null;
       File result = null;
       File unzipFile;
       ZipFile zipFile = null;
       try{
           // 创建zip文件对象
           zipFile = new ZipFile(file);
           // 创建本zip文件解压目录
           String name = file.getName().substring(0, file.getName().lastIndexOf("."));
           unzipFile = new File(unzipDirectory + "/" + name);
           if(unzipFile.exists()){
               unzipFile.delete();
           }
           unzipFile.mkdir();
           // 得到zip文件条目枚举对象
           Enumeration<? extends ZipEntry> zipEnum = zipFile.entries();
           // 定义对象
           ZipEntry entry;
           String entryName;
           String[] names;
           int length;
           // 循环读取条目
           while(zipEnum.hasMoreElements()){
               // 得到当前条目
               entry = zipEnum.nextElement();
               entryName =entry.getName();
               // 用/分隔条目名称
               names = entryName.split("/");
               length = names.length;
               for(int v = 0; v < length; v++){
                   if(entryName.endsWith(".app/Info.plist")){ // 为Info.plist文件,则输出到文件
                       input = zipFile.getInputStream(entry);
                       result = new File(unzipFile.getAbsolutePath() + "/Info.plist");
                       output = new FileOutputStream(result);
                       byte[] buffer = new byte[1024 * 8];
                       int readLen;
                       while((readLen = input.read(buffer, 0, 1024 * 8)) != -1){
                           output.write(buffer, 0, readLen);
                       }
                       break;
                   }
               }
           }
       }
       catch(Exception ex){
           System.out.println(ex);
       } finally{
           zipFile.close();
           if(output != null){
               output.flush();
               output.close();
           }
           if(input != null){
               input.close();
           }

       }

       // 如果有必要删除多余的文件
       if(file.exists()){
           file.delete();
       }
       return result;
   }

   /**
    * IPA文件的拷贝，把一个IPA文件复制为Zip文件,同时返回Info.plist文件 参数 oldfile 为 IPA文件
    */
   private static File getIpaInfo(File oldfile) throws IOException {
       InputStream inStream = null;
       FileOutputStream fs = null;
       try{
           int byteread;
           String filename = oldfile.getAbsolutePath().replaceAll(".ipa", ".zip");
           File newfile = new File(filename);
           if(oldfile.exists()){
               // 创建一个Zip文件
               inStream = new FileInputStream(oldfile);
               fs = new FileOutputStream(newfile);
               byte[] buffer = new byte[1444];
               while((byteread = inStream.read(buffer)) != -1){
                   fs.write(buffer, 0, byteread);
               }
               // 解析Zip文件
               return getZipInfo(newfile, newfile.getParent());
           }
       }
       catch(Exception e){
           e.printStackTrace();
       }finally {
           if (inStream!=null)
           inStream.close();
           if (fs!=null){
               fs.close();
           }
       }
       return null;
   }

   /**
    * 通过IPA文件获取Info信息
    */
 public    static Map<String, String> getVersionInfo(File ipa) throws Exception{

       File file = getIpaInfo(ipa);
       Map<String,String> map = new HashMap<>();
       // 需要第三方jar包dd-plist
       NSDictionary rootDict = (NSDictionary) PropertyListParser.parse(file);
       // 应用包名
       NSString parameters = (NSString)rootDict.objectForKey("CFBundleIdentifier");
       map.put("CFBundleIdentifier", parameters.toString());
       // 应用名称
       parameters = (NSString)rootDict.objectForKey("CFBundleName");
       map.put("CFBundleName", parameters.toString());
       // 应用版本
       parameters = (NSString)rootDict.objectForKey("CFBundleVersion");
       map.put("CFBundleVersion", parameters.toString());
       // 应用展示的名称
       parameters = (NSString)rootDict.objectForKey("CFBundleDisplayName");
       map.put("CFBundleDisplayName", parameters.toString());
       // 应用所需IOS最低版本
       parameters = (NSString)rootDict.objectForKey("MinimumOSVersion");
       map.put("MinimumOSVersion", parameters.toString());
       // 应用所需IOS最低版本
       parameters = (NSString)rootDict.objectForKey("CFBundleShortVersionString");
       map.put("CFBundleShortVersionString", parameters.toString());
       rootDict.clear();
       return map;
   }
}
