package com.hqmpms.dao;



import com.hqmpms.utils.MysqlBaseUtil;
import com.hqmpms.utils.SqlServerBaseUtil;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;


public class AdminDatabase {
	
	private static final Logger m_logger = Logger.getLogger(AdminDatabase.class);

     /**
      * @param os_type 系统类型
      * @return
      * @throws Exception
      */
	//不同类型的查询条件，统计记录数
		 public static int getAppCount(String os_type) throws Exception{
				int count = 0;			
			    String	sql = "";
			    if("1".equals(os_type) || os_type == "1"){
			    	sql = "select count(1) from yx_app_version where os_type = 1";
			    }else if("2".equals(os_type) || os_type == "2"){
			    	sql = "select count(2) from yx_app_version where os_type = 2";
			    }else{
			    	sql = "select count(0) from yx_app_version ";
			    }
				ArrayList<Object> params = new ArrayList<Object>();
				String result =  SqlServerBaseUtil.getOneColumnByRow(sql, params);
				if(result != null || !"".equals(result)){
					count = Integer.parseInt(result);
				}
			return count;
		}
		/**
		 * 
		 * @param os_type   系统类型
		 * @param update_version   版本号
		 * @return
		 * @throws Exception
		 */
		//刪除APP版本
		public static  int delAppLists(String os_type,String update_version) throws Exception{
			ArrayList<Object> params = new ArrayList<Object>();
			params.add(os_type)	;
			params.add(update_version);
			String sql = "delete from yx_app_version where os_type = ? and update_version = ?";	
			return SqlServerBaseUtil.updateSQL(sql, params);
		}

		/**
		 * @param ostype  系统类型
		 * @param page    当前页数
		 * @param pageSize  每页显示的记录数
		 * @return
		 * @throws Exception
		 */
		//按系统类型查询信息
		public static ArrayList<HashMap<String, String>> getAppType(int ostype,int page,int pageSize)  throws Exception{
			ArrayList<Object> param = new ArrayList<Object>();
			param.add(ostype);
			String sql = "select * from(select row_number()over(order by upload_date desc)as row_num,y.os_type,y.update_version,y.update_description,y.upload_date,y.open_type from yx_app_version as y where y.os_type = ?) as a where row_num between ? and ?;";		
			param.add(((page-1)*pageSize)+1);
			param.add((page)*pageSize);
			return SqlServerBaseUtil.querySql(sql, param);
		}
		/**
		 * @param desc  更新内容
		 * @param cate  更新方式
		 * @param op_ty 是否公开
		 * @param ver   版本号
		 * @return
		 * @throws Exception
		 */
		//修改app更新说明
		public static int updateApp(String desc,String cate,String op_ty,String ver) throws Exception{
			ArrayList<Object> param = new ArrayList<Object>();
			param.add(desc);
			param.add(cate);
			param.add(op_ty);
			param.add(ver);
			String sql = "update yx_app_version set update_description =?,category=?,open_type=? where update_version=?";		
			return SqlServerBaseUtil.updateSQL(sql, param);
		}




		/**
		 * @param page  当前页数
		 * @param pageSize 每页显示的记录数
		 * @return
		 * @throws Exception
		 */
		//App版本列表查询带分页
		    public  static  ArrayList<HashMap<String, String>> getAppLists(int page,int pageSize) throws Exception{
		    String sql = "select * from(select row_number() over (order by upload_date desc) as row_num,y.os_type,y.update_version,y.update_description,y.upload_date,y.category,y.open_type,y.download_url from yx_app_version as y) as a where row_num between ? and ?";
			ArrayList<Object> params = new ArrayList<Object>();
			
			params.add(((page-1)*pageSize)+1);
			params.add((page)*pageSize);
			return SqlServerBaseUtil.querySql(sql, params);
		}
		/**
		 * @param page  当前页数
		 * @param pageSize 每页显示的记录数
		 * @return
		 * @throws Exception
		 */
		//查询意见反馈列表
		public static ArrayList<HashMap<String, String>> getSuggList(int page,int pageSize) throws Exception{
			String sql = "select * from (select row_number() over (order by yx_feedback.create_time desc ) as rownum,yx_feedback.type,yx_feedback.content,yx_feedback.create_time,[user].username,[user].realname from yx_feedback left join [user] on yx_feedback.uid = [user].id) as t where t.rownum between ? and ? ;";
			ArrayList<Object> params = new ArrayList<Object>();
			params.add(((page-1)*pageSize)+1);
			params.add(page*pageSize);		
			return SqlServerBaseUtil.querySql(sql, params);
		}
		/**
		 * 
		 * @param type  问题类型
		 * @return
		 * @throws Exception
		 */
		//查询意见反馈列表记录数
		public static int getSuggCount(String type) throws Exception{
			int count = 0;
			String sql = "";
			if("1".equals(type)){
				sql ="select count(1) from yx_feedback where type = 1" ;
			}else if("2".equals(type)){
				sql ="select count(2) from yx_feedback where type = 2" ;
			}else if("3".equals(type)){
				sql ="select count(3) from yx_feedback where type = 3" ;
			}else{
				sql ="select count(0) from yx_feedback " ;
			}
			ArrayList<Object> params = new ArrayList<Object>();
			String result =  SqlServerBaseUtil.getOneColumnByRow(sql, params);
			if(result != null || !"".equals(result)){
				count = Integer.parseInt(result);
			}
		  return count;
	    }
		/** 	
		 * @param type   问题类型
		 * @param page   当前页数
		 * @param pageSize  每页显示的记录数
		 * @return
		 * @throws Exception
		 */
		// 按问题类型查询列表
		public static ArrayList<HashMap<String, String>> getSuggType(int type,int page,int pageSize)  throws Exception{
			ArrayList<Object> param = new ArrayList<Object>();
			param.add(type);
			String sql = "select * from (select row_number() over (order by yx_feedback.create_time desc ) as rownum,yx_feedback.type,yx_feedback.content,yx_feedback.create_time,[user].username,[user].realname from yx_feedback left join [user] on yx_feedback.uid = [user].id  where yx_feedback.type = ? ) as t where t.rownum between ? and ? ;";		
			param.add(((page-1)*pageSize)+1);
			param.add(page*pageSize);	
			return SqlServerBaseUtil.querySql(sql, param);
		}
}