package com.hqmpms.dao;

import com.hqmpms.utils.SqlServerBaseUtil;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class AppPublishDatabase {
	private static final Logger m_logger = Logger.getLogger(AppPublishDatabase.class);
	/**
	 * 版本号
	 * @param
	 * @return
	 * @throws SQLException 
	 */
	public static ArrayList<HashMap<String, String>> inquireVersion(String version,String ostype) throws Exception {		
		String sql = "select update_version from yx_app_version where update_version = ? and os_type=?";
		ArrayList<Object> param = new ArrayList<Object>();
		param.add(version);
		param.add(ostype);
		return SqlServerBaseUtil.querySql(sql, param);
	}
}
