package com.hqmpms.dao;

import com.hqmpms.utils.SqlServerBaseUtil;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class ClientLogDataBase {
	private static final Logger m_Logger = Logger.getLogger(ClientLogDataBase.class);
	
	
	
	
	/**
     * @param os_type 系统类型
     * @return
	  * @throws Exception 
     * @throws Exception
     */
	//不同类型的查询条件，统计记录数
	 public static int  getClientLogCount(String os_type) throws Exception{
		 int count = 0;
		 String sql = "";
		 if("1".equals(os_type) || os_type=="1"){
			 sql="select count(1) from yx_client_log where os_type = 1";
		 }else if("2".equals(os_type) || os_type=="2"){
			 sql="select count(2) from yx_client_log where os_type = 2";
		 }else{
			 sql="select count(0) from yx_client_log";
		 }
		 ArrayList<Object> params = new ArrayList<Object>();
		 String result = SqlServerBaseUtil.getOneColumnByRow(sql, params);
		 if(result!=null || !"".equals(result)){
			 count = Integer.parseInt(result);
			 
		 }
		 return count;
	 }
	
	
	/**
	 * @param ostype  系统类型
	 * @param page    当前页数
	 * @param pageSize  每页显示的记录数
	 * @return
	 * @throws Exception
	 */
	//按系统类型查询客户端日志
	public static ArrayList<HashMap<String, String>> getClientLogForType(int ostype,int page,int pageSize)  throws Exception{
		ArrayList<Object> param = new ArrayList<Object>();
		param.add(ostype);
		String sql = "select * from(select row_number()over(order by create_time desc)as row_num,y.id,y.file_url,y.os_type,y.file_name,y.log_type,y.create_time from yx_client_log as y where y.os_type = ?) as a where row_num between ? and ?;";		
		param.add(((page-1)*pageSize)+1);
		param.add((page)*pageSize);
		return SqlServerBaseUtil.querySql(sql, param);
	}
	
	

	/**
	 * 
	 * @param os_type   系统类型
	 * @return
	 * @throws Exception
	 */
	//按系统类型刪除客户端日志
	public static  int delClientLogLists(long id) throws Exception{
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(id);
		String sql = "delete from yx_client_log where id = ?";	
		return SqlServerBaseUtil.updateSQL(sql, params);
	}
	
	

	/**
	 * 上传文件
	 * @param  paperFile
	 * @return
	 * @throws SQLException
	 */
	public static void createFiles(HashMap<String, Object> paperFile) throws SQLException {
		 String sql = "insert into yx_client_log(id,file_name,file_url,create_time,log_type,os_type)values(?,?,?,getdate(),1,?)";
		 ArrayList<Object> param = new ArrayList<Object>();
		 param.add(paperFile.get("id"));
		 param.add(paperFile.get("name"));
		 param.add(paperFile.get("url"));
		 param.add(paperFile.get("os"));
		 
		 SqlServerBaseUtil.updateSQL(sql, param);
	}
	

}
