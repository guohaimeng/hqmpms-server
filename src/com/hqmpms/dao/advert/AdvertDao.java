package com.hqmpms.dao.advert;



import com.hqmpms.dispatcher.Startup;
import com.hqmpms.utils.MysqlBaseUtil;
import com.hqmpms.utils.SqlServerBaseUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 消息端口处理
 * 
 */

public class AdvertDao {
	public final static String REDIS_CITY_KEY="city.codes";

	private static final Logger m_logger = Logger.getLogger(AdvertDao.class);

	/**
	 * 端获取广告附件
	 * @param adid
	 * @return
	 * @throws SQLException
	 */
	public static  ArrayList<HashMap<String, String>> getAdvertAttach(String adid) throws SQLException {
		ArrayList<Object> params = new ArrayList<Object>();
		String Sql = "select attach_url as attachurl,attach_type as type,link_url as linkurl,order_no as orderno from yx_advert_attach where advert_id = ? order by order_no";
		params.add(adid);
		return SqlServerBaseUtil.querySql(Sql, params);
	}

	/**
	 * 添加广告附件
	 * @param condition
	 * @return
	 * @throws SQLException
	 */
	public static boolean addAdvert(Map<String,String > condition)throws Exception {
		try{
			ArrayList<Object> params = new ArrayList<Object>();
			String sql = "INSERT INTO tb_advert (advert_id,title,priority_level,creator_id,type,is_enable,Immediate_show,clicks,is_delete,product_id,begin_time,end_time,create_time";
			String  sqlval=" VALUES (?,?,?,?,?,?,?,0,0,?,?,?,now()";
			params.add(condition.get("adid"));
			params.add(condition.get("title"));
			params.add(condition.get("prioritylevel"));
			params.add(condition.get("uid"));
			params.add(condition.get("type"));
			params.add(condition.get("isenable"));
			params.add(condition.get("immediateshow"));
			params.add(condition.get("productid"));
			params.add(condition.get("begintime"));
			params.add(condition.get("endtime"));
			if(!StringUtils.isEmpty(condition.get("location"))){
				params.add(condition.get("location"));
				sql+=",location";
				sqlval+=",?";
			}
			if(!StringUtils.isEmpty(condition.get("linkurl"))){
				params.add(condition.get("linkurl"));
				sql+=",link_url";
				sqlval+=",?";
			}
			if(!StringUtils.isEmpty(condition.get("description"))){
				params.add(condition.get("description"));
				sql+=",advert_desc";
				sqlval+=",?";
			}
			sql+=")";
			sqlval+=")";
			sql=sql+sqlval;
			ArrayList<Object> attachparams = new ArrayList<Object>();
			String attachsql = "INSERT INTO tb_advert_picture (advert_picture_id,advert_id,advert_picture_url,picture_width,picture_height) VALUES";
			int count=0;
			String  sss = condition.get("jsonimg");
			JSONArray jsonArray = new JSONArray(condition.get("jsonimg"));
				for (int i=0;i<jsonArray.length();i++) {
					attachparams.add(String.valueOf(Startup.getId()));
					attachparams.add(condition.get("adid"));
					JSONObject jsonobject = jsonArray.getJSONObject(i);
					if(count==0){
						attachsql+="(?,?,?,?,?)";
						count++;
					}else{
						attachsql+=",(?,?,?,?,?)";
					}
					attachparams.add(jsonobject.getString("furl"));
					attachparams.add(jsonobject.getString("imgwidth"));
					attachparams.add(jsonobject.getString("imgheight"));
				}
			ArrayList<String> sqls = new ArrayList<>();
			sqls.add(sql);
			sqls.add(attachsql);

			ArrayList<ArrayList<Object>> paramslist = new ArrayList<>();
			paramslist.add(params);
			paramslist.add(attachparams);

			boolean flag = MysqlBaseUtil.batchUpdateSql(sqls,paramslist);

			return flag;
		}catch (Exception e) {
			m_logger.debug("RndSendCode fail"+e.getCause());
			throw e;
		}
	}



	/**
	 * 修改广告
	 * @param condition
	 * @return
	 * @throws SQLException
	 */
	public static boolean AdvertUpdate(Map<String,String > condition)throws Exception {
		ArrayList<String> sqls = new ArrayList<>();
		ArrayList<ArrayList<Object>> paramslist = new ArrayList<>();
		try{
			ArrayList<Object> params = new ArrayList<Object>();
			String sql = "UPDATE tb_advert set title=?,priority_level=?,creator_id=?,type=?,is_enable=?,Immediate_show=?,is_delete=0,product_id=?,begin_time=?,end_time=?,create_time=NOW()";
			params.add(condition.get("title"));
			params.add(condition.get("prioritylevel"));
			params.add(condition.get("uid"));
			params.add(condition.get("type"));
			params.add(condition.get("isenable"));
			params.add(condition.get("immediateshow"));
			params.add(condition.get("productid"));
			params.add(condition.get("begintime"));
			params.add(condition.get("endtime"));
			if(!StringUtils.isEmpty(condition.get("location"))){
				params.add(condition.get("location"));
				sql+=",location=?";
			}
			if(!StringUtils.isEmpty(condition.get("linkurl"))){
				params.add(condition.get("linkurl"));
				sql+=",link_url=?";
			}
			if(!StringUtils.isEmpty(condition.get("description"))){
				params.add(condition.get("advert_desc"));
				sql+=",advert_desc=?";
			}
             sql+=" where advert_id=?";
			params.add(condition.get("advertid"));
			ArrayList<Object> attachparams = new ArrayList<Object>();
			int count=0;
			String  jsonimg = condition.get("jsonimg");
			if(!StringUtils.isEmpty(jsonimg)) {
				String attachsql = "INSERT INTO tb_advert_picture (advert_picture_id,advert_id,advert_picture_url,picture_width,picture_height) VALUES";
				JSONArray jsonArray = new JSONArray(jsonimg);
				for (int i = 0; i < jsonArray.length(); i++) {
					attachparams.add(String.valueOf(Startup.getId()));
					attachparams.add(condition.get("advertid"));
					JSONObject jsonobject = jsonArray.getJSONObject(i);
					if (count == 0) {
						attachsql += "(?,?,?,?,?)";
						count = 1;
					} else {
						attachsql += ",(?,?,?,?,?)";
					}
					attachparams.add(jsonobject.getString("furl"));
					attachparams.add(jsonobject.getString("imgwidth"));
					attachparams.add(jsonobject.getString("imgheight"));
				}
				sqls.add(attachsql);
				paramslist.add(attachparams);
			}
			String  delimgid = condition.get("delimgid");
			if(!StringUtils.isEmpty(delimgid)){
				String attachsqldel = "delete from  tb_advert_picture where advert_id=? and advert_picture_id in(";
				String [] delimgidStr = delimgid.split(",");
                for(int i=0;i<delimgidStr.length;i++){
					if(i==0){
						attachsqldel+=Long.parseLong(delimgidStr[i]);
					}else{
						attachsqldel+=","+Long.parseLong(delimgidStr[i]);
					}
				}
				attachsqldel+=")";
				ArrayList<Object> attachparamsdel = new ArrayList<Object>();
				attachparamsdel.add(condition.get("advertid"));
				sqls.add(attachsqldel);
				paramslist.add(attachparamsdel);

			}

			sqls.add(sql);

			paramslist.add(params);
			boolean flag = MysqlBaseUtil.batchUpdateSql(sqls,paramslist);

			return flag;
		}catch (Exception e) {
			m_logger.debug("RndSendCode fail"+e.getCause());
			throw e;
		}
	}

	public static ArrayList<String> getAdvertId () throws SQLException{
		String sql ="select id from yx_advert where status=1 and begin_time<=getdate() and end_time>getdate() order by id";
		return SqlServerBaseUtil.getOneColumns(sql,new ArrayList<Object>());
	}


	/**
	 * 查询广告列表
	 * @param pagesize
	 * @param page
	 * @return
	 * @throws SQLException
     */
	public static  HashMap<String,Object>  getAdvertList (String productid,int page,int pagesize,String isenablesta,String isreadydel,String immediateshow,String advertstatus,String startdate, String enddate ) throws SQLException{
		HashMap<String,Object> map = new HashMap<String,Object>();

		String sqlcount ="select count(product_id) from tb_advert where 1=1 and product_id=? ";
		ArrayList<Object> paramscount = new ArrayList<>();
		paramscount.add(productid);
		String sql;
		ArrayList<Object> params;
		ArrayList<HashMap<String,String>> rows = new ArrayList<HashMap<String,String>>();
		sql ="SELECT product_id,advert_id,title,priority_level,link_url,create_time,type,location,Immediate_show,is_enable,begin_time,end_time,clicks,advert_desc as advertdesc,CONCAT(NOW(),'|',begin_time,'|',end_time) AS status,(SELECT user_name FROM tb_manager  WHERE manager_id = tbd.creator_id) AS user_name FROM tb_advert tbd where 1=1  and product_id=?  ";
		params = new ArrayList<>();
		params.add(productid);
		if(!StringUtils.isEmpty(isenablesta)){
			sqlcount+=" and is_enable=? ";
			paramscount.add(isenablesta);
			sql+=" and is_enable=? ";
			params.add(isenablesta);
		}
		if(!StringUtils.isEmpty(isreadydel)){
			sqlcount+=" and is_delete=? ";
			paramscount.add(isreadydel);
			sql+=" and is_delete=? ";
			params.add(isreadydel);
		}else{
			sqlcount+=" and is_delete=0 ";
			sql+=" and is_delete=0 ";
		}
		if(!StringUtils.isEmpty(immediateshow)){
			sqlcount+=" and Immediate_show=? ";
			paramscount.add(immediateshow);
			sql+=" and Immediate_show =? ";
			params.add(immediateshow);
		}
		if(!StringUtils.isEmpty(advertstatus)){
			if(advertstatus.equals("1")){
				sqlcount+=" and begin_time < NOW() and end_time < NOW()";
				sql+=" and begin_time < NOW() and end_time < NOW()";
			}else if(advertstatus.equals("2")){
				sqlcount+=" and NOW()>= begin_time  and NOW()<=end_time ";
				sql+=" and NOW()>= begin_time  and NOW()<=end_time ";
			}else{
				sqlcount+=" and  end_time>NOW() and begin_time > NOW() ";
				sql+=" and  end_time>NOW() and begin_time > NOW() ";
			}

		}
		if(!StringUtils.isEmpty(startdate)){
			sqlcount+=" and create_time >=? ";
			paramscount.add(startdate);
			sql+=" and create_time >=? ";
			params.add(startdate);
		}
		if(!StringUtils.isEmpty(enddate)){
			sqlcount+=" and create_time <=? ";
			paramscount.add(enddate);
			sql+=" and create_time <=? ";
			params.add(enddate);
		}
		sql+=" ORDER BY create_time DESC,advert_id DESC LIMIT ?,?";
		params.add(((page-1)*pagesize));
		params.add(pagesize);
		String   count = MysqlBaseUtil.getOneColumnByRow(sqlcount,paramscount);
		if(Integer.parseInt(count)>0){
			rows = MysqlBaseUtil.querySql(sql,params);
		}
		map.put("count",Integer.parseInt(count));
		map.put("rows",rows);
		return map;
	}

	/**
	 * 获取单条广告
	 * @param advertid
	 */

	public static HashMap<String,String> getAdvertByAdvertId(String advertid) throws SQLException{
		String sql ="SELECT advert_id,title FROM tb_advert where advert_id=?";
		ArrayList<Object> param = new ArrayList<Object>();
		param.add(advertid);
		return MysqlBaseUtil.getOneRow(sql,param);
	}
	



	/**
	 * 删除广告及广告图片
	 * @param advertid
     */
	public static void deleteById(String advertid,String  productid){
		String adsql ="UPDATE tb_advert set is_delete=1 WHERE  advert_id=? and product_id=?";
		ArrayList<Object> paramad =new ArrayList<>();
		paramad.add(advertid);
		paramad.add(productid);
		String adtsql =" delete from tb_advert_picture WHERE  advert_id=?";
		ArrayList<Object> paramAdt = new ArrayList<>();
		paramAdt.add(advertid);

		ArrayList<String> sqls = new ArrayList<>();
		sqls.add(adsql);
		sqls.add(adtsql);

		ArrayList<ArrayList<Object>> paramss = new ArrayList<>();

		paramss.add(paramad);
		paramss.add(paramAdt);

		MysqlBaseUtil.batchUpdateSql(sqls,paramss);
	}

	/**
	 * 查询广告图片
	 * @param advertid
	 * @return
	 * @throws SQLException
	 */
	public static  ArrayList<HashMap<String,String>> getAdvertPicture (String advertid) throws SQLException{
		String sql ="SELECT  advert_picture_url,advert_picture_id FROM tb_advert_picture WHERE  advert_id=?";
		ArrayList<Object> param = new ArrayList<>();
		param.add(advertid);
		return MysqlBaseUtil.querySql(sql,param);
	}

	/**
	 * 获取客户端最匹配广告图片
	 * @param adtype
	 * @param imgwidth
	 * @param imgheight
	 * @return
	 * @throws SQLException
	 */
	public static  ArrayList<HashMap<String,String>> getClientAdvertPicture (String  productid,String adtype,String imgwidth,String imgheight) throws SQLException{
		String sql ="SELECT advert_id,title,advert_picture_url,link_url,location,TYPE,picture_height,picture_width,create_time,begin_time,end_time,is_enable, product_id FROM (" +
				" SELECT tad.advert_id,tad.title,tdp.advert_picture_url,tad.link_url,tad.location,tad.type,tdp.picture_width AS picture_width ,tdp.picture_height AS picture_height, " +
				" tdp.abs_picture_width,tdp.abs_picture_height,create_time,tad.begin_time,tad.end_time,tad.is_enable,tad.product_id  FROM tb_advert tad " +
				" INNER JOIN (SELECT advert_id, advert_picture_url,ABS(picture_width-?) AS abs_picture_width ,ABS(picture_height-?) AS abs_picture_height ,picture_width ,picture_height  FROM tb_advert_picture " +
				" ORDER BY abs_picture_width,abs_picture_height ) AS tdp " +
				" ON tdp.advert_id=tad.advert_id WHERE tad.type =? AND tad.product_id=? AND is_enable=1 AND NOW()>=begin_time AND NOW()<=end_time  ORDER BY abs_picture_width,abs_picture_height,create_time\n" +
				" ) AS retad LIMIT 1 ";
		ArrayList<Object> param = new ArrayList<>();
		param.add(imgwidth);
		param.add(imgheight);
		param.add(adtype);
		param.add(productid);
		return MysqlBaseUtil.querySql(sql,param);
	}




	
	/**
	 * 删除广告附件
	 * @param id
	 * @throws SQLException 
     */
	public static void deleteAttachById(String id) throws SQLException{
		String sql =" delete from yx_advert_attach where id=?";
		ArrayList<Object> params = new ArrayList<>();
		params.add(id);
		SqlServerBaseUtil.updateSQL(sql,params);
	}
	
	
	
	
	/**
	 * 附件总数
	 * @return
	 * @throws SQLException
     */
	public static  String attachqueryCount(String id) throws SQLException{
		String sql = "select count(1) from yx_advert_attach where advert_id=?";
		ArrayList<Object> param = new ArrayList<>();
		param.add(id);
		return SqlServerBaseUtil.getOneColumnByRow(sql,param);
	}
	
	
	/**
	 * 查询广告附件列表
	 * @param pagesize
	 * @param pagenum
	 * @return
	 * @throws SQLException
     */
	public static  ArrayList<HashMap<String,String>> advertAttachList (String id,String pagesize,String pagenum ) throws SQLException{
		String sql ="select * from(select row_number()over(order by order_no desc)as row_num,y.id,y.advert_id,y.attach_url,y.attach_type,y.link_url,y.order_no from yx_advert_attach as y where y.advert_id = ?) as a where row_num>? and row_num<=?;";
		ArrayList<Object> param = new ArrayList<>();
		param.add(id);
		param.add((Integer.parseInt(pagenum)-1)*Integer.parseInt(pagesize));
		param.add(Integer.parseInt(pagenum)*Integer.parseInt(pagesize));

		return SqlServerBaseUtil.querySql(sql,param);
	}
	
	
	/**
	 * 增加广告附件
	 * @param advertattach
	 * @return 
	 * @return
	 * @throws SQLException
     */
	public static  int advertAttachAdd (Map<String,String>  advertattach) throws SQLException{
		String sql = "INSERT INTO [yx_advert_attach] (ID,advert_id,attach_url,attach_type,link_url,order_no) VALUES (?,?,?,0,?,0)";
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(advertattach.get("adid"));
		params.add(advertattach.get("advertid"));
		params.add(advertattach.get("attachurl"));
		params.add(advertattach.get("linkurl"));

		return SqlServerBaseUtil.updateSQL(sql,params);
	}
	
	
	
	
	
	
 }