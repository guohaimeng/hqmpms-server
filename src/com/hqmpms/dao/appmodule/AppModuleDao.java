package com.hqmpms.dao.appmodule;

import com.hqmpms.utils.MysqlBaseUtil;
import com.hqmpms.utils.SqlServerBaseUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * app功能模块接口
 * Created by dml on  2018/7/25
 */
public class AppModuleDao {

    /**
     * 统计app功能模块总条数
     * @param status            状态
     * @param isIndex           是否首页展示
     * @param search            模糊查询条件
     */
    public static long getAppModuleCount(String userType,String status,String isIndex,String search) throws SQLException {
        long count = 0;
        String sql = "SELECT COUNT(module_id) FROM yx_app_module WHERE is_delete = 0 AND user_type = ?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(userType);
        if (!StringUtils.isEmpty(status)) {
            sql += "  AND status = ?";
            params.add(status);
        }
        if (!StringUtils.isEmpty(isIndex)) {
            sql += " AND is_index = ?";
            params.add(isIndex);
        }
        if (!StringUtils.isEmpty(search)){
            sql += " AND module_name LIKE '%"+search+"%';";
        }
        String result = SqlServerBaseUtil.getOneColumnByRow(sql, params);
        if (!StringUtils.isEmpty(result)) {
            count = Long.parseLong(result);
        }
        return count;
    }

    /**
     * 查询app功能模块列表
     * @param userType          用户类型
     * @param status            状态
     * @param isIndex           是否首页展示
     * @param search            模糊查询
     * @param page              当前页码
     * @param pageSize        页面大小
     */
    public  static List<HashMap<String, String>> getAppModuleList(String userType,String status,String isIndex,String search,int page, int pageSize) throws SQLException{
        String sql = "SELECT * from yx_app_module WHERE user_type = ? ";
        ArrayList<Object> params = new ArrayList<>();
        params.add(userType);
        if (!StringUtils.isEmpty(status)) {
            sql += " AND status = ?";
            params.add(status);
        }
        if (!StringUtils.isEmpty(isIndex)) {
            sql += " AND is_index = ?";
            params.add(isIndex);
        }
        if (!StringUtils.isEmpty(search)){
            sql += " AND module_name LIKE '%"+search+"%'";
        }
        sql += " AND is_delete = 0 ORDER BY order_num asc OFFSET ? ROWS FETCH NEXT ? ROWS ONLY ";
        params.add((page-1)*pageSize);
        params.add(pageSize);
        return SqlServerBaseUtil.querySql(sql, params);
    }

    /**
     * 判断是否唯一
     * @param moduleId                  模块id
     * @param moduleName            模块名称
     * @param orderNum                  序号
     * @param moduleCode            模块唯一编码
     */
    public static int isOnlyOne(String moduleId,String userType,String moduleName,String orderNum,String moduleCode) throws SQLException{
        String sql;
        ArrayList<Object> params;
        List<HashMap<String, String>>  moduleList;
        //新增
        if (StringUtils.isEmpty(moduleId)){
            if (!StringUtils.isEmpty(moduleName)){
                sql = "SELECT module_id FROM yx_app_module WHERE is_delete=0 AND module_name = ? AND user_type = ?;";
                params = new ArrayList<>();
                params.add(moduleName);
                params.add(userType);
                moduleList = SqlServerBaseUtil.querySql(sql, params);
                if (!CollectionUtils.isEmpty(moduleList)){
                    return -1;
                }
            }
            if (!StringUtils.isEmpty(orderNum)){
                sql = "SELECT module_id FROM yx_app_module WHERE is_delete=0 AND order_num = ? AND user_type = ?;";
                params = new ArrayList<>();
                params.add(orderNum);
                params.add(userType);
                moduleList = SqlServerBaseUtil.querySql(sql, params);
                if (!CollectionUtils.isEmpty(moduleList)){
                    return -2;
                }
            }
            if (!StringUtils.isEmpty(moduleCode)){
                sql = "SELECT module_id FROM yx_app_module WHERE is_delete=0 AND module_code = ? AND user_type = ?;";
                params = new ArrayList<>();
                params.add(moduleCode);
                params.add(userType);
                moduleList = SqlServerBaseUtil.querySql(sql, params);
                if (!CollectionUtils.isEmpty(moduleList)){
                    return -3;
                }
            }
        }else {
            if (!StringUtils.isEmpty(moduleName)){
                sql = "SELECT module_id FROM yx_app_module WHERE is_delete=0 AND module_name = ? AND module_id !=? AND user_type = ?;";
                params = new ArrayList<>();
                params.add(moduleName);
                params.add(moduleId);
                params.add(userType);
                moduleList = SqlServerBaseUtil.querySql(sql, params);
                if (!CollectionUtils.isEmpty(moduleList)){
                    return -1;
                }
            }
            if (!StringUtils.isEmpty(orderNum)){
                sql = "SELECT module_id FROM yx_app_module WHERE is_delete=0 AND order_num = ? AND module_id !=? AND user_type = ?;";
                params = new ArrayList<>();
                params.add(orderNum);
                params.add(moduleId);
                params.add(userType);
                moduleList = SqlServerBaseUtil.querySql(sql, params);
                if (!CollectionUtils.isEmpty(moduleList)){
                    return -2;
                }
            }
            if (!StringUtils.isEmpty(moduleCode)){
                sql = "SELECT module_id FROM yx_app_module WHERE is_delete=0 AND module_code = ? AND module_id !=? AND user_type = ?;";
                params = new ArrayList<>();
                params.add(moduleCode);
                params.add(moduleId);
                params.add(userType);
                moduleList = SqlServerBaseUtil.querySql(sql, params);
                if (!CollectionUtils.isEmpty(moduleList)){
                    return -3;
                }
            }
        }
        return 0;
    }

    /**
     * 查询app版本号列表
     * @param osType        平台
     */
    public  static List<HashMap<String, String>> getAppVersionByOsType(String osType) throws SQLException{
        String sql = "SELECT app_version AS version FROM tb_client_version_test WHERE test_phase = 3 AND status = 3 AND test_id IN (SELECT test_id FROM tb_release_list WHERE is_open = 1 AND is_delete = 0 AND product_id = (SELECT product_id FROM tb_product WHERE product_code = 'yext')) ";
        ArrayList<Object> params = new ArrayList<>();
        if (!"0".equals(osType)){
            sql += " AND os_type = ?;";
            params.add(osType);
        }
        return MysqlBaseUtil.querySql(sql, params);
    }

    /**
     * app功能模块新增
     */
    public static int  appModuleAdd(long moduleId,String moduleName,String iconUrl,String status,String isIndex,String orderNum,String moduleType,String moduleLink,String moduleTag,String moduleCode,String userType) throws SQLException{
        String sql = "INSERT INTO yx_app_module (module_id,module_name,icon_url,create_time,status,is_delete,is_index,order_num,module_type,module_tag,module_code,user_type,module_link) VALUES(?,?,?,getdate(),?,0,?,?,?,?,?,?,?)";
        ArrayList<Object> params = new ArrayList<>();
        params.add(moduleId);
        params.add(moduleName);
        params.add(iconUrl);
        params.add(status);
        params.add(isIndex);
        params.add(orderNum);
        params.add(moduleType);
        params.add(moduleTag);
        params.add(moduleCode);
        params.add(userType);
        params.add(moduleLink);
        return SqlServerBaseUtil.updateSQL(sql, params);
    }

    /**
     * app功能模块修改
     */
    public static int  appModuleUpdate(String moduleName,String iconUrl,String status,String isIndex,String orderNum,String moduleType,String moduleLink,String moduleTag,String moduleCode,String userType,String moduleId) throws SQLException{
        String sql = "UPDATE yx_app_module SET module_name=?,icon_url=?,status=?,is_index=?,order_num=?,module_type=?,module_link=?,module_tag=?,module_code=?,user_type=? WHERE module_id = ?;";
        ArrayList<Object> params = new ArrayList<>();
        params.add(moduleName);
        params.add(iconUrl);
        params.add(status);
        params.add(isIndex);
        params.add(orderNum);
        params.add(moduleType);
        params.add(moduleLink);
        params.add(moduleTag);
        params.add(moduleCode);
        params.add(userType);
        params.add(moduleId);
       return SqlServerBaseUtil.updateSQL(sql, params);
    }

    /**
     * app功能模块删除(逻辑删除)
     */
    public static int  appModuleDelete(String moduleId) throws SQLException{
        String sql = "UPDATE yx_app_module SET is_delete = 1 WHERE module_id = ?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(moduleId);
       return SqlServerBaseUtil.updateSQL(sql, params);
    }

    /**
     * 获取app功能模块权限列表
     */
    public static List<HashMap<String, String>>  getAppModulePower(String moduleId) throws SQLException{
        String sql = "SELECT * FROM yx_app_module_power WHERE module_id = ? AND is_delete = 0 ;";
        ArrayList<Object> params = new ArrayList<>();
        params.add(moduleId);
        return  SqlServerBaseUtil.querySql(sql, params);
    }

    /**
     * app功能模块权限新增
     */
    public static int  appModulePowerAdd(long modulePowerId,long moduleId,String moduleArea,String osType,String versions,String userIds) throws SQLException{
        String sql = "INSERT INTO yx_app_module_power (module_power_id,module_id,module_area,os_type,versions,is_delete,module_power_user_id) VALUES(?,?,?,?,?,0,?)";
        ArrayList<Object> params = new ArrayList<>();
        params.add(modulePowerId);
        params.add(moduleId);
        params.add(moduleArea);
        params.add(osType);
        params.add(versions);
        params.add(userIds);
        return SqlServerBaseUtil.updateSQL(sql, params);
    }

    /**
     * app功能模块权限修改
     */
    public static int  appModulePowerUpdate(String moduleId,String moduleAreaId,String osType,String versions,String modulePowerId) throws SQLException{
        String sql = "UPDATE yx_app_module_power SET module_id=?,module_area_id=?,os_type=?,versions=? WHERE module_power_id = ?;";
        ArrayList<Object> params = new ArrayList<>();
        params.add(moduleId);
        params.add(moduleAreaId);
        params.add(osType);
        params.add(versions);
        params.add(modulePowerId);
        return SqlServerBaseUtil.updateSQL(sql, params);
    }

    /**
     * app功能模块权限删除(逻辑删除)
     */
    public static int  appModulePowerDelete(String moduleId) throws SQLException{
        String sql = "UPDATE yx_app_module_power SET is_delete = 1 WHERE module_power_id = ?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(moduleId);
        return SqlServerBaseUtil.updateSQL(sql, params);
    }
}
