package com.hqmpms.dao.apprelease;

import com.hqmpms.utils.MysqlBaseUtil;
import org.apache.commons.lang3.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 版本发布接口
 * Created by dml on  2018/5/18
 */
public class AppReleaseDao {

    /**
     * 统计版本发布列表总条数
     * @param osType 系统类型
     */
    public static long getAppReleaseCount(String osType,String productId,String startDate,String endDate) throws SQLException {
        long count = 0;
        String sql;
        ArrayList<Object> params = new ArrayList<>();
        if ("1".equals(osType)) {
            sql = "SELECT COUNT(release_id) FROM tb_release_list WHERE test_id IN(SELECT test_id FROM tb_client_version_test WHERE os_type = 1) AND is_delete = 0 AND product_id = ?";
        } else if ("2".equals(osType)) {
            sql = "SELECT COUNT(release_id) FROM tb_release_list WHERE test_id IN(SELECT test_id FROM tb_client_version_test WHERE os_type = 2) AND is_delete = 0 AND product_id = ?";
        } else {
            sql = "SELECT COUNT(release_id) FROM tb_release_list WHERE is_delete = 0 AND product_id = ?";
        }
        params.add(productId);
        if(!StringUtils.isEmpty(startDate)){
            sql += " AND create_time >= ? ";
            params.add(startDate);
        }
        if(!StringUtils.isEmpty(endDate)){
            sql += "AND create_time <= ? ";
            params.add(endDate);
        }
        String result = MysqlBaseUtil.getOneColumnByRow(sql, params);
        if (!StringUtils.isEmpty(result)) {
            count = Long.parseLong(result);
        }
        return count;
    }

    /**
     * 查询版本发布列表(只有分页)
     * @param page  当前页数
     * @param pageSize 每页显示的记录数
     */
    public  static List<HashMap<String, String>> getAppReleaseLists(String productId,String startDate,String endDate,int page, int pageSize) throws SQLException{
        String sql = "SELECT a.*,b.*,c.real_name as RealName FROM tb_client_version_test a INNER JOIN tb_release_list b on a.test_id = b.test_id INNER JOIN tb_manager c ON a.creator_id = c.manager_id WHERE b.is_delete=0 AND b.product_id = ? ";
        ArrayList<Object> params = new ArrayList<>();
        params.add(productId);
        if(!StringUtils.isEmpty(startDate)){
            sql += " AND b.create_time >= ? ";
            params.add(startDate);
        }
        if(!StringUtils.isEmpty(endDate)){
            sql += "AND b.create_time <= ? ";
            params.add(endDate);
        }
        sql += " ORDER BY b.create_time desc limit ?, ?";
        params.add((page-1)*pageSize);
        params.add(pageSize);
        return MysqlBaseUtil.querySql(sql, params);
    }

    /**
     * 查询版本发布列表(系统类型、分页)
     * @param osType  系统类型
     * @param page    当前页数
     * @param pageSize  每页显示的记录数
     */
    public static List<HashMap<String, String>> getAppReleaseType(int osType,String productId,String startDate,String endDate, int page, int pageSize)  throws SQLException{
        ArrayList<Object> param = new ArrayList<>();
        param.add(osType);
        param.add(productId);
        String sql = "SELECT a.*,b.*,c.real_name as RealName FROM tb_client_version_test a INNER JOIN tb_release_list b on a.test_id = b.test_id INNER JOIN tb_manager c ON a.creator_id = c.manager_id WHERE a.os_type=? AND b.product_id=? AND b.is_delete=0 ";
        if(!StringUtils.isEmpty(startDate)){
            sql += " AND b.create_time >= ? ";
            param.add(startDate);
        }
        if(!StringUtils.isEmpty(endDate)){
            sql += "AND b.create_time <= ? ";
            param.add(endDate);
        }
        sql += " ORDER BY b.create_time desc limit ?, ?";
        param.add((page-1)*pageSize);
        param.add(pageSize);
        return MysqlBaseUtil.querySql(sql, param);
    }


    /**
     * app版本发布
     */
    public static int  appReleasePublish(long releaseId,int isMandatory,String remark,String creatorId,String testId,String productId) throws SQLException{
        String sql = "INSERT INTO tb_release_list VALUES(?,?,?,0,?,NOW(),0,?,?)";
        ArrayList<Object> params = new ArrayList<>();
        params.add(releaseId);
        params.add(isMandatory);
        params.add(remark);
        params.add(creatorId);
        params.add(testId);
        params.add(productId);
        return MysqlBaseUtil.updateSQL(sql, params);
    }

    /**
     * app版本发布修改
     */
    public static int  appReleaseUpdate(String remark,String isMandatory,String releaseId) throws SQLException{
        String sql = "UPDATE tb_release_list SET remark=?,is_mandatory=? WHERE release_id = ?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(remark);
        params.add(isMandatory);
        params.add(releaseId);
       return MysqlBaseUtil.updateSQL(sql, params);
    }

    /**
     * app版本发布修改-公开状态修改
     */
    public static int  appReleaseUpdateOpenStatus(String isOpen,String releaseId) throws SQLException{
        String sql = "UPDATE tb_release_list SET is_open=? WHERE release_id = ?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(isOpen);
        params.add(releaseId);
        return MysqlBaseUtil.updateSQL(sql, params);
    }

    /**
     * app版本发布删除(逻辑删除)
     */
    public static int  appReleaseDelete(String releaseId) throws SQLException {
        String sql = "UPDATE tb_release_list SET is_delete = 1 WHERE release_id = ?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(releaseId);
       return MysqlBaseUtil.updateSQL(sql, params);
    }
}
