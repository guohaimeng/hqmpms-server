package com.hqmpms.dao.apptest;

import com.hqmpms.dispatcher.Startup;
import com.hqmpms.utils.MysqlBaseUtil;
import org.apache.commons.lang3.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 测试版本接口
 * Created by dml on  2018/5/18
 */
public class AppTestDao {

    /**
     * 统计当前平台、版本号、测试阶段下的最高测试轮次
     * @param osType 系统类型
     */
    public static int getAppTestMaxTestRotation(String osType,String appVersion,String testPhase,String productId) throws SQLException {
        String sql ="SELECT IFNULL(MAX(test_rotation),0) FROM tb_client_version_test WHERE is_delete = 0 AND status !=-1 AND os_type = ? AND app_version = ? AND test_phase = ? AND product_id = ?;";
        ArrayList<Object> params = new ArrayList<>();
        params.add(osType);
        params.add(appVersion);
        params.add(testPhase);
        params.add(productId);
        String result = MysqlBaseUtil.getOneColumnByRow(sql, params);
        return result==null?1:Integer.valueOf(result)+1;
    }

    /**
     * 统计版本测试列表总条数
     * @param osType 系统类型
     */
    public static long getAppTestCount(String osType,String productId) throws SQLException {
        long count = 0;
        String sql;
        if ("1".equals(osType)) {
            sql = "select count(1) from tb_client_version_test where os_type = 1 and is_delete = 0 and product_id = ? ;";
        } else if ("2".equals(osType)) {
            sql = "select count(2) from tb_client_version_test where os_type = 2 and is_delete = 0 and product_id = ?;";
        } else {
            sql = "select count(0) from tb_client_version_test where is_delete = 0 and product_id = ?;";
        }
        ArrayList<Object> params = new ArrayList<>();
        params.add(productId);
        String result = MysqlBaseUtil.getOneColumnByRow(sql, params);
        if (!StringUtils.isEmpty(result)) {
            count = Long.parseLong(result);
        }
        return count;
    }

    /**
     * App版本测试列表查询带分页
     * @param page  当前页数
     * @param pageSize 每页显示的记录数
     */
    public  static List<HashMap<String, String>> getAppTestLists(String productId,int page, int pageSize) throws SQLException{
        String sql = "SELECT a.*,b.real_name as creator_name FROM tb_client_version_test a INNER JOIN tb_manager b on a.creator_id = b.manager_id WHERE a.is_delete=0 AND a.product_id = ? ORDER BY a.create_time desc limit ?, ?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(productId);
        params.add(((page-1)*pageSize));
        params.add(pageSize);
        return MysqlBaseUtil.querySql(sql, params);
    }

    /**
     * 按系统类型查询版本测试列表信息
     * @param osType  系统类型
     * @param page    当前页数
     * @param pageSize  每页显示的记录数
     */
    public static List<HashMap<String, String>> getAppTestType(int osType,String productId,int page,int pageSize)  throws SQLException{
        ArrayList<Object> param = new ArrayList<>();
        param.add(osType);
        param.add(productId);
        String sql = "SELECT a.*,b.real_name as creator_name FROM tb_client_version_test a INNER JOIN tb_manager b on a.creator_id = b.manager_id WHERE a.os_type=? AND a.is_delete=0 AND a.product_id = ? ORDER BY a.create_time desc LIMIT ?,?;";
        param.add((page-1)*pageSize);
        param.add(pageSize);
        return MysqlBaseUtil.querySql(sql, param);
    }

    /**
     * 查询正式版最高测试轮次的信息
     * @param osType  系统类型
     */
    public static List<HashMap<String, String>> getTestPhase3(String productId,String osType)  throws SQLException{
        ArrayList<Object> param = new ArrayList<>();
        param.add(productId);
        param.add(osType);
        String sql = "SELECT test_id,app_version FROM tb_client_version_test WHERE is_delete = 0 AND product_id = ? AND test_phase = 3 AND os_type = ? AND test_id NOT IN (SELECT test_id FROM tb_release_list WHERE is_delete=0);";
        return MysqlBaseUtil.querySql(sql, param);
    }


    /**
     * 根据版本测试编号查询版本测试记录总条数
     * @param testId        版本测试编号
     */
    public static long totalGetAppTestRecord(String testId)  throws SQLException{
        long count = 0;
        ArrayList<Object> params = new ArrayList<>();
        params.add(testId);
        String sql = "SELECT COUNT(test_record_id) FROM  tb_test_record  WHERE  test_id = ? ;";
        String result = MysqlBaseUtil.getOneColumnByRow(sql, params);
        if (!StringUtils.isEmpty(result)) {
            count = Long.parseLong(result);
        }
        return count;
    }

    /**
     * 根据版本测试编号查询版本测试记录列表
     * @param testId        版本测试编号
     * @param page         当前页码
     * @param pageSize  每页显示的记录数
     */
    public static List<HashMap<String, String>> listGetAppTestRecord(String testId,int page,int pageSize)  throws SQLException{
        ArrayList<Object> param = new ArrayList<>();
        param.add(testId);
        String sql = "SELECT a.*,b.remark as Remark,b.alter_to_status,b.create_time,c.real_name as RealName FROM tb_client_version_test a INNER JOIN tb_test_record b on a.test_id = b.test_id INNER JOIN tb_manager c ON b.creator_id = c.manager_id WHERE  a.is_delete=0 AND a.test_id = ? ORDER BY b.create_time desc LIMIT ?,?;";
        param.add((page-1)*pageSize);
        param.add(pageSize);
        return MysqlBaseUtil.querySql(sql, param);
    }

    /**
     * 根据版本测试平台&版本号查询列表
     * @param osType        平台
     * @param appVersion  版本号
     */
    public static HashMap<String, String> getAppTestPhase(String osType,String appVersion,String productId)  throws SQLException{
        ArrayList<Object> param = new ArrayList<>();
        String sql = "SELECT test_phase,status FROM tb_client_version_test WHERE os_type = ? AND app_version = ? AND product_id = ? AND is_delete =0 ORDER BY test_phase DESC LIMIT 1;";
        param.add(osType);
        param.add(appVersion);
        param.add(productId);
        return MysqlBaseUtil.getOneRow(sql, param);
    }

    /**
     * 根据版本测试编号&版本状态查询版本测试说明
     * @param testId        测试编号
     * @param appStatus 版本号
     */
    public static HashMap<String, String> getAppTestRecordRemark(String testId,String appStatus)  throws SQLException{
        ArrayList<Object> param = new ArrayList<>();
        String sql = "SELECT DISTINCT(remark) FROM tb_test_record WHERE test_id = ? AND alter_to_status =?";
        param.add(testId);
        param.add(appStatus);
        return MysqlBaseUtil.getOneRow(sql, param);
    }

    /**
     * app版本测试发布
     */
    public static int  appTestPublish(long testId,String osType,String appVersion,String buildCode,String fileName,String fileUrl,String testPhase,int testRotation,String creatorId,String remark,String productId,String qrCodeUrl,String fileSize) throws SQLException{
        String sql = "INSERT INTO tb_client_version_test VALUES(?,?,?,?,?,?,?,?,?,NOW(),0,?,'0',?,?,?)";
        ArrayList<Object> params = new ArrayList<>();
        params.add(testId);
        params.add(osType);
        params.add(appVersion);
        params.add(buildCode);
        params.add(fileName);
        params.add(fileUrl);
        params.add(testPhase);
        params.add(testRotation);
        params.add(creatorId);
        params.add(remark);
        params.add(productId);
        params.add(qrCodeUrl);
        params.add(fileSize);
        return MysqlBaseUtil.updateSQL(sql, params);
    }

    /**
     * app版本测试修改
     */
    public static int  appTestUpdate(String testPhase,String remark,String testRotation,String testId) throws SQLException{
        String sql = "UPDATE tb_client_version_test SET test_phase=?,remark=? ";
        ArrayList<Object> params = new ArrayList<>();
        params.add(testPhase);
        params.add(remark);
        if("1".equals(testRotation)){
            sql+=",test_rotation = ?";
            params.add(testRotation);
        }
        sql+=" WHERE test_id = ?;";
        params.add(testId);
       return MysqlBaseUtil.updateSQL(sql, params);
    }

    /**
     * app版本测试状态更改
     */
    public static boolean  appTestStatusChange(String status,String testId,String remark,String creatorId){
        String updSql = "UPDATE tb_client_version_test SET status =? WHERE test_id = ?";
        ArrayList<Object> updParams = new ArrayList<>();
        updParams.add(status);
        updParams.add(testId);
        String insertSql = "INSERT INTO tb_test_record VALUES(?,?,?,NOW(),?,?);";
        ArrayList<Object> insertParams = new ArrayList<>();
        insertParams.add(Startup.getId());
        insertParams.add(remark);
        insertParams.add(creatorId);
        insertParams.add(testId);
        insertParams.add(status);
        List<String> sqlList = new ArrayList<>();
        sqlList.add(updSql);
        sqlList.add(insertSql);
        ArrayList<ArrayList<Object>> paramsList = new ArrayList<>();
        paramsList.add(updParams);
        paramsList.add(insertParams);
        return MysqlBaseUtil.batchUpdateSql(sqlList,paramsList);
    }

    /**
     * app版本测试删除(逻辑删除)
     */
    public static int  appTestDelete(String testId) throws SQLException{
        ArrayList<Object> params = new ArrayList<>();
        String sql = "SELECT status FROM  tb_client_version_test WHERE test_id = ?";
        params.add(testId);
        HashMap<String,String> testStatus =  MysqlBaseUtil.getOneRow(sql,params);
        if(null!=testStatus&&!"0".equals(testStatus.get("status"))){
            return -1;
        }
        sql = "UPDATE tb_client_version_test SET is_delete = 1 WHERE test_id = ?";
        params = new ArrayList<>();
        params.add(testId);
       return MysqlBaseUtil.updateSQL(sql, params);
    }
}
