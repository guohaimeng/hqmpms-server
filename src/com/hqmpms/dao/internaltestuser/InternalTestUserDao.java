package com.hqmpms.dao.internaltestuser;

import com.hqmpms.utils.MysqlBaseUtil;
import com.hqmpms.utils.SqlServerBaseUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 内测用户
 * Created by dml on  2018/5/22
 */
public class InternalTestUserDao {

    /**
     * 根据用户名(账号)精确查找用户
     */
    public  static List<HashMap<String, String>> getUserListsByName(String userName) throws SQLException{
        String sql = "SELECT ID,UserName,RealName FROM [User] WHERE UserName = ?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(userName);
        return SqlServerBaseUtil.querySql(sql, params);
    }

    /**
     * 查询内测用户列表总条数
     * @param productId     产品编号
     */
    public  static long totalGetAllTestUser(String productId) throws SQLException{
        long count =0;
        String sql = "SELECT COUNT(internal_test_user_id) FROM tb_internal_test_user WHERE is_delete = 0 AND product_id = ?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(productId);
        String result = MysqlBaseUtil.getOneColumnByRow(sql, params);
        if (!StringUtils.isEmpty(result)) {
            count = Long.parseLong(result);
        }
        return count;
    }
    /**
     * 查询内测用户列表
     * @param productId  产品编号
     * @param page  当前页数
     * @param pageSize 每页显示的记录数
     */
    public  static List<HashMap<String, String>> listGetTestUserLists(String productId,int page, int pageSize) throws SQLException{
        List<HashMap<String, String>> internalTestUserList = new ArrayList<>();
        String sql = "SELECT * FROM tb_internal_test_user WHERE is_delete =0 AND product_id = ? LIMIT ?, ?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(productId);
        params.add((page-1)*pageSize);
        params.add(pageSize);
        ArrayList<HashMap<String, String>> testUserList = MysqlBaseUtil.querySql(sql,params);
        //非空后遍历，转库查询
        if (!CollectionUtils.isEmpty(testUserList)){
            for (HashMap<String, String> user:testUserList) {
                sql = "SELECT ID as UserID,UserName,RealName,Mobile FROM [User] WHERE ID = ?";
                params = new ArrayList<>();
                params.add(user.get("user_id"));
                HashMap<String,String> sqlServerUser = SqlServerBaseUtil.getOneRow(sql,params);
                sqlServerUser.put("id",user.get("internal_test_user_id"));
                internalTestUserList.add(sqlServerUser);
            }
        }
        return internalTestUserList;
    }

    /**
     * 内测用户新增
     */
    public static int  internalTestUserAdd(long id,String userId,String productId) throws SQLException{
        String sql = "SELECT user_id FROM tb_internal_test_user WHERE user_id = ? AND product_id = ? AND is_delete = 0;";
        ArrayList<Object> params = new ArrayList<>();
        params.add(userId);
        params.add(productId);
        ArrayList<HashMap<String, String>> testUserList = MysqlBaseUtil.querySql(sql,params);
        if (!CollectionUtils.isEmpty(testUserList)){
            return -2;
        }
        sql = "INSERT INTO tb_internal_test_user VALUES(?,0,?,?)";
        params = new ArrayList<>();
        params.add(id);
        params.add(userId);
        params.add(productId);
        return   MysqlBaseUtil.updateSQL(sql, params);
    }

    /**
     * 内测用户删除(逻辑删除)
     */
    public static int  internalTestUserDelete(String testUserId) throws SQLException{
        String sql = "UPDATE tb_internal_test_user SET is_delete = 1 WHERE internal_test_user_id = ?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(testUserId);
       return MysqlBaseUtil.updateSQL(sql, params);
    }
}
