package com.hqmpms.dao.mail;

import com.hqmpms.utils.MysqlBaseAnKangUtil;
import org.apache.commons.lang3.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Administrator on 2018/9/19.
 */
public class MailDao {
    /**
     * * 查询邮件列表
    * @param pagesize
    * @param page
    * @return
            * @throws SQLException
    */
    public static HashMap<String,Object> getMailList (String sendmobile, int page, int pagesize, String sendmail, String receivemail,String startTime,
                                                        String endTime) throws SQLException{
        HashMap<String,Object> map = new HashMap<String,Object>();

        String sqlcount ="SELECT count(1) from(SELECT tu.phone_number,tmc.send_mail,tmc.receive_mail,send_title,create_time FROM t_mail_count AS tmc INNER JOIN t_user AS tu  ON  tu.uid=tmc.send_uid)  tmu ";
        ArrayList<Object> paramscount = new ArrayList<>();
        String sql;
        ArrayList<Object> params;
        ArrayList<HashMap<String,String>> rows = new ArrayList<HashMap<String,String>>();
        sql ="SELECT tmu.* from (SELECT  tu.phone_number,tmc.send_mail,tmc.receive_mail,send_title,create_time FROM t_mail_count AS tmc INNER JOIN t_user AS tu  ON  tu.uid=tmc.send_uid ) tmu   ";
        params = new ArrayList<>();
        String where =" where 1=1";
        if(!StringUtils.isEmpty(sendmobile)){
            where+=" and phone_number='"+sendmobile+"' ";
        }
        if(!StringUtils.isEmpty(sendmail)){
            where+=" and send_mail='"+sendmail+"' ";
        }
        if(!StringUtils.isEmpty(receivemail)){
            where+=" and receive_mail='"+receivemail+"' ";
        }

        if(!StringUtils.isEmpty(startTime)){
            where+=" and create_time >='"+startTime+"' ";
        }
        if(!StringUtils.isEmpty(endTime)){
            where+=" and create_time <='"+endTime+"' ";

        }
        sqlcount = sqlcount+where;
        sql=sql+where+" ORDER BY create_time DESC LIMIT ?,?";
        params.add(((page-1)*pagesize));
        params.add(pagesize);
        String   count = MysqlBaseAnKangUtil.getOneColumnByRow(sqlcount,paramscount);
        if(Integer.parseInt(count)>0){
            rows = MysqlBaseAnKangUtil.querySql(sql,params);
        }
        map.put("count",Integer.parseInt(count));
        map.put("rows",rows);
        return map;
    }






}
