package com.hqmpms.dao.message;

import com.hqmpms.dispatcher.Startup;
import com.hqmpms.utils.MysqlBaseUtil;
import org.apache.commons.lang3.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 消息列表
 * Created by dml on  2018/5/25
 */
public class MessageDao {

    /**
     * 根据时间或类型查询消息列表
     * @param productId                 产品编号
     * @param messageType           消息类型
     * @param startDate                  开始时间
     * @param endDate                   结束时间
     * @param page                        当前页码
     * @param pageSize                  页面大小
     */
    public static List<HashMap<String, String>> getMessageListByAllConditions(String productId, String messageType, String startDate,String endDate,Integer page, Integer pageSize) throws SQLException {
        ArrayList<Object> params = new ArrayList<>();
        String  sql = "SELECT a.*,b.real_name FROM tb_message_list a INNER JOIN tb_manager b ON a.creator_id = b.manager_id  WHERE a.product_id = ? AND a.is_delete = 0 ";
        params.add(productId);
        if(!StringUtils.isEmpty(messageType)){
            sql += " AND a.message_type = ? ";
            params.add(messageType);
        }
        if(!StringUtils.isEmpty(startDate)){
            sql += "AND a.create_time >= ? ";
            params.add(startDate);
        }
        if(!StringUtils.isEmpty(endDate)){
            sql += "AND a.create_time <= ? ";
            params.add(endDate);
        }
        sql += "  ORDER BY a.create_time desc";
        List<HashMap<String, String>> messageList;
        //分页查询
        if (page!=null&&pageSize!=null){
            sql += " LIMIT ?,?";
            params.add((page-1)*pageSize);
            params.add(pageSize);
            messageList = MysqlBaseUtil.querySql(sql, params);
        }else {
            messageList =  MysqlBaseUtil.querySql(sql, params);
        }
        return messageList;
    }

    public static int messagePublish(long messageId,String messageType,String title,String content,String status,String creatorId,String messageUrl,String productId,String conditionRecord) throws SQLException{
                String  sql = "INSERT INTO tb_message_list VALUES (?,?,?,?,NOW(),?,?,?,NOW(),0,?,?);";
                ArrayList<Object> params = new ArrayList<>();
                params.add(messageId);
                params.add(messageType);
                params.add(title);
                params.add(content);
                params.add(conditionRecord);
                params.add(status);
                params.add(creatorId);
                params.add(messageUrl);
                params.add(productId);
                return MysqlBaseUtil.updateSQL(sql,params);
    }

    public static int messageUpdate(String messageId,String messageType,String title,String content,String status,String messageUrl,String conditionRecord,String creatorId,String productId) throws SQLException{
        String sql;
        ArrayList<Object> params;
        if (StringUtils.isEmpty(messageId)){
            messageId = String.valueOf(Startup.getId());
            sql = "INSERT INTO tb_message_list VALUES (?,?,?,?,NOW(),?,?,?,NOW(),0,?,?);";
            params = new ArrayList<>();
            params.add(messageId);
            params.add(messageType);
            params.add(title);
            params.add(content);
            params.add(conditionRecord);
            params.add(status);
            params.add(creatorId);
            params.add(messageUrl);
            params.add(productId);
            return MysqlBaseUtil.updateSQL(sql,params);
        }else {
                sql = "UPDATE tb_message_list SET message_type=?,title = ?,content=?,send_time=NOW(),condition_record=?,message_status=?,creator_id=?,message_url=? WHERE message_id = ?;";
                params = new ArrayList<>();
                params.add(messageType);
                params.add(title);
                params.add(content);
                params.add(conditionRecord);
                params.add(status);
                params.add(creatorId);
                params.add(messageUrl);
                params.add(messageId);
                return MysqlBaseUtil.updateSQL(sql,params);
            }
        }

        public static void addMessageRecord(String recordId,String userId,String conditionRecord) throws SQLException{
            String sql = "INSERT INTO tb_message_condition_record VALUES(?,?,?,0,NOW());";
            ArrayList<Object> params = new ArrayList<>();
            params.add(recordId);
            params.add(userId);
            params.add(conditionRecord);
            MysqlBaseUtil.updateSQL(sql,params);
        }

    /**
     * 根据编号查询消息详情
     * @param messageId     消息编号
     */
    public static HashMap<String,String> getMessageById(String messageId) throws SQLException{
        String sql = "SELECT a.*,b.real_name FROM tb_message_list a INNER JOIN tb_manager b ON a.creator_id = b.manager_id  WHERE a.message_id = ?;";
        ArrayList<Object> params = new ArrayList<>();
        params.add(messageId);
        return MysqlBaseUtil.getOneRow(sql,params);
    }

    /**
     * 根据编号删除消息
     * @param messageId     消息编号
     */
    public static int delMessageById(String messageId) throws SQLException{
        String sql = "UPDATE tb_message_list SET is_delete = 1 WHERE message_id = ?;";
        ArrayList<Object> params = new ArrayList<>();
        params.add(messageId);
        return MysqlBaseUtil.updateSQL(sql,params);
    }

    /**
     * 根据用户编号查询发送记录列表
     * @param userId                 用户编号
     */
    public static List<HashMap<String, String>> getMessageConditionRecordListByUserId(String userId) throws SQLException {
        ArrayList<Object> params = new ArrayList<>();
        String  sql = "SELECT * FROM tb_message_condition_record  WHERE user_id = ? AND is_delete=0 GROUP BY condition_record ORDER BY last_use_time desc LIMIT 0,10";
        params.add(userId);
        return MysqlBaseUtil.querySql(sql, params);
    }

    /**
     * 根据发送记录编号查询
     * @param recordId                 发送记录编号编号
     */
    public static HashMap<String, String> getMessageConditionRecordByRecordId(String recordId) throws SQLException {
        ArrayList<Object> params = new ArrayList<>();
        String  sql = "SELECT * FROM tb_message_condition_record  WHERE record_id = ?;";
        params.add(recordId);
        return MysqlBaseUtil.getOneRow(sql, params);
    }

    /**
     * 根据编号删除发送记录
     * @param recordId     发送记录编号
     */
    public static int delMessageRecordById(String recordId) throws SQLException{
        ArrayList<Object> params = new ArrayList<>();
        String sql = "SELECT condition_record FROM tb_message_condition_record WHERE record_id = ?";
        params.add(recordId);
        HashMap<String, String> recordMap = MysqlBaseUtil.getOneRow(sql,params);
        if (null!=recordMap){
            params = new ArrayList<>();
            sql = "DELETE FROM tb_message_condition_record  WHERE condition_record = '" + recordMap.get("condition_record")+"'";
        }
        return MysqlBaseUtil.updateSQL(sql,params);
    }
}
