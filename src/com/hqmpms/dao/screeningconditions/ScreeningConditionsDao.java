package com.hqmpms.dao.screeningconditions;

import com.hqmpms.utils.SqlServerBaseUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 用户筛选条件
 * Created by dml on  2018/5/23
 */
public class ScreeningConditionsDao {

    /**
     * 查询行政区划列表
     * @param  parentId     父级编号
     * @param nStar           级别
     */
    public  static List<HashMap<String, String>> getDistrictList(String parentId,String nStar) throws SQLException {
        String sql = "SELECT ID,Name FROM [District] WHERE ParentId= ? AND nStar = ? ORDER BY nOrder asc;";
        ArrayList<Object> params = new ArrayList<>();
        params.add(parentId);
        params.add(nStar);
        return SqlServerBaseUtil.querySql(sql, params);
    }

    /**
     * 根据编号查询行政区划
     * @param  id     编号
     */
    public  static HashMap<String, String> getDistrictById(String id) throws SQLException {
        String sql = "SELECT ID,Name FROM [District] WHERE ID= ? ;";
        ArrayList<Object> params = new ArrayList<>();
        params.add(id);
        return SqlServerBaseUtil.getOneRow(sql, params);
    }

    /**
     * 根据行政区划编号查询学校列表
     * @param districtId        行政区划编号
     */
    public static List<HashMap<String, String>>  getSchoolListByDistrictId(String districtId) throws SQLException{
        String sql = "SELECT ID,Name FROM [School] WHERE DistrictId= ? AND isPause = 0;";
        ArrayList<Object> params = new ArrayList<>();
        params.add(districtId);
        return SqlServerBaseUtil.querySql(sql, params);
    }

    /**
     * 根据学校编号查询学校
     * @param schoolId        学校编号
     */
    public static HashMap<String, String>  getSchoolById(String schoolId) throws SQLException{
        String sql = "SELECT ID,Name FROM [School] WHERE ID= ?;";
        ArrayList<Object> params = new ArrayList<>();
        params.add(schoolId);
        return SqlServerBaseUtil.getOneRow(sql, params);
    }

    /**
     * 根据学校编号查询班级列表
     * @param schoolId        学校编号
     */
    public static List<HashMap<String, String>>  getClassInfoListBySchoolId(String schoolId) throws SQLException{
        String sql = "SELECT class_id,nick_code FROM [yx_class_info] WHERE is_audit= 1 AND quit_status != 2 AND school_id = ? ;";
        ArrayList<Object> params = new ArrayList<>();
        params.add(schoolId);
        return SqlServerBaseUtil.querySql(sql, params);
    }

    /**
     * 根据班级编号查询班级
     * @param classId        班级编号
     */
    public static HashMap<String, String>  getClassInfoById(String classId) throws SQLException{
        String sql = "SELECT class_id,nick_code FROM [yx_class_info] WHERE class_id = ? ;";
        ArrayList<Object> params = new ArrayList<>();
        params.add(classId);
        return SqlServerBaseUtil.getOneRow(sql, params);
    }

    /**
     * 根据筛选信息查询用户列表
     * @param provinceId   省
     * @param cityId            市
     * @param countyId      区/县
     * @param schoolId      学校编号
     * @param classId        班级编号
     * @param memberType    身份
     */
    public static List<HashMap<String, String>>  getUserListByAllConditions(String provinceId,String cityId,String countyId,String schoolId,String classId,String memberType,List<String> uids) throws SQLException{
        String  sql;
        ArrayList<Object> params = new ArrayList<>();
        if(!CollectionUtils.isEmpty(uids)){
            List<HashMap<String, String>> allList = new ArrayList<>();
            for (String uid:uids) {
                sql = "SELECT ID as UserID FROM [User] WHERE ID = ?;";
                params = new ArrayList<>();
                params.add(uid);
                allList.addAll(SqlServerBaseUtil.querySql(sql,params));
            }
           return allList;
        }else {
            //排除法  如果班级编号或学校编号不为空，就查询学校所有
            if(!StringUtils.isEmpty(classId)){
                //老师
                if("1".equals(memberType)){
                    sql = "SELECT a.uid as UserID,b.RealName  FROM yx_class_members a INNER JOIN [User] b ON a.uid=b.ID WHERE a.class_id =? AND b.UserType =1 AND a.uid IN (SELECT uid FROM yx_user_extend);";
                }//学生
                else if("2".equals(memberType)){
                    sql = "SELECT a.uid as UserID,b.RealName FROM yx_class_members a INNER JOIN [User] b ON a.uid=b.ID WHERE a.class_id =? AND b.UserType = 7 AND a.uid IN (SELECT uid FROM yx_user_extend);";
                }//全部
                else {
                    sql = "SELECT a.uid as UserID,b.RealName FROM yx_class_members a INNER JOIN [User] b ON a.uid=b.ID WHERE a.class_id =? AND (b.UserType=1 OR b.UserType=7) AND a.uid IN (SELECT uid FROM yx_user_extend);";
                }
                params.add(classId);
            }else {
                //班级编号为空学校编号不为空时
                if (!StringUtils.isEmpty(schoolId)){
                    if("1".equals(memberType)){
                        sql = "SELECT UserID FROM Teacher WHERE SchoolID = ? AND UserID IN (SELECT uid FROM yx_user_extend)";
                        params.add(schoolId);
                    }else if("2".equals(memberType)){
                        sql = "SELECT UserID FROM Student WHERE SchoolID = ? AND UserID IN (SELECT uid FROM yx_user_extend)";
                        params.add(schoolId);
                    }else {
                        sql = "SELECT UserID FROM Student WHERE SchoolID = ? AND UserID IN (SELECT uid FROM yx_user_extend) UNION ALL SELECT UserID FROM Teacher WHERE SchoolID = ? AND UserID IN (SELECT uid FROM yx_user_extend)";
                        params.add(schoolId);
                        params.add(schoolId);
                    }
                }else {
                    //区/县编号不为空时
                    if (!StringUtils.isEmpty(countyId)){
                        if("1".equals(memberType)){
                            sql = "SELECT UserID FROM Teacher WHERE SchoolID IN (SELECT ID FROM School where DistrictId = ?) AND UserID IN (SELECT uid FROM yx_user_extend)";
                            params.add(countyId);
                        }else if("2".equals(memberType)){
                            sql = "SELECT UserID FROM Student WHERE SchoolID IN (SELECT ID FROM School where DistrictId = ?) AND UserID IN (SELECT uid FROM yx_user_extend)";
                            params.add(countyId);
                        }else {
                            sql = "SELECT UserID FROM Student WHERE SchoolID IN (SELECT ID FROM School where DistrictId = ?) AND UserID IN (SELECT uid FROM yx_user_extend) UNION ALL SELECT UserID FROM Teacher WHERE SchoolID IN (SELECT ID FROM School where DistrictId = ?) AND UserID IN (SELECT uid FROM yx_user_extend)";
                            params.add(countyId);
                            params.add(countyId);
                        }

                    }else {
                        //市编号不为空时
                        if (!StringUtils.isEmpty(cityId)){
                            if("1".equals(memberType)){
                                sql = "SELECT UserID FROM Teacher WHERE SchoolID IN (SELECT ID FROM School where DistrictId IN(SELECT ID FROM District where ParentID = ?)) AND UserID IN (SELECT uid FROM yx_user_extend)";
                                params.add(cityId);
                            }else if("2".equals(memberType)){
                                sql = "SELECT UserID FROM Student WHERE SchoolID IN (SELECT ID FROM School where DistrictId IN(SELECT ID FROM District where ParentID =?)) AND UserID IN (SELECT uid FROM yx_user_extend)";
                                params.add(cityId);
                            }else {
                                sql = "SELECT UserID FROM Student WHERE SchoolID IN (SELECT ID FROM School where DistrictId IN(SELECT ID FROM District where ParentID =?)) AND UserID IN (SELECT uid FROM yx_user_extend) UNION ALL SELECT UserID FROM Teacher WHERE SchoolID IN (SELECT ID FROM School where DistrictId IN(SELECT ID FROM District where ParentID = ?)) AND UserID IN (SELECT uid FROM yx_user_extend)";
                                params.add(cityId);
                                params.add(cityId);
                            }
                        }else {
                            //省编号不为空时
                            if(!StringUtils.isEmpty(provinceId)){
                                if("1".equals(memberType)){
                                    sql = "SELECT UserID FROM Teacher WHERE SchoolID IN (SELECT ID FROM School where DistrictId IN(SELECT ID FROM District where ParentID IN(SELECT ID FROM District where ParentID = ?))) AND UserID IN (SELECT uid FROM yx_user_extend)";
                                    params.add(provinceId);
                                }else if("2".equals(memberType)){
                                    sql = "SELECT UserID FROM Student WHERE SchoolID IN (SELECT ID FROM School where DistrictId IN(SELECT ID FROM District where ParentID IN(SELECT ID FROM District where ParentID = ?))) AND UserID IN (SELECT uid FROM yx_user_extend)";
                                    params.add(provinceId);
                                }else {
                                    sql = "SELECT UserID FROM Student WHERE SchoolID IN (SELECT ID FROM School where DistrictId IN(SELECT ID FROM District where ParentID IN(SELECT ID FROM District where ParentID = ?))) AND UserID IN (SELECT uid FROM yx_user_extend) UNION ALL SELECT UserID FROM Teacher WHERE SchoolID IN (SELECT ID FROM School where DistrictId IN(SELECT ID FROM District where ParentID IN(SELECT ID FROM District where ParentID = ?))) AND UserID IN (SELECT uid FROM yx_user_extend)";
                                    params.add(provinceId);
                                    params.add(provinceId);
                                }
                            }else {
                                if("1".equals(memberType)){
                                    sql = "SELECT UserID FROM Teacher WHERE UserID IN (SELECT uid FROM yx_user_extend)";
                                }else if("2".equals(memberType)){
                                    sql = "SELECT UserID FROM Student WHERE UserID IN (SELECT uid FROM yx_user_extend)";
                                }else {
                                    sql = "SELECT ID as UserID FROM [User] WHERE ID IN(SELECT uid FROM yx_user_extend)";
                                }
                            }

                        }
                    }
                }
            }
        }
        return SqlServerBaseUtil.querySql(sql, params);
    }
}
