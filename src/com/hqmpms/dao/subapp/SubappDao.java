package com.hqmpms.dao.subapp;

import com.hqmpms.dispatcher.Startup;
import com.hqmpms.utils.SqlServerBaseUtil;
import org.apache.commons.lang3.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class SubappDao {
	/**
	 * 获取子应用列表
	 * @param pagenum
	 * @param pagesize
	 * @param ostype
	 * @throws SQLException 
	 */
	public  static ArrayList<HashMap<String,String>> getSubappList(String pagenum,String pagesize,String ostype) throws SQLException{
		String sql ="select * from(select row_number()over(order by create_time desc)as rownum,ys.subapp_id as subappid,ys.os_type as ostype,ys.OpenApp_id as openappid ,ys.app_status as appstatus,ys.create_time as createtime,ys.islocal,(select AppName from OpenApp as oa where oa.ID=ys.OpenApp_id) as appname  from yx_subapp as ys where 1=1";
		ArrayList<Object> param = new ArrayList<>();
		if(!StringUtils.isEmpty(ostype)){
			sql+=" and os_type=? ";
			param.add(ostype);
		}
		sql+=") as a where rownum>? and rownum<=?";
		param.add((Integer.parseInt(pagenum)-1)*Integer.parseInt(pagesize));
		param.add(Integer.parseInt(pagenum)*Integer.parseInt(pagesize));
		return SqlServerBaseUtil.querySql(sql,param);
	}
	/**
	 * 获取子应用数量
	 * @param os_type
	 * @throws SQLException 
	 */
	public  static int getSubappCount(String os_type) throws SQLException{
		 int count = 0;
		 String sql = "";
		 if("1".equals(os_type) || os_type=="1"){
			 sql="select count(1) from yx_subapp where os_type = 1";
		 }else if("2".equals(os_type) || os_type=="2"){
			 sql="select count(2) from yx_subapp where os_type = 2";
		 }else{
			 sql="select count(0) from yx_subapp";
		 }
		 ArrayList<Object> params = new ArrayList<Object>();
		 String result = SqlServerBaseUtil.getOneColumnByRow(sql, params);
		 if(result!=null || !"".equals(result)){
			 count = Integer.parseInt(result);
		 }
		 return count;
	}
	/**
	 * 添加子应用
	 * @param condition
	 * @throws SQLException 
	 */
	public static boolean  addSubapp(HashMap<String,String> condition) throws Exception{
		boolean flag = false;
		String sql = "INSERT INTO yx_subapp(subapp_id,OpenApp_id,os_type,app_status,create_time,islocal) VALUES(?,?,?,1,getDate(),1)";
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(condition.get("subappid"));
		params.add(condition.get("openappid"));
		params.add(condition.get("ostype"));
		int count = SqlServerBaseUtil.updateSQL(sql, params);
		if(count>0){
			flag = true;
		}
		return flag;
	}
	
	/**
	 * 修改子应用
	 * @param condition
	 * @throws SQLException 
	 */
	public static boolean  updateSubapp(HashMap<String,String> condition) throws Exception{
		boolean flag = false;
		ArrayList<Object> params = new ArrayList<Object>();
		String sql="update yx_subapp set ";
		if(!StringUtils.isEmpty(condition.get("appstatus"))){
			sql+=" app_status=?,";
			params.add(condition.get("appstatus"));
		}
		if(!StringUtils.isEmpty(condition.get("islocal"))){
			sql+=" islocal=?,";
			params.add(condition.get("islocal"));
		}
		sql+=" create_time=getdate() where subapp_id=?";
		params.add(condition.get("subappid"));
		int count = SqlServerBaseUtil.updateSQL(sql, params);
		if(count>0){
			flag = true;
		}
		return flag;
	}
	/**
	 * 删除子应用
	 * @param subappid
	 * @throws SQLException 
	 */
	public static boolean  deleteSubapp(String subappid) throws Exception{
		boolean flag = false;
		if(!StringUtils.isEmpty(subappid) && subappid.length()>0){
			subappid = DealwithParam(subappid);
		}
		String sql = "delete from yx_subapp where subapp_id in("+subappid+")";
		ArrayList<Object> params = new ArrayList<Object>();
		int count = SqlServerBaseUtil.updateSQL(sql, params);
		if(count>0){
			flag = true;
		}
		return flag;
	}
	/**
	 * 子应用版本号
	 * @param
	 * @return
	 * @throws SQLException 
	 */
	public static HashMap<String, String> inquireSubAppVersion(String openappid,String ostype) throws Exception {
		String sql = "select subapp_id from yx_subapp where OpenApp_id = ? and os_type=?  ";
		ArrayList<Object> param = new ArrayList<Object>();
		param.add(openappid);
		param.add(ostype);
		return SqlServerBaseUtil.getOneRow(sql, param);
	}
	/**
	 * 处理参数
	 * @param string
     */
	private static String DealwithParam(String string){
		String strs="";
		String[] str = string.split(",");
		for(int i=0;i<str.length;i++){
			if(!StringUtils.isEmpty(str[i])){
				strs+=""+str[i]+",";
			}
		}
		strs = strs.substring(0,strs.length()-1);
		return strs;
	}


	/**
	 * 客户端查询子应用
	 * @param osType
	 * @param version
	 * @param phase
	 * @return
	 * @throws SQLException
	 */
	public static  ArrayList<HashMap<String,String>> getClientSubapp(String osType,String version,String phase) throws SQLException{
		String sql="SELECT subapp_id AS said, subapp_name AS saname, subapp_version AS saver, subapp_icon AS saicon, DATEDIFF(SECOND, '1970-01-01 08:00:00', create_time) AS createtime FROM (SELECT *, row_number() OVER (PARTITION BY subapp_name ORDER BY subapp_version DESC) AS rn FROM yx_subapp WHERE app_status = 1 AND CHARINDEX(?, depend_version)>0 AND CHARINDEX(?, subapp_phase)>0 and os_type=?) t WHERE rn = 1 ORDER BY said DESC";
		ArrayList<Object> params = new ArrayList<>();
		params.add(version);
		params.add(phase);
		params.add(osType);

		return SqlServerBaseUtil.querySql(sql,params);
	}


	/**
	 * 查询webapp
	 * @return
	 * @throws SQLException
	 */
	public static  ArrayList<HashMap<String,String>> getMarketWebApp()throws SQLException{
		String sql ="select id,AppName as appName from OpenApp a  where QLoginMode=5 and AppState=3 and IsDrop=0";
		return SqlServerBaseUtil.querySql(sql,new ArrayList<Object>());
	}

	/**
	 * 查询某一条应用
	 * @param subappid
	 * @return
	 * @throws SQLException
	 */
	public static  HashMap<String,String> getMarketWebAppById(String subappid)throws SQLException{
		String sql ="select id,AppName as appName from OpenApp a  where QLoginMode=5 and AppState=3 and IsDrop=0 and ID=? ";
		ArrayList<Object> params = new ArrayList<>();
		params.add(subappid);
		return SqlServerBaseUtil.getOneRow(sql,params);
	}
	
	
	
	

}
