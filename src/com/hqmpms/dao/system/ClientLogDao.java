package com.hqmpms.dao.system;


import com.hqmpms.dispatcher.Startup;
import com.hqmpms.utils.MysqlBaseUtil;
import com.hqmpms.utils.SqlServerBaseUtil;
import org.apache.commons.lang3.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Administrator on 2018/6/6.
 */
public class ClientLogDao {
    public  static boolean addClientLog(HashMap<String, String> condition) throws SQLException{
        boolean flag =false;
        ArrayList<Object> params = new ArrayList<Object>();
        String sql = "INSERT INTO tb_client_log (client_log_id,file_name,file_url,log_type,os_type,product_id,create_time";
        String sqlvlaues = "VALUES(?,?,?,1,?,?,NOW()";
        params.add(String.valueOf(Startup.getId()));
        params.add(condition.get("filename"));
        params.add(condition.get("fileurl"));
        params.add(condition.get("ostype"));
        params.add(condition.get("productid"));
        if(!StringUtils.isEmpty(condition.get("appversion"))){
            sql+=",app_version";
            sqlvlaues+=",?";
            params.add(condition.get("appversion"));
        }
        if(!StringUtils.isEmpty(condition.get("abstract"))){
            sql+=",file_abstract";
            sqlvlaues+=",?";
            params.add(condition.get("abstract"));
        }
        if(!StringUtils.isEmpty(condition.get("exceptionname"))){
            sql+=",exception_name";
            sqlvlaues+=",?";
            params.add(condition.get("exceptionname"));
        }
        if(!StringUtils.isEmpty(condition.get("osversion"))){
            sql+=",os_version";
            sqlvlaues+=",?";
            params.add(condition.get("osversion"));
        }
        if(!StringUtils.isEmpty(condition.get("devicename"))){
            sql+=",device_name";
            sqlvlaues+=",?";
            params.add(condition.get("devicename"));
        }
        sql+=")";
        sqlvlaues+=")";
        sql = sql+sqlvlaues;
        int  count  =  MysqlBaseUtil.updateSQL(sql,params);
        if(count>0){
            flag=true;
        }

        return flag;
    }

    /**
     * 查询客户端日志列表
     * @param productid
     * @param pagesize
     * @param page
     * @param ostype
     * @param startdate
     * @param enddate
     * @return
     * @throws SQLException
     */
    public static  HashMap<String,Object>  getClientLogList (String productid,int page,int pagesize,String ostype,String startdate, String enddate,String file_abstract ) throws SQLException{
        HashMap<String,Object> map = new HashMap<String,Object>();

        String sqlcount ="select count(1) from tb_client_log where product_id=?";
        ArrayList<Object> paramscount = new ArrayList<>();
        paramscount.add(productid);
        if(!StringUtils.isEmpty(ostype)){
            sqlcount+=" and os_type=? ";
            paramscount.add(ostype);
        }
        if(!StringUtils.isEmpty(startdate)){
            sqlcount+=" and create_time >=? ";
            paramscount.add(startdate);
        }
        if(!StringUtils.isEmpty(enddate)){
            sqlcount+=" and create_time <=? ";
            paramscount.add(enddate);
        }
        if(!StringUtils.isEmpty(file_abstract)){
            sqlcount+=" and file_abstract like '%"+file_abstract+"%' ";
        }
        String   count = MysqlBaseUtil.getOneColumnByRow(sqlcount,paramscount);

        String sql;
        ArrayList<Object> params;
        ArrayList<HashMap<String,String>> rows = new ArrayList<HashMap<String,String>>();
        if(!count.equals("0")){
            sql ="SELECT client_log_id,file_name,file_url,file_abstract,log_type,os_type,create_time,product_id FROM tb_client_log where product_id=?";
            params = new ArrayList<>();
            params.add(productid);
            if(!StringUtils.isEmpty(ostype)){
                sql+=" and os_type=? ";
                params.add(ostype);
            }
            if(!StringUtils.isEmpty(startdate)){
                sql+=" and create_time >=? ";
                params.add(startdate);
            }
            if(!StringUtils.isEmpty(enddate)){
                sql+=" and create_time <=? ";
                params.add(enddate);
            }
            if(!StringUtils.isEmpty(file_abstract)){
                sql+=" and file_abstract like '%"+file_abstract+"%' ";
            }
            sql+=" ORDER BY create_time DESC,client_log_id DESC LIMIT ?,?";
            params.add(((page-1)*pagesize));
            params.add(pagesize);
            rows = MysqlBaseUtil.querySql(sql,params);
        }
        map.put("count",Integer.parseInt(count));
        map.put("rows",rows);
        return map;
    }


    /**
     *
     * @param peoductid   产品ID
     * @param timenode    时间节点
     * @return
     * @throws Exception
     */
    //按系统类型刪除客户端日志
    public static  int delClientLogLists(String  peoductid,String timenode) throws Exception{
        String sql = "delete from tb_client_log where product_id = ?  and  create_time<?";
        ArrayList<Object> params = new ArrayList<Object>();
        params.add(peoductid);
        params.add(timenode);


        return MysqlBaseUtil.updateSQL(sql, params);
    }







}
