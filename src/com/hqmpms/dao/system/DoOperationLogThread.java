package com.hqmpms.dao.system;

import com.hqmpms.utils.YsServerSDK;
import org.apache.log4j.Logger;

import java.util.HashMap;


public class DoOperationLogThread extends Thread{
    private static final Logger m_logger = Logger.getLogger(DoOperationLogThread.class);
    private String creator_id;
    private String content;
    private String object_id;
    public DoOperationLogThread(String creator_id,String content,String object_id)
    {
        this.creator_id = creator_id;
        this.content = content;
        this.object_id = object_id;
    }
    public void run()
    {
        try {
            OperationLogDao.operationLogAddDB(creator_id,content,object_id);
        } catch (Exception e) {
            m_logger.error(String.format("DoOperationLogThread fail params=%s",e.getMessage()));
        }
    }

}
