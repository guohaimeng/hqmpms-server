package com.hqmpms.dao.system;

import com.hqmpms.utils.SqlServerBaseUtil;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Administrator on 2018/6/5.
 */
public class FeedBackDao {
    /**
     * @param type   问题类型
     * @param page   当前页数
     * @param pageSize  每页显示的记录数
     * @return
     * @throws Exception
     */
    // 按问题类型查询列表
    public static ArrayList<HashMap<String, String>> getSuggestion(String  type, int page, int pageSize)  throws Exception{
        ArrayList<Object> param = new ArrayList<Object>();
        String sql = "select * from (select row_number() over (order by yx_feedback.create_time desc ) as rownum,yx_feedback.type,yx_feedback.content,yx_feedback.create_time,[user].username,[user].realname from yx_feedback left join [user] on yx_feedback.uid = [user].id  where 1=1 ";
        if(!StringUtils.isEmpty(type)){
            sql+=" and  yx_feedback.type = ?";
            param.add(type);
        }
        sql+=" ) as t where t.rownum between ? and ? ";
        param.add(((page-1)*pageSize)+1);
        param.add(page*pageSize);
        return SqlServerBaseUtil.querySql(sql, param);
    }

    /**
     *
     * @param type  问题类型
     * @return
     * @throws Exception
     */
    //查询意见反馈列表记录数
    public static int getSuggCount(String type) throws Exception{
        int count = 0;
        String sql = "select count(1) from yx_feedback where 1 = 1";
        ArrayList<Object> params = new ArrayList<Object>();
        if(!StringUtils.isEmpty(type)){
            sql+=" and type=?";
            params.add(type);
        }
        String result =  SqlServerBaseUtil.getOneColumnByRow(sql, params);
        if(result != null || !"".equals(result)){
            count = Integer.parseInt(result);
        }
        return count;
    }




}
