package com.hqmpms.dao.system;

import com.hqmpms.utils.MysqlBaseUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ManagerDao {
    private static final Logger m_logger = Logger.getLogger(ManagerDao.class) ;


    /**
     * 添加管理员
     * @throws SQLException
     */
    public static void managerAdd(Map<String,String> manager) throws SQLException {
        String sql = "insert into tb_manager (manager_id,real_name,user_name,pwd,creator_id,is_enable) values (?,?,?,?,?,?)";
        ArrayList<Object> pramas = new ArrayList<>();
        String managerId= manager.get("manager_id");
        pramas.add(managerId);
        pramas.add(manager.get("real_name"));
        pramas.add(manager.get("user_name"));
        pramas.add(manager.get("pwd"));
        pramas.add(manager.get("creator_id"));
        pramas.add(manager.get("is_enable"));

        String[] role_id = manager.get("role_id").split(",");
        StringBuffer managerRoleSqlbuff = new StringBuffer("insert into tb_manager_role(manager_id,role_id) values");
        for(String rid:role_id){
            managerRoleSqlbuff.append("('"+managerId+"','"+rid+"'),");
        }
        managerRoleSqlbuff.deleteCharAt(managerRoleSqlbuff.length()-1);
        String managerRoleSql = managerRoleSqlbuff.toString();
        ArrayList<ArrayList<Object>> paramGroups = new ArrayList<>();
        ArrayList<String> sqlList = new ArrayList<>();
        sqlList.add(sql);
        sqlList.add(managerRoleSql);
        paramGroups.add(pramas);
        paramGroups.add(new ArrayList<Object>());
        boolean res= MysqlBaseUtil.batchUpdateSql(sqlList,paramGroups);
        if(!res){
            throw new SQLException("managerAdd error");
        }
    }

    /**
     * 查询总数
     * @return
     * @throws SQLException
     */
    public static  String queryCount()throws SQLException{
        String sql = "SELECT COUNT(1) FROM tb_manager where is_delete=0  ";
        return MysqlBaseUtil.getOneColumnByRow(sql, new ArrayList<Object>());
    }

    /**
     * 查询管理员列表
     * @param pagesize
     * @param pagenum
     * @return
     * @throws SQLException
     */
    public static  ArrayList<HashMap<String,String>> managerList (String pagenum, String  pagesize ) throws SQLException{
        String sql =" SELECT t.*, m.real_name AS creator_name,(SELECT GROUP_CONCAT(role_name) FROM tb_role WHERE role_id IN (SELECT role_id FROM tb_manager_role mr WHERE mr.manager_id=t.manager_id)) AS role_name,(SELECT GROUP_CONCAT(role_id) FROM tb_role WHERE role_id IN (SELECT role_id FROM tb_manager_role mr WHERE mr.manager_id=t.manager_id)) AS role_id FROM ( SELECT manager_id, real_name, user_name, is_enable , create_time, creator_id FROM tb_manager WHERE is_delete=0 ORDER BY manager_id DESC LIMIT ?,?  ) t LEFT JOIN tb_manager m ON t.creator_id = m.manager_id";
        ArrayList<Object> param = new ArrayList<>();
        param.add((Integer.parseInt(pagenum)-1)*Integer.parseInt(pagesize));
        param.add(Integer.parseInt(pagesize));

        return MysqlBaseUtil.querySql(sql,param);
    }

    /**
     *管理员删除
     * @param manager_id
     * @throws SQLException
     */
    public static void managerDelete(String manager_id) throws SQLException{
        String sql = "update tb_manager set is_delete=1 where manager_id=?";
        String delsql = "delete from  tb_manager_role where manager_id=?";
        ArrayList<Object> pramas = new ArrayList<>();
        pramas.add(manager_id);

        ArrayList<ArrayList<Object>> paramGroups = new ArrayList<>();
        ArrayList<String> sqlList = new ArrayList<>();
        sqlList.add(sql);
        sqlList.add(delsql);
        paramGroups.add(pramas);
        paramGroups.add(pramas);
        boolean res= MysqlBaseUtil.batchUpdateSql(sqlList,paramGroups);
        if(!res){
            throw new SQLException("managerDelete error");
        }
    }

    /**
     * 管理员修改
     * @param manager
     * @throws SQLException
     */
    public static void managerUpdate (Map<String,String> manager) throws SQLException{
        if (manager.size()<2){
            return;
        }
        String sqlparamsStr = "";
        ArrayList<Object> pramas = new ArrayList<>();
        if(!StringUtils.isEmpty(manager.get("real_name"))){
            sqlparamsStr+="real_name=?,";
            pramas.add(manager.get("real_name"));
        }
        if(!StringUtils.isEmpty(manager.get("user_name"))){
            sqlparamsStr+="user_name=?,";
            pramas.add(manager.get("user_name"));
        }
        if(!StringUtils.isEmpty(manager.get("pwd"))){
            sqlparamsStr+="pwd=?,";
            pramas.add(manager.get("pwd"));
        }
        if(!StringUtils.isEmpty(manager.get("is_enable"))){
            sqlparamsStr+="is_enable=?,";
            pramas.add(manager.get("is_enable"));
        }
        pramas.add(manager.get("manager_id"));
        sqlparamsStr = StringUtils.substring(sqlparamsStr,0,sqlparamsStr.length()-1);
        String sql = "update tb_manager set "+ sqlparamsStr + " where manager_id=?";

        ArrayList<ArrayList<Object>> paramGroups = new ArrayList<>();
        ArrayList<String> sqlList = new ArrayList<>();
        sqlList.add(sql);
        paramGroups.add(pramas);

        if(!StringUtils.isEmpty(manager.get("role_id")) && manager.get("role_id").equals(manager.get("original_role_id"))){
            String clearSql="delete from tb_manager_role where manager_id="+manager.get("manager_id");

            String[] mids = manager.get("role_id").split(",");
            StringBuffer roleManagerqlbuff = new StringBuffer("insert into tb_manager_role(manager_id,role_id) values");
            for(String mid:mids){
                roleManagerqlbuff.append("('"+manager.get("manager_id")+"','"+mid+"'),");
            }
            roleManagerqlbuff.deleteCharAt(roleManagerqlbuff.length()-1);
            String roleManagerSql = roleManagerqlbuff.toString();

            sqlList.add(clearSql);
            sqlList.add(roleManagerSql);
            paramGroups.add(new ArrayList<Object>());
            paramGroups.add(new ArrayList<Object>());
        }

        boolean res= MysqlBaseUtil.batchUpdateSql(sqlList,paramGroups);
        if(!res){
            throw new SQLException("managerUpdate error");
        }
    }

    /**
     * 检查用户名
     * @param username
     * @return
     * @throws SQLException
     */
    public  static HashMap<String ,String> CheckUserName(String username) throws SQLException{
        String sql = "select manager_id ,user_name  from tb_manager where user_name=? and is_delete=0";
        ArrayList<Object> params = new ArrayList<>();
        params.add(username);
        return  MysqlBaseUtil.getOneRow(sql,params);
    }

    /**
     * 查询用户信息
     * @param username
     * @return
     * @throws SQLException
     */
    public  static HashMap<String ,String> getUserByUserName(String username) throws SQLException{
        String sql = "select manager_id as mid,user_name as username,pwd,real_name as realNmae from tb_manager where user_name=? and is_enable=1 and is_delete=0";
        ArrayList<Object> params = new ArrayList<>();
        params.add(username);
        return  MysqlBaseUtil.getOneRow(sql,params);
    }
}
