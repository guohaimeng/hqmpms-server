package com.hqmpms.dao.system;

import com.hqmpms.utils.MysqlBaseUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MenuDao {
    private static final Logger m_logger = Logger.getLogger(MenuDao.class) ;


    /**
     * 添加系统菜单
     * @throws SQLException
     */
    public static void menuAdd(Map<String,String> menu) throws SQLException {
        String sql = "insert into tb_menu (menu_id,menu_name,menu_code,menu_link,product_id,parent_id,is_sys_menu,is_top,menu_type,menu_order,creator_id) values (?,?,?,?,?,?,?,?,?,?,?)";
        ArrayList<Object> pramas = new ArrayList<>();
        pramas.add(menu.get("menu_id"));
        pramas.add(menu.get("menu_name"));
        pramas.add(menu.get("menu_code"));
        pramas.add(menu.get("menu_link"));
        pramas.add(menu.get("product_id"));
        pramas.add(menu.get("parent_id"));
        pramas.add(menu.get("is_sys_menu"));
        pramas.add(menu.get("is_top"));
        pramas.add(menu.get("menu_type"));
        pramas.add(menu.get("menu_order"));
        pramas.add(menu.get("creator_id"));
        int res= MysqlBaseUtil.updateSQL(sql,pramas);
        if(res<1){
            throw new SQLException("menuAdd error");
        }
    }


    /**
     * 查询菜单列表
     * @param pagesize
     * @param pagenum
     * @return
     * @throws SQLException
     */
    public static  ArrayList<HashMap<String,String>> menuList () throws SQLException{
        ArrayList<HashMap<String, String>> resultList = new ArrayList<>();

        resultList.add(builMenuTopNode("0","系统菜单"));
        ArrayList<HashMap<String,String>> producList = queryProductIdAndName();
        if(producList.size()>0){
            for (HashMap<String,String> map: producList
                 ) {
                resultList.add(builMenuTopNode(map.get("product_id"),map.get("product_name")));
            }
        }
        String prodectMenusql ="SELECT m.menu_id, m.menu_name, m.menu_code, m.menu_link, m.product_id , m.parent_id as _parentId, m.is_sys_menu, m.is_top, m.menu_type, m.menu_order , m.is_enable, m.create_time, a.real_name AS creator_name FROM tb_menu m LEFT JOIN tb_manager a ON m.creator_id = a.manager_id WHERE m.is_delete = 0 ORDER BY  m.menu_order";
        ArrayList<Object> param = new ArrayList<>();
        resultList.addAll(MysqlBaseUtil.querySql(prodectMenusql,param));
        return resultList;
    }


    /**
     * 查询菜单选择树
     * @param pagesize
     * @param pagenum
     * @return
     * @throws SQLException
     */
    public static  ArrayList<HashMap<String,String>> menuComboTree (Integer is_sys_menu, String product_id) throws SQLException{
        String sql="";
        ArrayList<HashMap<String,String>> result = null;
        if(is_sys_menu!=null){
             sql ="SELECT m.menu_id, m.menu_name, m.parent_id, m.menu_type, m.menu_order FROM tb_menu m WHERE m.is_delete = 0 AND m.product_id = ? ORDER BY m.menu_order";
            ArrayList<Object> param = new ArrayList<>();
            param.add(product_id);
            result= MysqlBaseUtil.querySql(sql,param);
        }else{
            sql ="SELECT m.menu_id, m.menu_name, m.parent_id, m.menu_type, m.menu_order FROM tb_menu m WHERE m.is_delete = 0 AND m.is_enable=1 ORDER BY m.menu_order";
            result= MysqlBaseUtil.querySql(sql,new ArrayList<Object>());
            result.add(builMenuTopNode("0","系统菜单"));
            ArrayList<HashMap<String,String>> producList = queryProductIdAndName();
            if(producList.size()>0){
                for (HashMap<String,String> map: producList
                        ) {
                    result.add(builMenuTopNode(map.get("product_id"),map.get("product_name")));
                }
            }
        }
        return result;
    }



    /**
     * 构造菜单节点
     * @param menu_id
     * @param menu_name
     * @return
     */
    private static HashMap<String, String> builMenuTopNode(String menu_id,String menu_name){
        HashMap<String, String> sysTopNode = new HashMap<>();
        sysTopNode.put("menu_id",menu_id);
        sysTopNode.put("menu_name",menu_name);
        sysTopNode.put("parent_id","");
        sysTopNode.put("menu_code","");
        sysTopNode.put("menu_link","");
        sysTopNode.put("product_id","");
        sysTopNode.put("is_sys_menu","");
        sysTopNode.put("is_top","");
        sysTopNode.put("menu_type","");
        sysTopNode.put("menu_order","");
        sysTopNode.put("is_enable","");
        sysTopNode.put("create_time","");
        sysTopNode.put("creator_name","");
        return sysTopNode;
    }

    /**
     * 查询产品id和名称
     * @param pagesize
     * @param pagenum
     * @return
     * @throws SQLException
     */
    public static  ArrayList<HashMap<String,String>> queryProductIdAndName () throws SQLException{
        ArrayList<HashMap<String, String>> resultList = new ArrayList<>();
        String sysMenusql ="SELECT product_id,product_name FROM tb_product WHERE is_enable=1 and product_id !=0";
        ArrayList<Object> param = new ArrayList<>();

        return MysqlBaseUtil.querySql(sysMenusql,param);
    }

    /**
     *菜单删除
     * @param menu_id
     * @throws SQLException
     */
    public static void menuDelete(String menu_id) throws SQLException{
        String sql = "update tb_menu set is_delete=1 where menu_id=?";
        ArrayList<Object> pramas = new ArrayList<>();
        pramas.add(menu_id);
        int res= MysqlBaseUtil.updateSQL(sql,pramas);
        if(res<1){
            throw new SQLException("menuDelete error");
        }
    }

    /**
     * 菜单修改
     * @param menu
     * @throws SQLException
     */
    public static void menuUpdate (Map<String,String> menu) throws SQLException{
        if (menu.size()<2){
            return;
        }
        String sqlparamsStr = "";
        ArrayList<Object> pramas = new ArrayList<>();
        if(!StringUtils.isEmpty(menu.get("menu_name"))){
            sqlparamsStr+="menu_name=?,";
            pramas.add(menu.get("menu_name"));
        }
        if(!StringUtils.isEmpty(menu.get("menu_code"))){
            sqlparamsStr+="menu_code=?,";
            pramas.add(menu.get("menu_code"));
        }
        if(!StringUtils.isEmpty(menu.get("menu_link"))){
            sqlparamsStr+="menu_link=?,";
            pramas.add(menu.get("menu_link"));
        }
        if(!StringUtils.isEmpty(menu.get("parent_id"))){
            sqlparamsStr+="parent_id=?,";
            pramas.add(menu.get("parent_id"));
        }
        if(!StringUtils.isEmpty(menu.get("product_id"))){
            sqlparamsStr+="product_id=?,";
            pramas.add(menu.get("product_id"));
        }

        if(!StringUtils.isEmpty(menu.get("is_sys_menu"))){
            sqlparamsStr+="is_sys_menu=?,";
            pramas.add(menu.get("is_sys_menu"));
        }

        if(!StringUtils.isEmpty(menu.get("menu_type"))){
            sqlparamsStr+="menu_type=?,";
            pramas.add(menu.get("menu_type"));
        }

        if(!StringUtils.isEmpty(menu.get("is_top"))){
            sqlparamsStr+="is_top=?,";
            pramas.add(menu.get("is_top"));
        }

        if(!StringUtils.isEmpty(menu.get("menu_order"))){
            sqlparamsStr+="menu_order=?,";
            pramas.add(menu.get("menu_order"));
        }

        if(!StringUtils.isEmpty(menu.get("is_enable"))){
            sqlparamsStr+="is_enable=?,";
            pramas.add(menu.get("is_enable"));
        }

        pramas.add(menu.get("menu_id"));
        sqlparamsStr = StringUtils.substring(sqlparamsStr,0,sqlparamsStr.length()-1);
        String sql = "update tb_menu set "+ sqlparamsStr + " where menu_id=?";
        int res= MysqlBaseUtil.updateSQL(sql,pramas);
        if(res<1){
            throw new SQLException("menuUpdate error");
        }
    }


    /**
     * 检查菜单code
     * @param username
     * @return
     * @throws SQLException
     */
    public  static HashMap<String ,String> CheckMenuCode(String menu_code) throws SQLException{
        String sql = "select menu_id ,menu_code from tb_menu where menu_code=? and is_delete=0";
        ArrayList<Object> params = new ArrayList<>();
        params.add(menu_code);
        return  MysqlBaseUtil.getOneRow(sql,params);
    }



}
