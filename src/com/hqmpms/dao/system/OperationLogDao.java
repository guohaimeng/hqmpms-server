package com.hqmpms.dao.system;

import com.hqmpms.dispatcher.Startup;
import com.hqmpms.utils.DoCachedThreadPool;
import com.hqmpms.utils.MysqlBaseUtil;
import com.sun.corba.se.spi.ior.ObjectId;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * 系统操作日志
 */
public class OperationLogDao {
    private static final Logger m_logger = Logger.getLogger(OperationLogDao.class) ;


    /**
     * 记录日志
     * @throws SQLException
     */
    public static void operationLogRecord(String creator_id,String content,String object_id){
        try {
            DoCachedThreadPool.getCachedThreadPool().cachedThreadPool.execute(new DoOperationLogThread(creator_id, content, object_id));
        }catch (Exception e){
            m_logger.error(String.format("operationLogAdd fail params=%s",e.getMessage()));
        }
    }


    public static void operationLogAddDB(String creator_id,String content,String object_id) throws SQLException{
        String sql = "insert into tb_operation_log (operation_log_id,creator_id,content,object_id) values (?,?,?,?)";
        String newid= String.valueOf(Startup.getId());
        ArrayList<Object> pramas = new ArrayList<>();
        pramas.add(newid);
        pramas.add(creator_id);
        pramas.add(content);
        pramas.add(object_id);
        int res= MysqlBaseUtil.updateSQL(sql,pramas);
        if(res<1){
            throw new SQLException("OperationLogAdd error");
        }
    }



    /**
     * 查询日志列表
     * @param pagesize
     * @param pagenum
     * @return
     * @throws SQLException
     */
    public static  HashMap<String,Object> operationLogList (
            String pagenum,
            String pagesize ,
            String operator,
            String ObjectId,
            String startTime,
            String endTime) throws SQLException{
        HashMap<String,Object> result = new HashMap<>();
        ArrayList<HashMap<String,String>> list = new ArrayList<HashMap<String,String>>();
        String where =" where 1=1";
        if(!StringUtils.isEmpty(operator)){
            where+=" AND creator_id IN ( SELECT manager_id FROM tb_manager WHERE real_name LIKE '%"+operator+"%') ";
        }
        if(!StringUtils.isEmpty(ObjectId)){
            where+=" AND object_id='"+ObjectId+"'";
        }

        if(!StringUtils.isEmpty(startTime)){
            where+=" AND create_time >='"+startTime+"'";
        }

        if(!StringUtils.isEmpty(endTime)){
            where+=" AND create_time <='"+endTime+"'";
        }


        String countSql=" SELECT COUNT(1) FROM tb_operation_log" + where;

        String count= MysqlBaseUtil.getOneColumnByRow(countSql, new ArrayList<Object>());
        result.put("count",count);
        if(Integer.parseInt(count)==0){
            result.put("list", list);
            return result;
        }

        String sql =" SELECT t.*, m.real_name AS creator_name FROM(SELECT operation_log_id, creator_id, content, create_time, object_id FROM tb_operation_log "+ where +" ORDER BY operation_log_id DESC LIMIT ?,?) t LEFT JOIN tb_manager m ON t.creator_id = m.manager_id  ";

        ArrayList<Object> param = new ArrayList<>();
        param.add((Integer.parseInt(pagenum)-1)*Integer.parseInt(pagesize));
        param.add(Integer.parseInt(pagesize));

        list = MysqlBaseUtil.querySql(sql,param);
        result.put("list", list);
        return result;
    }

}
