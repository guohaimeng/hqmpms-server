package com.hqmpms.dao.system;

import com.hqmpms.api.utils.EasyUiMenu;
import com.hqmpms.api.utils.EasyUiTree;
import com.hqmpms.utils.MysqlBaseUtil;
import org.apache.log4j.Logger;
import org.json.JSONArray;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class PermissionDao {
    private static final Logger m_logger = Logger.getLogger(PermissionDao.class) ;

    /**
     * 查询管理员权限from DB
     * @param mid
     * @return
     * @throws SQLException
     */
    public static HashMap<String,Object> getManagerPermission(String mid) throws SQLException{
        String menusql = "SELECT m.menu_code,m.menu_id,m.menu_name,m.menu_link,m.menu_type,m.parent_id FROM tb_menu m WHERE menu_id IN ( SELECT menu_id FROM tb_role_menu WHERE role_id IN ( SELECT role_id FROM tb_manager_role WHERE manager_id = ? ) ) AND m.is_enable = 1 AND m.is_delete = 0";
        ArrayList<Object> param = new ArrayList<>();
        param.add(mid);
        ArrayList<HashMap<String,String>> menus = MysqlBaseUtil.querySql(menusql,param);
        StringBuffer permissionsbuff= new StringBuffer();

        if(menus.size()>0){
            for(HashMap<String,String> menu:menus){
                permissionsbuff.append(menu.get("menu_code")+",");

            }
            permissionsbuff.deleteCharAt(permissionsbuff.length()-1);
        }


        String productsql = "SELECT t.product_id, p.product_name FROM ( SELECT m.product_id FROM tb_menu m WHERE menu_id IN ( SELECT menu_id FROM tb_role_menu WHERE role_id IN ( SELECT role_id FROM tb_manager_role WHERE manager_id = ? ) ) AND m.is_enable = 1 AND m.is_delete = 0 GROUP BY product_id ) t LEFT JOIN tb_product p ON t.product_id = p.product_id WHERE p.is_enable = 1";
        ArrayList<HashMap<String,String>> products = MysqlBaseUtil.querySql(productsql,param);
        HashMap<String,Object> res = new HashMap<>();
        res.put("permissions", permissionsbuff.toString());
        res.put("permiproducts",new JSONArray(products).toString());
        if(products.size()>0){
            String [] productmenus = getHomeMenus(mid, products.get(0).get("product_id"));
            res.put("menus",productmenus[1]);
            res.put("loginpid",products.get(0).get("product_id"));
            res.put("loginpcode",productmenus[0]);
        }else{
            res.put("menus","");
            res.put("loginpid","");
            res.put("loginpcode","");
        }

        return res;
    }


    /**
     * 查询home页面菜单
     * @param mid
     * @param pid
     * @return
     * @throws SQLException
     */
    public static String[] getHomeMenus(String mid,String pid) throws SQLException{
        String [] res = new String[2];
        String productSql = "select product_id,product_code from tb_product where product_id=? and is_enable=1";
        ArrayList<Object> productparam = new ArrayList<>();
        productparam.add(pid);
        ArrayList<HashMap<String,String>> productList = MysqlBaseUtil.querySql(productSql,productparam);
        if(productList.size()<1){
            return null;
        }else{
            res[0] = productList.get(0).get("product_code");
        }

        String menusql = "SELECT m.menu_code,m.menu_id,m.menu_name,m.menu_link,m.menu_type,m.parent_id FROM tb_menu m WHERE menu_id IN ( SELECT menu_id FROM tb_role_menu WHERE role_id IN ( SELECT role_id FROM tb_manager_role WHERE manager_id = ? ) ) AND m.is_enable = 1 AND m.is_delete = 0 AND m.product_id=?";
        ArrayList<Object> param = new ArrayList<>();
        param.add(mid);
        param.add(pid);
        ArrayList<HashMap<String,String>> menus = MysqlBaseUtil.querySql(menusql,param);

        ArrayList<EasyUiMenu> rootnode = new ArrayList<>();
        ArrayList<HashMap<String,String>> subnode = new ArrayList<>();
        ArrayList<String> pids = new ArrayList<>();
        for(HashMap<String,String> menu:menus){
            pids.add(menu.get("menu_id"));
        }
        for(HashMap<String,String> menu:menus){
            if(pids.contains(menu.get("parent_id"))){
                subnode.add(menu);
            }else{
                rootnode.add(buildMenuObject(menu));
            }
        }

        for(EasyUiMenu node:rootnode){
            buildMenuNode(node,subnode);
        }
        res[1] = new JSONArray(rootnode).toString();
        return res;
    }



    private static   void  buildMenuNode(EasyUiMenu node, ArrayList<HashMap<String,String>> nodeList){
        for(HashMap<String,String> map :nodeList){
            if(map.get("parent_id").equals(node.getMenuid())){
                EasyUiMenu tnode = buildMenuObject(map);
                node.addChildren(tnode);
                buildMenuNode(tnode,nodeList);
            }
        }
    }


    private static EasyUiMenu buildMenuObject ( HashMap<String,String> map){
        EasyUiMenu tnode = new EasyUiMenu();
        tnode.setMenuid(map.get("menu_id"));
        tnode.setMenuname(map.get("menu_name"));
        tnode.setType(map.get("menu_type"));
        tnode.setUrl(map.get("menu_link"));
        tnode.setCode(map.get("menu_code"));
        return  tnode;
    }
}
