package com.hqmpms.dao.system;

import com.hqmpms.utils.MysqlBaseUtil;
import org.apache.log4j.Logger;
import org.apache.commons.lang3.StringUtils;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Administrator on 2018/5/21.
 */
public class ProductDao {
    private static final Logger m_logger = Logger.getLogger(ProductDao.class);
    /**
     * 检验产品是否存在
     * @param dbname 数据名称
     * @return
     * @throws SQLException
     */
    public static Boolean isProductExist(String  dbname,String procode,String productid)throws SQLException{
        String  sql="select product_id from tb_product where 1!=1 ";
        ArrayList<Object> param = new ArrayList<Object>();
        if (!StringUtils.isEmpty(dbname)) {
            sql+="  or product_name = ? ";
            param.add(dbname);
        }
        if (!StringUtils.isEmpty(procode)) {
            sql+="  or  product_code=?";
            param.add(procode);
        }

        return  MysqlBaseUtil.querySql(sql, param).size()>0?true:false;
    }
    /**
     * 根据ID检验产品是否存在
     * @param dbname 数据名称
     * @return
     * @throws SQLException
     */
    public static Boolean isProductExistByid(String  dbname,String procode,String productid)throws SQLException{
        String  sql="select product_id from tb_product  where  product_id!=?  ";
        ArrayList<Object> param = new ArrayList<Object>();
        param.add(productid);
        if (!StringUtils.isEmpty(dbname) && StringUtils.isEmpty(procode)) {
            sql+=" and product_name = ? ";
            param.add(dbname);
        }
        if (!StringUtils.isEmpty(procode) && StringUtils.isEmpty(dbname)) {
            sql+="  and   product_code=?";
            param.add(procode);
        }
        if (!StringUtils.isEmpty(procode) && !StringUtils.isEmpty(dbname)) {
            sql+="  and   (product_code=? or product_name = ?)";
            param.add(procode);
            param.add(dbname);
        }

        return  MysqlBaseUtil.querySql(sql, param).size()>0?true:false;
    }

    /**
     * 添加产品
     * @param condition 数据参数
     * @return
     * @throws SQLException
     */
    public static Boolean addProduct(HashMap<String,String> condition)throws SQLException{
        String  sql="INSERT INTO tb_product(product_id,creator_id,product_name,db_manage_sys,jdbc_url,db_name,db_password,is_enable,product_code";
        String  sqlval=")VALUES(?,?,?,?,?,?,?,?,?";
        ArrayList<Object> params = new ArrayList<Object>();
        params.add(condition.get("productid"));
        params.add(condition.get("creatorid"));
        params.add(condition.get("productname"));
        params.add(condition.get("dbmanagesys"));
        params.add(condition.get("jdbcurl"));
        params.add(condition.get("dbname"));
        params.add(condition.get("dbpwd"));
        params.add(condition.get("isenable"));
        params.add(condition.get("procode"));
        if(!StringUtils.isEmpty(condition.get("remark"))){
            sql+=",remark";
            sqlval+=",?";
            params.add(condition.get("remark"));
        }
        if(!StringUtils.isEmpty(condition.get("description"))){
            sql+=",description";
            sqlval+=",?";
            params.add(condition.get("description"));
        }
        sql = sql+sqlval+")";
        return  MysqlBaseUtil.updateSQL(sql,params)>0?true:false;
    }

    /**
     * 获取产品列表
     * @param page
     * @param pagesize
     * @throws SQLException
     */
    public  static HashMap<String,Object> getProductList(int page,int pagesize) throws SQLException{
        HashMap<String,Object> map = new HashMap<String,Object>();

        String sqlcount ="select count(product_id) from tb_product where  product_id!=0 ";
        ArrayList<Object> paramscount = new ArrayList<>();
        String   count = MysqlBaseUtil.getOneColumnByRow(sqlcount,paramscount);

        String sql;
        ArrayList<Object> params;
        ArrayList<HashMap<String,String>> rows = new ArrayList<HashMap<String,String>>();
        if(!count.equals("0")){
            sql ="SELECT product_id,creator_id,product_name,db_manage_sys,jdbc_url,db_name,db_password,remark,description,is_enable,create_time,product_code as procode,(SELECT real_name FROM tb_manager  WHERE manager_id = tbp.creator_id) AS real_name FROM tb_product AS tbp  where  tbp.product_id!=0  ORDER BY create_time DESC,product_id DESC LIMIT ?,?";
            params = new ArrayList<>();
            params.add(((page-1)*pagesize));
            params.add(pagesize);
            rows = MysqlBaseUtil.querySql(sql,params);
        }
        map.put("count",Integer.parseInt(count));
        map.put("rows",rows);
        return map;
    }

    /**
     * 修改产品
     * @param condition 数据参数
     * @return
     * @throws SQLException
     */
    public static Boolean updateProduct(HashMap<String,String> condition)throws SQLException{
        String  sql="UPDATE tb_product set creator_id=?,product_name=?,db_manage_sys=?,jdbc_url=?,db_name=?,db_password=?,is_enable=?,create_time = NOW()";
        ArrayList<Object> params = new ArrayList<Object>();
        params.add(condition.get("creatorid"));
        params.add(condition.get("productname"));
        params.add(condition.get("dbmanagesys"));
        params.add(condition.get("jdbcurl"));
        params.add(condition.get("dbname"));
        params.add(condition.get("dbpwd"));
        params.add(condition.get("isenable"));
        if(!StringUtils.isEmpty(condition.get("procode"))){
            sql+=",product_code=?";
            params.add(condition.get("procode"));
        }
        if(!StringUtils.isEmpty(condition.get("remark"))){
            sql+=",remark=?";
            params.add(condition.get("remark"));
        }
        if(!StringUtils.isEmpty(condition.get("description"))){
            sql+=",description=?";
            params.add(condition.get("description"));
        }
        sql+=" where  product_id = ?";
        params.add(condition.get("productid"));
        return  MysqlBaseUtil.updateSQL(sql,params)>0?true:false;
    }


    /**
     * 获取产品列表
     * @param productid
     * @throws SQLException
     */
    public  static HashMap<String,String> getProductById(String productid) throws SQLException{

        String  sql ="SELECT product_id,product_name,product_code  FROM tb_product   where  product_id=? ";
        ArrayList<Object> params = new ArrayList<>();
        params.add(productid);
        return MysqlBaseUtil.getOneRow(sql,params);

    }

}
