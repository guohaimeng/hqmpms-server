package com.hqmpms.dao.system;

import com.hqmpms.utils.MysqlBaseUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RoleDao {
    private static final Logger m_logger = Logger.getLogger(RoleDao.class) ;


    /**
     * 添加系统角色
     * @throws SQLException
     */
    public static void roleAdd(Map<String,String> role) throws SQLException {
        String sql = "insert into tb_role (role_id,role_name,creator_id) values (?,?,?)";
        ArrayList<Object> pramas = new ArrayList<>();
        String roleId= role.get("role_id");
        pramas.add(roleId);
        pramas.add(role.get("role_name"));
        pramas.add(role.get("creator_id"));

        String[] mids = role.get("mids").split(",");
        StringBuffer roleMenuSqlbuff = new StringBuffer("insert into tb_role_menu(role_id,menu_id) values");
        for(String mid:mids){
            roleMenuSqlbuff.append("('"+roleId+"','"+mid+"'),");
        }
        roleMenuSqlbuff.deleteCharAt(roleMenuSqlbuff.length()-1);
        String roleMenuSql = roleMenuSqlbuff.toString();
        ArrayList<ArrayList<Object>> paramGroups = new ArrayList<>();
        ArrayList<String> sqlList = new ArrayList<>();
        sqlList.add(sql);
        sqlList.add(roleMenuSql);
        paramGroups.add(pramas);
        paramGroups.add(new ArrayList<Object>());
        boolean res= MysqlBaseUtil.batchUpdateSql(sqlList,paramGroups);
        if(!res){
            throw new SQLException("roleAdd error");
        }
    }


    /**
     * 查询角色列表
     * @param pagesize
     * @param pagenum
     * @return
     * @throws SQLException
     */
    public static  ArrayList<HashMap<String,String>> roleList (String pagenum, String  pagesize) throws SQLException{
        String sql =" SELECT t.*, m.real_name AS creator_name FROM ( SELECT role_id, role_name, is_enable, create_time,creator_id FROM tb_role where is_delete=0 order by role_id desc LIMIT ?,?  ) t LEFT JOIN tb_manager m ON t.creator_id = m.manager_id ";
        ArrayList<Object> param = new ArrayList<>();
        param.add((Integer.parseInt(pagenum)-1)*Integer.parseInt(pagesize));
        param.add(Integer.parseInt(pagesize));

        return MysqlBaseUtil.querySql(sql,param);
    }


    /**
     * 查询角色详情
     * @param pagesize
     * @param pagenum
     * @return
     * @throws SQLException
     */
    public static  HashMap<String,String> roleInfo (String role_id) throws SQLException{
        String sql ="select role_id,role_name,is_enable,(SELECT GROUP_CONCAT(menu_id) FROM tb_role_menu WHERE role_id=r.role_id) as mids from tb_role r where r.role_id=? and r.is_delete=0";
        ArrayList<Object> param = new ArrayList<>();
        param.add(role_id);
        MysqlBaseUtil.updateSQL("SET SESSION group_concat_max_len = 10240",new ArrayList<Object>());
        return MysqlBaseUtil.getOneRow(sql,param);
    }


    /**
     * 查询总数
     * @return
     * @throws SQLException
     */
    public static  String queryCount()throws SQLException{
        String sql = "SELECT COUNT(1) FROM tb_role where is_delete=0  ";
        return MysqlBaseUtil.getOneColumnByRow(sql, new ArrayList<Object>());
    }


    /**
     * 查询角色选择树
     * @param pagesize
     * @param pagenum
     * @return
     * @throws SQLException
     */
    public static  ArrayList<HashMap<String,String>> roleComboTree (String product_id) throws SQLException{

        String sql ="SELECT m.role_id, m.role_name, m.parent_id, m.role_type, m.role_order FROM tb_role m WHERE m.is_delete = 0 AND m.product_id = ? ORDER BY m.role_order";
        ArrayList<Object> param = new ArrayList<>();
        param.add(product_id);
        return MysqlBaseUtil.querySql(sql,param);
    }



    /**
     * 构造角色节点
     * @param role_id
     * @param role_name
     * @return
     */
    private static HashMap<String, String> builRoleTopNode(String role_id,String role_name){
        HashMap<String, String> sysTopNode = new HashMap<>();
        sysTopNode.put("role_id",role_id);
        sysTopNode.put("role_name",role_name);
        sysTopNode.put("role_code","");
        sysTopNode.put("role_link","");
        sysTopNode.put("product_id","");
        sysTopNode.put("is_sys_role","");
        sysTopNode.put("is_top","");
        sysTopNode.put("role_type","");
        sysTopNode.put("role_order","");
        sysTopNode.put("is_enable","");
        sysTopNode.put("create_time","");
        sysTopNode.put("creator_name","");
        return sysTopNode;
    }

    /**
     * 查询产品id和名称
     * @param pagesize
     * @param pagenum
     * @return
     * @throws SQLException
     */
    public static  ArrayList<HashMap<String,String>> queryProductIdAndName () throws SQLException{
        ArrayList<HashMap<String, String>> resultList = new ArrayList<>();
        String sysRolesql ="SELECT product_id,product_name FROM tb_product WHERE is_enable=1";
        ArrayList<Object> param = new ArrayList<>();

        return MysqlBaseUtil.querySql(sysRolesql,param);
    }

    /**
     *角色删除
     * @param role_id
     * @throws SQLException
     */
    public static void roleDelete(String role_id) throws SQLException{
        String sql = "update tb_role set is_delete=1 where role_id=?";
        ArrayList<Object> pramas = new ArrayList<>();
        pramas.add(role_id);
        int res= MysqlBaseUtil.updateSQL(sql,pramas);
        if(res<1){
            throw new SQLException("roleDelete error");
        }
    }


    /**
     * 查询使用该角色的管理员
     * @param roleId
     * @return
     * @throws SQLException
     */
    public static ArrayList<HashMap<String, String>> selectRoleUseInManager(String roleId)  throws SQLException{
        String sql = "select m.manager_id,m.real_name,m.user_name from tb_manager m where is_delete=0 and m.manager_id in(select manager_id from tb_manager_role where role_id=?)";
        ArrayList<Object> params = new ArrayList<>();
        params.add(roleId);
        return  MysqlBaseUtil.querySql(sql,params);
    }

    /**
     * 角色修改
     * @param role
     * @throws SQLException
     */
    public static void roleUpdate (Map<String,String> role) throws SQLException{
        if (role.size()<2){
            return;
        }
        String sqlparamsStr = "";
        ArrayList<Object> pramas = new ArrayList<>();
        if(!StringUtils.isEmpty(role.get("role_name"))){
            sqlparamsStr+="role_name=?,";
            pramas.add(role.get("role_name"));
        }

        if(!StringUtils.isEmpty(role.get("is_enable"))){
            sqlparamsStr+="is_enable=?,";
            pramas.add(role.get("is_enable"));
        }

        pramas.add(role.get("role_id"));
        sqlparamsStr = StringUtils.substring(sqlparamsStr,0,sqlparamsStr.length()-1);
        String sql = "update tb_role set "+ sqlparamsStr + " where role_id=?";

        String clearroleMenuSql="delete from tb_role_menu where role_id="+role.get("role_id");

        String[] mids = role.get("mids").split(",");
        m_logger.info("roleUpdate:m_size="+mids.length);
        StringBuffer roleMenuSqlbuff = new StringBuffer("insert into tb_role_menu(role_id,menu_id) values");
        for(String mid:mids){
            roleMenuSqlbuff.append("('"+role.get("role_id")+"','"+mid+"'),");
        }
        roleMenuSqlbuff.deleteCharAt(roleMenuSqlbuff.length()-1);
        String roleMenuSql = roleMenuSqlbuff.toString();

        ArrayList<ArrayList<Object>> paramGroups = new ArrayList<>();
        ArrayList<String> sqlList = new ArrayList<>();

        sqlList.add(sql);
        sqlList.add(clearroleMenuSql);
        sqlList.add(roleMenuSql);

        paramGroups.add(pramas);
        paramGroups.add(new ArrayList<Object>());
        paramGroups.add(new ArrayList<Object>());

        boolean res= MysqlBaseUtil.batchUpdateSql(sqlList,paramGroups);
        if(!res){
            throw new SQLException("roleUpdate error");
        }
    }


    /**
     * 根据code查询角色
     * @param pagesize
     * @param pagenum
     * @return
     * @throws SQLException
     */
    public static  ArrayList<HashMap<String,String>> getroleByCode (String code) throws SQLException{
        String sql ="SELECT role_value,role_text FROM tb_role WHERE role_code=? AND is_delete=0";
        ArrayList<Object> param = new ArrayList<>();
        param.add(code);
        return MysqlBaseUtil.querySql(sql,param);
    }


    /**
     * 查询产品id和名称
     * @param pagesize
     * @param pagenum
     * @return
     * @throws SQLException
     */
    public static  ArrayList<HashMap<String,String>> queryRoleIdAndName () throws SQLException{
        ArrayList<HashMap<String, String>> resultList = new ArrayList<>();
        String sysMenusql ="SELECT role_id,role_name FROM tb_role WHERE is_enable=1 and is_delete=0";
        ArrayList<Object> param = new ArrayList<>();

        return MysqlBaseUtil.querySql(sysMenusql,param);
    }


    /**
     * 检查角色名
     * @param username
     * @return
     * @throws SQLException
     */
    public  static HashMap<String ,String> CheckRoleName(String Rolename) throws SQLException{
        String sql = "select role_id ,role_name from tb_role where role_name=? and is_delete=0";
        ArrayList<Object> params = new ArrayList<>();
        params.add(Rolename);
        return  MysqlBaseUtil.getOneRow(sql,params);
    }
}
