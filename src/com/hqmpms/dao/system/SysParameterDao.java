package com.hqmpms.dao.system;

import com.hqmpms.utils.MysqlBaseUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SysParameterDao {
    private static final Logger m_logger = Logger.getLogger(SysParameterDao.class) ;


    /**
     * 添加系统参数
     * @throws SQLException
     */
    public static void sysParameterAdd(Map<String,String> sysParameter) throws SQLException {
        String sql = "insert into tb_system_parameter (parameter_id,parameter_name,parameter_code,parameter_value,parameter_text,creator_id) values (?,?,?,?,?,?)";
        ArrayList<Object> pramas = new ArrayList<>();
        pramas.add(sysParameter.get("parameter_id"));
        pramas.add(sysParameter.get("parameter_name"));
        pramas.add(sysParameter.get("parameter_code"));
        pramas.add(sysParameter.get("parameter_value"));
        pramas.add(sysParameter.get("parameter_text"));
        pramas.add(sysParameter.get("creator_id"));
        int res= MysqlBaseUtil.updateSQL(sql,pramas);
        if(res<1){
            throw new SQLException("SysParameterAdd error");
        }
    }

    /**
     * 查询总数
     * @return
     * @throws SQLException
     */
    public static  String queryCount()throws SQLException{
        String sql = "SELECT COUNT(1) FROM tb_system_parameter where is_delete=0  ";
        return MysqlBaseUtil.getOneColumnByRow(sql, new ArrayList<Object>());
    }

    /**
     * 查询参数列表
     * @param pagesize
     * @param pagenum
     * @return
     * @throws SQLException
     */
    public static  ArrayList<HashMap<String,String>> sysParameterList (String pagenum, String  pagesize ) throws SQLException{
        String sql =" SELECT t.*, m.real_name AS creator_name FROM ( SELECT parameter_id, parameter_name, parameter_code, parameter_value, parameter_text , create_time, creator_id FROM tb_system_parameter where is_delete=0 order by parameter_id desc LIMIT ?,?  ) t LEFT JOIN tb_manager m ON t.creator_id = m.manager_id ";
        ArrayList<Object> param = new ArrayList<>();
        param.add((Integer.parseInt(pagenum)-1)*Integer.parseInt(pagesize));
        param.add(Integer.parseInt(pagesize));

        return MysqlBaseUtil.querySql(sql,param);
    }

    /**
     *参数删除
     * @param parameter_id
     * @throws SQLException
     */
    public static void sysParameterDelete(String parameter_id) throws SQLException{
        String sql = "update tb_system_parameter set is_delete=1 where parameter_id=?";
        ArrayList<Object> pramas = new ArrayList<>();
        pramas.add(parameter_id);
        int res= MysqlBaseUtil.updateSQL(sql,pramas);
        if(res<1){
            throw new SQLException("sysParameterDelete error");
        }
    }

    /**
     * 参数修改
     * @param sysParameter
     * @throws SQLException
     */
    public static void sysParameterUpdate (Map<String,String> sysParameter) throws SQLException{
        if (sysParameter.size()<2){
            return;
        }
        String sqlparamsStr = "";
        ArrayList<Object> pramas = new ArrayList<>();
        if(!StringUtils.isEmpty(sysParameter.get("parameter_name"))){
            sqlparamsStr+="parameter_name=?,";
            pramas.add(sysParameter.get("parameter_name"));
        }
        if(!StringUtils.isEmpty(sysParameter.get("parameter_code"))){
            sqlparamsStr+="parameter_code=?,";
            pramas.add(sysParameter.get("parameter_code"));
        }
        if(!StringUtils.isEmpty(sysParameter.get("parameter_value"))){
            sqlparamsStr+="parameter_value=?,";
            pramas.add(sysParameter.get("parameter_value"));
        }
        if(!StringUtils.isEmpty(sysParameter.get("parameter_text"))){
            sqlparamsStr+="parameter_text=?,";
            pramas.add(sysParameter.get("parameter_text"));
        }
        pramas.add(sysParameter.get("parameter_id"));
        sqlparamsStr = StringUtils.substring(sqlparamsStr,0,sqlparamsStr.length()-1);
        String sql = "update tb_system_parameter set "+ sqlparamsStr + " where parameter_id=?";
        int res= MysqlBaseUtil.updateSQL(sql,pramas);
        if(res<1){
            throw new SQLException("sysParameterUpdate error");
        }
    }


    /**
     * 根据code查询参数
     * @param pagesize
     * @param pagenum
     * @return
     * @throws SQLException
     */
    public static  ArrayList<HashMap<String,String>> getSysParameterByCode (String code) throws SQLException{
        String sql ="SELECT parameter_value,parameter_text FROM tb_system_parameter WHERE parameter_code=? AND is_delete=0";
        ArrayList<Object> param = new ArrayList<>();
        param.add(code);
        return MysqlBaseUtil.querySql(sql,param);
    }
}
