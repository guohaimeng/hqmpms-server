package com.hqmpms.dao.userbehavior;

import com.hqmpms.utils.MysqlBaseUtil;
import com.hqmpms.utils.SqlServerBaseUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 用户操作记录列表
 * Created by dml on  2018/5/25
 */
public class UserBehaviorDao {

    /**
     * 根据筛选信息查询开通移动端的用户列表
     * @param productId     产品编号
     * @param temp           搜索类型(字段)
     * @param search         查询内容
     */
    public static List<HashMap<String, String>>  getUserBehaviorListByAllConditions(String productId,String temp,String search,String userIds,Integer page,Integer pageSize) throws SQLException{
            ArrayList<Object> params = new ArrayList<>();
            String  sql = "SELECT * FROM tb_user_behavior WHERE product_id = ? ";
            params.add(productId);
            if(!StringUtils.isEmpty(temp)&&!StringUtils.isEmpty(search)){
                String sqlServerSql = "SELECT ID FROM [User] WHERE "+temp+" = ?;";
                ArrayList<Object> sqlServerParams = new ArrayList<>();
                sqlServerParams.add(search);
                HashMap<String,String> userMap = SqlServerBaseUtil.getOneRow(sqlServerSql,sqlServerParams);
                sql += " AND UserID = ? ";
                params.add(userMap.get("ID"));
            }
            if (!StringUtils.isEmpty(userIds)){
                    sql += " AND FIND_IN_SET(UserID,?)";
                    params.add(userIds);
            }
        List<HashMap<String, String>> userList;
        //分页查询
        if (page!=null&&pageSize!=null){
            sql += " LIMIT ?,?";
            params.add((page-1)*pageSize);
            params.add(pageSize);
        }
        userList =  MysqlBaseUtil.querySql(sql, params);
        if(!CollectionUtils.isEmpty(userList)){
            for (HashMap<String, String> user:userList) {
                 sql = "SELECT UserName,RealName FROM [User] WHERE ID = ?;";
                 params = new ArrayList<>();
                 params.add(user.get("UserID"));
                 HashMap<String,String> userMap = SqlServerBaseUtil.getOneRow(sql,params);
                 user.putAll(userMap);
            }
        }
        return userList;
    }

    /**
     * 新增用户操作记录
     * @param userId                用户编号
     * @param osType              终端类型
     * @param operateType     操作类型
     * @param productId         产品编号
     */
    public static int userBehaviorAdd(long id,String userId,String osType,String operateType,String productId) throws SQLException{
            String sql = "INSERT INTO tb_user_behavior VALUES (?,?,?,?,NOW(),?);";
            ArrayList<Object> params = new ArrayList<>();
            params.add(id);
            params.add(userId);
            params.add(osType);
            params.add(operateType);
            params.add(productId);
            return  MysqlBaseUtil.updateSQL(sql, params);
    }
}
