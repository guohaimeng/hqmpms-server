package com.hqmpms.dao.userlist;

import com.hqmpms.utils.SqlServerBaseUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 开通移动端的用户列表
 * Created by dml on  2018/5/24
 */
public class UserListDao {

    /**
     * 根据筛选信息查询开通移动端的用户列表
     * @param temp           搜索类型(字段)
     * @param search         查询内容
     */
    public static List<HashMap<String, String>>  getUserListByAllConditions(String userIds,String temp,String search,Integer page,Integer pageSize) throws SQLException{
        String  sql;
        ArrayList<Object> params = new ArrayList<>();
            //排除法  如果班级编号或学校编号不为空，就查询学校所有
            if(!StringUtils.isEmpty(userIds)){
                    sql = "SELECT * FROM(SELECT b.ID,b.UserName,b.RealName,b.UserType,b.IsAudit,b.RegDate,b.IDCard,b.Mobile FROM [User] b WHERE b.ID IN(SELECT uid FROM yx_user_extend) AND  CHARINDEX(b.ID,?)>0) a";
                params.add(userIds);
            }else {
                sql = "SELECT * FROM(SELECT b.ID,b.UserName,b.RealName,b.UserType,b.IsAudit,b.RegDate,b.IDCard,b.Mobile FROM [User] b WHERE b.ID IN(SELECT uid FROM yx_user_extend)) a";
            }
            if(!StringUtils.isEmpty(temp)&&!StringUtils.isEmpty(search)){
                sql += " WHERE a."+temp+" = ?";
                params.add(search);
            }
        List<HashMap<String, String>> userList;
            //分页查询
            if (page!=null&&pageSize!=null){
                sql += " ORDER BY a.ID OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
                params.add((page-1)*pageSize);
                params.add(pageSize);
                userList = SqlServerBaseUtil.querySql(sql, params);
                if (!CollectionUtils.isEmpty(userList)){
                    for (HashMap<String, String> user:userList) {
                        //学生
                        if("7".equals(user.get("UserType"))){
                            sql = "SELECT a.Phase,b.Name as schoolName,d.Name as county ,(SELECT nick_code FROM yx_class_info WHERE class_id = (SELECT TOP 1 class_id FROM yx_class_members WHERE uid = '"+user.get("ID")+"')) as className,(SELECT Name FROM District WHERE ID = d.ParentID) as city,(SELECT Name FROM District WHERE ID =(SELECT ParentID FROM District WHERE ID = d.ParentID)) as proName " +
                                    "FROM Student a INNER JOIN School b ON a.SchoolID = b.ID " +
                                    "INNER JOIN yx_class_members e ON a.UserID = e.uid " +
                                    "INNER JOIN District d ON b.DistrictID = d.ID " +
                                    "WHERE a.UserID = ?";

                        }else {
                            //老师
                            sql = "SELECT a.Phase,b.Name as schoolName,c.Name as county ,(SELECT Name FROM District WHERE ID = c.ParentID) as city,(SELECT Name FROM District WHERE ID =(SELECT ParentID FROM District WHERE ID = c.ParentID)) as proName FROM Teacher a INNER JOIN School b ON a.SchoolID = b.ID INNER JOIN District c ON b.DistrictID = c.ID WHERE a.UserID = ?;";
                        }
                        params = new ArrayList<>();
                        params.add(user.get("ID"));
                        HashMap<String,String> userMap = SqlServerBaseUtil.getOneRow(sql,params);
                        user.putAll(userMap);
                    }
                }
            }else {
                userList =  SqlServerBaseUtil.querySql(sql, params);
            }
        return userList;
    }

    /**
     * 根据用户编号查询详情
     * @param userId   用户编号
     */
    public static HashMap<String, String>  getUserDetailById(String userId) throws SQLException{
        String  sql = "SELECT a.ID,a.UserName,a.RealName,a.Sex,a.IsAudit,a.Birthday,a.Nation,a.Mobile,a.IDCard,a.Tel,a.QQ,a.PostCode,a.Address,a.Remark FROM [User] a WHERE ID = ?;";
        ArrayList<Object> params = new ArrayList<>();
        params.add(userId);
        return SqlServerBaseUtil.getOneRow(sql, params);
    }
}
