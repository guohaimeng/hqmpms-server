package com.hqmpms.dispatcher;


import com.hqmpms.api.utils.DatabaseManager;
import com.hqmpms.utils.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;


/**
 * SERVLET
 * @author Jason.Liu
 *
 */
@WebServlet("/Startup")
public class Startup extends HttpServlet
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger m_logger = Logger.getLogger(Startup.class);
	
	/**
	 * singleton模式中的唯一实例
	 */
	private static Startup m_instance = new Startup();
	
	public static Startup getInstance()
	{
		return m_instance;
	}
	
	
	private String m_servletConfigPath;
	
	public String getServletConfigPath(){
		return m_servletConfigPath;
	}
	
	
	/**
	 * id生成器
	 */
	private static IdWorker m_idWorker = new IdWorker(1);
	
	/**
	 * user id生成器
	 */
	private static IdWorker m_userIdWorker = new IdWorker(10);
	
	/**
	 * class id生成器
	 */
	private static IdWorker m_classIdWorker = new IdWorker(20);
	
	
	/**
	 * group id生成器
	 */
	private static IdWorker m_groupIdWorker = new IdWorker(30);
	
	
	/**
	 * moment id生成器
	 */
	private static IdWorker m_momentIdWorker = new IdWorker(40);
	
	
	/**
	 * id生成器
	 */
	public static long getId(){
		return m_idWorker.nextId();
	}
	
	/**
	 * 获取用户id
	 */
	public static long getUserId(){
		return m_userIdWorker.nextId();
	}
	
	/**
	 * 获取班级id
	 */
	public static long getClassId(){
		return m_classIdWorker.nextId();
	}
	
	/**
	 * 获取群组id
	 */
	public static long getGroupId(){
		return m_groupIdWorker.nextId();
	}
	
	/**
	 * 获取动态id
	 */
	public static long getMomentId(){
		return m_momentIdWorker.nextId();
	}
	
	
	/**
	 * 这个SERVER服务器节点配置
	 */
	private int m_servletId;
	
	public int getServletId(){
		return m_servletId;
	}
	
	
	
	/**
	 * 整个SEVLET API框架的数据初始化主要入口
	 */
	public void init(ServletConfig config) throws ServletException {
		/**
		 * 启动后台服务
		 */
		Startup.getInstance().startup(config);
	}
	
	//定义LOG4J配置文件路径
	private void initLog4jConfigLocation(ServletConfig config) throws Exception{ 
		String prefix = config.getServletContext().getRealPath("/"); 
		String file = config.getInitParameter("log4jConfigLocation");
		String logPath = prefix+config.getInitParameter("log4jLogPath");
		String configPath = prefix+config.getInitParameter("unionpayConfigLocation");
		System.setProperty ("WORKDIR",logPath);//定义当前工作路径
		System.setProperty ("CONFIGDIR",configPath);
		PropertyConfigurator.configure(prefix+file); 
	} 

	//定义SERVLET服务配置文件路径
	private void initServletConfigLocation(ServletConfig config) throws Exception{  

		String prefix = config.getServletContext().getRealPath("/"); 
		String file = config.getInitParameter("servletConfigLocation"); 
		m_servletConfigPath = prefix+file;
	} 
	

	
	/**
	 * 目前针对后台消息服务处理流程进行调整
	 * 采用SERVLET技术进行API整合
	 * SERVLET+TOMCAT可以多台部署，;
	 */
	public void startup(ServletConfig config)
	{
		try
		{

			initLog4jConfigLocation(config);
			initServletConfigLocation(config);
			m_servletId = Config.getInstance().getInt(ConfigKey.KEY_SERVLET_ID);
			m_logger.warn("***************************************************");
			m_logger.warn("*******  Start Hengqian API Server v1.0  ***********");
			m_logger.warn("***************************************************");

			//1初始化链接池
			SqlServerBaseUtil.intializeConnectionPool();
			MysqlBaseUtil.intializeConnectionPool();
			MysqlBaseAnKangUtil.intializeConnectionPool();
			RedisBaseUtil.initJedisPoolList();
			FastDfsUtil.initFastDfs(config.getServletContext().getRealPath("/"));

			m_logger.warn("*********************HqdbDataSqlServerBaseUtil******************************");

			
			//new ClassTimerManager();
			//new UserTimerManager();
//			m_logger.warn("***************************************************");
//			m_logger.warn("******** TaskTimer Init! ********");
//			m_logger.warn("***************************************************");

			//GlobalSet.getBaseConfig();
			
			m_logger.warn("***************************************************");
			m_logger.warn("******** Start Hengqian API Server startup! ********");
			m_logger.warn("***************************************************");
			
			//System.out.println("uuid:"+UUID.randomUUID());
		}
		catch (Exception e)
		{
			m_logger.fatal("Start Message Server failed, exit."+ e);
			m_logger.warn("***************************************************");
			m_logger.warn("*******Start Hengqian API Server failed, exit.******");
			m_logger.warn("***************************************************");
			shutdownConnectionPool();
			System.exit(-2);
		}
	}
	
	
	/**
	 * 停止服务
	 */
	public void shutdown()
	{
		try
		{
			m_logger.warn("Shutdown Message Server");
			shutdownConnectionPool();
			m_logger.warn("System exit");
		}
		catch (Exception ex)
		{
			m_logger.warn("Shutdown failed, force to exit", ex);
		}
		finally
		{
			System.exit(1);
		}
	}

	/**
	 * 关闭数据库连接
	 */
	private void shutdownConnectionPool(){
		MysqlBaseUtil.shutdownConnectionPool();
		SqlServerBaseUtil.shutdownConnectionPool();
	}
}
