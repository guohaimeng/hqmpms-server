package com.hqmpms.filter;

import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.api.utils.CheckParameterException;
import com.hqmpms.api.utils.CheckParameters;
import com.hqmpms.api.utils.DatabaseManager;
import com.hqmpms.dispatcher.Config;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class GlobalFilter implements Filter {
	private static final Logger m_logger = Logger.getLogger(GlobalFilter.class);
	public GlobalFilter() {
	}

	@Override
	public void destroy() {
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		req.setCharacterEncoding("UTF-8");
		String contentEncrypt = req.getHeader("Content-encrypt");
		HttpServletResponse res = (HttpServletResponse) response;
		res.setCharacterEncoding("UTF-8");
		res.setHeader("Access-Control-Allow-Origin", "*");
		res.setHeader("Access-Control-Allow-Methods", "*");
		res.setHeader("Content-type", "application/json;charset=UTF-8");




		String requestURL = req.getRequestURL().toString();
		try {
			if(!StringUtils.endsWith(requestURL,"login.do")){
				String ss = req.getParameter("ss");
				Map<String,String[]> ssParameters = new HashMap<>();
				ssParameters.put("ss",new String[]{ss});
				CheckParameters check = new CheckParameters(request.getParameterMap());
				check.addParameter("ss", CheckParameters.paraType.EXP, Config.getInstance().getString("params_ss"));

				// session校验
				Map<String, String> selfInfo = DatabaseManager.checkAdminSession(check.opt("ss").toString());
				if (selfInfo == null || selfInfo.size() == 0) {
					String ret = ApiErrorCode.echoErr(ApiErrorCode.SESSION_INVALID);
					m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SESSION_INVALID));
					response.getWriter().write(ret);
					return;
				}
				requestURL = StringUtils.substringBetween(requestURL,Config.getInstance().getString("server_name")+"/",".do").replace("/","_");
				//String uncheck=Config.getInstance().getString("uncheck_permission_request").replace("/","_");
				if(!StringUtils.contains(requestURL,"commonapi")){
					if(!StringUtils.contains(selfInfo.get("permissions"),requestURL)){
						String ret = ApiErrorCode.echoErr(ApiErrorCode.HANDLE_NO_POWER);
						m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.HANDLE_NO_POWER));
						response.getWriter().write(ret);
						return;
					}
				}
				req.getSession().setAttribute("selfInfo",selfInfo);
			}
		} catch (CheckParameterException e) {
			String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
			m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
			response.getWriter().write(ret);
			return;
		}catch (Exception e){
			res.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
			m_logger.error("GlobalFilter error",e);
			return;
		}
		chain.doFilter(req, response);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
	}
	
	

}
