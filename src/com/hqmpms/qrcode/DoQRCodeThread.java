package com.hqmpms.qrcode;


import com.hqmpms.utils.SqlServerBaseUtil;
import com.hqmpms.utils.aes.AESCodecBase16;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Administrator on 2016/6/17 0017.
 */
public class DoQRCodeThread extends Thread{
    private static final Logger m_logger = Logger.getLogger(DoQRCodeThread.class);
    public static final String PRIVATE_KEY = "ha6pyel9h0o82hnh";
    private String id;
    private int type;
    private String logoUrl;
    public DoQRCodeThread(String id,int type,String logoUrl)
    {
        this.id = id;
        this.type = type;
        this.logoUrl=logoUrl;
    }
    public void run()
    {
        try {
            HashMap<String ,Object> contentmap = new HashMap<>();
            contentmap.put("type",type);
            contentmap.put("id",id);
            String content = new JSONObject(contentmap).toString();
            content = AESCodecBase16.encrypt(content, PRIVATE_KEY);
            String qrurl = EncoderHandler.encoderQRCoder(content);
            if(!StringUtils.isEmpty(qrurl)){
                String sql =null;
                switch (type){
                    case 1 : sql = "update yx_class_extend set qrcode_url='" + qrurl + "' where class_id='" +id +"'";
                        break;
                    case 2 : sql = "update yx_group_info set qrcode_url='" + qrurl + "' where group_id=" +id;
                        break;
                    case 3 : sql = "update yx_user_extend set qrcode_url='" + qrurl + "' where uid='" +id +"'";
                        break;
                }
                SqlServerBaseUtil.updateSQL(sql,new ArrayList<Object>());
            }
        } catch (Exception e) {
            m_logger.error(String.format("DoQRCodeThread fail params=%s",e.getMessage()));
        }
    }

}
