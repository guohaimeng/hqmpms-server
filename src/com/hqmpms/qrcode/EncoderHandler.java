package com.hqmpms.qrcode;

import com.hqmpms.utils.FastDfsUtil;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import com.swetake.util.Qrcode;
import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;


public class EncoderHandler {
    private static final Logger m_logger = Logger.getLogger(EncoderHandler.class);
    //private static String bgurl= "http://180.150.186.42/group1/M00/04/B9/CgqcUFh4LeKAC3IpAAC49UXJ5Qs063.png";
    public static String encoderQRCoder(String content) throws IOException {

        ByteArrayOutputStream outputStream = null;
        try {
            Qrcode handler = new Qrcode();
            handler.setQrcodeErrorCorrect('L');
            handler.setQrcodeEncodeMode('B');
            handler.setQrcodeVersion(8);

            //System.out.println(content);
            byte[] contentBytes = content.getBytes("UTF-8");

           // URL bgurl = new URL(EncoderHandler.bgurl);

            String classPackage = EncoderHandler.class.getResource("/").getFile().toString();

            String filePath = classPackage.substring(0,classPackage.lastIndexOf("WEB-INF/classes/")) + "/resources/images/qrcode_temp1.png";
            filePath =  URLDecoder.decode(filePath,"utf-8");
            m_logger.info("temp_path=" + filePath);
            File tempFile = new File(filePath);
            //System.out.println(System.getProperty("user.dir"));
            BufferedImage bufImg = new BufferedImage(586, 586, BufferedImage.TYPE_INT_RGB);
            //BufferedImage bufImg = ImageIO.read(bgurl);
            //Image bgimg = ImageIO.read(bgurl);
            Image bgimg = ImageIO.read(tempFile);
            Graphics2D gs = bufImg.createGraphics();
            gs.setBackground(Color.WHITE);
            gs.clearRect(0, 0, 586, 586);
            gs.drawImage(bgimg, 0, 0, null);

            //设定图像颜色：BLACK
           // Color co = new Color("")
            //gs.setColor(new Color(Integer.parseInt("ff1028", 16)));

            //gs.clearRect(123, 123, 340, 340);



            BufferedImage bi = new BufferedImage(690, 690, BufferedImage.TYPE_INT_RGB);
            // createGraphics
            Graphics2D g = bi.createGraphics();
            // set background
            g.setBackground(Color.WHITE);
            g.clearRect(0, 0, 690, 690);
            //设置二维码图片颜色
            g.setColor(new Color(Integer.parseInt("ff1028", 16)));

            //设置偏移量  不设置肯能导致解析出错
            int pixoff = 4;
            //输出内容：二维码
            if(contentBytes.length > 0 && contentBytes.length < 256) {
                boolean[][] codeOut = handler.calQrcode(contentBytes);
                for(int i = 0; i < codeOut.length; i++) {
                    for(int j = 0; j < codeOut.length; j++) {
                        if(codeOut[j][i]) {
                            g.fillRect(j * 14 + pixoff, i * 14 + pixoff,14, 14);
                        }
                    }
                }
            } else {
                m_logger.info("QRCode content bytes length = " + contentBytes.length + " not in [ 0,120 ]. ");
                return null;
            }

            g.dispose();
            bi.flush();

            gs.drawImage(bi.getScaledInstance(340, 340, Image.SCALE_SMOOTH), 123, 123, null);
            //URL url = new URL("http://www.hengqian.net/youyi/images/ll.png");
//            URL url = new URL(logoUrl);
//            Image img = ImageIO.read(url);
//            gs.drawImage(img.getScaledInstance(64, 64, Image.SCALE_SMOOTH), 261, 261, null);
            gs.dispose();
            bufImg.flush();



            //生成二维码QRCode图片

           outputStream = new ByteArrayOutputStream();

            JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(outputStream);
            JPEGEncodeParam jep = JPEGCodec.getDefaultJPEGEncodeParam(bufImg);
            jep.setQuality((float) 1.0, true);
            encoder.encode(bufImg,jep);
           //ImageIO.write(bufImg, "jpg", outputStream);
            String[]  res =  FastDfsUtil.uploadFileByStream(outputStream,"calssqrcode.jpg",outputStream.size(),"qrcopde");
            return  res[0];
        } catch (Exception e) {
            m_logger.error(" EncoderHandler ERROR " ,e);
            return  "";
        }
    }
}
