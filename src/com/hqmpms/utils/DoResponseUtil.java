package com.hqmpms.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;

/**
 * Response处理工具类
 */
public class DoResponseUtil {
    /**
     * ResponseWrite
     * @param request
     * @param response
     * @param outStr
     * @throws Exception
     */
    public static void ResponseWrite(HttpServletRequest request, HttpServletResponse response, String outStr) throws IOException {
        String headEncoding = request.getHeader("accept-encoding");
        if (headEncoding == null || (headEncoding.indexOf("gzip") == -1)) {
            response.getWriter().write(outStr);
        } else {
            response.addHeader("Content-Encoding","gzip");
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            GZIPOutputStream gout = new GZIPOutputStream(out);
            gout.write(outStr.getBytes("UTF-8"));
            gout.close();
            byte[]  result = out.toByteArray();
            response.getOutputStream().write(result);
        }
    }
}
