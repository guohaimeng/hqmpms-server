package com.hqmpms.utils;

import org.apache.log4j.Logger;

import java.util.HashMap;

/**
 * Created by Administrator on 2016/6/17 0017.
 */
public class DoSDKPostThread extends Thread{
    private static final Logger m_logger = Logger.getLogger(DoSDKPostThread.class);
    private String url;
    private HashMap<Object,Object> webparams;
    public DoSDKPostThread(String url, HashMap<Object,Object> webparams)
    {
        this.url = url;
        this.webparams = webparams;
    }
    public void run()
    {
        try {
            YsServerSDK.doPost(url,webparams);
        } catch (Exception e) {
            m_logger.error(String.format("DoSDKPost fail params=%s",e.getMessage()));
        }
    }

}
