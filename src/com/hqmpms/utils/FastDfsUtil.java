package com.hqmpms.utils;

import com.hqmpms.api.utils.Tools;
import com.hqmpms.dispatcher.Config;
import com.hqmpms.dispatcher.ConfigKey;
import org.apache.log4j.Logger;
import org.csource.common.NameValuePair;
import org.csource.fastdfs.*;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.*;

public class FastDfsUtil {
	private static final Logger m_logger = Logger.getLogger(FastDfsUtil.class);
	private static String m_tempFileDirectory="d:\\";
	
	public static void initFastDfs(String prefix) throws Exception{
  		ClientGlobal.init(); 		   		
	}
	
	public static String[] getDfsServerList()
	{
		/*String validServerSql = "SELECT category,CONCAT(local_ip,':',local_port) ip FROM rkcloud_server_config WHERE category =5 AND is_enable = 1";

		ArrayList<HashMap<String,String>> validserversMap = null;
		try {
			validserversMap = MysqlBaseUtil.querySql(validServerSql, new ArrayList<Object>());
		}
		catch (SQLException localSQLException)
		{
		}
		if (validserversMap != null) {
			String[] dfsList = new String[validserversMap.size()];
			for (int i = 0; i < validserversMap.size(); i++) {
				String serverAddr = validserversMap.get(i).get("ip");
				dfsList[i] = serverAddr;
				m_logger.info("Initialize FastDFS server: " + serverAddr);
			}
			return dfsList;
		}*/
		String ip = Config.getInstance().getString(ConfigKey.KEY_FDFS_HOST);
		String port = Config.getInstance().getString(ConfigKey.KEY_FDFS_LOCAL_PORT);
		String[] dfsList = new String[]{ip+":"+port};
		return dfsList;
		
	}

	private static TrackerServer trackerServer = null;
	public static StorageClient1 getStorageClient() throws Exception{
	    TrackerClient tracker = new TrackerClient();
        trackerServer = tracker.getConnection();
        StorageServer storageServer = null;
        StorageClient1 client = new StorageClient1(trackerServer, storageServer);  
        return client;        	
	}
		
	public static byte[] downloadFile(String fileId){
		byte[] result = null;
		try { 
			result = getStorageClient().download_file1(fileId);
			trackerServer.close();
		} catch (Exception e) {  
			m_logger.warn("download fileid=\"" + fileId,e);
		}
		return result;
	}
	
	public static String uploadFile(String uploadFileName, long fileLength){
		String result = null;  
		String fileExtName = "";  
		if (uploadFileName.contains(".")) {  
			fileExtName = uploadFileName.substring(uploadFileName.lastIndexOf(".") + 1);  
		} else {  
			m_logger.warn("Fail to upload file, because the format of filename is illegal.");  
			return null;  
		}  
		NameValuePair[] metaList = new NameValuePair[3];  
		metaList[0] = new NameValuePair("fileName", uploadFileName);  
		metaList[1] = new NameValuePair("fileExtName", fileExtName);  
		metaList[2] = new NameValuePair("fileLength", String.valueOf(fileLength));

		try { 
			result = getStorageClient().upload_file1(uploadFileName, null, metaList);
			//results = getStorageClient().upload_file("group1", fileLength, new UploadFileSender(inStream), fileExtName, metaList);
			trackerServer.close();
		} catch (Exception e) {  
			m_logger.warn("Upload file \"" + uploadFileName,e);
			return null;
		}

		return result;
		
	}
	
	public static String[] uploadFileByStream(ByteArrayOutputStream mmsFile,String uploadFileName, long fileLength,String imgType){
		String fastdfs = Config.getInstance().getString(ConfigKey.KEY_FDFS_DOWNLOAD)+"/";
		String[] results = null;  
		String fileExtName = "";  
		if (uploadFileName.contains(".")) {  
			fileExtName = uploadFileName.substring(uploadFileName.lastIndexOf(".") + 1);  
		} else {
			fileExtName = "els";
		}  
		NameValuePair[] metaList = new NameValuePair[3];  
		metaList[0] = new NameValuePair("fileName", uploadFileName);  
		metaList[1] = new NameValuePair("fileExtName", fileExtName);  
		metaList[2] = new NameValuePair("fileLength", String.valueOf(fileLength));
		try { 
			InputStream inStream =new ByteArrayInputStream(mmsFile.toByteArray());			
			results = getStorageClient().upload_file(null, fileLength, new UploadStream(inStream,fileLength), fileExtName, metaList);
			if(results!=null){
				if(imgType.equals("avatar") || imgType.equals("image")){
					//生成缩略图		
					InputStream thumbStream = new ByteArrayInputStream(mmsFile.toByteArray());
					InputStream thumbIs=thumbImage(thumbStream,imgType,fileExtName);
					if(thumbIs!=null){
						ByteArrayOutputStream thumb = Tools.InputStreamCopy(thumbIs);
						InputStream thumbIsStream =  new ByteArrayInputStream(thumb.toByteArray());
						int thumbFileLength = thumb.size();
						NameValuePair[] metaThumbList = new NameValuePair[3];
						metaThumbList[0] = new NameValuePair("fileName", uploadFileName);  
						metaThumbList[1] = new NameValuePair("fileExtName", fileExtName);  
						metaThumbList[2] = new NameValuePair("fileLength", String.valueOf(thumbFileLength));
						//String[] results1 = getStorageClient().upload_file(null, thumbFileLength, new UploadStream(thumbIsStream,thumbFileLength), fileExtName, metaThumbList);
						String[] results1 = getStorageClient().upload_file(results[0],results[1], "_thumb",thumbFileLength, new UploadStream(thumbIsStream,thumbFileLength), fileExtName, metaThumbList);
						m_logger.info("-------group---"+results1[0]);
						m_logger.info("-------fileurl---"+results1[1]);
						trackerServer.close();
						if(results1!=null){
							String[] ret = new String[]{fastdfs+results[0]+"/"+results[1],fastdfs+results1[0]+"/"+results1[1]};
							return ret;//返回原始图和缩略图映射
						}else{
							return null;//上传缩略图失败
						}
					}else{
						return new String[]{fastdfs+results[0]+"/"+results[1],fastdfs+results[0]+"/"+results[1]};//返回原始文件的映射地址
					}
				}else
				{
					return new String[]{fastdfs+results[0]+"/"+results[1]};//返回原始文件的映射地址
				}
			}else{
				return null;//上传原始文件失败
			}			
		} catch (Exception e) {  
			m_logger.error("Upload file \"" + uploadFileName,e);
			return null;
		}	
	}
	
	
	public static String saveFile(byte[] bs){	 
		try {			
			String tempFileName = m_tempFileDirectory+ Tools.getRandomStr(20)+".dfs";
			//File tempUploadFile = new File(tempFileName);
			FileOutputStream fos = new FileOutputStream(tempFileName);
			fos.write(bs);
			fos.flush();
			fos.close();
			return tempFileName;
		} catch (IOException e) {
			
			return null;
		}    	    
	}
	
	
	public static InputStream thumbImage(InputStream is,String imageType,String fileExtName){

		try {
			InputStream thumbImageStream = null;
			BufferedImage bis = ImageIO.read(is);//读取原始图片
			int imgWidth = bis.getWidth();
			int imgHeight = bis.getHeight();
			int thumbWidth=0;
			int thumbHeight =0 ;
			if(imageType.equals("avatar")){
				thumbWidth = Config.getInstance().getInt(ConfigKey.KEY_AVATAR_THUMB_WIDTH, ConfigKey.V_AVATAR_THUMB_WIDTH);
				thumbHeight = Config.getInstance().getInt(ConfigKey.KEY_AVATAR_THUMB_HEIGHT, ConfigKey.V_AVATAR_THUMB_HEIGHT);
			}else
			{
				thumbWidth = Config.getInstance().getInt(ConfigKey.KEY_THUMB_WIDTH, ConfigKey.V_THUMB_WIDTH);
				thumbHeight = Config.getInstance().getInt(ConfigKey.KEY_THUMB_HEIGHT, ConfigKey.V_THUMB_HEIGHT);
			}
			return ImageUtil.resize(bis,thumbWidth,fileExtName);
		} catch (Exception e) {
			m_logger.error("thumbImage error", e);
			return null;
		}	
	}
	

	
	private static InputStream resize(BufferedImage bis,double sx,double sy,int thumbwidth,int thumbHeight,String fileExtName){
		try {
			AffineTransform transform = new AffineTransform();
			transform.setToScale(sx, sy);
			AffineTransformOp ato = new AffineTransformOp(transform,null);
			BufferedImage bid = new BufferedImage(thumbwidth,thumbHeight,BufferedImage.TYPE_3BYTE_BGR);
			ato.filter(bis, bid);	
			ByteArrayOutputStream bs = new ByteArrayOutputStream();
			ImageOutputStream imOut = ImageIO.createImageOutputStream(bs); 
			ImageIO.write(bid,fileExtName,imOut);
			InputStream is= new ByteArrayInputStream(bs.toByteArray());
			return is;
		} catch (Exception e) {
			m_logger.error("resize error", e);
			return null;
		}		
	}



	private synchronized static InputStream disposeImage(BufferedImage src, int new_w, int new_h) {
		try {
			// 得到图片
		int old_w = src.getWidth();
		// 得到源图宽
		int old_h = src.getHeight();
		// 得到源图长
		BufferedImage newImg = null;
		// 判断输入图片的类型
		switch (src.getType()) {
			case 13:
				// png,gifnewImg = new BufferedImage(new_w, new_h,
				// BufferedImage.TYPE_4BYTE_ABGR);
				break;
			default:
				newImg = new BufferedImage(new_w, new_h, BufferedImage.TYPE_INT_RGB);
				break;
		}
		Graphics2D g = newImg.createGraphics();
		// 从原图上取颜色绘制新图
		g.drawImage(src, 0, 0, old_w, old_h, null);
		g.dispose();
		// 根据图片尺寸压缩比得到新图的尺寸
		newImg.getGraphics().drawImage(
				src.getScaledInstance(new_w, new_h, Image.SCALE_SMOOTH), 0, 0,
				null);
		// 调用方法输出图片文件
		//OutImage(outImgPath, newImg);
		ByteArrayOutputStream bs = new ByteArrayOutputStream();
		ImageOutputStream imOut = null;

			imOut = ImageIO.createImageOutputStream(bs);
		ImageIO.write(newImg,"jpeg",imOut);
		InputStream is= new ByteArrayInputStream(bs.toByteArray());
		return is;
		} catch (IOException e) {
			e.printStackTrace();
			return  null;
		}
	}


	private synchronized static InputStream cutPic(BufferedImage src, int thumbwidth, int thumbheight,String fileExtName) {
		try {
			if(src == null){
				return  null;
			}
			int imgWidth = src.getWidth();
			int imgHeight = src.getHeight();
			int x=0;
			int y=0;
			int width = imgWidth;
			int heith = imgHeight;
			if(imgWidth > thumbwidth){
				x = (imgWidth-thumbwidth)/2;
				width = thumbwidth;
			}
			if(imgHeight > thumbheight){
				y = (imgHeight-thumbheight)/2;
				heith = thumbheight;
			}

			BufferedImage tempImage=  src.getSubimage(x, y, width, heith);

			ByteArrayOutputStream bs = new ByteArrayOutputStream();
			ImageOutputStream imOut = null;

			imOut = ImageIO.createImageOutputStream(bs);
			ImageIO.write(tempImage,fileExtName,imOut);
			InputStream is= new ByteArrayInputStream(bs.toByteArray());
			return is;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String[] uploadFileByString(String text,String uploadFileName){
		try {
		String fastdfs = Config.getInstance().getString(ConfigKey.KEY_FDFS_DOWNLOAD)+"/";
		String[] results = null;
		String fileExtName = "";
		if (uploadFileName.contains(".")) {
			fileExtName = uploadFileName.substring(uploadFileName.lastIndexOf(".") + 1);
		} else {
			fileExtName = "els";
		}
		InputStream inStream =new ByteArrayInputStream(text.getBytes("UTF-8"));
		NameValuePair[] metaList = new NameValuePair[3];
		metaList[0] = new NameValuePair("fileName", uploadFileName);
		metaList[1] = new NameValuePair("fileExtName", fileExtName);
		metaList[2] = new NameValuePair("fileLength", String.valueOf(inStream.available()));

			results = getStorageClient().upload_file(null, inStream.available(), new UploadStream(inStream,inStream.available()), fileExtName, metaList);
			if(results!=null){
				return new String[]{fastdfs+results[0]+"/"+results[1]};//返回原始文件的映射地址
			}else{
				return null;//上传原始文件失败
			}
		} catch (Exception e) {
			m_logger.warn("Upload file \"" + uploadFileName,e);
			return null;
		}
	}



	private static String[] getGroupName(String furl){
		String[] res = new String[2];

		String [] strarr = furl.split("/");
		for(String s : strarr){
			if(s.contains("group")){
				res[0] = s;
				break;
			}
		}
		res[1] = furl.substring(furl.indexOf(res[0])+res[0].length()+1);
		return res;
	}


	public static  void main(String [] args){
		String url="group1/M00/00/00/wKgBt1fiUsWAZ7LuAB9oAL7xtZA7381112.222.docx";


		String groupName="";

		String fname = "";



		String [] strarr = url.split("/");

		for(String s : strarr){
			if(s.contains("group")){
				groupName = s;
				break;
			}
		}

		fname = url.substring(url.indexOf(groupName)+groupName.length()+1);
		System.out.println(groupName);
		System.out.println(fname);

	}
}

   

