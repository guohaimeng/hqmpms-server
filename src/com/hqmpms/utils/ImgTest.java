package com.hqmpms.utils;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class ImgTest {

    //    
    public static int getLength(String text) {
        int length = 0;
        for (int i = 0; i < text.length(); i++) {
            if (new String(text.charAt(i) + "").getBytes().length > 1) {
                length += 2;
            } else {
                length += 1;
            }
        }
        return length / 2;
    }

    public static String TextToPic(String text, int width, int height,
                                   int fontSize) {
        try {
            String filepath = "E:\\appfile\\userimage"
                    + getDate() + ".png";

            // String
            // filepath="/www/newaudi_cms/app/UserFiles/Image/texttopic/image"+getDate()+".png";

            File file = new File(filepath);

            // Font font = new Font("甲骨文", 36, fontSize);

            System.out.println("topic=" + text);
            Font font = new Font("汉仪双线简体", Font.BOLD, fontSize);
            // Font font = new Font("新宋体", Font.BOLD, fontSize);
            BufferedImage bi = new BufferedImage(width, height,BufferedImage.TYPE_4BYTE_ABGR);
            Graphics2D g2 = (Graphics2D) bi.getGraphics();

            g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2.setFont(font);
            g2.setPaint(Color.black);

            paintString(g2, text,2,42, fontSize);
            g2.dispose();
            ImageIO.write(bi, "png", file);
            return "image" + getDate() + ".png";

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");

        return formatter.format(new Date());
    }

    private static void paintString(Graphics2D g2d, String str, int x, int y,
                                    int fontSize) {
        FontMetrics metrics = g2d.getFontMetrics();
        for (char ca : str.toCharArray()) {
            int px = metrics.stringWidth("" + ca);
            g2d.drawString("" + ca, x + (fontSize - px) / 2, y);
            x += fontSize;
        }
    }

    public static Date parsePlainDate(String source) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

        return sdf.parse(source, new ParsePosition(0));
    }






    /**
     *
     * @param filesrc
     * @param logosrc
     * @param outsrc
     * @param x 位置
     * @param y 位置
     */
    public void composePic(String filesrc,String logosrc,String outsrc,int x,int y) {
        try {
            File bgfile = new File(filesrc);
            Image bg_src = ImageIO.read(bgfile);

            File logofile = new File(logosrc);
            Image logo_src = ImageIO.read(logofile);

            int bg_width = bg_src.getWidth(null);
            int bg_height = bg_src.getHeight(null);
            int logo_width = logo_src.getWidth(null);;
            int logo_height = logo_src.getHeight(null);

            BufferedImage tag = new BufferedImage(bg_width, bg_height, BufferedImage.TYPE_INT_RGB);

            Graphics2D g2d = tag.createGraphics();
            g2d.drawImage(bg_src, 0, 0, bg_width, bg_height, null);

            g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP,1.0f)); //透明度设置开始
            g2d.drawImage(logo_src,x,y,logo_width,logo_height, null);
            g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER)); //透明度设置 结束

            FileOutputStream out = new FileOutputStream(outsrc);
            JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
            encoder.encode(tag);
            out.close();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String args[]) {
//        Long star = System.currentTimeMillis();
//        ImgTest pic = new ImgTest();
//        pic.composePic("c:\\bb.gif","c:\\bc.gif","c:\\out_pic.gif",490,360);
//        Long end =System.currentTimeMillis();
//        System.out.print("time====:"+(end-star));
        System.out.print("但是大多".getBytes().length);
        System.out.print(UUID.randomUUID().toString().replace("-",""));
    }










//    public static void main(String[] args) throws IOException, ParseException {
//        // pressText("我是文字", "d:\\1.bmp", "黑体", 36, Color.white, 80, 0, 0,
//        // 0.3f);
//        TextToPic("我是文字", 500, 100, 50);
//        //
//        System.out.print(parsePlainDate(getDate()));
//    }

}