package com.hqmpms.utils;
import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.dispatcher.Config;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class LiveRoomSDK {

	private static final Logger m_logger = Logger.getLogger(LiveRoomSDK.class);
	public final static int ADD_SUCCESS = 1;
	public final static int ADD_FAILED = 0;

	private static String getToken = "/token/get";//获取token
	private static String createRoom = "/room/create";//创建直播间
	private static String updateRoom = "/room/update";//修改直播间
	private static String deleteRoome = "/room/delete";//删除直播间
	private static String getRoom = "/room/get"; //获取直播间信息
	private static String getAddress = "/room/record/address/get";//获取播录地址
	private static String live_server_key = Config.getInstance().getString("live_server_key");
	private static String live_server_url =  Config.getInstance().getString("live_server_url");

	private static String token="";
	/**
	 * init url and server key
	 * @param key
	 */
	//public LiveRoomSDK(String key){
//		live_server_key = key;
//	}



	/**
	 * getToken
	 * @return
	 * @throws IOException
	 */
	public static String   getToken() throws IOException  {
		int ret = ApiErrorCode.DEFAULT_SYSTEM_ERROR;
		String URL_PATH = live_server_url+getToken;
		HashMap<Object,Object> params = new HashMap<Object,Object>();
		params.put("server_key", live_server_key);
		String result;
        try {
        	m_logger.info("getToken request: "+URL_PATH+" params:"+params);
			result = doPost(URL_PATH, params,null);
			m_logger.info("getToken res:"+result);
			if(!result.equals("")){
				JSONObject res = new JSONObject(result);
				ret = res.getInt("errcode");
				if(ret==0){
					token = res.getJSONObject("result").getString("token");
					return  token;
				}else {
					m_logger.error("getToken faile: server errcode="+ ret);
					return null;
				}
			}else{
				m_logger.error("getToken faile: server errcode="+ ret);
				return null;
			}
		} catch (Exception e) {
			m_logger.error("getToken faile:"+e);
			return null;
		}
	}
	/**
	 * 创建直播间
	 * @return fail:errcode success:gid
	 * @throws Exception
	 */
	public String createRoom(String topic,String anchor_name,String anchor_pwd,String viewer_pwd) throws Exception  {
		int ret = ApiErrorCode.DEFAULT_SYSTEM_ERROR;
		String wholeURL = live_server_url+createRoom;
		HashMap<Object, Object> params = new HashMap<Object,Object>();
		params.put("topic", topic);
		params.put("anchor_name", anchor_name);
		params.put("room_type","2");
		params.put("anchor_pwd",anchor_pwd);
		params.put("viewer_pwd",viewer_pwd);
		params.put("live_source","2");
		String result = doPost(wholeURL,params,token);
		if(null == result){
			return String.valueOf(ret);
		}
	    try {
	    	JSONObject json= new JSONObject(result);
			ret = json.getInt("errcode");
			if(ret != ApiErrorCode.DEFAULT_SDK_SUCCESS){
				if(ret == 1000){
					m_logger.error(String.format("createRoom fail,retcode=%s",ret));
					return String.valueOf(ApiErrorCode.DEFAULT_SYSTEM_ERROR);
				}else if(ret == 1001){
					m_logger.error(String.format("create group fail,retcode=%s",ret));
					return String.valueOf(ApiErrorCode.PARAMETER_ERROR);
				}else if(ret==1005)
				{
					m_logger.error(String.format("create group fail,retcode=%s",ret));
					String token = getToken();
					return createRoom(topic,anchor_name,anchor_pwd,viewer_pwd);
				}else if(ret==3015){
					m_logger.error(String.format("createRoom fail,retcode=%s",ret));
					return String.valueOf(ApiErrorCode.DEFAULT_SYSTEM_ERROR);
				}
			}else{
				return json.getJSONObject("result").getString("room_id");
			}
		} catch (Exception e) {
			m_logger.error(String.format("FAILED params=%s",e));	
		}
		return String.valueOf(ret);
	}

	/**
	 * 修改直播间
	 * @return fail:errcode success:gid
	 * @throws Exception
	 */
	public String updateRoom(String room_id, String topic,String anchor_name,String anchor_pwd,String viewer_pwd) throws Exception  {
		int ret = ApiErrorCode.DEFAULT_SYSTEM_ERROR;
		String wholeURL = live_server_url+createRoom;
		HashMap<Object, Object> params = new HashMap<Object,Object>();
		params.put("room_id", room_id);
		params.put("topic", topic);
		params.put("anchor_name", anchor_name);
		params.put("room_type","2");
		params.put("anchor_pwd",anchor_pwd);
		params.put("viewer_pwd",viewer_pwd);
		params.put("live_source","2");
		String result = doPost(wholeURL,params,token);
		if(null == result){
			return String.valueOf(ret);
		}
		try {
			JSONObject json= new JSONObject(result);
			ret = json.getInt("errcode");
			if(ret != ApiErrorCode.DEFAULT_SDK_SUCCESS){
				if(ret == 1000){
					m_logger.error(String.format("createRoom fail,retcode=%s",ret));
					return String.valueOf(ApiErrorCode.DEFAULT_SYSTEM_ERROR);
				}else if(ret == 1001){
					m_logger.error(String.format("create group fail,retcode=%s",ret));
					return String.valueOf(ApiErrorCode.PARAMETER_ERROR);
				}else if(ret==1005)
				{
					m_logger.error(String.format("create group fail,retcode=%s",ret));
					String token = getToken();
					return createRoom(topic,anchor_name,anchor_pwd,viewer_pwd);
				}else if(ret==3015){
					m_logger.error(String.format("createRoom fail,retcode=%s",ret));
					return String.valueOf(ApiErrorCode.DEFAULT_SYSTEM_ERROR);
				}
			}else{
				return json.getJSONObject("result").getString("room_id");
			}
		} catch (Exception e) {
			m_logger.error(String.format("FAILED params=%s",e));
		}
		return String.valueOf(ret);
	}
	/**
	 * 获取直播间状态
	 * @return fail:errcode success:gid
	 * @throws Exception
	 */
	public static JSONArray getRoom(String room_id) throws Exception  {
		int ret = ApiErrorCode.DEFAULT_SYSTEM_ERROR;
		String wholeURL = live_server_url+getRoom;
		HashMap<Object,Object> params = new HashMap<Object,Object>();
		params.put("roomid",room_id);
		params.put("token",token);
		String result = doPost(wholeURL,params,token);
		if(null == result){
			return null;
		}
	    JSONObject json= new JSONObject(result);
	    try {
			ret = json.getInt("errcode");
			if(ret != ApiErrorCode.DEFAULT_SDK_SUCCESS){
				if(ret == 1005){
					m_logger.error(String.format("getRoom fail,retcode=%s",ret));
					String token = getToken();
					return getRoom(room_id);
				}else{
					m_logger.error(String.format("getRoom fail,retcode=%s",ret));
					return null;
				}
			}else{
				return json.getJSONArray("result");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			m_logger.error(String.format("FAILED params=%s",e.getMessage()));	
		}
		return null;
	}





	/**
	 * 模拟doPost请求
	 * @param urlStr
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
    public static String  doPost(String urlStr,HashMap<Object,Object> paramMap,String token) throws Exception{
        URL url = new URL(urlStr);
        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setRequestMethod("POST");
        String paramStr = null;
        if(paramMap!=null){
        	paramStr = prepareParam(paramMap);
        }
		if(!StringUtils.isEmpty(token)){
			conn.setRequestProperty("token", token);
		}
        conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
		conn.setRequestProperty("Accept", "application/json;charset=utf-8");
        conn.setDoInput(true);
        conn.setDoOutput(true);
        OutputStream os = conn.getOutputStream();
        if(paramMap!=null){
        	os.write(paramStr.toString().getBytes("utf-8"));
        }
        os.close(); 
        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(),"utf-8"));
        String line ;
        String result ="";
        while( (line =br.readLine()) != null ){
            result += line;
        }
        br.close();
        return result;
    }
    
    /**
     * 参数解析

     * @return
     */
    private static String prepareParam(Map<Object,Object> paramMap){
        StringBuffer sb = new StringBuffer();
        if(paramMap.isEmpty()){
            return "" ;
        }else{
           return new JSONObject(paramMap).toString();
        }
    }
    
	/**
	 * http 请求，
	 * @param InURL
	 * @param params  "key":"value"
	 * @param InMethod  方法 “GET”,"POST"
	 * @return
	 * @throws IOException 
	 */
	public static HashMap<String,String> httpRequest(String InURL,HashMap<String,String> params,String InMethod) throws IOException{
		if(InURL==null || InMethod==null || InURL.isEmpty() || InMethod.isEmpty())
			return null;
		StringBuffer content = new StringBuffer(1024);
		// get params
		if(null != params){
			Iterator<String> it=params.keySet().iterator();
			int i = 0;
			while(it.hasNext()){
			    String key;  
			    String value;  
			    key=it.next().toString();  
			    value=params.get(key);
			    if(i==0)
					content.append(key+"=").append(value);
			    else
			    	content.append("&"+key+"=").append(value);
			    i++;
			}
		}
		if(InMethod.equals("GET") && !content.toString().isEmpty()){
			InURL = InURL+"?"+content.toString();
		}
		URL postUrl = new URL(InURL); 
		HttpURLConnection connection = (HttpURLConnection) postUrl.openConnection(); 
		connection.setDoOutput(true);                 
		connection.setDoInput(true); 
		connection.setRequestMethod(InMethod); 
		connection.setRequestProperty("Charset", "UTF-8");
		connection.setRequestProperty("Connection", "Keep-Alive");
		connection.setUseCaches(false); 
		connection.setInstanceFollowRedirects(true); 
		connection.setRequestProperty("content-type", "application/x-www-form-urlencoded;charset=UTF-8");
		
		//开始实际连接
		connection.connect();
		if(InMethod.equals("POST")){
			//发送请求参数
			DataOutputStream out = new DataOutputStream(connection.getOutputStream()); 
			out.writeBytes(content.toString()); 
			out.flush(); 
			out.close(); 
		}
		
		HashMap<String,String> result = new HashMap<String,String>();
//		int response = connection.getResponseCode(); 
		if(connection.getResponseCode() == 200){
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream())); 
			String line; 
			
			while((line=reader.readLine()) != null){
					String[] resultArray = line.split("=");
					result.put(resultArray[0], resultArray[1]);
//					ret = ret+"\n"+line.toString();
			}
			reader.close(); 
		}
		connection.disconnect();
		
		return result.isEmpty()?null:result;
	}
	/**
	 * http 请求，
	 * @param InURL
	 * @param params  "key":"value"
	 * @param InMethod  方法 “GET”,"POST"
	 * @return
	 * @throws IOException 
	 */
	public static String httpPostRequest(String InURL,HashMap<String,String> params,String InMethod) throws IOException{
		if(InURL==null || InMethod==null || InURL.isEmpty() || InMethod.isEmpty())
			return null;
		StringBuffer content = new StringBuffer(1024);
		if(null != params){
			Iterator<String> it=params.keySet().iterator();
			int i = 0;
			while(it.hasNext()){
			    String key;  
			    String value;  
			    key=it.next().toString();  
			    value=params.get(key);
			    if(i==0)
					content.append(key+"=").append(value);
			    else
			    	content.append("&"+key+"=").append(value);
			    i++;
			}
		}
		if(InMethod.equals("GET") && !content.toString().isEmpty()){
			InURL = InURL+"?"+content.toString();
		}
		URL postUrl = new URL(InURL); 
		HttpURLConnection connection = (HttpURLConnection) postUrl.openConnection(); 
		connection.setDoOutput(true);                 
		connection.setDoInput(true); 
		connection.setRequestMethod(InMethod); 
		connection.setRequestProperty("Connection", "Keep-Alive");
		connection.setUseCaches(false); 
		connection.setInstanceFollowRedirects(true); 
		connection.setRequestProperty("content-type", "application/x-www-form-urlencoded;charset=UTF-8");
		//开始实际连接
		connection.connect();
		if(InMethod.equals("POST")){
			//发送请求参数
			DataOutputStream out = new DataOutputStream(connection.getOutputStream()); 
			out.writeBytes(content.toString()); 
			out.flush(); 
			out.close(); 
		}
		int response = connection.getResponseCode(); 
		String result ="";
		if(response == 200){
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8")); 
			String line; 
			while((line=reader.readLine()) != null){
				result +=line;
			}
			reader.close(); 
		}
		connection.disconnect();
		return result;
	}



}