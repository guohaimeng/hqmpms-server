package com.hqmpms.utils;

import com.hqmpms.api.utils.Tools;
import com.hqmpms.dispatcher.Config;
import com.hqmpms.dispatcher.ConfigKey;
import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Iterator;

public class LocalUploadUtil {
	private static final Logger m_logger = Logger.getLogger(LocalUploadUtil.class);


	
	public static String[] uploadFileByStream(ByteArrayOutputStream mmsFile,String uploadFileName, long fileLength,String imgType,String path){

		String[] results = null;  
		String fileExtName = "";  
		if (uploadFileName.contains(".")) {  
			fileExtName = uploadFileName.substring(uploadFileName.lastIndexOf(".") + 1);  
		} else {
			fileExtName = "els";
		}
		try { 
			InputStream inStream =new ByteArrayInputStream(mmsFile.toByteArray());			
			//results = saveFile(inStream, fileExtName,path);
			results = saveFile(inStream, uploadFileName,path);
			if(results!=null){
				if(imgType.equals("avatar") || imgType.equals("image")){
					//生成缩略图		
					InputStream thumbStream = new ByteArrayInputStream(mmsFile.toByteArray());
					InputStream thumbIs=thumbImage(thumbStream,imgType,fileExtName);
					if(thumbIs!=null){
						ByteArrayOutputStream thumb = Tools.InputStreamCopy(thumbIs);
						InputStream thumbIsStream =  new ByteArrayInputStream(thumb.toByteArray());
						String[] results1 = saveFile(thumbIsStream, fileExtName,path);
						if(results1!=null){
							String[] ret = new String[]{results[0],results1[0]};
							return ret;//返回原始图和缩略图映射
						}else{
							return null;//上传缩略图失败
						}
					}else{
						return new String[]{results[0],results[0]};//返回原始文件的映射地址
					}
				}else
				{
					return new String[]{results[0]};//返回原始文件的映射地址
				}
			}else{
				return null;//上传原始文件失败
			}			
		} catch (Exception e) {  
			m_logger.warn("Upload file \"" + uploadFileName,e);
			return null;
		}	
	}

	
	public static InputStream thumbImage(InputStream is,String imageType,String fileExtName){
		InputStream thumbImageStream = null;
		try {
			BufferedImage bis = ImageIO.read(is);//读取原始图片
			int imgWidth = bis.getWidth();
			int imgHeight = bis.getHeight();
			int thumbWidth=0;
			int thumbHeight =0 ;
			if(imageType.equals("avatar")){
				thumbWidth = Config.getInstance().getInt(ConfigKey.KEY_AVATAR_THUMB_WIDTH, ConfigKey.V_AVATAR_THUMB_WIDTH);
				thumbHeight = Config.getInstance().getInt(ConfigKey.KEY_AVATAR_THUMB_HEIGHT, ConfigKey.V_AVATAR_THUMB_HEIGHT);
			}else
			{
				thumbWidth = Config.getInstance().getInt(ConfigKey.KEY_THUMB_WIDTH, ConfigKey.V_THUMB_WIDTH);
				thumbHeight = Config.getInstance().getInt(ConfigKey.KEY_THUMB_HEIGHT, ConfigKey.V_THUMB_HEIGHT);
			}
			return ImageUtil.resize(bis,thumbWidth,fileExtName);
		} catch (Exception e) {
			return null;
		}	
	}
	

	
	private static InputStream resize(BufferedImage bis,double sx,double sy,int thumbwidth,int thumbHeight){
		try {
			AffineTransform transform = new AffineTransform();
			transform.setToScale(sx, sy);
			AffineTransformOp ato = new AffineTransformOp(transform,null);
			BufferedImage bid = new BufferedImage(thumbwidth,thumbHeight,BufferedImage.TYPE_3BYTE_BGR);
			ato.filter(bis, bid);	
			ByteArrayOutputStream bs = new ByteArrayOutputStream();
			ImageOutputStream imOut = ImageIO.createImageOutputStream(bs); 
			ImageIO.write(bid,"jpeg",imOut);
			InputStream is= new ByteArrayInputStream(bs.toByteArray());
			return is;
		} catch (Exception e) {
			return null;
		}		
	}

	private static String[] saveFile(InputStream inStream,String fileExtName, String path) throws Exception {
		String  local_upload_dir = "";
		if(System.getProperties().getProperty("os.name").contains("Windows")){
			local_upload_dir = Config.getInstance().getString("local_upload_dir_windows");
		}
		else if(System.getProperties().getProperty("os.name").contains("Linux")){
			local_upload_dir = Config.getInstance().getString("local_upload_dir_linux");
		}else{
			local_upload_dir = Config.getInstance().getString("local_upload_dir_linux");
		}
		local_upload_dir = local_upload_dir + path ;
		String local_download_host = Config.getInstance().getString("local_download_host") + path ;
		File filedir = new File(local_upload_dir);
		if(!filedir.exists()){
			filedir.mkdirs();
		}
		//String fileName = UUID.randomUUID().toString().replaceAll("-","") +"."+ fileExtName;
		File newFile = new File(local_upload_dir+"/"+ fileExtName);

		OutputStream os = new FileOutputStream(newFile);
		int bytesRead = 0;
		byte[] buffer = new byte[8192];
		while ((bytesRead = inStream.read(buffer, 0, 8192)) != -1) {
			os.write(buffer, 0, bytesRead);
		}
		os.close();
		inStream.close();
		String [] result = new String[2];
		String fileUrl = local_download_host + "/" + fileExtName;
		result[0] = fileUrl;
		return result;
	}


	private synchronized static InputStream cutPic(InputStream src, int x, int y,
												   int width, int height,String fileExtName) {
		ImageInputStream iis = null;
		try {

			// ImageReader声称能够解码指定格式
			Iterator<ImageReader> it = ImageIO.getImageReadersByFormatName(fileExtName);
			ImageReader reader = it.next();

			// 获取图片流
			iis = ImageIO.createImageInputStream(src);

			// 输入源中的图像将只按顺序读取
			reader.setInput(iis, true);

			// 描述如何对流进行解码
			ImageReadParam param = reader.getDefaultReadParam();

			// 图片裁剪区域
			Rectangle rect = new Rectangle(x, y, width, height);

			// 提供一个 BufferedImage，将其用作解码像素数据的目标
			param.setSourceRegion(rect);

			// 使用所提供的 ImageReadParam 读取通过索引 imageIndex 指定的对象
			BufferedImage bi = reader.read(0, param);

			ByteArrayOutputStream bs = new ByteArrayOutputStream();
			ImageOutputStream imOut = null;

			imOut = ImageIO.createImageOutputStream(bs);
			ImageIO.write(bi,"jpeg",imOut);
			InputStream is= new ByteArrayInputStream(bs.toByteArray());
			return is;

		} catch (Exception e) {
			e.printStackTrace();

			return null;
		} finally {
			try {
				if (iis != null) {
					iis.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}


	private  static InputStream cutPic(BufferedImage src, int thumbwidth, int thumbheight) {
		try {
			if(src == null){
				return  null;
			}
			int imgWidth = src.getWidth();
			int imgHeight = src.getHeight();
			int x=0;
			int y=0;
			int width = imgWidth;
			int heith = imgHeight;
			if(imgWidth > thumbwidth){
				x = (imgWidth-thumbwidth)/2;
				width = thumbwidth;
			}
			if(imgHeight > thumbheight){
				y = (imgHeight-thumbheight)/2;
				heith = thumbheight;
			}

			BufferedImage tempImage=  src.getSubimage(x, y, width, heith);

			ByteArrayOutputStream bs = new ByteArrayOutputStream();
			ImageOutputStream imOut = null;

			imOut = ImageIO.createImageOutputStream(bs);
			ImageIO.write(tempImage,"jpeg",imOut);
			InputStream is= new ByteArrayInputStream(bs.toByteArray());
			return is;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}

   

