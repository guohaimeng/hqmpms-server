package com.hqmpms.utils;
import com.hqmpms.api.utils.ApiErrorCode;
import com.hqmpms.dispatcher.Config;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class YsServerSDK {
	
	private static final Logger m_logger = Logger.getLogger(YsServerSDK.class);
	public final static int ADD_SUCCESS = 1;
	public final static int ADD_FAILED = 0;
	
	private static String AddUserAPI = "addUser.do";//添加sdk用户
	private static String sendUserMsgCustomAPI = "sendUserMsgCustom.do";//新版发送消息
	private static String SendUserMsgAPI = "sendUserMsg.do";//发送消息
	private static String BatchAddUserAPI = "batchAddUser.do";//批量添加用户
	private static String AddGroupAPI = "addGroup.do"; //创建群
	private static String AddGroupUserAPI = "addGroupUser.do";//新增群用户
	private static String delGroupUserAPI = "delGroupUser.do"; //删除群成员
	private static String changeGroupAPI = "changeGroup.do"; //转移群
	private static String delGroupAPI = "delGroup.do"; //删除群成员
	private static String sdk_login = "getProfile.do"; //sdk登录
	private static String sdk_key = "";
	private static String YS_URL =  Config.getInstance().getString("rkcloudapi_url");
	/**
	 * init url and server key
	 * @param key
	 * @param YSURL
	 */
	public YsServerSDK(String key){
		sdk_key = key;
	}

	/**
	 * Add user to YunShiHudong system
	 * @param uname
	 * @param pwd
	 * @return
	 * @throws IOException
	 */
	public int addUser(String uname,String pwd) throws IOException  {
		int ret = -1;
		String URL_PATH = YS_URL+AddUserAPI;
		HashMap<Object,Object> params = new HashMap<Object,Object>();
		params.put("key", sdk_key);
		params.put("username", uname);
		params.put("pwd", pwd);
		String returnCode;
        try {
        	m_logger.info("addUser request: "+URL_PATH+" params:"+params);
			returnCode = doPost(URL_PATH, params);
			m_logger.info("addUser res:"+returnCode);
			if(!returnCode.equals("")){
				JSONObject result = new JSONObject(returnCode);
				ret = result.getInt("errcode");
			}
		} catch (Exception e) {
			m_logger.error("addUser faile:"+e);
		}
        return ret;
	}


	/**
	 * 创建群聊
	 * @param uname
	 * @return fail:errcode success:gid
	 * @throws Exception
	 */
	public String addGroup(String uname,String gname,String members) throws Exception  {
		int ret = ApiErrorCode.DEFAULT_SYSTEM_ERROR;
		String wholeURL = YS_URL+AddGroupAPI;
		HashMap<Object, Object> params = new HashMap<Object,Object>();
		params.put("key", sdk_key);
		params.put("username", uname);
		params.put("members",members);
		params.put("gname",gname);
		String result = doPost(wholeURL,params);
		if(null == result){
			return String.valueOf(ret);
		}
		try {
			JSONObject json= new JSONObject(result);
			ret = json.getInt("errcode");
			if(ret != ApiErrorCode.DEFAULT_SDK_SUCCESS){
				if(ret == ApiErrorCode.DEFAULT_SDK_ILLEGAL_USER){
					m_logger.error(String.format("create group fail,retcode=%s",ret));
					return String.valueOf(ApiErrorCode.DEFAULT_SDK_ILLEGAL_USER);
				}else{
					m_logger.error(String.format("create group fail,retcode=%s",ret));
					return String.valueOf(ApiErrorCode.DEFAULT_SYSTEM_ERROR);
				}
			}else{
				return json.getString("gid");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			m_logger.error(String.format("FAILED params=%s",e));
		}
		return String.valueOf(ret);
	}
	/**
	 * 添加群聊用户
	 * @param uname
	 * @param pwd
	 * @return fail:errcode success:gid
	 * @throws Exception
	 */
	public int addGroupUser(String gid,String members) throws Exception  {
		int ret = ApiErrorCode.DEFAULT_SYSTEM_ERROR;
		String wholeURL = YS_URL+AddGroupUserAPI;
		HashMap<String,String> params = new HashMap<String,String>();
		params.put("key", sdk_key);
		params.put("gid",gid);
		params.put("members",members);
		String result = httpPostRequest(wholeURL,params,"POST");
		if(null == result){
			return ret;
		}
	    JSONObject json= new JSONObject(result);
	    try {
			ret = json.getInt("errcode");
			if(ret != ApiErrorCode.DEFAULT_SDK_SUCCESS && ret!=3010){
				if(ret == ApiErrorCode.DEFAULT_SDK_ILLEGAL_USER){
					m_logger.error(String.format("add groupUser fail,retcode=%s",ret));	
					return ApiErrorCode.DEFAULT_SDK_ILLEGAL_USER;
				}else{
					m_logger.error(String.format("add groupUser fail,retcode=%s",ret));	
					return ApiErrorCode.DEFAULT_SYSTEM_ERROR;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			m_logger.error(String.format("FAILED params=%s",e.getMessage()));	
		}
		return ret;
	}
	/**
	 * 删除群聊用户
	 * @param user_info_json
	 * @return
	 * @throws IOException
	 */
	public String delGroupUser(String gid,String members) throws Exception  {
		int ret = ApiErrorCode.DEFAULT_SYSTEM_ERROR;
		String wholeURL = YS_URL+delGroupUserAPI;
		HashMap<String,String> params = new HashMap<String,String>();
		params.put("key", sdk_key);
		params.put("gid",gid);
		params.put("members",members);
		String result = httpPostRequest(wholeURL,params,"POST");
		if(null == result){
			return String.valueOf(ret);
		}
	    JSONObject json= new JSONObject(result);
	    try {
			ret = json.getInt("errcode");
			if(ret != ApiErrorCode.DEFAULT_SDK_SUCCESS && ret != 3006 ){
				m_logger.error(String.format("delGroupUser fail,retcode=%s %s %s",ret,gid,members));	
				return String.valueOf(ApiErrorCode.DEFAULT_SYSTEM_ERROR);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			m_logger.error(String.format("FAILED params=%s",e.getMessage()));	
		}
		return String.valueOf(ret);
	}
	/**
	 * 解散群
	 * @param user_info_json
	 * @return
	 * @throws IOException
	 */
	public String delGroup(String gid) throws Exception  {
		int ret = ApiErrorCode.DEFAULT_SYSTEM_ERROR;
		String wholeURL = YS_URL+delGroupAPI;
		HashMap<String,String> params = new HashMap<String,String>();
		params.put("key", sdk_key);
		params.put("gid",gid);
		String result = httpPostRequest(wholeURL,params,"POST");
		if(null == result){
			return String.valueOf(ret);
		}
	    JSONObject json= new JSONObject(result);
	    try {
			ret = json.getInt("errcode");
			if(ret != ApiErrorCode.DEFAULT_SDK_SUCCESS){
				m_logger.error(String.format("delGroup fail,retcode=%s %s",gid,ret));	
				return String.valueOf(ApiErrorCode.DEFAULT_SYSTEM_ERROR);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			m_logger.error(String.format("FAILED params=%s",e.getMessage()));	
		}
		return String.valueOf(ret);
	}
	/*
	* *
	 * 转让群操作
	 * @param user_info_json
	 * @return
	 * @throws IOException
	 */
	public String changeGroupOwner(String gid,String members) throws Exception  {
		int ret = ApiErrorCode.DEFAULT_SYSTEM_ERROR;
		String wholeURL = YS_URL+changeGroupAPI;
		HashMap<String,String> params = new HashMap<String,String>();
		params.put("key", sdk_key);
		params.put("gid",gid);
		params.put("toname",members);
		String result = httpPostRequest(wholeURL,params,"POST");
		if(null == result){
			return String.valueOf(ret);
		}
	    JSONObject json= new JSONObject(result);
	    try {
			ret = json.getInt("errcode");
			if(ret != ApiErrorCode.DEFAULT_SDK_SUCCESS){
				m_logger.error(String.format("change groupUser fail,retcode=%s",ret));	
				return String.valueOf(ApiErrorCode.DEFAULT_SYSTEM_ERROR);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			m_logger.error(String.format("FAILED params=%s",e.getMessage()));	
		}
		return String.valueOf(ret);
	}
	/**
	 * 添加群用户
	 * @param uname
	 * @param pwd
	 * @return fail:errcode success:gid
	 * @throws Exception
	 */
/*	public String addGroupUser(String uname,String gname) throws IOException  {
		int ret = 6001;
		String wholeURL = YS_URL+AddGroupUserAPI;
		HashMap<String,String> params = new HashMap<String,String>();
		params.put("key", sdk_key);
		params.put("username", uname);
		params.put("gname",gname);
		HashMap<String,String> result = httpRequest(wholeURL,params,"POST");
		if(null == result){
			return String.valueOf(ret);
		}
	    JSONObject json= new JSONObject(result);
	    try {
			ret = json.getInt("errcode");
			if(ret == ApiErrorCode.SYSTEM_ERROR.errCode()){
				return String.valueOf(ret);
			}else{
				return json.getString("gid");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			m_logger.error(String.format("FAILED params=%s",e.getMessage()));	
		}
		return String.valueOf(ret);
	}*/
	public HashMap<String,String> BatchAddUser(String user_info_json)  throws IOException {
		String wholeURL = YS_URL+BatchAddUserAPI;
		HashMap<String,String> params = new HashMap<String,String>();
		params.put("key", sdk_key);
		params.put("userinfo", user_info_json);
		HashMap<String,String> result = httpRequest(wholeURL,params,"POST");
		
		return result;
	}
	/**
	 * Send user message to other device or app client
	 * @param srcUname
	 * @param dstUname
	 * @param content
	 * @return
	 * @throws IOException
	 */
	public int SendMessage(String srcUname,String dstUname,String content) throws Exception  {
		int ret = ApiErrorCode.DEFAULT_SYSTEM_ERROR;
		String URL_PATH = YS_URL + sendUserMsgCustomAPI;
		HashMap<Object, Object> params = new HashMap<Object, Object>();
		params.put("key", sdk_key);
		params.put("username", srcUname);
		params.put("dest", dstUname);
		params.put("sound", "1");
		params.put("text", content);
		String returnCode = doPost(URL_PATH, params);
		if (!returnCode.equals("")) {
			JSONObject result = new JSONObject(returnCode);
			if(result.getInt("errcode") == 0){
				return 0;
			}
		}
		return ret;
	}


	/**
	 * Send user message to other device or app client
	 * @param srcUname
	 * @param dstUname
	 * @param content
	 * @return
	 * @throws IOException
	 */
	public int SendMessageSilence(String srcUname,String dstUname,String content) throws Exception  {
		int ret = ApiErrorCode.DEFAULT_SYSTEM_ERROR;
		String URL_PATH = YS_URL + SendUserMsgAPI;
		HashMap<Object, Object> params = new HashMap<Object, Object>();
		params.put("key", sdk_key);
		params.put("username", srcUname);
		params.put("dest", dstUname);
		params.put("sound", "0");
		params.put("text", content);
		String returnCode = doPost(URL_PATH, params);
		if (!returnCode.equals("")) {
			JSONObject result = new JSONObject(returnCode);
			if(result.getInt("errcode") == 0){
				return 0;
			}
		}
		return ret;
	}


	public void SendMessageAsynchronous(String srcUname,String dstUname,String content) throws Exception  {
		//int ret = ApiErrorCode.DEFAULT_SYSTEM_ERROR;
		try{
		String URL_PATH = YS_URL + sendUserMsgCustomAPI;
		HashMap<Object, Object> params = new HashMap<Object, Object>();
		params.put("key", sdk_key);
		params.put("username", srcUname);
		params.put("dest", dstUname);
		params.put("sound", "1");
		params.put("text", content);
		//String returnCode = doPost(URL_PATH, params);
		DoCachedThreadPool.getCachedThreadPool().cachedThreadPool.execute(new DoSDKPostThread(URL_PATH,params));
//		if (!returnCode.equals("")) {
//			JSONObject result = new JSONObject(returnCode);
//			if(result.getInt("errcode") == 0){
//				return 0;
//			}
//		}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			m_logger.error(String.format("FAILED params=%s",e.getMessage()));
		}
//		return ret;
	}



	public void SendMessageAsynchronousSilence(String srcUname,String dstUname,String content) throws Exception  {
		//int ret = ApiErrorCode.DEFAULT_SYSTEM_ERROR;
		try{
			String URL_PATH = YS_URL + SendUserMsgAPI;
			HashMap<Object, Object> params = new HashMap<Object, Object>();
			params.put("key", sdk_key);
			params.put("username", srcUname);
			params.put("dest", dstUname);
			params.put("sound", "0");
			params.put("text", content);
			//String returnCode = doPost(URL_PATH, params);
			DoCachedThreadPool.getCachedThreadPool().cachedThreadPool.execute(new DoSDKPostThread(URL_PATH,params));
//		if (!returnCode.equals("")) {
//			JSONObject result = new JSONObject(returnCode);
//			if(result.getInt("errcode") == 0){
//				return 0;
//			}
//		}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			m_logger.error(String.format("FAILED params=%s",e.getMessage()));
		}
//		return ret;
	}



	/**
	 * 发送自定义消息
	 * @return
	 * @throws Exception
	 */
	public int SendCustomMsg(String srcUname,String dstUname,String content) throws Exception  {
		int ret = ApiErrorCode.DEFAULT_SYSTEM_ERROR;
		String wholeURL = YS_URL+sendUserMsgCustomAPI;
		HashMap<Object,Object> params = new HashMap<Object,Object>();
		params.put("key", sdk_key);
        params.put("username", srcUname);
        params.put("dest", dstUname);
		params.put("sound", "1");
        params.put("text", content);
		String result = doPost(wholeURL,params);
		if(null == result){
			return ret;
		}
	    JSONObject json= new JSONObject(result);
	    try {
			ret = json.getInt("errcode");
			if(ret != ApiErrorCode.DEFAULT_SDK_SUCCESS){
				m_logger.error(String.format("sendCustomMsg fail,retcode=%s",ret));	
				return ApiErrorCode.DEFAULT_SYSTEM_ERROR;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			m_logger.error(String.format("FAILED params=%s",e.getMessage()));	
		}
		return ret;
	}



	/**
	 * 异步发送自定义消息
	 * @return
	 * @throws Exception
	 */
	public void SendCustomMsgAsynchronous (String srcUname,String dstUname,String content) {
		//int ret = ApiErrorCode.DEFAULT_SYSTEM_ERROR;
		try{
		String wholeURL = YS_URL+sendUserMsgCustomAPI;
		HashMap<Object,Object> params = new HashMap<Object,Object>();
		params.put("key", sdk_key);
		params.put("username", srcUname);
		params.put("dest", dstUname);
		params.put("sound", "1");
		params.put("text", content);
		DoCachedThreadPool.getCachedThreadPool().cachedThreadPool.execute(new DoSDKPostThread(wholeURL,params));
		//String result = doPost(wholeURL,params);
//		if(null == result){
//			return ret;
//		}
//		JSONObject json= new JSONObject(result);
//		try {
//			ret = json.getInt("errcode");
//			if(ret != ApiErrorCode.DEFAULT_SDK_SUCCESS){
//				m_logger.error(String.format("sendCustomMsg fail,retcode=%s",ret));
//				return ApiErrorCode.DEFAULT_SYSTEM_ERROR;
//			}
		} catch (Exception e) {
			m_logger.error(String.format("FAILED params=%s",e.getMessage()));
		}

	}





	/**
	 * 异步发送自定义消息
	 * @return
	 * @throws Exception
	 */
	public void SendCustomMsgAsynchronousWithMsgsum (String srcUname,String dstUname,String content,String msgsum) {
		//int ret = ApiErrorCode.DEFAULT_SYSTEM_ERROR;
		try{
			String wholeURL = YS_URL+sendUserMsgCustomAPI;
			HashMap<Object,Object> params = new HashMap<Object,Object>();
			params.put("key", sdk_key);
			params.put("username", srcUname);
			params.put("srcname", "系统消息");
			params.put("dest", dstUname);
			params.put("sound", "1");
			params.put("text", content);
			params.put("msgsum", msgsum);
			DoCachedThreadPool.getCachedThreadPool().cachedThreadPool.execute(new DoSDKPostThread(wholeURL,params));
		} catch (Exception e) {
			m_logger.error(String.format("FAILED params=%s",e.getMessage()));
		}

	}






	/**
	 * 模拟doPost请求
	 * @param urlStr
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
    public static String  doPost(String urlStr,HashMap<Object,Object> paramMap) throws Exception{
        URL url = new URL(urlStr);
        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setRequestMethod("POST");
        String paramStr = null;
        if(paramMap!=null){
        	paramStr = prepareParam(paramMap);
        }
        conn.setRequestProperty("content-type", "application/x-www-form-urlencoded;charset=UTF-8");
        conn.setDoInput(true);
        conn.setDoOutput(true);
        OutputStream os = conn.getOutputStream();
        if(paramMap!=null){
        	os.write(paramStr.toString().getBytes("utf-8"));
        }
        os.close(); 
        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line ;
        String result ="";
        while( (line =br.readLine()) != null ){
            result += line;
        }
        br.close();
        return result;
    }
    
    /**
     * 参数解析
     * @param url
     * @param charset
     * @return
     */
    private static String prepareParam(Map<Object,Object> paramMap){
        StringBuffer sb = new StringBuffer();
        if(paramMap.isEmpty()){
            return "" ;
        }else{
            for(Object key: paramMap.keySet()){
                String value = (String)paramMap.get(key);
                if(sb.length()<1){
                    sb.append(key).append("=").append(value);
                }else{
                    sb.append("&").append(key).append("=").append(value);
                }
            }
            return sb.toString();
        }
    }
    
	/**
	 * http 请求，
	 * @param InURL
	 * @param params  "key":"value"
	 * @param InMethod  方法 “GET”,"POST"
	 * @return
	 * @throws IOException 
	 */
	public static HashMap<String,String> httpRequest(String InURL,HashMap<String,String> params,String InMethod) throws IOException{
		if(InURL==null || InMethod==null || InURL.isEmpty() || InMethod.isEmpty())
			return null;
		StringBuffer content = new StringBuffer(1024);
		// get params
		if(null != params){
			Iterator<String> it=params.keySet().iterator();
			int i = 0;
			while(it.hasNext()){
			    String key;  
			    String value;  
			    key=it.next().toString();  
			    value=params.get(key);
			    if(i==0)
					content.append(key+"=").append(value);
			    else
			    	content.append("&"+key+"=").append(value);
			    i++;
			}
		}
		if(InMethod.equals("GET") && !content.toString().isEmpty()){
			InURL = InURL+"?"+content.toString();
		}
		URL postUrl = new URL(InURL); 
		HttpURLConnection connection = (HttpURLConnection) postUrl.openConnection(); 
		connection.setDoOutput(true);                 
		connection.setDoInput(true); 
		connection.setRequestMethod(InMethod); 
		connection.setRequestProperty("Charset", "UTF-8");
		connection.setRequestProperty("Connection", "Keep-Alive");
		connection.setUseCaches(false); 
		connection.setInstanceFollowRedirects(true); 
		connection.setRequestProperty("content-type", "application/x-www-form-urlencoded;charset=UTF-8");
		
		//开始实际连接
		connection.connect();
		if(InMethod.equals("POST")){
			//发送请求参数
			DataOutputStream out = new DataOutputStream(connection.getOutputStream()); 
			out.writeBytes(content.toString()); 
			out.flush(); 
			out.close(); 
		}
		
		HashMap<String,String> result = new HashMap<String,String>();
//		int response = connection.getResponseCode(); 
		if(connection.getResponseCode() == 200){
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream())); 
			String line; 
			
			while((line=reader.readLine()) != null){
					String[] resultArray = line.split("=");
					result.put(resultArray[0], resultArray[1]);
//					ret = ret+"\n"+line.toString();
			}
			reader.close(); 
		}
		connection.disconnect();
		
		return result.isEmpty()?null:result;
	}
	/**
	 * http 请求，
	 * @param InURL
	 * @param params  "key":"value"
	 * @param InMethod  方法 “GET”,"POST"
	 * @return
	 * @throws IOException 
	 */
	public static String httpPostRequest(String InURL,HashMap<String,String> params,String InMethod) throws IOException{
		if(InURL==null || InMethod==null || InURL.isEmpty() || InMethod.isEmpty())
			return null;
		StringBuffer content = new StringBuffer(1024);
		if(null != params){
			Iterator<String> it=params.keySet().iterator();
			int i = 0;
			while(it.hasNext()){
			    String key;  
			    String value;  
			    key=it.next().toString();  
			    value=params.get(key);
			    if(i==0)
					content.append(key+"=").append(value);
			    else
			    	content.append("&"+key+"=").append(value);
			    i++;
			}
		}
		if(InMethod.equals("GET") && !content.toString().isEmpty()){
			InURL = InURL+"?"+content.toString();
		}
		URL postUrl = new URL(InURL); 
		HttpURLConnection connection = (HttpURLConnection) postUrl.openConnection(); 
		connection.setDoOutput(true);                 
		connection.setDoInput(true); 
		connection.setRequestMethod(InMethod); 
		connection.setRequestProperty("Connection", "Keep-Alive");
		connection.setUseCaches(false); 
		connection.setInstanceFollowRedirects(true); 
		connection.setRequestProperty("content-type", "application/x-www-form-urlencoded;charset=UTF-8");
		//开始实际连接
		connection.connect();
		if(InMethod.equals("POST")){
			//发送请求参数
			DataOutputStream out = new DataOutputStream(connection.getOutputStream()); 
			out.writeBytes(content.toString()); 
			out.flush(); 
			out.close(); 
		}
		int response = connection.getResponseCode(); 
		String result ="";
		if(response == 200){
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8")); 
			String line; 
			while((line=reader.readLine()) != null){
				result +=line;
			}
			reader.close(); 
		}
		connection.disconnect();
		return result;
	}

	/**
	 * sdk登录
	 * @param acc
	 * @param pwd
	 * @return
	 * @throws Exception
     */
	public static int sdkLogin(String acc ,String pwd ) throws Exception{

		int ret = ApiErrorCode.DEFAULT_SYSTEM_ERROR;
		String wholeURL = YS_URL+sdk_login;
		HashMap<Object,Object> params = new HashMap<Object,Object>();
		params.put("key", "9c1b461abc091261de1807f43699923f33e1c5a9");
		params.put("username", acc);
		params.put("pwd", pwd);
		params.put("os", "android");
		params.put("pkgnm", "com.hengqian.education.excellentlearning.network");
		params.put("sdkver", "2.2.8");
		params.put("md", "M57AC");
		params.put("cap", "0,1,2,3");
		params.put("mf", "Meizu");
		params.put("osv", "5.1");
		String result = doPost(wholeURL,params);
		if(null == result){
			return ret;
		}
		JSONObject json= new JSONObject(result);
		try {
			ret = json.getInt("errcode");
			if(ret != ApiErrorCode.DEFAULT_SDK_SUCCESS){
				m_logger.error(String.format("sendCustomMsg fail,retcode=%s",ret));
				return ApiErrorCode.DEFAULT_SYSTEM_ERROR;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			m_logger.error(String.format("FAILED params=%s",e.getMessage()));
		}
		return ret;
	}

}