package com.hqmpms.utils.aes;

/**
 * Created by Administrator on 2016/9/30 0030.
 */
public class AESCodecBase16Test {

    public static void main(String[] args) {
        try {
            String source = "最近在做一个项目，要用到数据加密算法，所以就看了下《java加密与解密的艺术》这本书，最后就参考了下AES加密算法来加密文件，一是它加密标准高、密钥建立时间短、灵敏性好、内存需求低，二是因为javaAPI已经自带了AES算法，用起来很方便顺手，当然，这个还不算，密钥的产生还调用了Base64算法对AES产生的密钥进行了二次加密，确保密钥的安全可靠，大家有兴趣的话可以去看看《java加密与解密的艺术》一书，这里我贴上代码供参考：";
            System.out.println("原文：" + source);

           // String key = AESCodecBase16.initkey();
            //System.out.println("密钥：" + key);
            String key = "aaaaaaaaassdfghj";
            String encryptData = AESCodecBase16.encrypt(source, key);
            System.out.println("加密：" + encryptData);

            String decryptData = AESCodecBase16.decrypt(encryptData, key);
            System.out.println("解密: " + decryptData);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
