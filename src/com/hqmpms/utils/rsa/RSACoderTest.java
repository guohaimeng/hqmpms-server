package com.hqmpms.utils.rsa;

import java.util.Map;

/**
 *
 * @version 1.0
 * @since 1.0
 */
public class RSACoderTest {
    private String publicKey;
    private String privateKey;


    public void setUp() throws Exception {
        Map<String, Object> keyMap = RSACoder.initKey();

        publicKey = RSACoder.getPublicKey(keyMap);
        privateKey = RSACoder.getPrivateKey(keyMap);
        System.err.println("公钥: \r" + publicKey);
        System.err.println("私钥： \r" + privateKey);
    }

    public void test() throws Exception {
        System.err.println("公钥加密——私钥解密");
        String inputStr = "abc";
        byte[] data = inputStr.getBytes();

        byte[] encodedData = RSACoder.encryptByPublicKey(data, publicKey);

        byte[] decodedData = RSACoder.decryptByPrivateKey(encodedData,
                privateKey);

        String outputStr = new String(decodedData);
        System.err.println("加密前: " + inputStr + "\r" + "解密后: " + outputStr);


    }


    public void testSign() throws Exception {
        System.err.println("私钥加密——公钥解密");
        String inputStr = "sign";
        byte[] data = inputStr.getBytes();

        byte[] encodedData = RSACoder.encryptByPrivateKey(data, privateKey);

        byte[] decodedData = RSACoder
                .decryptByPublicKey(encodedData, publicKey);

        String outputStr = new String(decodedData);
        System.err.println("加密前: " + inputStr + "\r" + "解密后: " + outputStr);


        System.err.println("私钥签名——公钥验证签名");
        // 产生签名
        String sign = RSACoder.sign(encodedData, privateKey);
        System.err.println("签名:\r" + sign);

        // 验证签名
        boolean status = RSACoder.verify(encodedData, publicKey, sign);
        System.err.println("状态:\r" + status);


    }

    public static  void main(String [] args) throws Exception {
        RSACoderTest rsat = new RSACoderTest();
       // rsat.setUp();
//        rsat.privateKey="MIIBCwIBADANBgkqhkiG9w0BAQEFAASB9jCB8wIBAAIxAK+7EaI6L1zWlXk2rQSu\naXSQkD1/H7cznUMK/LO+nAe7gZvfH6PnI9hym10wQrw8hwIDAQABAjBKQYURBP8w\nfqhIwRbZTI3ZKKEaq1YOhuswNc2vKrMNyNlwcUw3zYZXTs6hTwbGwmkCGQDlUxcb\nBnIPAUCBh0LjdIdR2t1kt0f0KEsCGQDELArJS0+xWDoCiFU2UFUiXZi3IN9KjzUC\nGQDHnPnhxkyGDxrtJKXug0I+1/oabwcs2d0CGEqcbaXNzQuogLfB3pb/ysF9U74i\n9IpJGQIZANnkuyxtH1b6Xtwr+r5UnzAtCfRBNLhGOA==";
//        rsat.publicKey="MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBALMK85PUW1gd9p3t4/XcopCp9EssA6Pd\nuKyOqMyLczsS4DHFq2I3JnDUaxv0hQk9DvkRhILMar6osv8GG/DrRjECAwEAAQ==";
        ///rsat.test();

        //String key = AESCodec.initkey();
        String content = "说到底+++======%%^^&&**##!@%%^()**&(&(撒打43344334ddfsadasdasdasdasds发倒萨倒萨啊大苏打撒撒旦艰苦的撒啊大飒飒倒萨的啊的时间卡萨丁肯定撒进咯大师金克拉撒旦就剪卡的数据库金克拉金克拉撒旦艰苦撒旦艰苦撒旦射点发射点就开始大幅亏损的付款方式打开asdasddsasdd发射点士大夫士大夫随风倒士大夫发射点十分的方式的是的房价快速的飞机喀什地方就开始大幅加快 设计的角色的封建士大夫解开了解开了士大夫加啊四的 吉萨大空间卡的手机卡手机卡剪卡了圣诞节";
        //System.out.println(key);


//        byte[] data = key.getBytes();
//
//        byte[] encodedData = RSACoder.encryptByPublicKey(data, rsat.publicKey);
//        String encodedStr = Base64.encode(encodedData);
//        //System.out.println(encodedStr);
//        byte[] decodedData = RSACoder.decryptByPrivateKey(Base64.decode(encodedStr),
//                rsat.privateKey);
//
        String encontent = RSACoder.encrypt(content, RSACoder.PUBLIC_KEY_APP);

       String outputStr = RSACoder.decrypt(encontent, RSACoder.PRIVATE_KEY_APP,"utf-8");


        System.err.println("加密前: " + content + "\r" + "解密后123132: " + outputStr);
    }
}
